﻿Imports System.Data.SqlClient
Public Class BRWORDSER
    Private bnd As Boolean = False

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelCiudadGeneral")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_Ciudad()
        Try
            BaseII.limpiaParametros()
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_ciudad")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"

            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
                GloClvCiudad = ComboBoxCiudad.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub CREAARBOL()
        Dim CON As New SqlConnection(MiConexion)
        Try

            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next
            If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
                CON.Open()
                Me.Dame_DetOrdSerTableAdapter.Connection = CON
                Me.Dame_DetOrdSerTableAdapter.Fill(Me.DataSetEDGAR.Dame_DetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_calleLabel2.Text, Long)))
                CON.Close()
                Dim FilaRow As DataRow
                Me.TreeView1.Nodes.Clear()
                For Each FilaRow In Me.DataSetEDGAR.Dame_DetOrdSer.Rows
                    Me.TreeView1.Nodes.Add(Trim(FilaRow("descripcion").ToString()))
                    I += 1
                Next
                Me.TreeView1.ExpandAll()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloBnd = False
        GloGuardo = True
        FrmOrdSer.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            GloBnd = False
            loccontratoordenes = Me.ContratoLabel1.Text
            Dim array As String() = ContratoCompania.Text.Split("-")
            eContratoCompania = array(0)
            GloIdCompania = array(1)
            'GloClv_TipSer = 1000
            gloClave = Me.Clv_calleLabel2.Text
            FrmOrdSer.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            GloBnd = False
            gloClave = Me.Clv_calleLabel2.Text
            loccontratoordenes = CLng(Me.ContratoLabel1.Text)
            Dim array As String() = ContratoCompania.Text.Split("-")
            eContratoCompania = array(0)
            GloIdCompania = array(1)
            'GloClv_TipSer = 1000
            eStatusOrdSer = Me.DataGridView1.SelectedCells(1).Value
            FrmOrdSer.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)

        If Me.ContratoLabel1.Text <> "" Then
            CON.Open()
            Me.BuscaBloqueadoTableAdapter.Connection = CON
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoLabel1.Text, NUM, num2)
            CON.Close()
            If NUM = 0 Or num2 = 0 Then
                modificar()
            ElseIf num2 = 1 Then
                MsgBox("El Cliente " + Me.ContratoLabel1.Text + " Ha Sido Bloqueado por lo que no se Podrá Ejecutar la Orden ", MsgBoxStyle.Exclamation)
            End If
        End If

    End Sub

    Private Sub busca1(ByVal Clv_TipSer As Integer, ByVal Clv_Orden As Long, ByVal Contrato As Integer, ByVal NOMBRE As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal OP As Integer, _
                       ByVal procauto As Boolean, ByVal prmClvColonia As Integer, oIdCompania As Integer)
        'busca1(0, 0, Me.TextBox1.Text, "", "", "", 30, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim consulta As String = Nothing
        Try
            CON.Open()
            If Len(NOMBRE) = 0 Then
                NOMBRE = "''"
            ElseIf Len(NOMBRE) > 0 Then
                NOMBRE = "'" + NOMBRE + "'"
            End If
            If Len(CALLE) = 0 Then
                CALLE = "''"
            ElseIf Len(CALLE) > 0 Then
                CALLE = "'" + CALLE + "'"
            End If
            If Len(NUMERO) = 0 Then
                NUMERO = "''"
            ElseIf Len(NUMERO) > 0 Then
                NUMERO = "'" + NUMERO + "'"
            End If
            consulta = "Exec uspBuscaOrdSer 0," + CStr(Clv_Orden) + "," + CStr(Contrato) + "," + CStr(NOMBRE) + "," + CStr(CALLE) + "," + CStr(NUMERO) + "," + CStr(OP) + "," + CStr(procauto) + "," + CStr(prmClvColonia) + "," + CStr(oIdCompania) + "," + GloClvCiudad.ToString

            CMD = New SqlDataAdapter(consulta, CON)

            Dim dt As New DataTable

            CMD.Fill(dt)

            Me.BUSCAORDSERBindingSource.DataSource = dt

            With DataGridView1
                .DataSource = Me.BUSCAORDSERBindingSource.DataSource
            End With

            CON.Close()
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()

            bnd = True

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Private Sub busca1(ByVal Clv_TipSer As Integer, ByVal Clv_Orden As Long, ByVal Contrato As Integer, ByVal NOMBRE As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal OP As Integer, ByVal procauto As Boolean)
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim CMD As New SqlClient.SqlDataAdapter()
    '    Dim consulta As String = Nothing
    '    Try
    '        CON.Open()
    '        If Len(NOMBRE) = 0 Then
    '            NOMBRE = "''"
    '        ElseIf Len(NOMBRE) > 0 Then
    '            NOMBRE = "'" + NOMBRE + "'"
    '        End If
    '        If Len(CALLE) = 0 Then
    '            CALLE = "''"
    '        ElseIf Len(CALLE) > 0 Then
    '            CALLE = "'" + CALLE + "'"
    '        End If
    '        If Len(NUMERO) = 0 Then
    '            NUMERO = "''"
    '        ElseIf Len(NUMERO) > 0 Then
    '            NUMERO = "'" + NUMERO + "'"
    '        End If
    '        consulta = "Exec BUSCAORDSER 0," + CStr(Clv_Orden) + "," + CStr(Contrato) + "," + CStr(NOMBRE) + "," + CStr(CALLE) + "," + CStr(NUMERO) + "," + CStr(OP) + "," + CStr(procauto)
    '        'MsgBox(consulta, MsgBoxStyle.Information)

    '        CMD = New SqlDataAdapter(consulta, CON)



    '        Dim dt As New DataTable


    '        CMD.Fill(dt)


    '        Me.BUSCAORDSERBindingSource.DataSource = dt

    '        With DataGridView1
    '            .DataSource = Me.BUSCAORDSERBindingSource.DataSource
    '        End With

    '        'bnd = True


    '        'CMD = New SqlClient.SqlCommand()
    '        'With CMD
    '        '    .CommandText = "BUSCAORDSER"
    '        '    .Connection = CON
    '        '    .CommandType = CommandType.StoredProcedure
    '        '    .CommandTimeout = 0
    '        '    ByVal Clv_TipSer As Integer, ByVal Clv_Orden As Long, ByVal Contrato As Integer,
    '        '     ByVal NOMBRE As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal OP As Integer, 
    '        '    ByVal procauto As Boolean)

    '        '    Dim PRM As New SqlParameter("@CLV_TIPSER", SqlDbType.Int)
    '        '    Dim PRM2 As New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
    '        '    Dim PRM3 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
    '        '    Dim PRM4 As New SqlParameter("@NOMBRE", SqlDbType.VarChar, 150)
    '        '    Dim PRM5 As New SqlParameter("@CALLE", SqlDbType.VarChar, 150)
    '        '    Dim PRM6 As New SqlParameter("@NUMERO", SqlDbType.VarChar, 50)
    '        '    Dim PRM7 As New SqlParameter("@OP", SqlDbType.Int)
    '        '    Dim PRM8 As New SqlParameter("@PROCAUTO", SqlDbType.Bit)

    '        '    PRM.Direction = ParameterDirection.Input
    '        '    PRM2.Direction = ParameterDirection.Input
    '        '    PRM3.Direction = ParameterDirection.Input
    '        '    PRM4.Direction = ParameterDirection.Input
    '        '    PRM5.Direction = ParameterDirection.Input
    '        '    PRM6.Direction = ParameterDirection.Input
    '        '    PRM7.Direction = ParameterDirection.Input
    '        '    PRM8.Direction = ParameterDirection.Input

    '        '    PRM.Value = Clv_TipSer
    '        '    PRM2.Value = Clv_Orden
    '        '    PRM3.Value = Contrato
    '        '    PRM4.Value = NOMBRE
    '        '    PRM5.Value = CALLE
    '        '    PRM6.Value = NUMERO
    '        '    PRM7.Value = OP
    '        '    PRM8.Value = procauto

    '        '    .Parameters.Add(PRM)
    '        '    .Parameters.Add(PRM2)
    '        '    .Parameters.Add(PRM3)
    '        '    .Parameters.Add(PRM4)
    '        '    .Parameters.Add(PRM5)
    '        '    .Parameters.Add(PRM6)
    '        '    .Parameters.Add(PRM7)
    '        '    .Parameters.Add(PRM8)

    '        '    Dim I As Integer = CMD.ExecuteNonQuery()
    '        '    Me.BUSCAORDSERTableAdapter.Fill(CMD.ExecuteNonQuery())
    '        '    Me.BUSCAORDSERBindingSource.DataSource = CMD
    '        'End With


    '        'With Me.DataGridView1
    '        '    .DataSource = BuscaBloqueadoBindingSource.DataSource
    '        'End With

    '        CON.Close()

    '        Me.TextBox1.Clear()
    '        Me.TextBox2.Clear()
    '        Me.TextBox3.Clear()
    '        Me.BNUMERO.Clear()
    '        Me.BCALLE.Clear()

    '        bnd = True



    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try


    'End Sub

    'Private Sub busca(ByVal op As Integer)
    '    Dim sTATUS As String = "P"
    '    Dim autom As Boolean = False

    '    If Me.RadioButton1.Checked = True Then
    '        sTATUS = "P"
    '    ElseIf Me.RadioButton2.Checked = True Then
    '        sTATUS = "E"
    '    ElseIf Me.RadioButton3.Checked = True Then
    '        sTATUS = "V"
    '    End If
    '    If Me.CheckBox1.Checked = True Then
    '        autom = True
    '    Else
    '        autom = False
    '    End If

    '    Select Case op
    '        Case 0
    '            If Me.TextBox1.Text = "" Or Len(Me.TextBox1.Text) = 0 Then
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            Else
    '                busca1(0, 0, Me.TextBox1.Text, "", "", "", 30, autom)
    '            End If
    '        Case 1
    '            If Len(Trim(Me.TextBox2.Text)) > 0 Then
    '                busca1(0, 0, 0, Me.TextBox2.Text, "", "", 31, autom)
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If
    '        Case 2
    '            busca1(0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, 32, autom)
    '        Case 3
    '            If IsNumeric(Me.TextBox3.Text) = True Then
    '                busca1(0, Me.TextBox3.Text, 0, "", "", "", 33, autom)
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If
    '        Case 199
    '            busca1(0, 0, 0, sTATUS, "", "", 399, autom)
    '        Case 4
    '            busca1(0, 0, 0, "", "", "", 49, autom)
    '    End Select

    'End Sub

    Private Sub busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim autom As Boolean = False

        If Me.RadioButton1.Checked = True Then
            sTATUS = "P"
        ElseIf Me.RadioButton2.Checked = True Then
            sTATUS = "E"
        ElseIf Me.RadioButton3.Checked = True Then
            sTATUS = "V"
        End If
        If Me.CheckBox1.Checked = True Then
            autom = True
        Else
            autom = False
        End If
        'op=0 por contrato
        Select Case op
            Case 0
                If Me.TextBox1.Text = "" Or Len(Me.TextBox1.Text) = 0 Then
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                Else
                    busca1(0, 0, loccon, "", "", "", 30, autom, Me.cmbColonias.SelectedValue, GloIdCompania)
                End If
            Case 1
                If Len(Trim(Me.TextBox2.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then

                    BuscaSeparado(0, 0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", 31, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            Case 2
                busca1(0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, 32, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
            Case 3
                If IsNumeric(Me.TextBox3.Text) = True Then
                    busca1(0, Me.TextBox3.Text, 0, "", "", "", 33, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            Case 199
                busca1(0, 0, 0, sTATUS, "", "", 399, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
            Case 4
                BuscaSeparado(0, 0, 0, "", "", "", "", "", 49, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
            Case 5
                BuscaSeparado(0, 0, 0, "", "", "", "", "", 5, autom, 0, ComboBoxCompanias.SelectedValue)
            Case 6
                BuscaSeparado(0, 0, 0, "", "", "", "", "", 6, autom, 0, 0)
        End Select

    End Sub

    Private Sub BuscaSeparado(ByVal Clv_TipSer As Integer, ByVal Clv_Orden As Long, ByVal Contrato As Integer, ByVal NOMBRE As String, ByVal ApePaterno As String, _
                             ByVal ApeMaterno As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal OP As Integer, ByVal procauto As Boolean, ByVal prmClvColonia As Integer, oIdCompania As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim stb As String = ""
        Dim tarjeta As String = ""
        Dim consulta As String = Nothing
        Try 'Op=4 BuscaSeparado(0, 0, 0, "", "", "", "", "", 49, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
            CON.Open()
            If Len(NOMBRE) = 0 Then
                NOMBRE = "''"
            ElseIf Len(NOMBRE) > 0 Then
                NOMBRE = "'" + NOMBRE + "'"
            End If
            If Len(ApePaterno) = 0 Then
                ApePaterno = "''"
            ElseIf Len(ApePaterno) > 0 Then
                ApePaterno = "'" + ApePaterno + "'"
            End If
            If Len(ApeMaterno) = 0 Then
                ApeMaterno = "''"
            ElseIf Len(ApeMaterno) > 0 Then
                ApeMaterno = "'" + ApeMaterno + "'"
            End If
            If Len(CALLE) = 0 Then
                CALLE = "''"
            ElseIf Len(CALLE) > 0 Then
                CALLE = "'" + CALLE + "'"
            End If
            If Len(NUMERO) = 0 Then
                NUMERO = "''"
            ElseIf Len(NUMERO) > 0 Then
                NUMERO = "'" + NUMERO + "'"
            End If
            If Len(TxtSetUpBox.Text) = 0 Then
                stb = "''"
            ElseIf Len(TxtSetUpBox.Text) > 0 Then
                stb = "'" + TxtSetUpBox.Text + "'"
            End If
            If Len(TxtTarjeta.Text) = 0 Then
                tarjeta = "''"
            ElseIf Len(TxtTarjeta.Text) > 0 Then
                tarjeta = "'" + TxtTarjeta.Text + "'"
            End If
            consulta = "Exec BuscaOrdSerSeparado2 0," + CStr(Clv_Orden) + "," + CStr(Contrato) + "," + CStr(NOMBRE) + "," + CStr(ApePaterno) + "," + CStr(ApeMaterno) + "," + CStr(CALLE) + "," + CStr(NUMERO) + "," + _
                        CStr(OP) + "," + CStr(procauto) + "," + CStr(prmClvColonia) + "," + CStr(oIdCompania) + "," + GloClvCiudad.ToString + "," + stb + "," + tarjeta

            CMD = New SqlDataAdapter(consulta, CON)

            Dim dt As New DataTable
            Dim BS As New BindingSource

            CMD.Fill(dt)
            BS.DataSource = dt
            Me.DataGridView1.DataSource = BS.DataSource
            Me.BUSCAORDSERBindingSource.DataSource = dt

            CON.Close()
            Try
                Me.Clv_calleLabel2.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
                gloClave = Me.Clv_calleLabel2.Text
                Me.ContratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(9).Value)
                'MsgBox("0b" & Me.DataGridView1.SelectedCells(0).Value & " 1b " & Me.DataGridView1.SelectedCells(1).Value & " 2b " & Me.DataGridView1.SelectedCells(2).Value & " 3b " & Me.DataGridView1.SelectedCells(3).Value & " 4b " & Me.DataGridView1.SelectedCells(4).Value & " 5b " & Me.DataGridView1.SelectedCells(5).Value & " 6 " & Me.DataGridView1.SelectedCells(6).Value & " 7b " & Me.DataGridView1.SelectedCells(7).Value & " 8b " & Me.DataGridView1.SelectedCells(8).Value & " 9b " & Me.DataGridView1.SelectedCells(9).Value)
                Me.CMBNombreTextBox.Text = CStr(Me.DataGridView1.SelectedCells(3).Value) + " " + CStr(Me.DataGridView1.SelectedCells(7).Value) + " " + CStr(Me.DataGridView1.SelectedCells(8).Value) + " "
                Me.CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
                Me.NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
                Me.ContratoCompania.Text = CStr(Me.DataGridView1.SelectedCells(2).Value)
            Catch ex As Exception

            End Try


            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()
            Me.AMaternoTextBox.Clear()
            Me.APaternoTextBox.Clear()

            bnd = True
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Private Sub Busca(ByVal op As Integer)
    '    Dim sTATUS As String = "P"
    '    Dim autom As Boolean = False
    '    Dim CON As New SqlConnection(MiConexion)

    '    Try
    '        If Me.RadioButton1.Checked = True Then
    '            sTATUS = "P"
    '        ElseIf Me.RadioButton2.Checked = True Then
    '            sTATUS = "E"
    '        ElseIf Me.RadioButton3.Checked = True Then
    '            sTATUS = "V"
    '        End If
    '        If Me.CheckBox1.Checked = True Then
    '            autom = True
    '        Else
    '            autom = False
    '        End If
    '        'If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
    '        If op = 0 Then 'contrato
    '            If IsNumeric(Me.TextBox1.Text) = True Then
    '                CON.Open()
    '                Me.BUSCAORDSERTableAdapter.Connection = CON
    '                Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(30, Integer)), autom)
    '                CON.Close()
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If
    '        ElseIf op = 199 Then 'contrato
    '            CON.Open()
    '            Me.BUSCAORDSERTableAdapter.Connection = CON
    '            Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, CInt(Me.ComboBox4.SelectedValue), 0, 0, sTATUS, "", "", 399, autom)
    '            CON.Close()
    '        ElseIf op = 1 Then
    '            If Len(Trim(Me.TextBox2.Text)) > 0 Then
    '                CON.Open()
    '                Me.BUSCAORDSERTableAdapter.Connection = CON
    '                Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(31, Integer)), autom)
    '                CON.Close()
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If
    '        ElseIf op = 2 And CInt(Me.ComboBox4.SelectedValue) <> 0 Then 'Calle y numero
    '            CON.Open()
    '            Me.BUSCAORDSERTableAdapter.Connection = CON
    '            Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(32, Integer)), autom)
    '            CON.Close()
    '        ElseIf op = 3 Then 'clv_Orden
    '            If IsNumeric(Me.TextBox3.Text) = True Then
    '                CON.Open()
    '                Me.BUSCAORDSERTableAdapter.Connection = CON
    '                Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(33, Integer)), autom)
    '                CON.Close()
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If

    '        Else
    '            'MsgBox(Me.ComboBox4.SelectedValue)
    '            'MsgBox("0,0,"","","",4")
    '            'MsgBox(autom)

    '            CON.Open()
    '            Me.BUSCAORDSERTableAdapter.Connection = CON
    '            Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, Me.ComboBox4.SelectedValue, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(49, Integer)), autom)
    '            CON.Close()
    '        End If
    '        Me.TextBox1.Clear()
    '        Me.TextBox2.Clear()
    '        Me.TextBox3.Clear()
    '        Me.BNUMERO.Clear()
    '        Me.BCALLE.Clear()
    '        'Else
    '        '    MsgBox("Seleccione el Tipo de Servicio")
    '        'End If

    '        Exit Sub
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '        'Resume Next
    '    End Try

    'End Sub
    Dim loccon As Integer = 0
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            cmbColonias.SelectedValue = 0
            If TextBox1.Text.Contains("-") Then
                Dim array As String()
                array = TextBox1.Text.Trim.Split("-")
                loccon = array(0).Trim
                If array(1).Length = 0 Then
                    Exit Sub
                End If
                GloIdCompania = array(1).Trim
                busca(0)
            Else
                loccon = TextBox1.Text
                GloIdCompania = 999
                busca(0)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros y guión
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Or val = 45 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            Try
                If TextBox1.Text.Contains("-") Then
                    Dim array As String()
                    array = TextBox1.Text.Trim.Split("-")
                    loccon = array(0).Trim
                    If array(1).Length = 0 Then
                        Exit Sub
                    End If
                    GloIdCompania = array(1).Trim
                    busca(0)
                Else
                    loccon = TextBox1.Text
                    GloIdCompania = 999
                    busca(0)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub BRWORDSER_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            'Me.ComboBox4.SelectedValue = GloClv_TipSer
            'Me.ComboBox4.Text = GloNom_TipSer
            'Me.ComboBox4.FindString(GloNom_TipSer)
            'Me.ComboBox4.Text = GloNom_TipSer
            busca(4)
        End If
        eActTecnico = True
    End Sub

    Private Sub BRWORDSER_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        Llena_Ciudad()
        Llena_companias()

        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If

        'CON.Open()
        'Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        'CON.Close()
        llenaComboColonias()
        busca(6)
        bnd = True

        'PARA LLENAR EL RESUMEN DE ORDENES (INICIO) --JUANJO
        uspChecaCuantasOrdenesQuejas()
        'PARA LLENAR EL RESUMEN DE ORDENES (FIN) --JUANJO
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(3)
        End If
    End Sub
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        cmbColonias.SelectedValue = 0
        busca(3)
    End Sub
    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
        Me.CREAARBOL()
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            Me.Clv_calleLabel2.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
            Me.ContratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(9).Value)
            Me.CMBNombreTextBox.Text = CStr(Me.DataGridView1.SelectedCells(3).Value) + " " + CStr(Me.DataGridView1.SelectedCells(7).Value) + " " + CStr(Me.DataGridView1.SelectedCells(8).Value) + " "
            Me.CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
            Me.NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
            Me.ContratoCompania.Text = CStr(Me.DataGridView1.SelectedCells(2).Value)
        Catch

        End Try
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        busca(199)
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        busca(199)
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        busca(199)
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.RadioButton1.Checked = True Or Me.RadioButton2.Checked = True Or Me.RadioButton3.Checked = True Then
            busca(199)
        End If
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
       
        busca(2)
    End Sub


    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub


    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        'Me.Clv_calleLabel2.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
        'Me.ContratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(2).Value)
        'Me.CMBNombreTextBox.Text = CStr(Me.DataGridView1.SelectedCells(3).Value)
        'Me.CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
        'Me.NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
    End Sub



    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try 'A.Clv_Orden,A.STATUS,RC.ContratoCompania as Contrato,B.Nombre,C.NOMBRE AS CALLE,B.NUMERO,A.Clv_TipSer, A.Contrato as ContratoBueno
            If bnd = True Then
                If Len(CStr(Me.DataGridView1.SelectedCells(0).Value)) > 0 Then
                    Me.Clv_calleLabel2.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
                    Me.ContratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(9).Value)
                    Me.CMBNombreTextBox.Text = CStr(Me.DataGridView1.SelectedCells(3).Value) + " " + CStr(Me.DataGridView1.SelectedCells(7).Value) + " " + CStr(Me.DataGridView1.SelectedCells(8).Value) + " "
                    Me.CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
                    Me.NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
                    Me.ContratoCompania.Text = CStr(Me.DataGridView1.SelectedCells(2).Value)
                End If
            End If
        Catch
            Exit Sub
        End Try
    End Sub

    Private Sub DataGridView1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridView1.KeyPress

    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Selecciona una Compañía")
        '    Exit Sub
        'End If
        cmbColonias.SelectedValue = 0
        busca(1)
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            '0A.Clv_Orden,1A.STATUS,2RC.ContratoCompania as Contrato,3ISNULL(D.NOMBRE,'' '') +'' '' + ISNULL(D.SegundoNombre,'''') Nombre,4D.Apellido_Paterno,5D.Apellido_Materno,
            '6C.NOMBRE AS CALLE,7B.NUMERO,8A.Clv_TipSer,9 A.Contrato as ContratoBueno '
            Me.Clv_calleLabel2.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
            Me.ContratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(9).Value)
            Me.CMBNombreTextBox.Text = CStr(Me.DataGridView1.SelectedCells(3).Value) + " " + CStr(Me.DataGridView1.SelectedCells(7).Value) + " " + CStr(Me.DataGridView1.SelectedCells(8).Value) + " "
            Me.CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
            Me.NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
            Me.ContratoCompania.Text = CStr(Me.DataGridView1.SelectedCells(2).Value)
        Catch ex As Exception

        End Try
    End Sub

#Region "Total Ordenes JUANJO"
    Private Sub uspChecaCuantasOrdenesQuejas()
        Dim Problema As New ClassClasificacionProblemas
        Dim DT As New DataTable

        Problema.OpAccion = 2
        DT = Problema.uspChecaCuantasOrdenesQuejas()

        For Each fila As DataRow In DT.Rows
            Me.lblTotalPendientes.Text = fila("pendietnes").ToString()
            Me.lblTotalConVisita.Text = fila("visitas").ToString()
            Me.lblTotalEnProceso.Text = fila("proceso").ToString()
        Next
    End Sub
#End Region

#Region "COMBO COLONIAS"
    Private Sub llenaComboColonias()
        Dim colonias As New BaseIII
        Try
            colonias.limpiaParametros()
            Me.cmbColonias.DataSource = colonias.ConsultaDT("uspConsultaColonias")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            If ComboBoxCompanias.Items.Count > 0 Then
                busca(4)
            End If
            'llenaComboColonias()
            uspChecaCuantasOrdenesQuejas()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ComboBoxCiudad_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        Try
            GloClvCiudad = ComboBoxCiudad.SelectedValue
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        If TxtSetUpBox.Text = "" And TxtTarjeta.Text = "" Then
            Exit Sub
        End If
        busca(5)
    End Sub

    Private Sub TxtSetUpBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtSetUpBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TxtSetUpBox.Text = "" And TxtTarjeta.Text = "" Then
                Exit Sub
            End If
            busca(5)
        End If
    End Sub

    Private Sub TxtTarjeta_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtTarjeta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TxtSetUpBox.Text = "" And TxtTarjeta.Text = "" Then
                Exit Sub
            End If
            busca(5)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        busca(4)
    End Sub
End Class