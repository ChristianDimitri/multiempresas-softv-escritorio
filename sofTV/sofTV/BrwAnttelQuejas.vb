Imports System.Data.SqlClient
Imports System.Text

Public Class BrwAnttelQuejas

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelCiudadGeneral")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_Ciudad()
        Try
            BaseII.limpiaParametros()
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_ciudad")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"

            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
                GloClvCiudad = ComboBoxCiudad.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ComboBoxCompanias.SelectedValue = 0 Then
            MsgBox("Selecciona una Compa��a")
            Exit Sub
        End If
        opcion = "N"
        GloClv_TipSer = 0
        FrmQuejasAntTel.Show()
    End Sub

    Private Sub consultar()

        If ComboBox2.Text.Length = 0 Then
            MessageBox.Show("Selecciona un tipo de servicio.")
            Exit Sub
        End If

        If DataGridView1.RowCount = 0 Then
            MessageBox.Show(mensaje2)
            Exit Sub
        End If
        eGloContrato = ContratoCompaniaLabel.Text
        GloClv_TipSer = DataGridView1.SelectedCells(6).Value
        gloClave = DataGridView1.SelectedCells(0).Value
        opcion = "C"
        FrmQuejasAntTel.Show()

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub modificar()

        If ComboBox2.Text.Length = 0 Then
            MessageBox.Show("Selecciona un tipo de servicio.")
            Exit Sub
        End If

        If DataGridView1.RowCount = 0 Then
            MessageBox.Show(mensaje1)
            Exit Sub
        End If

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BuscaBloqueadoTableAdapter.Connection = CON
        Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, CInt(ContratoCompaniaLabel.Text), NUM, num2)
        CON.Close()

        If num2 = 1 Then
            MsgBox("El Cliente " + DataGridView1.SelectedCells(2).Value.ToString + " ha sido bloqueado, por lo que no se podr� llevar a cabo la queja.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        eGloContrato = CInt(ContratoCompaniaLabel.Text)
        GloClv_TipSer = DataGridView1.SelectedCells(6).Value
        gloClave = DataGridView1.SelectedCells(0).Value
        opcion = "M"
        FrmQuejasAntTel.Show()

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        modificar()
    End Sub


    Private Sub BrwAnttelQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Label7.ForeColor = Color.Black
        Llena_Ciudad()
        GloClvCiudad = ComboBoxCiudad.SelectedValue
        Llena_companias()
        GloIdCompania = ComboBoxCompanias.SelectedValue
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        Me.MUESTRAUSUARIOSTableAdapter.Connection = CON
        Me.MUESTRAUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAUSUARIOS, 2)
        GloClv_TipSer = Me.ComboBox2.SelectedValue
        llenaComboColonias()
        Busca(45)
        GloBnd = False
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim sTATUS As Integer = 0
        Try

            If Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = False Then
                sTATUS = 1
            ElseIf Me.CheckBox2.Checked = True And Me.CheckBox1.Checked = False Then
                sTATUS = 2
            Else
                sTATUS = 0
            End If

            If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
                If op = 0 Then 'contrato
                    If Len(Trim(Me.TextBox1.Text)) > 0 Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, loccon, "", "", "", "", "", "", "", 0, 0, sTATUS, 0, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If

                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox2.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", 1, 0, sTATUS, 0, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 2 Then 'Calle y numero
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, "", "", 2, 0, sTATUS, Me.cmbColonias.SelectedValue, GloIdCompania)
                ElseIf op = 11 Then 'Usuario
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(11, Integer)), Me.ComboBox1.SelectedValue, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(11, Integer)), Me.ComboBox1.SelectedValue, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 11, Me.ComboBox1.SelectedValue, sTATUS, 0, GloIdCompania)
                ElseIf op = 12 Then 'con queja
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(12, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(12, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 12, 0, sTATUS, 0, GloIdCompania)
                ElseIf op = 13 Then 'Sin queja
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(13, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(13, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 13, 0, sTATUS, 0, GloIdCompania)
                ElseIf op = 199 Then 'Status
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, sTATUS, "", "", "", "", "", "", 199, 0, sTATUS, 0, GloIdCompania)
                ElseIf op = 3 Then 'clv_Orden
                    If IsNumeric(Me.Clv_llamada.Text) = True Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), Me.Clv_llamada.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, Me.Clv_llamada.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, Me.Clv_llamada.Text, 0, sTATUS, "", "", "", "", "", "", 3, 0, sTATUS, 0, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 5 Then
                    If Len(Trim(Me.TxtSetUpBox.Text)) > 0 Or Len(Trim(Me.TxtTarjeta.Text)) > 0 Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 5, 0, sTATUS, 0, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 45 Then
                    BUSCALLAMADASDEINTERNET_SEPARADO(0, 0, 0, "", "", "", "", "", "", "", 1, 0, sTATUS, 0, GloIdCompania)

                Else
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)), 0, sTATUS)
                    'BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, sTATUS, "", "", "", "", 4, "", "", 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 4, 0, sTATUS, 0, GloIdCompania)
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.Clv_llamada.Clear()
                Me.BNUMERO.Clear()
                Me.BCALLE.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

        Busca(12)

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged

        Busca(13)

    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            Busca(11)
        End If
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            If TextBox1.Text.Contains("-") Then
                Dim array As String()
                array = TextBox1.Text.Trim.Split("-")
                loccon = array(0).Trim
                If array(1).Length = 0 Then
                    Exit Sub
                End If
                GloIdCompania = array(1).Trim
                TextBox1.Text = array(0) + "-" + array(1)
                Busca(0)
            Else
                loccon = TextBox1.Text
                GloIdCompania = 999
                Busca(0)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
        TextBox2.Text = ""
        APaternoTextBox.Text = ""
        AMaternoTextBox.Text = ""
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Try
                If TextBox1.Text.Contains("-") Then
                    Dim array As String()
                    array = TextBox1.Text.Trim.Split("-")
                    loccon = array(0).Trim
                    If array(1).Length = 0 Then
                        Exit Sub
                    End If
                    GloIdCompania = array(1).Trim
                    TextBox1.Text = array(0) + "-" + array(1)
                    Busca(0)
                Else
                    loccon = TextBox1.Text
                    GloIdCompania = 999
                    Busca(0)
                End If
            Catch ex As Exception

            End Try

        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BrwAnttelQuejas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Me.ComboBox2.SelectedValue = GloClv_TipSer
            Me.ComboBox2.Text = GloNom_TipSer
            Me.ComboBox2.FindString(GloNom_TipSer)
            Me.ComboBox2.Text = GloNom_TipSer
            Busca(4)


        End If
    End Sub



    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub



    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim comando As New SqlCommand()
        comando.Connection = CON
        Dim array As String()
        array = DataGridView1.SelectedCells(2).Value.ToString.Split("-")
        loccon = array(0)
        GloIdCompania = array(1)
        comando.CommandText = "select contrato from Rel_Contratos_Companias where ContratoCompania=" & array(0) & " and IdCompania=" & array(1)
        ContratoCompaniaLabel.Text = comando.ExecuteScalar().ToString
        Me.Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells(0).Value
        Me.ContratoLabel1.Text = Me.DataGridView1.SelectedCells(2).Value
        Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells(3).Value
        CON.Close()

    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button4.Enabled = True Then
            modificar()
        ElseIf Button3.Enabled = True Then
            consultar()
        End If
    End Sub

    Private Sub Clv_llamada_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Clv_llamada.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            Busca(11)
        End If
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If Me.ComboBox2.SelectedIndex <> -1 Then
            GloClv_TipSer = Me.ComboBox2.SelectedValue
            Busca(4)
        End If
    End Sub
    Private Sub BUSCALLAMADASDEINTERNET_SEPARADO(ByVal Clv_TipSerAtn As Integer, ByVal Clv_LlamdaAtn As Long, ByVal ContratoAtn As Long, ByVal NombreAtn As String, ByVal ApePaternoAtn As String, _
                                                 ByVal ApeMaternoAtn As String, ByVal CalleAtn As String, ByVal NumeroAtn As String, ByVal SetUpBox As String, ByVal Tarjeta As String, _
                                                 ByVal OpAtn As Integer, ByVal Clv_UsuarioAtn As Integer, ByVal TipoQuejaAtn As Integer, ByVal prmClvColonia As Integer, oIdCompania As Integer)
        'op=4 TODOS BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 4, 0, sTATUS, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
        'op=0 CONTRATO BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, Me.TextBox1.Text, "", "", "", "", "", "", "", 0, 0, sTATUS, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC uspBuscaLLamadasDeInternet ")
        StrSQL.Append(CStr(Clv_TipSerAtn) & ", ")
        StrSQL.Append(CStr(Clv_LlamdaAtn) & ", ")
        StrSQL.Append(CStr(ContratoAtn) & ", ")
        StrSQL.Append("'" & NombreAtn & "', ")
        StrSQL.Append("'" & ApePaternoAtn & "', ")
        StrSQL.Append("'" & ApeMaternoAtn & "', ")
        StrSQL.Append("'" & CalleAtn & "', ")
        StrSQL.Append("'" & NumeroAtn & "', ")
        StrSQL.Append("'" & SetUpBox & "', ")
        StrSQL.Append("'" & Tarjeta & "', ")
        StrSQL.Append(CStr(OpAtn) & ", ")
        StrSQL.Append(CStr(Clv_UsuarioAtn) & ", ")
        StrSQL.Append(CStr(TipoQuejaAtn) & ", ")
        StrSQL.Append(CStr(prmClvColonia) & ", ")
        StrSQL.Append(CStr(oIdCompania) & ", ")
        StrSQL.Append(CStr(GloClvCiudad))

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try

            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
            'gloClave = Me.DataGridView1.SelectedCells(0).Value
            If DataGridView1.Rows.Count > 0 Then
                Dim comando As New SqlCommand()
                comando.Connection = CON
                Dim array As String()
                array = DataGridView1.SelectedCells(2).Value.ToString.Split("-")
                loccon = array(0)
                GloIdCompania = array(1)
                comando.CommandText = "select contrato from Rel_Contratos_Companias where ContratoCompania=" & array(0) & " and IdCompania=" & array(1)
                ContratoCompaniaLabel.Text = comando.ExecuteScalar().ToString
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Dim loccon As Integer = 0
    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Busca(5)
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""
    End Sub

#Region "COMBO COLONIAS"
    Private Sub llenaComboColonias()
        Dim colonias As New BaseIII
        Try
            colonias.limpiaParametros()
            Me.cmbColonias.DataSource = colonias.ConsultaDT("uspConsultaColonias")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Busca(4)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxCiudad_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        Try
            GloClvCiudad = ComboBoxCiudad.SelectedValue
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub
End Class