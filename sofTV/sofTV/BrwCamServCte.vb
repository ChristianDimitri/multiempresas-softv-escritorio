Imports System.Data.SqlClient
Public Class BrwCamServCte

    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim contrato As Long = Nothing
    Private busca As Integer = 0

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelCiudadGeneral")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_Ciudad()
        Try
            BaseII.limpiaParametros()
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_ciudad")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"

            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
                GloClvCiudad = ComboBoxCiudad.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        eContrato = 0
        FrmCamServCte.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        If Len(Me.TextBox1.Text) > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim array As String()
            Try
                array = TextBox1.Text.Trim.Split("-")
                If (array.Length = 1) Then
                    GloIdCompania = 999
                Else
                    GloIdCompania = array(1)

                End If

            Catch ex As Exception
                MsgBox("Contrato inv�lido. Int�ntelo nuevamente.")
                Exit Sub
            End Try
            'Me.ConCambioServClienteTableAdapter.Connection = CON
            'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, Me.TextBox1.Text, "", 0, 2)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(array(0)))
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 2)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
            ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
            CON.Close()
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
                ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
                'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
                Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
            Else
                limpiacampos()
            End If
            busca = 1
        ElseIf Len(Me.TextBox1.Text) = 0 Then
            busca = 0
        End If
    End Sub

    Private Sub nuevoTipSer()
        Try
            Dim con As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("TipSerNew", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
            par.Value = contrato
            cmd.Parameters.Add(par)

            Dim par1 As New SqlParameter("@Desc", SqlDbType.VarChar, 150)
            par1.Direction = ParameterDirection.Output
            par1.Value = ""
            cmd.Parameters.Add(par1)

            con.Open()
            cmd.ExecuteNonQuery()
            Label7.Text = par1.Value.ToString
            con.Close()
            con.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress

        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validaci�n enteros y gui�n
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Or val = 45 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
 
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox1.Text) > 0 Then
                'CON.Open()
                'Me.ConCambioServClienteTableAdapter.Connection = CON
                'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, Me.TextBox1.Text, "", 0, 2)
                'CON.Close()
                Dim array As String()
                Try
                    array = TextBox1.Text.Trim.Split("-")
                    If (array.Length = 1) Then
                        GloIdCompania = 999
                    Else
                        GloIdCompania = array(1)

                    End If

                Catch ex As Exception
                    MsgBox("Contrato inv�lido. Int�ntelo nuevamente.")
                    Exit Sub
                End Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, array(0))
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, 2)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
                'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
                ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
                ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
                If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                    contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                    nuevoTipSer()
                    ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                    NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                    ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                    Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
                    'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                    Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                    StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
                    Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
                Else
                    limpiacampos()
                End If
                busca = 1
            ElseIf Len(Me.TextBox1.Text) = 0 Then
                busca = 0
            End If
        End If

    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If TextBox2.Text.Length() = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.ConCambioServClienteTableAdapter.Connection = CON
        'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, Me.TextBox2.Text, 0, 3)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Me.TextBox2.Text)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
        'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
        ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
        CON.Close()
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
            ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
            NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
            ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
            Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
            'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
            StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
            Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
        Else
            limpiacampos()
        End If
        If Len(Me.TextBox2.Text) = 0 Then
            busca = 0
        Else
            busca = 1
        End If

    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            'CON.Open()
            'Me.ConCambioServClienteTableAdapter.Connection = CON
            'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, Me.TextBox2.Text, 0, 3)
            'CON.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Me.TextBox2.Text)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
            ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
                ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
                'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
                Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
            Else
                limpiacampos()
            End If
        End If

    End Sub


    Private Sub BrwCamServCte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If busca <> 1 Then
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.ConCambioServClienteTableAdapter.Connection = CON
            'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
            'CON.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
            ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
                ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
                'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
                Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
            Else
                limpiacampos()
            End If
        End If
    End Sub

    Private Sub BrwCamServCte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        Llena_Ciudad()
        Llena_companias()
        'ComboBoxCompanias.Items.RemoveAt(0)
        '@Clave bigint,@Contrato bigint,@Nombre varchar(200),@Clv_TipSer int,@Op int, @idcompania int)
        'Me.ConCambioServClienteTableAdapter.Connection = CON
        'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
        'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
        ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
        CON.Close()
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
            ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
            NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
            ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
            Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
            'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
            StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
            Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
        Else
            limpiacampos()
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub RealizarTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RealizarTextBox.TextChanged

        If Me.RealizarTextBox.Text = 3 Then
            Me.Label5.Text = "Servicio Actual :"
            Me.Label6.Text = "Servicio Anterior :"
        End If
        If Me.RealizarTextBox.Text = 1 Then
            Me.Label5.Text = "Servicio Solicitado :"
            Me.Label6.Text = "Servicio Actual :"
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorCambioServClienteTableAdapter.Connection = CON
                Me.BorCambioServClienteTableAdapter.Fill(Me.DataSetEric.BorCambioServCliente, contrato, 0, eRes, eMsg)
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje6)
                    Refrescar()
                End If


            Else
                MsgBox("Selecciona un Cambio de Servicio.", , "Atenci�n")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            If Me.RealizarTextBox.Text = 1 Then
                eContrato = Me.ContratoLabel2.Text
                eTipSer = Me.Clv_TipSerTextBox.Text
                eOpcion = "M"
                FrmCamServCte.Show()
            Else
                MsgBox("S�lo se pueden Modificar Procesos Activos.")
            End If
        Else
            MsgBox("Selecciona un Cliente.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Refrescar()
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.ConCambioServClienteTableAdapter.Connection = CON
        'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
        'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
        ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
        'CON.Close()
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
            ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
            NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
            ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
            Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
            'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
            StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
            Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
        Else
            limpiacampos()
        End If
    End Sub

    Private Sub ConCambioServClienteDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConCambioServClienteDataGridView.CellContentClick
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
        End If
    End Sub

    Private Sub Button3_Click_1(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        
        eOpcion = "N"
        eContrato = 0
        FrmCamServCte.Show()
    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorCambioServClienteTableAdapter.Connection = CON
                Me.BorCambioServClienteTableAdapter.Fill(Me.DataSetEric.BorCambioServCliente, contrato, 0, eRes, eMsg)
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje6)
                    Refrescar()
                End If


            Else
                MsgBox("Selecciona un Cambio de Servicio.", , "Atenci�n")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("ConCambioServCliente")
            ConCambioServClienteDataGridView.Columns("Contrato").Visible = False
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
                ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
                'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
                Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
            Else
                limpiacampos()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConCambioServClienteDataGridView_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConCambioServClienteDataGridView.CellClick
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
                ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                Servicio_AnteriorLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(8).Value.ToString
                'Label7.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(13).Value.ToString
                Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub limpiacampos()
        contrato = 0
        ContratoLabel2.Text = ""
        NombreLabel2.Text = ""
        ConceptoLabel2.Text = ""
        Servicio_AnteriorLabel2.Text = ""
        Label7.Text = ""
        Servicio_ActualLabel2.Text = ""
        StatusLabel2.Text = ""
        Fecha_SolLabel1.Text = ""
    End Sub

    Private Sub ComboBoxCiudad_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        Try
            GloClvCiudad = ComboBoxCiudad.SelectedValue
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub

End Class