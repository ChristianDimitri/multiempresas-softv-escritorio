﻿Public Class BrwComisionCajera

    Private Sub BrwComisionCajera_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        MuestraRangosComisionCajera()
    End Sub

    Private Sub BrwComisionCajera_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        Usp_MuestraGruposComisionCajera()
        MuestraRangosComisionCajera()

        If Me.DataGridView1.RowCount > 0 Then
            idRanCoCaj = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idRanCoCaj = 0
        End If
    End Sub

    Private Sub MuestraRangosComisionCajera()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idGrupo", SqlDbType.Int, ComboBox2.SelectedValue)
        DataGridView1.DataSource = BaseII.ConsultaDT("Usp_MuestraRangosComisionCajera")

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        opRanCoCaj = "N"
        FrmComisionCajera.Show()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If idRanCoCaj > 0 Then
            opRanCoCaj = "C"
            FrmComisionCajera.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If idRanCoCaj > 0 Then
            opRanCoCaj = "M"
            FrmComisionCajera.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If Me.DataGridView1.RowCount > 0 Then
            idRanCoCaj = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idRanCoCaj = 0
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, idRanCoCaj)
        BaseII.Inserta("Usp_BorRangosComisionCajera")

        MsgBox("Borrado con exito")

        MuestraRangosComisionCajera()

    End Sub

    Private Sub Usp_MuestraGruposComisionCajera()
        Try
            BaseII.limpiaParametros()
            ComboBox2.DataSource = BaseII.ConsultaDT("Usp_MuestraGruposComisionCajera")
            ComboBox2.DisplayMember = "Grupo"
            ComboBox2.ValueMember = "idGrupo"

            If ComboBox2.Items.Count > 0 Then
                ComboBox2.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        Try
            MuestraRangosComisionCajera()

            If Me.DataGridView1.RowCount > 0 Then
                idRanCoCaj = Me.DataGridView1.SelectedCells(0).Value.ToString
            Else
                idRanCoCaj = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class