﻿Imports sofTV.BAL
Public Class BrwConceptosPoliza
    Dim AplicaFiltro As Boolean = True
    Dim CargaGrupos As Boolean = False
    Dim dTLimpio As New DataTable

    Structure ColumnasGrid
        Dim idxClave As Integer
        Dim idxConcepto As Integer
    End Structure

    Private Sub BrwConceptosPoliza_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If AplicaFiltro Then
            AplicaFiltro = False

            uspFiltraConceptosIngresos(4, 0, cbxGrupo.SelectedValue, "")

            'Me.dgvConceptos.DataSource = FiltraConceptos(4)
            'ocultaColumnas(Me.dgvConceptos)

            If Me.dgvConceptos.RowCount > 0 Then
                SeleccionaFila(Me.dgvConceptos, 1)
            End If

        End If
    End Sub

    Private Sub BrwConceptosPoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, GloClvCompania)
        cbCompania.DataSource = BaseII.ConsultaDT("MUESTRAtblCompanias")


        dTLimpio.Columns.Add("Clave")
        dTLimpio.Columns.Add("Concepto")

        'Dim GrupoConcepto As New GrupoConceptoIngreso

        'CargaGrupos = False
        'With Me.cbxGrupo
        '    .DataSource = GrupoConcepto.GetAll
        '    .ValueMember = "IdGrupo"
        '    .DisplayMember = "Concepto"
        'End With
        'CargaGrupos = True
    End Sub




    Private Sub ocultaColumnas(ByRef dgv As DataGridView)

        For Each dc As DataGridViewColumn In dgv.Columns

            If (dc.Name = "Clave") Then
                dc.ReadOnly = True
                dc.Visible = True
            ElseIf (dc.Name = "Concepto") Then
                dc.ReadOnly = True
                dc.Visible = True
                dc.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End If
        Next

    End Sub

    Private Sub SeleccionaFila(ByVal dgv As DataGridView, ByVal Fila As Integer)
        Try
            Dim i As Integer = 0

            If Fila = 0 Then
                i = dgv.CurrentRow.Index
            End If

            Dim indexGrid As ColumnasGrid = obtenerIdexGridTrabajos(dgv)

            Me.IdGrupoLabel1.Text = dgv.Item(indexGrid.idxClave, i).Value
            Me.ConceptoLabel1.Text = dgv.Item(indexGrid.idxConcepto, i).Value
        Catch ex As Exception
            'MsgBox(ex.Message, vbInformation, "Atencion!!!")
        End Try
    End Sub

    Private Function obtenerIdexGridTrabajos(ByVal dgv As DataGridView) As ColumnasGrid
        Dim resultado As New ColumnasGrid
        Dim indx As Integer = 0

        Dim idxClienteVigilanteID As Integer
        Dim idxContrato As Integer
        Dim idxFecha As Integer
        Dim idxclv_Calle As Integer
        Dim idxclv_Colonia As Integer
        Dim idxclv_Ciudad As Integer
        Dim idxNoExterior As Integer
        Dim idxNoInterior As Integer
        Dim idxclv_Tecnico As Integer
        Dim idxFechaRevision As Integer
        Dim idxProcedio As Integer

        For Each dc As DataGridViewColumn In dgv.Columns
            If (dc.Name = "Clave") Then
                resultado.idxClave = indx
                indx = indx + 1
            ElseIf (dc.Name = "Concepto") Then
                resultado.idxConcepto = indx
                indx = indx + 1
            Else
                indx = indx + 1
            End If
        Next
        Return resultado
    End Function

    Private Sub dgvConceptos_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvConceptos.CellClick
        SeleccionaFila(Me.dgvConceptos, 0)
    End Sub


    Private Sub btnBuscarID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarID.Click

        If cbCompania.Text.Length = 0 Then
            dgvConceptos.DataSource = dTLimpio
            Exit Sub
        End If
        If cbxGrupo.Text.Length = 0 Then
            dgvConceptos.DataSource = dTLimpio
            Exit Sub
        End If
        If txtClave.Text.Length = 0 Then
            dgvConceptos.DataSource = dTLimpio
            Exit Sub
        End If
        If IsNumeric(txtClave.Text) = False Then
            dgvConceptos.DataSource = dTLimpio
            MessageBox.Show("Teclea una Clave numérica válida.")
            Exit Sub
        End If

        uspFiltraConceptosIngresos(1, txtClave.Text, cbxGrupo.SelectedValue, "")

        'If IsNumeric(Me.txtClave.Text) Then
        '    AplicaFiltro = True
        '    Me.dgvConceptos.DataSource = FiltraConceptos(1)
        '    ocultaColumnas(Me.dgvConceptos)
        'End If

    End Sub

    Private Sub btnConcepto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConcepto.Click
        If cbCompania.Text.Length = 0 Then
            dgvConceptos.DataSource = dTLimpio
            Exit Sub
        End If
        If cbxGrupo.Text.Length = 0 Then
            dgvConceptos.DataSource = dTLimpio
            Exit Sub
        End If
        If txtConcepto.Text.Length = 0 Then
            dgvConceptos.DataSource = dTLimpio
            Exit Sub
        End If
        uspFiltraConceptosIngresos(2, 0, cbxGrupo.SelectedValue, txtConcepto.Text)
        'AplicaFiltro = True
        'Me.dgvConceptos.DataSource = FiltraConceptos(2)
        'ocultaColumnas(Me.dgvConceptos)
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        opcion = "N"
        AplicaFiltro = True
        GloClvCompania = cbCompania.SelectedValue
        FrmConceptosPoliza.Clave = 0
        FrmConceptosPoliza.Show()
    End Sub

    Private Sub ConsultarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarButton.Click
        If Me.dgvConceptos.Rows.Count > 0 Then
            opcion = "C"
            GloClvCompania = cbCompania.SelectedValue
            FrmConceptosPoliza.Clave = CInt(Me.IdGrupoLabel1.Text)
            FrmConceptosPoliza.Show()
        End If

    End Sub

    Private Sub ModificarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarButton.Click
        If Me.dgvConceptos.Rows.Count > 0 Then
            opcion = "M"
            AplicaFiltro = True
            GloClvCompania = cbCompania.SelectedValue
            FrmConceptosPoliza.Clave = CInt(Me.IdGrupoLabel1.Text)
            FrmConceptosPoliza.Show()
        End If
    End Sub

    Private Sub SalirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirButton.Click
        Me.Close()

    End Sub


    Private Sub dgvConceptos_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvConceptos.CurrentCellChanged
        SeleccionaFila(Me.dgvConceptos, 0)
    End Sub

    Private Sub cbCompania_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCompania.SelectedIndexChanged
        If (cbCompania.Text.Length = 0) Then
            Exit Sub
        End If

        cbxGrupo.Text = ""
        dgvConceptos.DataSource = dTLimpio
        GloClvCompania = cbCompania.SelectedValue

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        cbxGrupo.DataSource = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngreso")
    End Sub

    Private Sub cbxGrupo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxGrupo.SelectedIndexChanged
        If cbxGrupo.Text.Length = 0 Then
            dgvConceptos.DataSource = dTLimpio
            Exit Sub
        End If

        GloClvGrupo = cbxGrupo.SelectedValue
        uspFiltraConceptosIngresos(4, 0, cbxGrupo.SelectedValue, "")

        'If CargaGrupos Then
        '    Me.dgvConceptos.DataSource = FiltraConceptos(4)
        '    ocultaColumnas(Me.dgvConceptos)
        'End If
    End Sub

    Private Sub uspFiltraConceptosIngresos(ByVal Opcion As Integer, ByVal IdConcepto As Integer, ByVal IdGrupo As Integer, ByVal Concepto As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, CObj(Opcion))
        BaseII.CreateMyParameter("@IdConcepto", SqlDbType.Int, CObj(IdConcepto))
        BaseII.CreateMyParameter("@IdGrupo", SqlDbType.Int, CObj(IdGrupo))
        BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, CObj(Concepto), 250)
        BaseII.CreateMyParameter("@Clv_Compania", SqlDbType.Int, GloClvCompania)
        dgvConceptos.DataSource = BaseII.ConsultaDT("uspFiltraConceptosIngresos")
    End Sub

    'Public Function FiltraConceptos(ByVal prmOpcion As Integer) As DataTable
    '    Try
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@Opcion", SqlDbType.BigInt, prmOpcion)
    '        Select Case prmOpcion
    '            Case 1
    '                BaseII.CreateMyParameter("@IDGrupo", SqlDbType.BigInt, Me.cbxGrupo.SelectedValue)
    '                BaseII.CreateMyParameter("@IDConcepto", SqlDbType.BigInt, CInt(Me.txtClave.Text))
    '            Case 2
    '                BaseII.CreateMyParameter("@IDGrupo", SqlDbType.BigInt, Me.cbxGrupo.SelectedValue)
    '                BaseII.CreateMyParameter("@Concepto", SqlDbType.NVarChar, Me.txtConcepto.Text, 250)
    '            Case 4
    '                BaseII.CreateMyParameter("@IDGrupo", SqlDbType.BigInt, Me.cbxGrupo.SelectedValue)
    '        End Select

    '        Dim tblConceptos As DataTable = BaseII.ConsultaDT("uspFiltraConceptosIngresos")
    '        Return tblConceptos
    '    Catch ex As Exception
    '        MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
    '    End Try
    'End Function

End Class