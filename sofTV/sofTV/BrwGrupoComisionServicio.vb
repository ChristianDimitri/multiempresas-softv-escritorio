﻿Public Class BrwGrupoComisionServicio

    Private Sub BrwGrupoComisionServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenadgvcluster(1, "", "")
    End Sub

    Private Sub llenadgvcluster(ByVal opcion As Integer, ByVal clave As String, ByVal descripcion As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opcion)
        BaseII.CreateMyParameter("@clave", SqlDbType.VarChar, clave)
        BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, descripcion)        
        dgvcluster.DataSource = BaseII.ConsultaDT("MuestraGrupoComisionServicio")
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        llenadgvcluster(2, tbclave.Text, "")
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        llenadgvcluster(3, "", tbdescripcion.Text)
    End Sub

    Private Sub BrwCluster_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        llenadgvcluster(1, "", "")
    End Sub


    Private Sub dgvcluster_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvcluster.SelectionChanged
        Clv_TxtLabel1.Text = dgvcluster.CurrentRow.Cells(0).Value
        DescripcionLabel1.Text = dgvcluster.CurrentRow.Cells(1).Value
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        opcion = "N"
        FrmGrupoComisionServicio.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If dgvcluster.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "C"
        gloIdGrupoComisionServicio = CInt(Clv_TxtLabel1.Text)
        FrmGrupoComisionServicio.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If dgvcluster.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "M"
        gloIdGrupoComisionServicio = CInt(Clv_TxtLabel1.Text)
        FrmGrupoComisionServicio.Show()
    End Sub
End Class