﻿Imports System.Data.SqlClient
Public Class BrwMovimientosCartera

    Private Sub MUESTRAMovimientosCartera(ByVal Op As Integer, ByVal Clv_Cartera As Integer, ByVal Fecha As DateTime)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
            BaseII.CreateMyParameter("@Clv_Cartera", SqlDbType.Int, Clv_Cartera)
            BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha)
            BaseII.CreateMyParameter("@CadCiudades", SqlDbType.VarChar, cbCiudades.SelectedValue)
            BaseII.CreateMyParameter("@CadCompanias", SqlDbType.VarChar, cbcompanias.SelectedValue)
            dgvMovimientos.DataSource = BaseII.ConsultaDT("MUESTRAMovimientosCartera")
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub tbMovimiento_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbMovimiento.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbMovimiento.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbMovimiento.Text) = False Then Exit Sub
        MUESTRAMovimientosCartera(1, tbMovimiento.Text, DateTime.Today)
    End Sub

    Private Sub bnBuscarMovimiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscarMovimiento.Click
        If tbMovimiento.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbMovimiento.Text) = False Then Exit Sub
        MUESTRAMovimientosCartera(1, tbMovimiento.Text, DateTime.Today)
    End Sub

    Private Sub bnBuscarFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscarFecha.Click
        MUESTRAMovimientosCartera(2, 0, dtpFecha.Value.ToShortDateString)
    End Sub

    Private Sub BrwMovimientosCartera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenacbcompanias()
        llenacbciudades()
        MUESTRAMovimientosCartera(0, 0, DateTime.Today)
        BndMovimientoCartera = False
    End Sub
    Private Sub llenacbciudades()
        Try
            BaseII.limpiaParametros()
            cbCiudades.DisplayMember = "cds"
            cbCiudades.ValueMember = "clave_cds"
            BaseII.CreateMyParameter("@cadenacompanias", SqlDbType.VarChar, cbcompanias.SelectedValue)
            cbCiudades.DataSource = BaseII.ConsultaDT("ElegirCuidadesMovCartera")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub llenacbcompanias()
        Try
            BaseII.limpiaParametros()
            cbcompanias.DisplayMember = "cds"
            cbcompanias.ValueMember = "clave_cds"
            cbcompanias.DataSource = BaseII.ConsultaDT("ElegirCompaniasMovCartera")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click
        If dgvMovimientos.RowCount = 0 Then
            MessageBox.Show("Selecciona un movimiento de cartera.")
            Exit Sub
        End If
        GloMovimientoCartera = dgvMovimientos.SelectedCells(0).Value
        BndMovimientoCartera = True
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        GloMovimientoCartera = 0
        Me.Close()
    End Sub

    Private Sub cbCiudades_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCiudades.SelectedIndexChanged
        Try
            MUESTRAMovimientosCartera(0, 0, DateTime.Today)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbcompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbcompanias.SelectedIndexChanged
        Try
            llenacbciudades()
        Catch ex As Exception

        End Try
    End Sub
End Class