Imports System.Data.SqlClient

Public Class BrwProgramaciones
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BrwProgramaciones_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            busca("", 0, 0)
            bec_bnd = False
        End If
    End Sub

    Private Sub BrwProgramaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        busca("", 0, 0)
        Llena_companias()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub busca(ByVal nombre As String, ByVal fechas As Integer, ByVal opcion As Integer) '(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            'Dim conlidia As New SqlClient.SqlConnection(MiConexion)
            'conlidia.Open()
            'Me.Busca_ProgTableAdapter.Connection = conlidia
            'Me.Busca_ProgTableAdapter.Fill(Me.DataSetLidia.Busca_Prog, nombre, fechas, opcion)
            'conlidia.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, nombre, 150)
            BaseII.CreateMyParameter("@fecha", SqlDbType.Int, fechas)
            BaseII.CreateMyParameter("@op", SqlDbType.Int, opcion)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            DataGridView1.DataSource = BaseII.ConsultaDT("Busca_Prog")
            If DataGridView1.Rows.Count > 0 Then
                Clv_calleLabel2.Text = DataGridView1.SelectedCells(1).Value.ToString
                Dim fecha As String = DataGridView1.SelectedCells(7).Value.ToString
                CMBNombreTextBox.Text = Mid(fecha, 7, 2) + "/" + Mid(fecha, 5, 2) + "/" + Mid(fecha, 1, 4)
            End If
            DataGridView1.Columns("job_id").Visible = False
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        busca(Me.TextBox1.Text, 0, 1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        DateTimePicker1.Format = DateTimePickerFormat.Custom
        DateTimePicker1.CustomFormat = "yyyyMMdd"
        busca("", CInt(Me.DateTimePicker1.Text), 2)
        DateTimePicker1.Format = DateTimePickerFormat.Short
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        TextBox2.Text = DataGridView1.SelectedCells(0).Value.ToString
        If Me.TextBox2.Text <> "" Then
            gloJobId = Me.TextBox2.Text
            opcion = "C"
            FrmConsulta_Prog.Show()
        Else
            MsgBox("Se debe Seleccionar una Programación", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        TextBox2.Text = DataGridView1.SelectedCells(0).Value.ToString
        If Me.TextBox2.Text <> "" Then
            gloJobId = Me.TextBox2.Text
            opcion = "M"
            FrmConsulta_Prog.Show()
        Else
            MsgBox("Se debe Seleccionar una Programación", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(Me.TextBox1.Text, 0, 1)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            DateTimePicker1.Format = DateTimePickerFormat.Custom
            DateTimePicker1.CustomFormat = "yyyyMMdd"
            busca("", CInt(Me.DateTimePicker1.Text), 2)
            DateTimePicker1.Format = DateTimePickerFormat.Short
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        TextBox2.Text = DataGridView1.Columns("job_id").Selected
        If Me.TextBox2.Text <> "" Then
            gloJobId = Me.TextBox2.Text
            opcion = "C"
            FrmConsulta_Prog.Show()
        Else
            MsgBox("Se debe Seleccionar Una Programación ")
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Using CON As New SqlConnection(MiConexion)
            CON.Open()
            LocOp = 22
            eBndMenIns = True
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            GloClv_tipser2 = 3
            CON.Close()
        End Using
        Programacion = 27
        FrmTipoClientes.Show()
        'FrmProgramacion_msjs.Show()
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim RESP As MsgBoxResult = MsgBoxResult.Cancel
        RESP = MsgBox("¿ ESTA SEGURO DE QUE DESEA BORRAR LA PROGRAMACIÓN:" + Me.Clv_calleLabel2.Text + " ?", MsgBoxStyle.YesNo)
        If RESP = MsgBoxResult.Yes Then
            Dim CONLIDIAS As New SqlClient.SqlConnection(MiConexion)
            CONLIDIAS.Open()
            Dim comando2 As New SqlClient.SqlCommand
            With comando2
                .Connection = CONLIDIAS
                .CommandText = "Eliminar_Programacion "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@NAME", SqlDbType.VarChar, 300)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Clv_calleLabel2.Text
                .Parameters.Add(prm)

                Dim i As Integer = comando2.ExecuteNonQuery()
                'eRes = prm1.Value
            End With
            busca("", 0, 0)
            CONLIDIAS.Close()
        End If


    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        busca("", 0, 3)
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Clv_calleLabel2.Text = DataGridView1.SelectedCells(1).Value.ToString
        Dim fecha As String = DataGridView1.SelectedCells(7).Value.ToString
        CMBNombreTextBox.Text = Mid(fecha, 7, 2) + "/" + Mid(fecha, 5, 2) + "/" + Mid(fecha, 1, 4)

    End Sub
End Class