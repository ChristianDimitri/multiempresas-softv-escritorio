﻿Public Class BrwPuntosRecuperacion

    Private Sub BrwPuntosRecuperacion_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        MuestraPuntosRecuperacion()
    End Sub

    Private Sub BrwPuntosRecuperacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        MuestraPuntosRecuperacion()

        If Me.DataGridView1.RowCount > 0 Then
            idPtsRec = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idPtsRec = 0
        End If
    End Sub

    Private Sub MuestraPuntosRecuperacion()

        BaseII.limpiaParametros()
        DataGridView1.DataSource = BaseII.ConsultaDT("Usp_MuestraPuntosRecuperacion")

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        opPtsRec = "N"
        FrmPuntosRecuperacion.Show()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If idPtsRec > 0 Then
            opPtsRec = "C"
            FrmPuntosRecuperacion.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If idPtsRec > 0 Then
            opPtsRec = "M"
            FrmPuntosRecuperacion.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If Me.DataGridView1.RowCount > 0 Then
            idPtsRec = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idPtsRec = 0
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, idPtsRec)
        BaseII.Inserta("Usp_BorPuntosRecuperacion")

        MsgBox("Borrado con exito")

        MuestraPuntosRecuperacion()

    End Sub



End Class