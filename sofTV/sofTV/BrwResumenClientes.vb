﻿Public Class BrwResumenClientes

    Private Sub BrwResumenClientes_Load(sender As Object, e As EventArgs) Handles Me.Load
        llenaResumenCliente(0)
        If Me.ResumenClienteDataGridView.RowCount > 0 Then
            GloIdResumenCliente = Me.ResumenClienteDataGridView.SelectedCells(0).Value.ToString
        End If
    End Sub

    Private Sub llenaResumenCliente(ByVal Op As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Fecha", SqlDbType.Date, DateTimePicker1.Value)

        ResumenClienteDataGridView.DataSource = BaseII.ConsultaDT("Usp_ConsultaResumenClientes")

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        llenaResumenCliente(1)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        eOpPPE = 26
        FrmImprimirPPE.Show()
    End Sub

    Private Sub ResumenClienteDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles ResumenClienteDataGridView.CellClick
        If Me.ResumenClienteDataGridView.RowCount > 0 Then
            GloIdResumenCliente = Me.ResumenClienteDataGridView.SelectedCells(0).Value.ToString
        End If
    End Sub

    Private Sub ResumenClienteDataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ResumenClienteDataGridView.CellContentClick

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        eOpPPE = 27
        FrmImprimirPPE.Show()
    End Sub
End Class