﻿Imports System.Data.SqlClient
Public Class BrwSelContratoCambioPlaza

    Private Sub BrwSelContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        colorea(Me, Me.Name)
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", "", "", "", 3)
        llenaComboColonias()
        llenaGridClientes(0, "", "", "", "", 3, Me.cmbColonias.SelectedValue, "", "")
        'CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CONTRATOLabel1.Text.Length > 0 Then
            eGloContrato = Me.CONTRATOLabel1.Text
            Dim array As String() = ContratoCompaniaLabel.Text.Split("-")
            eContratoCompania = array(0)
            GloIdCompania = array(1)
            eGloSubContrato = Me.CONTRATOLabel1.Text
            eContrato = Me.CONTRATOLabel1.Text
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            eGloContrato = 0
            eGloSubContrato = 0
            eContrato = 0
        End If
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        eGloContrato = 0
        eGloSubContrato = 0
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            Try
                If TextBox1.Text.Contains("-") Then
                    Dim array As String()
                    array = TextBox1.Text.Trim.Split("-")
                    If array(1).Length = 0 Then
                        Exit Sub
                    End If
                    GloIdCompania = array(1).Trim
                    llenaGridClientes(array(0), "", "", "", "", 0, 0, "", "")
                Else
                    GloIdCompania = 999
                    llenaGridClientes(Me.TextBox1.Text, "", "", "", "", 0, 0, "", "")
                End If
            Catch ex As Exception

            End Try
        End If
        'CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If IsNumeric(Me.TextBox1.Text) = True Then
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        Try
            If TextBox1.Text.Contains("-") Then
                Dim array As String()
                array = TextBox1.Text.Trim.Split("-")
                If array(1).Length = 0 Then
                    Exit Sub
                End If
                GloIdCompania = array(1).Trim
                llenaGridClientes(array(0), "", "", "", "", 0, 0, "", "")
            Else
                GloIdCompania = 999
                llenaGridClientes(Me.TextBox1.Text, "", "", "", "", 0, 0, "", "")
            End If
        Catch ex As Exception

        End Try


        'CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox2.Text.Length > 0 Then
                'Me.DameClientesActivosTableAdapter.Connection = CON
                'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
                llenaGridClientes(0, Me.TextBox2.Text, "", "", "", 1, 0, "", "")
            End If
        End If
        'CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Me.TextBox2.Text.Length > 0 Then
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
            llenaGridClientes(0, Me.TextBox2.Text, "", "", "", 1, 0, "", "")
        End If
        'CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue, "", "")
        End If
        'CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue, "", "")
        'CON.Close()
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue, "", "")
        End If
        'CON.Close()
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            llenaGridClientes(0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2, Me.cmbColonias.SelectedValue, "", "")
        End If
        'CON.Close()
    End Sub

#Region "GRID DE CLIENTES" 'Me.TextBox1.Text, "", "", "", "", 0, Me.cmbColonias.SelectedValue
    Private Sub llenaGridClientes(ByVal prmContrato As Long, ByVal prmNombre As String, ByVal prmCalle As String, ByVal prmNumero As String, ByVal prmCiudad As String, _
                                  ByVal prmOp As Integer, ByVal prmClvColonia As Integer, ByVal setUpBox As String, ByVal noTarjeta As String)
        Dim clientes As New BaseIII
        Try
            clientes.limpiaParametros()
            clientes.CreateMyParameter("@contrato", SqlDbType.BigInt, prmContrato)
            clientes.CreateMyParameter("@nombre", SqlDbType.VarChar, prmNombre, 250)
            clientes.CreateMyParameter("@calle", SqlDbType.VarChar, prmCalle, 250)
            clientes.CreateMyParameter("@numero", SqlDbType.VarChar, prmNumero, 250)
            clientes.CreateMyParameter("@ciudad", SqlDbType.VarChar, prmCiudad, 250)
            clientes.CreateMyParameter("@op", SqlDbType.Int, prmOp)
            clientes.CreateMyParameter("@clvColonia", SqlDbType.Int, prmClvColonia)
            clientes.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            clientes.CreateMyParameter("@SETUPBOX", SqlDbType.VarChar, setUpBox, 250)
            clientes.CreateMyParameter("@TARJETA", SqlDbType.VarChar, noTarjeta, 250)
            clientes.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)

            Me.DameClientesActivosDataGridView.DataSource = clientes.ConsultaDT("uspDameClientesActivosCambioPlaza")
            'DameClientesActivosDataGridView.Columns("Contrato").Visible = False
            'A.CONTRATO, RC.ContratoCompania,A.NOMBRE,B.NOMBRE CALLE,C.NOMBRE COLONIA,A.NUMERO,D.NOMBRE CIUDAD,ISNULL(A.SOLOINTERNET,0) SOLOINTERNET,ISNULL(A.ESHOTEL,0)ESHOTEL 
            If Me.DameClientesActivosDataGridView.RowCount > 0 Then
                Me.CONTRATOLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(0).Value.ToString
                Me.ContratoCompaniaLabel.Text = Me.DameClientesActivosDataGridView.SelectedCells(1).Value.ToString
                Me.NOMBRELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(2).Value.ToString
                Me.CALLELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(3).Value.ToString
                Me.NUMEROLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(5).Value.ToString
                Me.COLONIALabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(4).Value.ToString
                Me.CIUDADLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(6).Value.ToString
                Me.SOLOINTERNETCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(7).Value)
                Me.ESHOTELCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(8).Value)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "COMBO COLONIAS"
    Private Sub llenaComboColonias()
        Dim colonias As New BaseIII
        Try
            colonias.limpiaParametros()
            colonias.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            Me.cmbColonias.DataSource = colonias.ConsultaDT("[uspConsultaColoniasPorUsuario]")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub DameClientesActivosDataGridView_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DameClientesActivosDataGridView.CellClick
        Try
            Me.CONTRATOLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(0).Value.ToString
            Me.ContratoCompaniaLabel.Text = Me.DameClientesActivosDataGridView.SelectedCells(1).Value.ToString
            Me.NOMBRELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(2).Value.ToString
            Me.CALLELabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(3).Value.ToString
            Me.NUMEROLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(5).Value.ToString
            Me.COLONIALabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(4).Value.ToString
            Me.CIUDADLabel1.Text = Me.DameClientesActivosDataGridView.SelectedCells(6).Value.ToString
            Me.SOLOINTERNETCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(7).Value)
            Me.ESHOTELCheckBox.Checked = CBool(Me.DameClientesActivosDataGridView.SelectedCells(8).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        If TxtSetUpBox.Text.Length = 0 Then TxtSetUpBox.Text = ""
        If TxtTarjeta.Text.Length = 0 Then TxtTarjeta.Text = ""
        llenaGridClientes(0, "", "", "", "", 6, 0, TxtSetUpBox.Text, TxtTarjeta.Text)
    End Sub
End Class