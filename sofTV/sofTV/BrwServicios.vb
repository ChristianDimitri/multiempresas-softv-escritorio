Imports System.IO
Imports System.Data.SqlClient

Public Class BrwServicios
    Dim fileExistEric As Boolean = False
    Dim opt As Integer
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            GloIdCompania = 0
            
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            BUSCA(Me.ComboBox4.SelectedValue, 2)
        End If


        'fileExistEric = My.Computer.FileSystem.FileExists(RutaReportes + "\SofPa3.txt")
        'If fileExistEric = True Then
        '    fileExistEric = False
        '    File.Delete(RutaReportes + "\SofPa3.txt")
        '    BUSCA(Me.ComboBox4.SelectedValue, 2)
        'End If
    End Sub

    Private Sub BrwServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Llena_companias()

        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEDGAR.MuestraTipSerPrincipal_SER' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipal_SERTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal_SERTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipal_SER)
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        Me.MUESTRA_TIPOCLIENTESTableAdapter.Connection = CON
        Me.MUESTRA_TIPOCLIENTESTableAdapter.Fill(Me.DataSetarnoldo.MUESTRA_TIPOCLIENTES, 0)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        BUSCA(Me.ComboBox4.SelectedValue, 2)
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub BUSCA(ByVal CLV_TIPSER As Integer, ByVal OP As Integer)
        Dim CON3 As New SqlConnection(MiConexion)

        CON3.Open()
        Try
          
            If OP = 0 Then
                If Len(Trim(Me.TextBox1.Text)) > 0 Then
                    Me.BUSCASERVICIOSTableAdapter.Connection = CON3
                    'Me.BUSCASERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASERVICIOS, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", Me.TextBox1.Text + "/" + GloIdCompania, New System.Nullable(Of Integer)(CType(0, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
                    BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, TextBox1.Text)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASERVICIOS")
                    If DataGridView1.Rows.Count = 0 Then
                        Exit Sub
                    End If
                    Me.BUSCASERVICIOS2TableAdapter.Connection = CON3
                    'Me.BUSCASERVICIOS2TableAdapter.Fill(Me.Procedimientosarnoldo4.BUSCASERVICIOS2, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", Me.TextBox1.Text, New System.Nullable(Of Integer)(CType(0, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(Clv_calleLabel2.Text))
                    BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, TextBox1.Text)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
                    DataGridView2.DataSource = BaseII.ConsultaDT("BUSCASERVICIOS2")
                    If IsNumeric(Me.Clv_calleLabel2.Text) = True And IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                        If Me.ComboBox4.SelectedValue = 5 Then
                            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON3
                            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, Me.Clv_calleLabel2.Text, Me.ComboBox1.SelectedValue)
                        Else
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_calleLabel2.Text, Integer)), 0, Me.ComboBox1.SelectedValue)
                        End If


                    End If
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            ElseIf OP = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCASERVICIOSTableAdapter.Connection = CON3
                    'Me.BUSCASERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASERVICIOS, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, Me.TextBox2.Text, "" + "/" + GloIdCompania, New System.Nullable(Of Integer)(CType(1, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
                    BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, TextBox2.Text)
                    BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASERVICIOS")
                    If DataGridView1.Rows.Count = 0 Then
                        Exit Sub
                    End If
                    Me.BUSCASERVICIOS2TableAdapter.Connection = CON3
                    'Me.BUSCASERVICIOS2TableAdapter.Fill(Me.Procedimientosarnoldo4.BUSCASERVICIOS2, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, Me.TextBox2.Text, "", New System.Nullable(Of Integer)(CType(1, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(Clv_calleLabel2.Text))
                    BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, TextBox2.Text)
                    BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                    DataGridView2.DataSource = BaseII.ConsultaDT("BUSCASERVICIOS2")
                    
                    If IsNumeric(Me.Clv_calleLabel2.Text) = True And IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                        If Me.ComboBox4.SelectedValue = 5 Then
                            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON3
                            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, Me.Clv_calleLabel2.Text, Me.ComboBox1.SelectedValue)
                        Else
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_calleLabel2.Text, Integer)), 0, Me.ComboBox1.SelectedValue)
                        End If
                    End If
                    Else
                        MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                    End If
            Else
                Me.BUSCASERVICIOSTableAdapter.Connection = CON3
                'Me.BUSCASERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASERVICIOS, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", "" + "/" + GloIdCompania, New System.Nullable(Of Integer)(CType(2, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
                BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 2)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASERVICIOS")
                If DataGridView1.Rows.Count = 0 Then
                    Exit Sub
                End If
                Me.BUSCASERVICIOS2TableAdapter.Connection = CON3
                'Me.BUSCASERVICIOS2TableAdapter.Fill(Me.Procedimientosarnoldo4.BUSCASERVICIOS2, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", "", New System.Nullable(Of Integer)(CType(2, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(Clv_calleLabel2.Text))
                BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 2)
                DataGridView2.DataSource = BaseII.ConsultaDT("BUSCASERVICIOS2")
                If IsNumeric(Me.Clv_calleLabel2.Text) = True And IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                    If Me.ComboBox4.SelectedValue = 5 Then
                        Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON3
                        Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, Me.Clv_calleLabel2.Text, Me.ComboBox1.SelectedValue)
                    Else
                        Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                        Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_calleLabel2.Text, Integer)), 0, Me.ComboBox1.SelectedValue)
                    End If
                End If
                End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON3.Close()
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        GloClv_TipSer = Me.ComboBox4.SelectedValue
        BUSCA(Me.ComboBox4.SelectedValue, 2)
        If Me.ComboBox4.SelectedValue = 5 Then
            Me.Panel2.Show()
            Me.Panel3.Hide()
        Else
            Me.Panel2.Hide()
            Me.Panel3.Show()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        BUSCA(Me.ComboBox4.SelectedValue, 0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BUSCA(Me.ComboBox4.SelectedValue, 1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(Me.ComboBox4.SelectedValue, 0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(Me.ComboBox4.SelectedValue, 1)
        End If
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        GloClv_Servicio = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ComboBoxCompanias.SelectedValue = 0 Then
            MsgBox("Selecciona una Compa��a")
            Exit Sub
        End If
        opcion = "N"
        GloClv_TipSer = Me.ComboBox4.SelectedValue
        GloClv_Servicio = 0 'Me.Clv_calleLabel2.Text
        If Me.ComboBox4.SelectedValue <> 4 And Me.ComboBox4.SelectedValue <> 5 Then
            FrmServicios.Show()
        ElseIf Me.ComboBox4.SelectedValue = 4 Then
            eOpcion = "N"
            FrmServicioPPE2.Show()

            'Dim txt As String
            'Dim myProcess As New Process()

            'Using sw As StreamWriter = File.CreateText("C:\SofConex.txt")
            '    txt = MiConexion
            '    sw.Write(txt)
            '    sw.Close()
            'End Using

            'Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa2.txt")
            '    txt = "FrmServicioPPE" + "," + CStr(opcion) + "," + CStr("0") + "," + CStr("0")
            '    sw.Write(txt)
            '    sw.Close()
            'End Using
            'Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
            'myProcess.StartInfo = myProcessStartInfo
            'myProcess.Start()
        ElseIf Me.ComboBox4.SelectedValue = 5 Then
            FrmServiciosTelefonia.Show()
        End If

    End Sub
    Private Sub consultar()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        If Me.Clv_calleLabel2.Text > 0 Then
            opcion = "C"
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            GloClv_Servicio = Me.Clv_calleLabel2.Text

            If Me.ComboBox4.SelectedValue <> 4 And Me.ComboBox4.SelectedValue <> 5 Then
                FrmServicios.Show()
            ElseIf Me.ComboBox4.SelectedValue = 4 Then

                eOpcion = "C"

                Me.DameClv_PPETableAdapter.Connection = CON2
                Me.DameClv_PPETableAdapter.Fill(Me.DataSetEric.DameClv_PPE, Me.Clv_calleLabel2.Text)

                eClv_PPE = Me.Clv_PPELabel1.Text
                eClv_Servicio = GloClv_Servicio

                FrmServicioPPE2.Show()

                'Dim txt As String
                'Dim myProcess As New Process()
                'Using sw As StreamWriter = File.CreateText("C:\SofConex.txt")
                '    txt = MiConexion
                '    sw.Write(txt)
                '    sw.Close()
                'End Using
                'Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa2.txt")
                '    txt = "FrmServicioPPE" + "," + CStr(opcion) + "," + CStr(GloClv_Servicio) + "," + CStr(Me.Clv_PPELabel1.Text)
                '    sw.Write(txt)
                '    sw.Close()
                'End Using
                'Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
                'myProcess.StartInfo = myProcessStartInfo
                'myProcess.Start()
            ElseIf Me.ComboBox4.SelectedValue = 5 Then
                FrmServiciosTelefonia.Show()
            End If
        Else
            MsgBox(mensaje1, MsgBoxStyle.Information)
        End If
        CON2.Close()

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub
    Private Sub Modificar()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        If Me.Clv_calleLabel2.Text > 0 Then
            opcion = "M"
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            GloClv_Servicio = Me.Clv_calleLabel2.Text

            If Me.ComboBox4.SelectedValue <> 4 And Me.ComboBox4.SelectedValue <> 5 Then
                opcFrm = 9

                Acceso_TipoServicios.Show()
            ElseIf Me.ComboBox4.SelectedValue = 4 Then

                eOpcion = "M"

                Me.DameClv_PPETableAdapter.Connection = CON2
                Me.DameClv_PPETableAdapter.Fill(Me.DataSetEric.DameClv_PPE, Me.Clv_calleLabel2.Text)

                eClv_PPE = Me.Clv_PPELabel1.Text
                eClv_Servicio = GloClv_Servicio

                FrmServicioPPE2.Show()


                'Me.DameClv_PPETableAdapter.Connection = CON2
                'Me.DameClv_PPETableAdapter.Fill(Me.DataSetEric.DameClv_PPE, Me.Clv_calleLabel2.Text)
                'Dim txt As String
                'Dim myProcess As New Process()
                'Using sw As StreamWriter = File.CreateText("C:\SofConex.txt")
                '    txt = MiConexion
                '    sw.Write(txt)
                '    sw.Close()
                'End Using
                'Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa2.txt")
                '    txt = "FrmServicioPPE" + "," + CStr(opcion) + "," + CStr(GloClv_Servicio) + "," + CStr(Me.Clv_PPELabel1.Text)
                '    sw.Write(txt)
                '    sw.Close()
                'End Using
                'Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
                'myProcess.StartInfo = myProcessStartInfo
                'myProcess.Start()
            ElseIf Me.ComboBox4.SelectedValue = 5 Then
                FrmServiciosTelefonia.Show()
            End If
        Else
            MsgBox(mensaje1, MsgBoxStyle.Information)
        End If
        CON2.Close()
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            GloBnd = False
            Modificar()
        Else
            MsgBox(mensaje1)
        End If

    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            Modificar()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        If Me.ComboBox4.SelectedValue = 5 Then
            CON.Open()
            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON
            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, GloClv_Servicio, Me.ComboBox1.SelectedValue)
            CON.Close()
        Else
            CON.Open()
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(GloClv_Servicio, Integer)), 0, Me.ComboBox1.SelectedValue)
            CON.Close()
        End If
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Dim CON As New SqlConnection(MiConexion)
        If Me.ComboBox4.SelectedValue = 5 Then
            CON.Open()
            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON
            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, GloClv_Servicio, Me.ComboBox1.SelectedValue)
            CON.Close()
        Else
            CON.Open()
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(GloClv_Servicio, Integer)), 0, Me.ComboBox1.SelectedValue)
            CON.Close()
        End If
    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.Click

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim cmd As New SqlClient.SqlCommand
        Dim Cone As New SqlClient.SqlConnection(MiConexion)
        Dim sale As Integer
        If Me.ComboBox4.SelectedValue <> 5 Then
            Cone.Open()
            With cmd
                .CommandText = "Dime_Si_Cartera"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = Cone
                Dim Prm As New SqlParameter("@clv_servicio", SqlDbType.Int)
                Dim Prm2 As New SqlParameter("@sale", SqlDbType.Int)
                Prm.Direction = ParameterDirection.Input
                Prm2.Direction = ParameterDirection.Output
                Prm.Value = Me.Clv_calleLabel2.Text
                Prm2.Value = 0
                .Parameters.Add(Prm)
                .Parameters.Add(Prm2)
                Dim i As Integer = .ExecuteNonQuery
                sale = Prm2.Value
            End With
            Cone.Close()
            If sale = 1 Then
                GloClv_Servicio = Me.Clv_calleLabel2.Text
                LocreportServicio = True
                FrmImprimirContrato.Show()
            ElseIf sale = 0 Then
                MsgBox("Este Servicio no se Puede Imprimir", MsgBoxStyle.Information)
            End If
        ElseIf Me.ComboBox4.SelectedValue = 5 Then
            MsgBox("Este Servicio no se Puede Imprimir", MsgBoxStyle.Information)
        End If
       
    End Sub

  
    
    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            BUSCA(Me.ComboBox4.SelectedValue, 2)
            
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            CLV_TXTLabel1.Text = DataGridView1.SelectedCells(1).Value
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(Clv_calleLabel2.Text))
            BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, TextBox2.Text)
            BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
            DataGridView2.DataSource = BaseII.ConsultaDT("BUSCASERVICIOS2")
            cambiaseleccion2()
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            If IsNumeric(Me.Clv_calleLabel2.Text) = True And IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                If Me.ComboBox4.SelectedValue = 5 Then
                    Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON3
                    Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, Me.Clv_calleLabel2.Text, Me.ComboBox1.SelectedValue)
                Else
                    Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                    Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_calleLabel2.Text, Integer)), 0, Me.ComboBox1.SelectedValue)
                End If
            End If
            CON3.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView2_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView2.SelectionChanged
        Try
            CMBNombreTextBox.Text = DataGridView2.SelectedCells(2).Value
            PrecioLabel1.Text = DataGridView2.SelectedCells(3).Value

        Catch ex As Exception

        End Try
    End Sub
    Private Sub cambiaseleccion2()
        Try
            CMBNombreTextBox.Text = DataGridView2.SelectedCells(2).Value
            PrecioLabel1.Text = DataGridView2.SelectedCells(3).Value

        Catch ex As Exception

        End Try
    End Sub
End Class