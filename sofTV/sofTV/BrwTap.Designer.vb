<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwTap
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnClave = New System.Windows.Forms.Button()
        Me.dgvTaps = New System.Windows.Forms.DataGridView()
        Me.IdTap = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClaveTecnica = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cluster = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sector = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Poste = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Colonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Calle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.tbSector = New System.Windows.Forms.TextBox()
        Me.tbPoste = New System.Windows.Forms.TextBox()
        Me.tbColonia = New System.Windows.Forms.TextBox()
        Me.tbCalle = New System.Windows.Forms.TextBox()
        Me.bnSector = New System.Windows.Forms.Button()
        Me.bnPoste = New System.Windows.Forms.Button()
        Me.bnColonia = New System.Windows.Forms.Button()
        Me.bnCalle = New System.Windows.Forms.Button()
        Me.bnNuevo = New System.Windows.Forms.Button()
        Me.bnConsultar = New System.Windows.Forms.Button()
        Me.bnModifica = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tbcluster = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.dgvTaps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnClave
        '
        Me.bnClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnClave.Location = New System.Drawing.Point(32, 100)
        Me.bnClave.Name = "bnClave"
        Me.bnClave.Size = New System.Drawing.Size(75, 23)
        Me.bnClave.TabIndex = 0
        Me.bnClave.Text = "&Buscar"
        Me.bnClave.UseVisualStyleBackColor = True
        '
        'dgvTaps
        '
        Me.dgvTaps.AllowUserToAddRows = False
        Me.dgvTaps.AllowUserToDeleteRows = False
        Me.dgvTaps.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTaps.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvTaps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTaps.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdTap, Me.ClaveTecnica, Me.Cluster, Me.Sector, Me.Poste, Me.Colonia, Me.Calle})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTaps.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvTaps.Location = New System.Drawing.Point(225, 22)
        Me.dgvTaps.Name = "dgvTaps"
        Me.dgvTaps.ReadOnly = True
        Me.dgvTaps.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTaps.Size = New System.Drawing.Size(624, 668)
        Me.dgvTaps.TabIndex = 1
        '
        'IdTap
        '
        Me.IdTap.DataPropertyName = "IdTap"
        Me.IdTap.HeaderText = "IdTap"
        Me.IdTap.Name = "IdTap"
        Me.IdTap.ReadOnly = True
        Me.IdTap.Visible = False
        '
        'ClaveTecnica
        '
        Me.ClaveTecnica.DataPropertyName = "ClaveTecnica"
        Me.ClaveTecnica.HeaderText = "ClaveTecnica"
        Me.ClaveTecnica.Name = "ClaveTecnica"
        Me.ClaveTecnica.ReadOnly = True
        Me.ClaveTecnica.Width = 150
        '
        'Cluster
        '
        Me.Cluster.DataPropertyName = "Cluster"
        Me.Cluster.HeaderText = "Cluster"
        Me.Cluster.Name = "Cluster"
        Me.Cluster.ReadOnly = True
        '
        'Sector
        '
        Me.Sector.DataPropertyName = "Sector"
        Me.Sector.HeaderText = "Sector"
        Me.Sector.Name = "Sector"
        Me.Sector.ReadOnly = True
        '
        'Poste
        '
        Me.Poste.DataPropertyName = "Poste"
        Me.Poste.HeaderText = "Poste"
        Me.Poste.Name = "Poste"
        Me.Poste.ReadOnly = True
        '
        'Colonia
        '
        Me.Colonia.DataPropertyName = "Colonia"
        Me.Colonia.HeaderText = "Colonia"
        Me.Colonia.Name = "Colonia"
        Me.Colonia.ReadOnly = True
        '
        'Calle
        '
        Me.Calle.DataPropertyName = "Calle"
        Me.Calle.HeaderText = "Calle"
        Me.Calle.Name = "Calle"
        Me.Calle.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(28, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Buscar Tap por:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(29, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Clave Técnica:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(29, 212)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Sector:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(29, 299)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 15)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Poste:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(29, 385)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 15)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Colonia:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(29, 466)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 15)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Calle:"
        '
        'tbClave
        '
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(32, 73)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.Size = New System.Drawing.Size(163, 21)
        Me.tbClave.TabIndex = 8
        '
        'tbSector
        '
        Me.tbSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSector.Location = New System.Drawing.Point(32, 230)
        Me.tbSector.Name = "tbSector"
        Me.tbSector.Size = New System.Drawing.Size(163, 21)
        Me.tbSector.TabIndex = 9
        '
        'tbPoste
        '
        Me.tbPoste.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPoste.Location = New System.Drawing.Point(32, 317)
        Me.tbPoste.Name = "tbPoste"
        Me.tbPoste.Size = New System.Drawing.Size(163, 21)
        Me.tbPoste.TabIndex = 10
        '
        'tbColonia
        '
        Me.tbColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColonia.Location = New System.Drawing.Point(32, 403)
        Me.tbColonia.Name = "tbColonia"
        Me.tbColonia.Size = New System.Drawing.Size(163, 21)
        Me.tbColonia.TabIndex = 11
        '
        'tbCalle
        '
        Me.tbCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalle.Location = New System.Drawing.Point(32, 484)
        Me.tbCalle.Name = "tbCalle"
        Me.tbCalle.Size = New System.Drawing.Size(163, 21)
        Me.tbCalle.TabIndex = 12
        '
        'bnSector
        '
        Me.bnSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSector.Location = New System.Drawing.Point(32, 257)
        Me.bnSector.Name = "bnSector"
        Me.bnSector.Size = New System.Drawing.Size(75, 23)
        Me.bnSector.TabIndex = 13
        Me.bnSector.Text = "&Buscar"
        Me.bnSector.UseVisualStyleBackColor = True
        '
        'bnPoste
        '
        Me.bnPoste.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPoste.Location = New System.Drawing.Point(32, 344)
        Me.bnPoste.Name = "bnPoste"
        Me.bnPoste.Size = New System.Drawing.Size(75, 23)
        Me.bnPoste.TabIndex = 14
        Me.bnPoste.Text = "&Buscar"
        Me.bnPoste.UseVisualStyleBackColor = True
        '
        'bnColonia
        '
        Me.bnColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnColonia.Location = New System.Drawing.Point(32, 427)
        Me.bnColonia.Name = "bnColonia"
        Me.bnColonia.Size = New System.Drawing.Size(75, 23)
        Me.bnColonia.TabIndex = 15
        Me.bnColonia.Text = "&Buscar"
        Me.bnColonia.UseVisualStyleBackColor = True
        '
        'bnCalle
        '
        Me.bnCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnCalle.Location = New System.Drawing.Point(32, 511)
        Me.bnCalle.Name = "bnCalle"
        Me.bnCalle.Size = New System.Drawing.Size(75, 23)
        Me.bnCalle.TabIndex = 16
        Me.bnCalle.Text = "&Buscar"
        Me.bnCalle.UseVisualStyleBackColor = True
        '
        'bnNuevo
        '
        Me.bnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNuevo.Location = New System.Drawing.Point(868, 22)
        Me.bnNuevo.Name = "bnNuevo"
        Me.bnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bnNuevo.TabIndex = 17
        Me.bnNuevo.Text = "&NUEVO"
        Me.bnNuevo.UseVisualStyleBackColor = True
        '
        'bnConsultar
        '
        Me.bnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnConsultar.Location = New System.Drawing.Point(868, 64)
        Me.bnConsultar.Name = "bnConsultar"
        Me.bnConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bnConsultar.TabIndex = 18
        Me.bnConsultar.Text = "&CONSULTA"
        Me.bnConsultar.UseVisualStyleBackColor = True
        '
        'bnModifica
        '
        Me.bnModifica.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnModifica.Location = New System.Drawing.Point(868, 106)
        Me.bnModifica.Name = "bnModifica"
        Me.bnModifica.Size = New System.Drawing.Size(136, 36)
        Me.bnModifica.TabIndex = 19
        Me.bnModifica.Text = "&MODIFICAR"
        Me.bnModifica.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(868, 686)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 20
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(32, 180)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 23
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tbcluster
        '
        Me.tbcluster.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcluster.Location = New System.Drawing.Point(32, 153)
        Me.tbcluster.Name = "tbcluster"
        Me.tbcluster.Size = New System.Drawing.Size(163, 21)
        Me.tbcluster.TabIndex = 22
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(29, 135)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 15)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Cluster:"
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'BrwTap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbcluster)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnModifica)
        Me.Controls.Add(Me.bnConsultar)
        Me.Controls.Add(Me.bnNuevo)
        Me.Controls.Add(Me.bnCalle)
        Me.Controls.Add(Me.bnColonia)
        Me.Controls.Add(Me.bnPoste)
        Me.Controls.Add(Me.bnSector)
        Me.Controls.Add(Me.tbCalle)
        Me.Controls.Add(Me.tbColonia)
        Me.Controls.Add(Me.tbPoste)
        Me.Controls.Add(Me.tbSector)
        Me.Controls.Add(Me.tbClave)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvTaps)
        Me.Controls.Add(Me.bnClave)
        Me.Name = "BrwTap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Taps"
        CType(Me.dgvTaps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnClave As System.Windows.Forms.Button
    Friend WithEvents dgvTaps As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents tbSector As System.Windows.Forms.TextBox
    Friend WithEvents tbPoste As System.Windows.Forms.TextBox
    Friend WithEvents tbColonia As System.Windows.Forms.TextBox
    Friend WithEvents tbCalle As System.Windows.Forms.TextBox
    Friend WithEvents bnSector As System.Windows.Forms.Button
    Friend WithEvents bnPoste As System.Windows.Forms.Button
    Friend WithEvents bnColonia As System.Windows.Forms.Button
    Friend WithEvents bnCalle As System.Windows.Forms.Button
    Friend WithEvents bnNuevo As System.Windows.Forms.Button
    Friend WithEvents bnConsultar As System.Windows.Forms.Button
    Friend WithEvents bnModifica As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tbcluster As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents IdTap As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClaveTecnica As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cluster As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sector As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Poste As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Colonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Calle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
