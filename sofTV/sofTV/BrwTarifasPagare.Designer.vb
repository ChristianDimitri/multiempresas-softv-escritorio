﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwTarifasPagare
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.NuevoButton = New System.Windows.Forms.Button()
        Me.ModificarButton = New System.Windows.Forms.Button()
        Me.EliminarButton = New System.Windows.Forms.Button()
        Me.SalirButton = New System.Windows.Forms.Button()
        Me.TipServCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CostosDataGrid = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TIPSER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Articulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Costo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Principal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adicional = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idcompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ADICIONAL2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ADICIONAL3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.CostosDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'NuevoButton
        '
        Me.NuevoButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NuevoButton.Location = New System.Drawing.Point(756, 12)
        Me.NuevoButton.Name = "NuevoButton"
        Me.NuevoButton.Size = New System.Drawing.Size(122, 35)
        Me.NuevoButton.TabIndex = 1
        Me.NuevoButton.Text = "&Nuevo"
        Me.NuevoButton.UseVisualStyleBackColor = True
        '
        'ModificarButton
        '
        Me.ModificarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModificarButton.Location = New System.Drawing.Point(756, 53)
        Me.ModificarButton.Name = "ModificarButton"
        Me.ModificarButton.Size = New System.Drawing.Size(122, 34)
        Me.ModificarButton.TabIndex = 2
        Me.ModificarButton.Text = "&Modificar"
        Me.ModificarButton.UseVisualStyleBackColor = True
        '
        'EliminarButton
        '
        Me.EliminarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EliminarButton.Location = New System.Drawing.Point(756, 93)
        Me.EliminarButton.Name = "EliminarButton"
        Me.EliminarButton.Size = New System.Drawing.Size(122, 34)
        Me.EliminarButton.TabIndex = 3
        Me.EliminarButton.Text = "&Eliminar"
        Me.EliminarButton.UseVisualStyleBackColor = True
        '
        'SalirButton
        '
        Me.SalirButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalirButton.Location = New System.Drawing.Point(756, 583)
        Me.SalirButton.Name = "SalirButton"
        Me.SalirButton.Size = New System.Drawing.Size(122, 34)
        Me.SalirButton.TabIndex = 4
        Me.SalirButton.Text = "&Salir"
        Me.SalirButton.UseVisualStyleBackColor = True
        '
        'TipServCombo
        '
        Me.TipServCombo.DisplayMember = "CONCEPTO"
        Me.TipServCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipServCombo.FormattingEnabled = True
        Me.TipServCombo.Location = New System.Drawing.Point(12, 62)
        Me.TipServCombo.Name = "TipServCombo"
        Me.TipServCombo.Size = New System.Drawing.Size(485, 24)
        Me.TipServCombo.TabIndex = 0
        Me.TipServCombo.ValueMember = "CLV_TIPSER"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(8, 18)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(106, 20)
        Me.CMBLabel1.TabIndex = 5
        Me.CMBLabel1.Text = "Buscar por :"
        '
        'CostosDataGrid
        '
        Me.CostosDataGrid.AllowUserToAddRows = False
        Me.CostosDataGrid.AllowUserToDeleteRows = False
        Me.CostosDataGrid.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CostosDataGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.CostosDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CostosDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.TIPSER, Me.Articulo, Me.Costo, Me.Principal, Me.Adicional, Me.idcompania, Me.ADICIONAL2, Me.ADICIONAL3})
        Me.CostosDataGrid.GridColor = System.Drawing.SystemColors.Window
        Me.CostosDataGrid.Location = New System.Drawing.Point(12, 138)
        Me.CostosDataGrid.Name = "CostosDataGrid"
        Me.CostosDataGrid.ReadOnly = True
        Me.CostosDataGrid.RowHeadersVisible = False
        Me.CostosDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.CostosDataGrid.Size = New System.Drawing.Size(738, 524)
        Me.CostosDataGrid.TabIndex = 6
        '
        'ID
        '
        Me.ID.DataPropertyName = "id"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Width = 5
        '
        'TIPSER
        '
        Me.TIPSER.DataPropertyName = "TIPSER"
        Me.TIPSER.HeaderText = "Tipo de Servicio"
        Me.TIPSER.Name = "TIPSER"
        Me.TIPSER.ReadOnly = True
        Me.TIPSER.Width = 150
        '
        'Articulo
        '
        Me.Articulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Articulo.DataPropertyName = "Articulo"
        Me.Articulo.FillWeight = 216.5303!
        Me.Articulo.HeaderText = "Paquete"
        Me.Articulo.Name = "Articulo"
        Me.Articulo.ReadOnly = True
        '
        'Costo
        '
        Me.Costo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Costo.DataPropertyName = "Costo"
        Me.Costo.FillWeight = 51.55483!
        Me.Costo.HeaderText = "Costo Artículo"
        Me.Costo.Name = "Costo"
        Me.Costo.ReadOnly = True
        Me.Costo.Visible = False
        '
        'Principal
        '
        Me.Principal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Principal.DataPropertyName = "Principal"
        Me.Principal.FillWeight = 31.91489!
        Me.Principal.HeaderText = "Renta de Aparato"
        Me.Principal.Name = "Principal"
        Me.Principal.ReadOnly = True
        Me.Principal.Visible = False
        '
        'Adicional
        '
        Me.Adicional.DataPropertyName = "Adicional"
        Me.Adicional.HeaderText = "Renta Adicional"
        Me.Adicional.Name = "Adicional"
        Me.Adicional.ReadOnly = True
        Me.Adicional.Visible = False
        Me.Adicional.Width = 5
        '
        'idcompania
        '
        Me.idcompania.DataPropertyName = "idcompania"
        Me.idcompania.HeaderText = "idcompania"
        Me.idcompania.Name = "idcompania"
        Me.idcompania.ReadOnly = True
        Me.idcompania.Visible = False
        Me.idcompania.Width = 5
        '
        'ADICIONAL2
        '
        Me.ADICIONAL2.DataPropertyName = "ADICIONAL2"
        Me.ADICIONAL2.HeaderText = "ADICIONAL2"
        Me.ADICIONAL2.Name = "ADICIONAL2"
        Me.ADICIONAL2.ReadOnly = True
        Me.ADICIONAL2.Visible = False
        Me.ADICIONAL2.Width = 5
        '
        'ADICIONAL3
        '
        Me.ADICIONAL3.DataPropertyName = "ADICIONAL3"
        Me.ADICIONAL3.HeaderText = "ADICIONAL3"
        Me.ADICIONAL3.Name = "ADICIONAL3"
        Me.ADICIONAL3.ReadOnly = True
        Me.ADICIONAL3.Visible = False
        Me.ADICIONAL3.Width = 5
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(9, 43)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(131, 16)
        Me.CMBLabel2.TabIndex = 7
        Me.CMBLabel2.Text = "Tipo de Servicio :"
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(12, 108)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(485, 24)
        Me.ComboBoxCompanias.TabIndex = 109
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 16)
        Me.Label1.TabIndex = 110
        Me.Label1.Text = "Compañía :"
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'BrwTarifasPagare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(889, 675)
        Me.Controls.Add(Me.CostosDataGrid)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxCompanias)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.TipServCombo)
        Me.Controls.Add(Me.SalirButton)
        Me.Controls.Add(Me.EliminarButton)
        Me.Controls.Add(Me.ModificarButton)
        Me.Controls.Add(Me.NuevoButton)
        Me.MaximizeBox = False
        Me.Name = "BrwTarifasPagare"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo Tarifas Pagaré"
        CType(Me.CostosDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents NuevoButton As System.Windows.Forms.Button
    Friend WithEvents ModificarButton As System.Windows.Forms.Button
    Friend WithEvents EliminarButton As System.Windows.Forms.Button
    Friend WithEvents SalirButton As System.Windows.Forms.Button
    Friend WithEvents TipServCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CostosDataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TIPSER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Articulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Costo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Principal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Adicional As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idcompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ADICIONAL2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ADICIONAL3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
