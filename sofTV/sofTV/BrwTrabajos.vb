﻿Imports sofTV.BAL
Imports System.Windows.Forms

Public Class BrwTrabajos
    Public clvTrabajo As Integer
    Public clvTipoServicio As Integer

    Private Sub BrwTrabajos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            colorea(Me, Me.Name)
            LlenaCmbTipoServicio()
            If IsNumeric(Me.cmbTipoServicio.SelectedValue) = True Then
                LlenaDgvTrabajos(Me.cmbTipoServicio.SelectedValue)
            End If

            UspDesactivaBotones(Me, Me.Name)
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub LlenaCmbTipoServicio()
        cmbTipoServicio.DisplayMember = "Concepto"
        cmbTipoServicio.ValueMember = "Clv_TipSerPrincipal"
        cmbTipoServicio.DataSource = Trabajo.MuestraTipSerPrincipal()
    End Sub

    Private Sub LlenaDgvTrabajos(ByVal Clv_TipoServicio As Integer)
        dgvTrabajos.DataSource = Trabajo.GetTrabajoByClv_TipSer(Clv_TipoServicio)
        ocultaColumnas(dgvTrabajos)
        Try
            Dim indexGrid As IndexGridTrabajos = obtenerIdexGridTrabajos()
            clvTrabajo = dgvTrabajos.SelectedCells(indexGrid.indexClave).Value
            Clv_calleLabel2.Text = dgvTrabajos.SelectedCells(indexGrid.indxCalle).Value
            CMBNombreTextBox.Text = dgvTrabajos.SelectedCells(indexGrid.indxNombre).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LlenaDgvTrabajos(ByVal Clv_TipoServicio As Integer, ByVal strTrabajo As String)
        dgvTrabajos.DataSource = Trabajo.GetTrabajoByTRABAJOClv_TipSer(strTrabajo, Clv_TipoServicio)
        ocultaColumnas(dgvTrabajos)
    End Sub

    Private Sub LlenaDgvTrabajos(ByVal Descripcion As String, ByVal Clv_TipoServicio As Integer)
        dgvTrabajos.DataSource = Trabajo.GetTrabajoByClv_TipSerDESCRIPCION(Clv_TipoServicio, Descripcion)

        ocultaColumnas(dgvTrabajos)
       

    End Sub

    Private Sub ocultaColumnas(ByRef dgv As DataGridView)
        'Dim columnas As New List(Of Integer)
        'columnas.AddRange({0, 1, 3})

        For Each dc As DataGridViewColumn In dgv.Columns
            If (dc.Name = "Clave" Or
            dc.Name = "colTrabajo" Or
                dc.Name = "colDescripcion") Then
                dc.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            Else
                dc.Visible = False
            End If
        Next
        'Dim i As Integer
        'For i = 0 To dgv.Columns.Count - 1
        '    If (Not columnas.Contains(i)) Then
        '        dgv.Columns(i).Visible = False
        '    Else
        '        dgv.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        '    End If
        'Next
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnBuscaTrabajo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaTrabajo.Click
        If IsNumeric(Me.cmbTipoServicio.SelectedValue) = True Then
            If Len(Me.txtTrabajo.Text) > 0 Then
                LlenaDgvTrabajos(Me.cmbTipoServicio.SelectedValue, Me.txtTrabajo.Text)
            Else
                MsgBox("Ingrese un Trabajo para realizar la búsqueda", MsgBoxStyle.Information)
                Me.txtTrabajo.Clear()
            End If
        End If
        Me.txtTrabajo.Clear()
    End Sub

    Private Sub btnBuscaDescripcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaDescripcion.Click
        If IsNumeric(Me.cmbTipoServicio.SelectedValue) = True Then
            If Len(Me.txtDescripcion.Text) > 0 Then
                LlenaDgvTrabajos(Me.txtDescripcion.Text, Me.cmbTipoServicio.SelectedValue)
            Else
                MsgBox("Ingrese una Descripción para realizar la búsqueda", MsgBoxStyle.Information)
                Me.txtDescripcion.Clear()
            End If
        End If
        Me.txtDescripcion.Clear()
    End Sub

    Private Sub cmbTipoServicio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoServicio.SelectedIndexChanged
        Me.txtTrabajo.Clear()
        Me.txtDescripcion.Clear()

        If IsNumeric(Me.cmbTipoServicio.SelectedValue) = True Then
            LlenaDgvTrabajos(Me.cmbTipoServicio.SelectedValue)
            clvTipoServicio = Me.cmbTipoServicio.SelectedValue
        End If
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Dim frm As New FrmTrabajos
        frm.clvTipoServicio = clvTipoServicio
        Globals.GlobalSettings.Acc = Globals.Accion.Nuevo
        frm.ShowDialog()
        If IsNumeric(Me.cmbTipoServicio.SelectedValue) = True Then
            LlenaDgvTrabajos(Me.cmbTipoServicio.SelectedValue)
        End If
    End Sub

    Private Sub dgvTrabajos_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvTrabajos.CurrentCellChanged
        Try
            Dim indexGrid As IndexGridTrabajos = obtenerIdexGridTrabajos()
            clvTrabajo = dgvTrabajos.SelectedCells(indexGrid.indexClave ).Value
            Clv_calleLabel2.Text = dgvTrabajos.SelectedCells(indexGrid.indxCalle).Value
            CMBNombreTextBox.Text = dgvTrabajos.SelectedCells(indexGrid.indxNombre).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        If IsNumeric(clvTrabajo) = False Or Len(clvTrabajo) = 0 Or clvTrabajo = 0 Then
            MsgBox("Seleccione un registro para Consultar")
            Exit Sub
        End If

        Dim frm As New FrmTrabajos
        frm.clvTrabajo = clvTrabajo
        frm.clvTipoServicio = clvTipoServicio
        Globals.GlobalSettings.Acc = Globals.Accion.Consultar
        frm.ShowDialog()
        If IsNumeric(Me.cmbTipoServicio.SelectedValue) = True Then
            LlenaDgvTrabajos(Me.cmbTipoServicio.SelectedValue)
        End If
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If IsNumeric(clvTrabajo) = False Or Len(clvTrabajo) = 0 Or clvTrabajo = 0 Then
            MsgBox("Seleccione un registro para Modificar")
            Exit Sub
        End If

        Dim frm As New FrmTrabajos
        frm.clvTrabajo = clvTrabajo
        frm.clvTipoServicio = clvTipoServicio
        Globals.GlobalSettings.Acc = Globals.Accion.Modificar
        frm.ShowDialog()
        If IsNumeric(Me.cmbTipoServicio.SelectedValue) = True Then
            LlenaDgvTrabajos(Me.cmbTipoServicio.SelectedValue)
        End If
    End Sub

    Private Function obtenerIdexGridTrabajos() As IndexGridTrabajos
        Dim resultado As New IndexGridTrabajos()
        Dim indx As Integer = 0

        For Each dc As DataGridViewColumn In dgvTrabajos.Columns
            If (dc.Name = "Clave") Then
                resultado.indexClave = indx
                indx = indx + 1
            ElseIf (dc.Name = "colTrabajo") Then
                resultado.indxCalle = indx
                indx = indx + 1
            ElseIf (dc.Name = "colDescripcion") Then
                resultado.indxNombre = indx
                indx = indx + 1
            Else
                indx = indx + 1
            End If
        Next
        Return resultado

    End Function

End Class

Public Class IndexGridTrabajos

    Public indexClave As Integer
    Public indxCalle As Integer
    Public indxNombre As Integer

End Class