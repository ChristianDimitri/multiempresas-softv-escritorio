﻿Public Class BrwVelInternet

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Public Sub obtenerlistaPoliticas()
        BaseII.limpiaParametros()
        DataGridView1.DataSource = BaseII.ConsultaDT("Sp_listadoPoliticas")
    End Sub

    Private Sub BrwVelInternet_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            obtenerlistaPoliticas()
        End If
    End Sub

    Private Sub BrwVelInternet_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        obtenerlistaPoliticas()
    End Sub


    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            lbClvEq.Text = DataGridView1.SelectedCells(1).Value
            lbSub.Text = DataGridView1.SelectedCells(2).Value
            lbBaj.Text = DataGridView1.SelectedCells(3).Value
        Catch ex As Exception

        End Try

    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        opcion = "N"
        gloClave = 0
        FrmVelInternet.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If IsNumeric(DataGridView1.SelectedCells(0).Value) Then
            opcion = "C"
            gloClave = DataGridView1.SelectedCells(0).Value
            FrmVelInternet.Show()
        Else
            MsgBox("Selecciona un elemento válido")
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If IsNumeric(DataGridView1.SelectedCells(0).Value) Then
            opcion = "M"
            gloClave = DataGridView1.SelectedCells(0).Value
            FrmVelInternet.Show()
        Else
            MsgBox("Selecciona un elemento válido")
        End If

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvEquivalente", SqlDbType.VarChar, TextBox1.Text, 20)
        DataGridView1.DataSource = BaseII.ConsultaDT("Sp_filtroPoliticas")
    End Sub
End Class