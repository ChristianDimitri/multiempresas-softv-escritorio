Imports System.Data.SqlClient
Public Class BrwPaises
    'Public KeyAscii As Short
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
    Private Sub BrwPaises_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            bec_bnd = False
            busca(3)
        End If
    End Sub

    Private Sub BrwPaises_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        busca(3)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub busca(ByVal opt As Integer)
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        Try
            If Me.TextBox1.Text = "" Then
                Me.TextBox1.Text = 0
            End If
            cone.Open()
            Me.BUSCA_PaisesTableAdapter.Connection = cone
            Me.BUSCA_PaisesTableAdapter.Fill(Me.DataSetLidia2.BUSCA_Paises, Me.TextBox1.Text, Me.TextBox3.Text, Me.TextBox2.Text, opt)
            cone.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmPaises.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            opcion = "C"
            Clv_Pais = Me.Clv_calleLabel2.Text
            FrmPaises.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            opcion = "M"
            Clv_Pais = Me.Clv_calleLabel2.Text
            FrmPaises.Show()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        busca(0)
        Me.TextBox1.Clear()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busca(1)
        Me.TextBox2.Clear()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            busca(0)
            Me.TextBox1.Clear()
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
            Me.TextBox2.Clear()
        End If
    End Sub
    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            opcion = "C"
            Clv_Pais = Me.Clv_calleLabel2.Text

        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            busca(2)
            Me.TextBox3.Clear()
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        busca(2)
        Me.TextBox3.Clear()
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub
End Class