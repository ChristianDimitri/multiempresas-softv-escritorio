﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaqueteAdicionalEntreClasificaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim PaqueteAdicionalLabel As System.Windows.Forms.Label
        Dim Clv_PaqAdiLabel As System.Windows.Forms.Label
        Dim ContratacionLabel1 As System.Windows.Forms.Label
        Dim MensualidadLabel1 As System.Windows.Forms.Label
        Dim DescripcionLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPaqueteAdicionalEntreClasificaciones))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.panelGeneralesPaqAdic = New System.Windows.Forms.Panel()
        Me.btnEliminaPaqAdic = New System.Windows.Forms.Button()
        Me.btnGuardarPaqAdic = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.txtNombrePaqAdic = New System.Windows.Forms.TextBox()
        Me.lblTipoClasificacion = New System.Windows.Forms.Label()
        Me.NumeroTextBox = New System.Windows.Forms.TextBox()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.txtIDPaqAdic = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.panelRelPlanTarifario = New System.Windows.Forms.Panel()
        Me.pbAyuda01 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtCostoMensualidad = New System.Windows.Forms.TextBox()
        Me.txtCostoContratacion = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmbxServicio = New System.Windows.Forms.ComboBox()
        Me.lblCostosPlanTarifario = New System.Windows.Forms.Label()
        Me.dgvRelServiciosPaqAdic = New System.Windows.Forms.DataGridView()
        Me.btnCancela_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnEliminar_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnGuardar_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnModifica_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnNuevo_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.lblClasificacines = New System.Windows.Forms.Label()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.BtnQuitar = New System.Windows.Forms.Button()
        Me.BtnAgregarTodas = New System.Windows.Forms.Button()
        Me.BtnQuitarTodas = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.chbxAplicaTarifa_PaqAdic = New System.Windows.Forms.CheckBox()
        PaqueteAdicionalLabel = New System.Windows.Forms.Label()
        Clv_PaqAdiLabel = New System.Windows.Forms.Label()
        ContratacionLabel1 = New System.Windows.Forms.Label()
        MensualidadLabel1 = New System.Windows.Forms.Label()
        DescripcionLabel1 = New System.Windows.Forms.Label()
        Me.panelGeneralesPaqAdic.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelRelPlanTarifario.SuspendLayout()
        CType(Me.pbAyuda01, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvRelServiciosPaqAdic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PaqueteAdicionalLabel
        '
        PaqueteAdicionalLabel.AutoSize = True
        PaqueteAdicionalLabel.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteAdicionalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PaqueteAdicionalLabel.Location = New System.Drawing.Point(13, 7)
        PaqueteAdicionalLabel.Name = "PaqueteAdicionalLabel"
        PaqueteAdicionalLabel.Size = New System.Drawing.Size(230, 16)
        PaqueteAdicionalLabel.TabIndex = 8
        PaqueteAdicionalLabel.Text = "Nombre del Paquete Adicional:"
        '
        'Clv_PaqAdiLabel
        '
        Clv_PaqAdiLabel.AutoSize = True
        Clv_PaqAdiLabel.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_PaqAdiLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_PaqAdiLabel.Location = New System.Drawing.Point(607, 676)
        Clv_PaqAdiLabel.Name = "Clv_PaqAdiLabel"
        Clv_PaqAdiLabel.Size = New System.Drawing.Size(59, 16)
        Clv_PaqAdiLabel.TabIndex = 2
        Clv_PaqAdiLabel.Text = "Clave :"
        Clv_PaqAdiLabel.Visible = False
        '
        'ContratacionLabel1
        '
        ContratacionLabel1.AutoSize = True
        ContratacionLabel1.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratacionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ContratacionLabel1.Location = New System.Drawing.Point(102, 8)
        ContratacionLabel1.Name = "ContratacionLabel1"
        ContratacionLabel1.Size = New System.Drawing.Size(107, 16)
        ContratacionLabel1.TabIndex = 25
        ContratacionLabel1.Text = "Contratación:"
        '
        'MensualidadLabel1
        '
        MensualidadLabel1.AutoSize = True
        MensualidadLabel1.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensualidadLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        MensualidadLabel1.Location = New System.Drawing.Point(352, 8)
        MensualidadLabel1.Name = "MensualidadLabel1"
        MensualidadLabel1.Size = New System.Drawing.Size(104, 16)
        MensualidadLabel1.TabIndex = 27
        MensualidadLabel1.Text = "Mensualidad:"
        '
        'DescripcionLabel1
        '
        DescripcionLabel1.AutoSize = True
        DescripcionLabel1.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel1.Location = New System.Drawing.Point(116, 8)
        DescripcionLabel1.Name = "DescripcionLabel1"
        DescripcionLabel1.Size = New System.Drawing.Size(70, 16)
        DescripcionLabel1.TabIndex = 0
        DescripcionLabel1.Text = "Servicio:"
        '
        'panelGeneralesPaqAdic
        '
        Me.panelGeneralesPaqAdic.Controls.Add(Me.chbxAplicaTarifa_PaqAdic)
        Me.panelGeneralesPaqAdic.Controls.Add(Me.btnEliminaPaqAdic)
        Me.panelGeneralesPaqAdic.Controls.Add(Me.btnGuardarPaqAdic)
        Me.panelGeneralesPaqAdic.Controls.Add(Me.Label4)
        Me.panelGeneralesPaqAdic.Controls.Add(Me.txtCosto)
        Me.panelGeneralesPaqAdic.Controls.Add(Me.txtNombrePaqAdic)
        Me.panelGeneralesPaqAdic.Controls.Add(Me.lblTipoClasificacion)
        Me.panelGeneralesPaqAdic.Controls.Add(PaqueteAdicionalLabel)
        Me.panelGeneralesPaqAdic.Controls.Add(Me.NumeroTextBox)
        Me.panelGeneralesPaqAdic.Location = New System.Drawing.Point(12, 21)
        Me.panelGeneralesPaqAdic.Name = "panelGeneralesPaqAdic"
        Me.panelGeneralesPaqAdic.Size = New System.Drawing.Size(931, 125)
        Me.panelGeneralesPaqAdic.TabIndex = 41
        '
        'btnEliminaPaqAdic
        '
        Me.btnEliminaPaqAdic.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminaPaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminaPaqAdic.Location = New System.Drawing.Point(802, 53)
        Me.btnEliminaPaqAdic.Name = "btnEliminaPaqAdic"
        Me.btnEliminaPaqAdic.Size = New System.Drawing.Size(120, 32)
        Me.btnEliminaPaqAdic.TabIndex = 51
        Me.btnEliminaPaqAdic.Text = "&Eliminar"
        Me.btnEliminaPaqAdic.UseVisualStyleBackColor = True
        '
        'btnGuardarPaqAdic
        '
        Me.btnGuardarPaqAdic.Enabled = False
        Me.btnGuardarPaqAdic.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarPaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarPaqAdic.Location = New System.Drawing.Point(802, 6)
        Me.btnGuardarPaqAdic.Name = "btnGuardarPaqAdic"
        Me.btnGuardarPaqAdic.Size = New System.Drawing.Size(120, 32)
        Me.btnGuardarPaqAdic.TabIndex = 50
        Me.btnGuardarPaqAdic.Text = "&Guardar"
        Me.btnGuardarPaqAdic.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(584, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(190, 16)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Costo Llamada Adicional:"
        '
        'txtCosto
        '
        Me.txtCosto.Enabled = False
        Me.txtCosto.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCosto.Location = New System.Drawing.Point(609, 26)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(149, 23)
        Me.txtCosto.TabIndex = 46
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNombrePaqAdic
        '
        Me.txtNombrePaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombrePaqAdic.Location = New System.Drawing.Point(16, 26)
        Me.txtNombrePaqAdic.Name = "txtNombrePaqAdic"
        Me.txtNombrePaqAdic.Size = New System.Drawing.Size(362, 23)
        Me.txtNombrePaqAdic.TabIndex = 23
        '
        'lblTipoClasificacion
        '
        Me.lblTipoClasificacion.AutoSize = True
        Me.lblTipoClasificacion.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoClasificacion.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblTipoClasificacion.Location = New System.Drawing.Point(409, 7)
        Me.lblTipoClasificacion.Name = "lblTipoClasificacion"
        Me.lblTipoClasificacion.Size = New System.Drawing.Size(55, 16)
        Me.lblTipoClasificacion.TabIndex = 20
        Me.lblTipoClasificacion.Text = "Xxxxx:"
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroTextBox.Location = New System.Drawing.Point(412, 26)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(149, 23)
        Me.NumeroTextBox.TabIndex = 11
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(836, 2)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(53, 20)
        Me.NumericUpDown1.TabIndex = 49
        Me.NumericUpDown1.Visible = False
        '
        'txtIDPaqAdic
        '
        Me.txtIDPaqAdic.BackColor = System.Drawing.Color.White
        Me.txtIDPaqAdic.Enabled = False
        Me.txtIDPaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDPaqAdic.Location = New System.Drawing.Point(610, 695)
        Me.txtIDPaqAdic.Name = "txtIDPaqAdic"
        Me.txtIDPaqAdic.ReadOnly = True
        Me.txtIDPaqAdic.Size = New System.Drawing.Size(100, 23)
        Me.txtIDPaqAdic.TabIndex = 3
        Me.txtIDPaqAdic.TabStop = False
        Me.txtIDPaqAdic.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(806, 381)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 15)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Minutos Incluidos :"
        Me.Label5.Visible = False
        '
        'lblTitulo
        '
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblTitulo.Location = New System.Drawing.Point(401, 2)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(170, 16)
        Me.lblTitulo.TabIndex = 45
        Me.lblTitulo.Text = "PAQUETE ADICIONAL:"
        '
        'panelRelPlanTarifario
        '
        Me.panelRelPlanTarifario.Controls.Add(Me.pbAyuda01)
        Me.panelRelPlanTarifario.Controls.Add(Me.Panel3)
        Me.panelRelPlanTarifario.Controls.Add(Me.Panel2)
        Me.panelRelPlanTarifario.Controls.Add(Me.lblCostosPlanTarifario)
        Me.panelRelPlanTarifario.Location = New System.Drawing.Point(44, 426)
        Me.panelRelPlanTarifario.Name = "panelRelPlanTarifario"
        Me.panelRelPlanTarifario.Size = New System.Drawing.Size(890, 105)
        Me.panelRelPlanTarifario.TabIndex = 42
        '
        'pbAyuda01
        '
        Me.pbAyuda01.Image = CType(resources.GetObject("pbAyuda01.Image"), System.Drawing.Image)
        Me.pbAyuda01.Location = New System.Drawing.Point(868, 3)
        Me.pbAyuda01.Name = "pbAyuda01"
        Me.pbAyuda01.Size = New System.Drawing.Size(22, 26)
        Me.pbAyuda01.TabIndex = 660
        Me.pbAyuda01.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtCostoMensualidad)
        Me.Panel3.Controls.Add(ContratacionLabel1)
        Me.Panel3.Controls.Add(Me.txtCostoContratacion)
        Me.Panel3.Controls.Add(MensualidadLabel1)
        Me.Panel3.Location = New System.Drawing.Point(368, 23)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(509, 68)
        Me.Panel3.TabIndex = 43
        '
        'txtCostoMensualidad
        '
        Me.txtCostoMensualidad.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostoMensualidad.Location = New System.Drawing.Point(300, 27)
        Me.txtCostoMensualidad.Name = "txtCostoMensualidad"
        Me.txtCostoMensualidad.Size = New System.Drawing.Size(177, 23)
        Me.txtCostoMensualidad.TabIndex = 41
        '
        'txtCostoContratacion
        '
        Me.txtCostoContratacion.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostoContratacion.Location = New System.Drawing.Point(66, 27)
        Me.txtCostoContratacion.Name = "txtCostoContratacion"
        Me.txtCostoContratacion.Size = New System.Drawing.Size(177, 23)
        Me.txtCostoContratacion.TabIndex = 40
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(DescripcionLabel1)
        Me.Panel2.Controls.Add(Me.cmbxServicio)
        Me.Panel2.Location = New System.Drawing.Point(16, 23)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(334, 68)
        Me.Panel2.TabIndex = 42
        '
        'cmbxServicio
        '
        Me.cmbxServicio.DisplayMember = "Descripcion"
        Me.cmbxServicio.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxServicio.FormattingEnabled = True
        Me.cmbxServicio.Location = New System.Drawing.Point(17, 27)
        Me.cmbxServicio.Name = "cmbxServicio"
        Me.cmbxServicio.Size = New System.Drawing.Size(296, 24)
        Me.cmbxServicio.TabIndex = 1
        Me.cmbxServicio.ValueMember = "Clv_Servicio"
        '
        'lblCostosPlanTarifario
        '
        Me.lblCostosPlanTarifario.AutoSize = True
        Me.lblCostosPlanTarifario.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostosPlanTarifario.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblCostosPlanTarifario.Location = New System.Drawing.Point(220, 0)
        Me.lblCostosPlanTarifario.Name = "lblCostosPlanTarifario"
        Me.lblCostosPlanTarifario.Size = New System.Drawing.Size(402, 16)
        Me.lblCostosPlanTarifario.TabIndex = 44
        Me.lblCostosPlanTarifario.Text = "COSTOS UNITARIOS PARA LOS PLANES TARIFARIOS:"
        '
        'dgvRelServiciosPaqAdic
        '
        Me.dgvRelServiciosPaqAdic.AllowUserToAddRows = False
        Me.dgvRelServiciosPaqAdic.AllowUserToDeleteRows = False
        Me.dgvRelServiciosPaqAdic.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRelServiciosPaqAdic.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRelServiciosPaqAdic.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvRelServiciosPaqAdic.Location = New System.Drawing.Point(44, 547)
        Me.dgvRelServiciosPaqAdic.Name = "dgvRelServiciosPaqAdic"
        Me.dgvRelServiciosPaqAdic.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRelServiciosPaqAdic.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvRelServiciosPaqAdic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRelServiciosPaqAdic.Size = New System.Drawing.Size(518, 184)
        Me.dgvRelServiciosPaqAdic.TabIndex = 45
        '
        'btnCancela_RelPlanTarifario
        '
        Me.btnCancela_RelPlanTarifario.Enabled = False
        Me.btnCancela_RelPlanTarifario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancela_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancela_RelPlanTarifario.Location = New System.Drawing.Point(804, 546)
        Me.btnCancela_RelPlanTarifario.Name = "btnCancela_RelPlanTarifario"
        Me.btnCancela_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnCancela_RelPlanTarifario.TabIndex = 38
        Me.btnCancela_RelPlanTarifario.Text = "&Cancelar"
        Me.btnCancela_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnEliminar_RelPlanTarifario
        '
        Me.btnEliminar_RelPlanTarifario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar_RelPlanTarifario.Location = New System.Drawing.Point(804, 584)
        Me.btnEliminar_RelPlanTarifario.Name = "btnEliminar_RelPlanTarifario"
        Me.btnEliminar_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnEliminar_RelPlanTarifario.TabIndex = 37
        Me.btnEliminar_RelPlanTarifario.Text = "&Eliminar"
        Me.btnEliminar_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnGuardar_RelPlanTarifario
        '
        Me.btnGuardar_RelPlanTarifario.Enabled = False
        Me.btnGuardar_RelPlanTarifario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar_RelPlanTarifario.Location = New System.Drawing.Point(630, 546)
        Me.btnGuardar_RelPlanTarifario.Name = "btnGuardar_RelPlanTarifario"
        Me.btnGuardar_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnGuardar_RelPlanTarifario.TabIndex = 36
        Me.btnGuardar_RelPlanTarifario.Text = "&Guardar"
        Me.btnGuardar_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnModifica_RelPlanTarifario
        '
        Me.btnModifica_RelPlanTarifario.Enabled = False
        Me.btnModifica_RelPlanTarifario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModifica_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModifica_RelPlanTarifario.Location = New System.Drawing.Point(630, 584)
        Me.btnModifica_RelPlanTarifario.Name = "btnModifica_RelPlanTarifario"
        Me.btnModifica_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnModifica_RelPlanTarifario.TabIndex = 35
        Me.btnModifica_RelPlanTarifario.Text = "&Modificar"
        Me.btnModifica_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnNuevo_RelPlanTarifario
        '
        Me.btnNuevo_RelPlanTarifario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo_RelPlanTarifario.Location = New System.Drawing.Point(631, 622)
        Me.btnNuevo_RelPlanTarifario.Name = "btnNuevo_RelPlanTarifario"
        Me.btnNuevo_RelPlanTarifario.Size = New System.Drawing.Size(120, 31)
        Me.btnNuevo_RelPlanTarifario.TabIndex = 34
        Me.btnNuevo_RelPlanTarifario.Text = "&Nuevo"
        Me.btnNuevo_RelPlanTarifario.UseVisualStyleBackColor = True
        Me.btnNuevo_RelPlanTarifario.Visible = False
        '
        'lblClasificacines
        '
        Me.lblClasificacines.AutoSize = True
        Me.lblClasificacines.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacines.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblClasificacines.Location = New System.Drawing.Point(335, 149)
        Me.lblClasificacines.Name = "lblClasificacines"
        Me.lblClasificacines.Size = New System.Drawing.Size(286, 16)
        Me.lblClasificacines.TabIndex = 46
        Me.lblClasificacines.Text = "CLASIFICACIONES A LAS QUE ALICA:"
        '
        'ListBox2
        '
        Me.ListBox2.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 18
        Me.ListBox2.Location = New System.Drawing.Point(574, 176)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(360, 220)
        Me.ListBox2.TabIndex = 49
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 18
        Me.ListBox1.Location = New System.Drawing.Point(44, 176)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(365, 220)
        Me.ListBox1.TabIndex = 48
        '
        'btnAgregar
        '
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(430, 176)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(120, 31)
        Me.btnAgregar.TabIndex = 50
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'BtnQuitar
        '
        Me.BtnQuitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnQuitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnQuitar.Location = New System.Drawing.Point(430, 227)
        Me.BtnQuitar.Name = "BtnQuitar"
        Me.BtnQuitar.Size = New System.Drawing.Size(120, 31)
        Me.BtnQuitar.TabIndex = 51
        Me.BtnQuitar.Text = "&Quitar"
        Me.BtnQuitar.UseVisualStyleBackColor = True
        '
        'BtnAgregarTodas
        '
        Me.BtnAgregarTodas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAgregarTodas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAgregarTodas.Location = New System.Drawing.Point(430, 365)
        Me.BtnAgregarTodas.Name = "BtnAgregarTodas"
        Me.BtnAgregarTodas.Size = New System.Drawing.Size(120, 31)
        Me.BtnAgregarTodas.TabIndex = 52
        Me.BtnAgregarTodas.Text = "&Agregar Todas"
        Me.BtnAgregarTodas.UseVisualStyleBackColor = True
        Me.BtnAgregarTodas.Visible = False
        '
        'BtnQuitarTodas
        '
        Me.BtnQuitarTodas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnQuitarTodas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnQuitarTodas.Location = New System.Drawing.Point(430, 277)
        Me.BtnQuitarTodas.Name = "BtnQuitarTodas"
        Me.BtnQuitarTodas.Size = New System.Drawing.Size(120, 31)
        Me.BtnQuitarTodas.TabIndex = 53
        Me.BtnQuitarTodas.Text = "&Quitar Todas"
        Me.BtnQuitarTodas.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(788, 695)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 46
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'chbxAplicaTarifa_PaqAdic
        '
        Me.chbxAplicaTarifa_PaqAdic.AutoSize = True
        Me.chbxAplicaTarifa_PaqAdic.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbxAplicaTarifa_PaqAdic.Location = New System.Drawing.Point(609, 53)
        Me.chbxAplicaTarifa_PaqAdic.Name = "chbxAplicaTarifa_PaqAdic"
        Me.chbxAplicaTarifa_PaqAdic.Size = New System.Drawing.Size(177, 18)
        Me.chbxAplicaTarifa_PaqAdic.TabIndex = 54
        Me.chbxAplicaTarifa_PaqAdic.Text = "Aplicará la tarifa preferencial"
        Me.chbxAplicaTarifa_PaqAdic.UseVisualStyleBackColor = True
        '
        'FrmPaqueteAdicionalEntreClasificaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(946, 743)
        Me.Controls.Add(Me.dgvRelServiciosPaqAdic)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me.BtnQuitarTodas)
        Me.Controls.Add(Me.BtnAgregarTodas)
        Me.Controls.Add(Me.BtnQuitar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.btnCancela_RelPlanTarifario)
        Me.Controls.Add(Clv_PaqAdiLabel)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.txtIDPaqAdic)
        Me.Controls.Add(Me.btnEliminar_RelPlanTarifario)
        Me.Controls.Add(Me.btnGuardar_RelPlanTarifario)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnModifica_RelPlanTarifario)
        Me.Controls.Add(Me.btnNuevo_RelPlanTarifario)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.lblClasificacines)
        Me.Controls.Add(Me.panelRelPlanTarifario)
        Me.Controls.Add(Me.panelGeneralesPaqAdic)
        Me.Controls.Add(Me.lblTitulo)
        Me.MaximizeBox = False
        Me.Name = "FrmPaqueteAdicionalEntreClasificaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paquetes Adicionales"
        Me.panelGeneralesPaqAdic.ResumeLayout(False)
        Me.panelGeneralesPaqAdic.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelRelPlanTarifario.ResumeLayout(False)
        Me.panelRelPlanTarifario.PerformLayout()
        CType(Me.pbAyuda01, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvRelServiciosPaqAdic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panelGeneralesPaqAdic As System.Windows.Forms.Panel
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents txtNombrePaqAdic As System.Windows.Forms.TextBox
    Friend WithEvents lblTipoClasificacion As System.Windows.Forms.Label
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents txtIDPaqAdic As System.Windows.Forms.TextBox
    Friend WithEvents panelRelPlanTarifario As System.Windows.Forms.Panel
    Friend WithEvents lblCostosPlanTarifario As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtCostoMensualidad As System.Windows.Forms.TextBox
    Friend WithEvents txtCostoContratacion As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmbxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancela_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnEliminar_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnGuardar_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnModifica_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnNuevo_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents dgvRelServiciosPaqAdic As System.Windows.Forms.DataGridView
    Friend WithEvents lblClasificacines As System.Windows.Forms.Label
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents BtnQuitar As System.Windows.Forms.Button
    Friend WithEvents BtnAgregarTodas As System.Windows.Forms.Button
    Friend WithEvents BtnQuitarTodas As System.Windows.Forms.Button
    Friend WithEvents btnEliminaPaqAdic As System.Windows.Forms.Button
    Friend WithEvents btnGuardarPaqAdic As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents pbAyuda01 As System.Windows.Forms.PictureBox
    Friend WithEvents chbxAplicaTarifa_PaqAdic As System.Windows.Forms.CheckBox
End Class
