﻿'Esta Clase express se implementó para tener acceso al Registro de Clientes Bloqueados para no cargar el Dataset viejito
Imports System.Data.SqlClient
Public Class DABloqueoDeClientes

    Public Shared Function uspConsultaClientes_Bloqueados(ByVal Clv_Orden As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim Bloqueado As Boolean = False
        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("uspConsultaClientes_Bloqueados", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Contrato", SqlDbType.BigInt).Value = Clv_Orden
            comando.Parameters.Add("@Bloqueado", SqlDbType.Bit).Value = 0

            comando.Parameters("@Bloqueado").Direction = ParameterDirection.Output

            comando.ExecuteNonQuery()
            Bloqueado = comando.Parameters("@Bloqueado").Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return Bloqueado
    End Function

End Class
