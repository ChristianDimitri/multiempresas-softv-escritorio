﻿Imports System.Data.SqlClient

Public Class FormAparatosRecibir


    Private Sub INSERTA_APARATOS_A_RECIBIR(ByVal oCLV_SESSION As Long, ByVal oCONTRATO As Long, ByVal oCLV_APARATO As Long, ByVal oTIPO_APARATO As String, ByVal oNO_SERIE As String, ByVal oCLV_ORDEN As Long, ByVal oCLAVE As Long, ByVal oRECIBI As Integer, ByVal oContratonet As Long)
        '--@CLV_SESSION BIGINT,@CONTRATO BIGINT,@CLV_APARATO BIGINT,@TIPO_APARATO VARCHAR(250),@NO_SERIE VARCHAR(250),@CLV_ORDEN BIGINT,@CLAVE BIGINT,@RECIBI BIT
        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, oCLV_SESSION)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oCONTRATO)
        BaseII.CreateMyParameter("@CLV_APARATO", SqlDbType.BigInt, oCLV_APARATO)
        BaseII.CreateMyParameter("@TIPO_APARATO", SqlDbType.VarChar, oTIPO_APARATO, 250)
        BaseII.CreateMyParameter("@NO_SERIE", SqlDbType.VarChar, oNO_SERIE, 250)
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, oCLV_ORDEN)
        BaseII.CreateMyParameter("@CLAVE", SqlDbType.BigInt, oCLAVE)
        BaseII.CreateMyParameter("@RECIBI", SqlDbType.Bit, oRECIBI)
        BaseII.CreateMyParameter("@CONTRATONET", SqlDbType.BigInt, oContratonet)
        BaseII.Inserta("SP_TBL_APARATOS_RECIBIDOS")
    End Sub


    Private Sub LLENA_APARATOS_A_RECIBIR(ByVal oContrato As Long, ByVal oOP As String)
        LabelnumAparatos.Text = "0"
        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
        BaseII.CreateMyParameter("@OP", SqlDbType.VarChar, oOP, 10)
        Me.DataGridView1.DataSource = BaseII.ConsultaDT("SP_APARATOS_POR_RECIBIR")
        If DataGridView1.RowCount > 0 Then
            LabelnumAparatos.Text = DataGridView1.RowCount.ToString
            DataGridView1.Columns.Item(0).Visible = False
            DataGridView1.Columns.Item(1).Visible = False
            DataGridView1.Columns.Item(2).Visible = False
            DataGridView1.Columns.Item(5).Visible = False
            DataGridView1.Columns.Item(6).Visible = False
            DataGridView1.Columns.Item(8).Visible = False
            DataGridView1.Columns.Item(3).Width = 150
            DataGridView1.Columns.Item(3).HeaderText = "Tipo de Aparato"
            DataGridView1.Columns.Item(4).Width = 350
            DataGridView1.Columns.Item(4).HeaderText = "No. Serie"
        End If
        'DataGridView1.Columns.Item(7).Visible = False
    End Sub


    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
        Dim resul As String = SP_VALIDA_APARATOS_RECON(eClv_Session, BndCambioServicio)
        If resul.Length > 0 Then
            MsgBox(resul)
            BndValRecon = 0
        Else
            BndValRecon = 1 'Autorizados si no nel

        End If
    End Sub

    Private Sub FormAparatosRecibir_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Guarda_Detalle()
    End Sub

    Private Sub FormAparatosRecibir_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BndValRecon = 0
        LLENA_APARATOS_A_RECIBIR(eContrato, BndCambioServicio)
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            If DataGridView1.RowCount > 0 Then
                Dim i As Integer
                For i = 0 To DataGridView1.RowCount - 1
                    DataGridView1.Item(7, i).Value = 1
                Next
            End If
        Else
            If DataGridView1.RowCount > 0 Then
                Dim i As Integer
                For i = 0 To DataGridView1.RowCount - 1
                    DataGridView1.Item(7, i).Value = 0
                Next
            End If

        End If

    End Sub

    'Private Function SP_VALIDA_APARATOS_RECON(ByVal oClv_Session As Long) As String
    '    '@Clv_Session Bigint,@BND INT,@MSJ VARCHAR(800) OUTPUT
    '    SP_VALIDA_APARATOS_RECON = ""
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim comando As New SqlCommand("SP_VALIDA_APARATOS_RECON", conexion)
    '    comando.CommandType = CommandType.StoredProcedure
    '    comando.CommandTimeout = 0

    '    Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
    '    parametro.Direction = ParameterDirection.Input
    '    parametro.Value = oClv_Session
    '    comando.Parameters.Add(parametro)

    '    Dim parametro3 As New SqlParameter("@BND", SqlDbType.Int)
    '    parametro3.Direction = ParameterDirection.Output
    '    comando.Parameters.Add(parametro3)

    '    Dim parametro4 As New SqlParameter("@MSJ", SqlDbType.VarChar, 800)
    '    parametro4.Direction = ParameterDirection.Output
    '    comando.Parameters.Add(parametro4)


    '    Try
    '        conexion.Open()
    '        comando.ExecuteNonQuery()
    '        SP_VALIDA_APARATOS_RECON = parametro4.Value
    '    Catch ex As Exception
    '        SP_VALIDA_APARATOS_RECON = 0
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try


    'End Function
    Private Sub Guarda_Detalle()
        Dim i As Integer
        For i = 0 To DataGridView1.RowCount - 1
            INSERTA_APARATOS_A_RECIBIR(eClv_Session, DataGridView1.Item(1, i).Value, DataGridView1.Item(2, i).Value, DataGridView1.Item(3, i).Value, DataGridView1.Item(4, i).Value, DataGridView1.Item(5, i).Value, DataGridView1.Item(6, i).Value, DataGridView1.Item(7, i).Value, DataGridView1.Item(8, i).Value)
        Next i
    End Sub

    Private Sub bnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnRegresar.Click
        Try

            Guarda_Detalle()

            Dim resul As String = SP_VALIDA_APARATOS_RECON(eClv_Session, BndCambioServicio)
            If resul.Length > 0 Then
                MsgBox(resul)
                BndValRecon = 0
            Else
                BndValRecon = 1 'Autorizados si no nel
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class