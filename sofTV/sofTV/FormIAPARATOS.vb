﻿Imports System.Data.SqlClient

Public Class FormIAPARATOS


    Private Sub SP_CONSULTAIAPARATOS(ByVal oClv_Orden As Long, oClave As Long)
        Try


            '@Clv_Orden Bigint=0,@Clave Bigint=0,@Contratonet bigint =0 OUTPUT,@Clv_Aparato bigint=0 OUTPUT
            Dim oContratonet As Long = 0
            Dim oClv_Aparato As Long = 0
            Dim oStatus As String = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@Contratonet", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Clv_Aparato", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Status", ParameterDirection.Output, SqlDbType.VarChar, 1)
            BaseII.ProcedimientoOutPut("SP_CONSULTAIAPARATOS")
            oContratonet = BaseII.dicoPar("@Contratonet").ToString
            oClv_Aparato = BaseII.dicoPar("@Clv_Aparato").ToString
            oStatus = BaseII.dicoPar("@Status").ToString

            If oContratonet > 0 Then
                ComboBoxPorAsignar.SelectedValue = oContratonet
            End If
            If oClv_Aparato > 0 Then
                ComboBoxAparatosDisponibles.SelectedValue = oClv_Aparato
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporAsignar(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)

            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_EstadoAparato()
        'Try
        '    '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
        '    '@Clv_Orden bigint=0,@Clave bigint=0
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, gloClv_Orden)
        '    BaseII.CreateMyParameter("@idFibra", SqlDbType.BigInt, ComboBoxPorAsignar.SelectedValue)
        '    BaseII.CreateMyParameter("@trabajo", SqlDbType.VarChar, GLOTRABAJO, 10)
        '    ComboBoxTipoAparato.DataSource = BaseII.ConsultaDT("SP_DameLosPosiblesTiposAparato")
        '    ComboBoxTipoAparato.DisplayMember = "concepto"
        '    ComboBoxTipoAparato.ValueMember = "TipoAparato"

        'Catch ex As Exception

        'End Try
    End Sub


    Private Sub Llena_AparatosporDisponibles(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            'BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, ComboBoxTipoAparato.SelectedValue, 10)
            ComboBoxAparatosDisponibles.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxAparatosDisponibles.DisplayMember = "Descripcion"
            ComboBoxAparatosDisponibles.ValueMember = "ContratoAnt"

            If ComboBoxAparatosDisponibles.Items.Count > 0 Then
                ComboBoxAparatosDisponibles.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SP_GuardaIAPARATOS(oClave As Long, oTrabajo As String, oClv_Orden As Long, oContratonet As Long, oClv_Aparato As Long, oOpcion As String, oStatus As String)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 10)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, oStatus, 1)
        BaseII.Inserta("SP_GuardaIAPARATOS")
    End Sub

    Private Sub CONCCABMBindingNavigatorSaveItem_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub FormIAPARATOS_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        Llena_AparatosporAsignar(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 0, gloClv_Orden, GloDetClave)
        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
        'Llena_EstadoAparato()     
        Me.Text = GLONOMTRABAJO

        SP_CONSULTAIAPARATOS(gloClv_Orden, GloDetClave)
        'LblEstadoAparato.Visible = False
        'CMBoxEstadoAparato.Visible = False

        If GLOTRABAJO = "CONUS" Or GLOTRABAJO = "CONUF" Or GLOTRABAJO = "CMINI" Or GLOTRABAJO = "CASAF" Or GLOTRABAJO = "CAFAS" Or GLOTRABAJO = "CAMAF" Or GLOTRABAJO = "CAFAM" Or GLOTRABAJO = "CAPAF" Then
            'LblEstadoAparato.Visible = True
            'CMBoxEstadoAparato.Visible = True
            If GLOTRABAJO = "CONUS" Or GLOTRABAJO = "CONUF" Or GLOTRABAJO = "CMINI" Or GLOTRABAJO = "CASAF" Or GLOTRABAJO = "CAFAS" Or GLOTRABAJO = "CAMAF" Or GLOTRABAJO = "CAFAM" Or GLOTRABAJO = "CAPAF" Then
                Label4.Text = "Aparato asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del aparato"
                Label1.Text = "Seleccione el aparato a instalar"
            End If


        End If

        If opcion = "M" Or opcion = "C" Then
            If FrmOrdSer.Panel6.Enabled = False Then
                ComboBoxAparatosDisponibles.Enabled = False
                ComboBoxPorAsignar.Enabled = False
                BtnAceptar.Enabled = False
                ComboBoxTipoAparato.Enabled = False
            End If
        End If


    End Sub

    Private Sub BtnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles BtnAceptar.Click
        If GLOTRABAJO = "CONUS" Or GLOTRABAJO = "CONUF" Or GLOTRABAJO = "CMINI" Or GLOTRABAJO = "CASAF" Or GLOTRABAJO = "CAFAS" Or GLOTRABAJO = "CAMAF" Or GLOTRABAJO = "CAFAM" Or GLOTRABAJO = "CAPAF" Then
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Nuevo Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        Else
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Instalar", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        End If

        SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxAparatosDisponibles.SelectedValue, opcion, "")
        Me.Close()

    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub ComboBoxTipoAparato_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxTipoAparato.SelectedValueChanged
        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
    End Sub

    Private Sub ComboBoxPorAsignar_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxPorAsignar.SelectedValueChanged
        Llena_EstadoAparato()
    End Sub
End Class