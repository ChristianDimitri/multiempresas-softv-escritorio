﻿Imports System.Data.SqlClient

Public Class FormREVCO

    Private Sub FormREVCO_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If opcion = "C" Then
                ComboBoxPorAsignar.Enabled = False
                BtnAceptar.Enabled = False
            End If
            BaseII.limpiaParametros()
            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MuestraRevco")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "clv_op"
            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
        Catch ex As Exception
            MsgBox("Contrato no válido. Vuelve a intentarlo.")
        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If ComboBoxPorAsignar.SelectedIndex = -1 Then
            MsgBox("Seleccione opcion")
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@clv_op", SqlDbType.Int, ComboBoxPorAsignar.SelectedValue)
        BaseII.Inserta("InsertaOpRevco")
        Me.Close()

    End Sub
End Class