Imports System.Data.SqlClient
Public Class FrmAccesopoUsuario

    Private Sub Permiso()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim bnd As Integer = 0
            Dim clv_usuario As Integer = 0
            Me.Dame_clvtipusuarioTableAdapter.Connection = CON
            Me.Dame_clvtipusuarioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clvtipusuario, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, clv_usuario)
            Select Case clv_usuario
                Case 1
                    If bnd_tipopago = True Then
                        bnd_tipopago = False
                        bnd = 0
                    ElseIf bnd_tipopago = False Then
                        Me.VerAcceso_ChecaTableAdapter.Connection = CON
                        Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(1, Integer)), bnd)
                    End If
                Case 40
                    Me.VerAcceso_ChecaTableAdapter.Connection = CON
                    Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(40, Integer)), bnd)
                Case 42
                    Me.VerAcceso_ChecaTableAdapter.Connection = CON
                    Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(42, Integer)), bnd)
                Case Else
                    bnd = 0
            End Select


            If bnd = 1 Then
                GloPermisoCortesia = 1
                eGloDescuento = 1
                Me.Close()
            Else
                GloPermisoCortesia = 0
                eGloDescuento = 0
                MsgBox("No Tiene Acceso ", MsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    'Private Sub Permiso()
    '    Try
    '        Dim bnd As Integer = 0
    '        Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(1, Integer)), bnd)

    '        If bnd = 1 Then
    '            GloPermisoCortesia = 1
    '            Me.Close()
    '        Else
    '            GloPermisoCortesia = 0
    '            MsgBox("No Tiene Acceso ", MsgBoxStyle.Information)
    '            Me.Close()
    '        End If
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Permiso()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        GloPermisoCortesia = 0
        Me.Close()
    End Sub

    Private Sub FrmAccesopoUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If GloOpPermiso = 0 Then
            Me.CMBClv_UsuarioLabel.Text = "Login del Administrador : "
            Me.CMBPasswordLabel.Text = "Contraseņa del Administrador :"
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Clv_UsuarioTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Clv_UsuarioTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Permiso()
        End If
    End Sub

    Private Sub Clv_UsuarioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_UsuarioTextBox.TextChanged

    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasaporteTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Permiso()
        End If
    End Sub

    Private Sub PasaporteTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasaporteTextBox.TextChanged

    End Sub


End Class