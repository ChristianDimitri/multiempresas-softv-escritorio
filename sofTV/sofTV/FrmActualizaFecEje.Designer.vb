﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmActualizaFecEje
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Fec_EjeTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnActualizaFecEje = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Fec_EjeTextBox
        '
        Me.Fec_EjeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fec_EjeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_EjeTextBox.Location = New System.Drawing.Point(107, 67)
        Me.Fec_EjeTextBox.Mask = "00/00/0000"
        Me.Fec_EjeTextBox.Name = "Fec_EjeTextBox"
        Me.Fec_EjeTextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Fec_EjeTextBox.Size = New System.Drawing.Size(107, 21)
        Me.Fec_EjeTextBox.TabIndex = 23
        Me.Fec_EjeTextBox.ValidatingType = GetType(Date)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(12, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(202, 16)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Nueva Fecha de Ejecución :"
        '
        'btnActualizaFecEje
        '
        Me.btnActualizaFecEje.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold)
        Me.btnActualizaFecEje.Location = New System.Drawing.Point(168, 126)
        Me.btnActualizaFecEje.Name = "btnActualizaFecEje"
        Me.btnActualizaFecEje.Size = New System.Drawing.Size(89, 33)
        Me.btnActualizaFecEje.TabIndex = 36
        Me.btnActualizaFecEje.Text = "Actualizar"
        Me.btnActualizaFecEje.UseVisualStyleBackColor = True
        '
        'FrmActualizaFecEje
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 181)
        Me.Controls.Add(Me.btnActualizaFecEje)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Fec_EjeTextBox)
        Me.Name = "FrmActualizaFecEje"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Actualiza Fecha Ejecución"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Fec_EjeTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnActualizaFecEje As System.Windows.Forms.Button
End Class
