﻿Public Class FrmActualizaFecEje

    Private Sub btnActualizaFecEje_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizaFecEje.Click
        If IsDate(Mid(Me.Fec_EjeTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = FormatDateTime(Mid(Me.Fec_EjeTextBox.Text, 1, 10), DateFormat.ShortDate)
            If DateValue(Fecha) >= DateValue(glofechaSol) Then

                Try
                    bitsist(GloUsuario, GloContrato, "Softv", Me.Name, "Se actualizo Fecha Ejecución de la orden:" + gloClv_Orden.ToString(), glofechaEje, Me.Fec_EjeTextBox.Text, "AG")

                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@clv_orden", SqlDbType.Int, gloClv_Orden)
                    BaseII.CreateMyParameter("@fec_Eje ", SqlDbType.DateTime, Fecha)
                    BaseII.Inserta("ActualizaFecEje")

                    glofechaEje = Fecha

                    MsgBox("Actualizado Correctamente", MsgBoxStyle.Information)
                    Me.Close()
                Catch ex As System.Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try

            Else
                MsgBox("la Fecha de Ejecución no puede ser menor a la Fecha de Solicitud", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("Formato de Fecha incorrecto", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub FrmActualizaFecEje_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'colorea(Me, Me.Name)
        Fec_EjeTextBox.Text = glofechaEje
    End Sub

End Class