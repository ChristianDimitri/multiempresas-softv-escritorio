Imports System.Data.SqlClient
Public Class FrmCAMDO

    Private opcionlocal As String = "N"

    Private Sub CONCAMDOBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Calle por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Colonia por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Ciudad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero Telef�nico por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.Validate()

        'Me.CONCAMDOBindingSource.EndEdit()
        'Me.CONCAMDOTableAdapter.Connection = CON
        'Me.CONCAMDOTableAdapter.Update(Me.NewSofTvDataSet.CONCAMDO)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLAVE", SqlDbType.BigInt, GloDetClave)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContratoord)
        BaseII.CreateMyParameter("@Clv_calle", SqlDbType.Int, Me.Clv_CalleComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Numero", SqlDbType.VarChar, NUMEROTextBox.Text, 50)
        BaseII.CreateMyParameter("@NumeroInt", SqlDbType.VarChar, NumIntTbox.Text, 50)
        BaseII.CreateMyParameter("@EntreCalles", SqlDbType.VarChar, Me.ENTRECALLESTextBox.Text, 250)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, Me.TELEFONOTextBox.Text, 50)
        BaseII.CreateMyParameter("@ClvTecnica", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_CiudadComboBox.SelectedValue)
        BaseII.Inserta("NUECAMDONoInt")

        MsgBox(mensaje5)
        bndCAMDO = True
        Me.Close()
        CON.Close()
    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCAMDOTableAdapter.Connection = CON
            Me.CONCAMDOTableAdapter.Fill(Me.NewSofTvDataSet.CONCAMDO, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asignacolonia()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = True Then
                Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                'Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaComboBox.SelectedValue)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Clv_CiudadComboBox.DataSource = BaseII.ConsultaDT("MuestraCVECOLCIUCOMPANIA")
                If opcionlocal = "N" Then Me.Clv_CiudadComboBox.Text = ""
                Me.Clv_CiudadComboBox.Enabled = True
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asiganacalle()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = True Then
                Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                'Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.Clv_CalleComboBox.SelectedValue, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_CALLE", SqlDbType.Int, Clv_CalleComboBox.SelectedValue)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Clv_ColoniaComboBox.DataSource = BaseII.ConsultaDT("DAMECOLONIA_CALLECOMPANIA")
                Me.Clv_ColoniaComboBox.Enabled = True
                If opcionlocal = "N" Then Me.Clv_ColoniaComboBox.Text = ""
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmCAMDO_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        GloBndTrabajo = True
        GloBloqueaDetalle = True
        CON.Close()
    End Sub

    Private Sub FrmCAMDO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        'Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        Clv_CalleComboBox.DataSource = BaseII.ConsultaDT("MUESTRACALLESCOMPANIAS")
        Me.Clv_ColoniaComboBox.Enabled = False
        Me.Clv_CiudadComboBox.Enabled = False
        Busca()
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            opcionlocal = "N"
            Me.CONCAMDOBindingSource.AddNew()
            Me.CONTRATOTextBox.Text = Contrato
            Me.Clv_OrdenTextBox.Text = gloClv_Orden
            Me.CLAVETextBox.Text = GloDetClave


        Else
            opcionlocal = "M"
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, Me.Clv_CalleTextBox.Text)
            Me.MuestraCVECOLCIUTableAdapter.Connection = CON
            Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaTextBox.Text)
            Busca()
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.CONCAMDOBindingNavigator.Enabled = False
            Me.Panel1.Enabled = False
        End If
        CON.Close()
        If opcionlocal = "N" Or opcionlocal = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub Clv_CalleComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleComboBox.SelectedIndexChanged
        asiganacalle()
    End Sub

    Private Sub Clv_ColoniaComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaComboBox.SelectedIndexChanged
        Me.asignacolonia()
    End Sub



    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CLAVETextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.CONCAMDOTableAdapter.Connection = CON
        Me.CONCAMDOTableAdapter.Delete(Me.CLAVETextBox.Text, Me.Clv_OrdenTextBox.Text, Me.CONTRATOTextBox.Text)
        CON.Close()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bndCAMDO = True
        Me.Close()
    End Sub

    Private Sub Clv_CiudadComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadComboBox.SelectedIndexChanged
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = True Then
            Me.Clv_CiudadComboBox.Enabled = True
        End If
    End Sub

    Private Sub Clv_CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadTextBox.TextChanged

    End Sub

    Private Sub Clv_CiudadLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


End Class