<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCamServCte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCamServCte))
        Me.gbDatosDelCliente = New System.Windows.Forms.GroupBox()
        Me.tbContratoCompania = New System.Windows.Forms.TextBox()
        Me.lbServiciosDelCliente = New System.Windows.Forms.Label()
        Me.bnBuscar = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbCiudad = New System.Windows.Forms.TextBox()
        Me.tbColonia = New System.Windows.Forms.TextBox()
        Me.tbNumero = New System.Windows.Forms.TextBox()
        Me.tbCalle = New System.Windows.Forms.TextBox()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.cbEsHotel = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbSoloInternet = New System.Windows.Forms.CheckBox()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbTipSer = New System.Windows.Forms.ComboBox()
        Me.dgvActuales = New System.Windows.Forms.DataGridView()
        Me.IDA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLV_SERVICIOA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SERVICIOA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONTRATONETA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARATOA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ULTIMOMESA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ULTIMOANIOA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAULTPAGOA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MENSUALIDADA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCUENTOCOMBOA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MENSUALIDADCONDESCUENTOA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgvPosibles = New System.Windows.Forms.DataGridView()
        Me.IDP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLV_SERVICIOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SERVICIOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MENSUALIDADP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCUENTOCOMBOB = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MENSUALIDADCONDESCUENTOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COBROP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnCambioDeServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbCambiar = New System.Windows.Forms.ToolStripButton()
        Me.gbDatosDelCliente.SuspendLayout()
        CType(Me.dgvActuales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPosibles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnCambioDeServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnCambioDeServicio.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbDatosDelCliente
        '
        Me.gbDatosDelCliente.Controls.Add(Me.tbContratoCompania)
        Me.gbDatosDelCliente.Controls.Add(Me.lbServiciosDelCliente)
        Me.gbDatosDelCliente.Controls.Add(Me.bnBuscar)
        Me.gbDatosDelCliente.Controls.Add(Me.Label10)
        Me.gbDatosDelCliente.Controls.Add(Me.tbCiudad)
        Me.gbDatosDelCliente.Controls.Add(Me.tbColonia)
        Me.gbDatosDelCliente.Controls.Add(Me.tbNumero)
        Me.gbDatosDelCliente.Controls.Add(Me.tbCalle)
        Me.gbDatosDelCliente.Controls.Add(Me.tbNombre)
        Me.gbDatosDelCliente.Controls.Add(Me.cbEsHotel)
        Me.gbDatosDelCliente.Controls.Add(Me.Label1)
        Me.gbDatosDelCliente.Controls.Add(Me.cbSoloInternet)
        Me.gbDatosDelCliente.Controls.Add(Me.tbContrato)
        Me.gbDatosDelCliente.Controls.Add(Me.Label6)
        Me.gbDatosDelCliente.Controls.Add(Me.Label5)
        Me.gbDatosDelCliente.Controls.Add(Me.Label4)
        Me.gbDatosDelCliente.Controls.Add(Me.Label3)
        Me.gbDatosDelCliente.Controls.Add(Me.Label2)
        Me.gbDatosDelCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDatosDelCliente.Location = New System.Drawing.Point(94, 40)
        Me.gbDatosDelCliente.Name = "gbDatosDelCliente"
        Me.gbDatosDelCliente.Size = New System.Drawing.Size(832, 234)
        Me.gbDatosDelCliente.TabIndex = 0
        Me.gbDatosDelCliente.TabStop = False
        Me.gbDatosDelCliente.Text = "Datos del Cliente"
        '
        'tbContratoCompania
        '
        Me.tbContratoCompania.Location = New System.Drawing.Point(165, 30)
        Me.tbContratoCompania.Name = "tbContratoCompania"
        Me.tbContratoCompania.Size = New System.Drawing.Size(100, 21)
        Me.tbContratoCompania.TabIndex = 15
        '
        'lbServiciosDelCliente
        '
        Me.lbServiciosDelCliente.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lbServiciosDelCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbServiciosDelCliente.ForeColor = System.Drawing.Color.Black
        Me.lbServiciosDelCliente.Location = New System.Drawing.Point(162, 190)
        Me.lbServiciosDelCliente.Name = "lbServiciosDelCliente"
        Me.lbServiciosDelCliente.Size = New System.Drawing.Size(632, 20)
        Me.lbServiciosDelCliente.TabIndex = 13
        Me.lbServiciosDelCliente.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'bnBuscar
        '
        Me.bnBuscar.Location = New System.Drawing.Point(271, 29)
        Me.bnBuscar.Name = "bnBuscar"
        Me.bnBuscar.Size = New System.Drawing.Size(37, 23)
        Me.bnBuscar.TabIndex = 1
        Me.bnBuscar.Text = "..."
        Me.bnBuscar.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(20, 195)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(142, 15)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Servicios del Cliente:"
        '
        'tbCiudad
        '
        Me.tbCiudad.BackColor = System.Drawing.Color.White
        Me.tbCiudad.Location = New System.Drawing.Point(165, 166)
        Me.tbCiudad.Name = "tbCiudad"
        Me.tbCiudad.ReadOnly = True
        Me.tbCiudad.Size = New System.Drawing.Size(314, 21)
        Me.tbCiudad.TabIndex = 12
        '
        'tbColonia
        '
        Me.tbColonia.BackColor = System.Drawing.Color.White
        Me.tbColonia.Location = New System.Drawing.Point(165, 139)
        Me.tbColonia.Name = "tbColonia"
        Me.tbColonia.ReadOnly = True
        Me.tbColonia.Size = New System.Drawing.Size(314, 21)
        Me.tbColonia.TabIndex = 11
        '
        'tbNumero
        '
        Me.tbNumero.BackColor = System.Drawing.Color.White
        Me.tbNumero.Location = New System.Drawing.Point(165, 112)
        Me.tbNumero.Name = "tbNumero"
        Me.tbNumero.ReadOnly = True
        Me.tbNumero.Size = New System.Drawing.Size(100, 21)
        Me.tbNumero.TabIndex = 10
        '
        'tbCalle
        '
        Me.tbCalle.BackColor = System.Drawing.Color.White
        Me.tbCalle.Location = New System.Drawing.Point(165, 85)
        Me.tbCalle.Name = "tbCalle"
        Me.tbCalle.ReadOnly = True
        Me.tbCalle.Size = New System.Drawing.Size(314, 21)
        Me.tbCalle.TabIndex = 9
        '
        'tbNombre
        '
        Me.tbNombre.BackColor = System.Drawing.Color.White
        Me.tbNombre.Location = New System.Drawing.Point(165, 58)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.ReadOnly = True
        Me.tbNombre.Size = New System.Drawing.Size(314, 21)
        Me.tbNombre.TabIndex = 8
        '
        'cbEsHotel
        '
        Me.cbEsHotel.AutoSize = True
        Me.cbEsHotel.Location = New System.Drawing.Point(611, 58)
        Me.cbEsHotel.Name = "cbEsHotel"
        Me.cbEsHotel.Size = New System.Drawing.Size(80, 19)
        Me.cbEsHotel.TabIndex = 7
        Me.cbEsHotel.Text = "Es Hotel"
        Me.cbEsHotel.UseVisualStyleBackColor = True
        Me.cbEsHotel.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(97, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Contrato:"
        '
        'cbSoloInternet
        '
        Me.cbSoloInternet.AutoSize = True
        Me.cbSoloInternet.Location = New System.Drawing.Point(611, 33)
        Me.cbSoloInternet.Name = "cbSoloInternet"
        Me.cbSoloInternet.Size = New System.Drawing.Size(108, 19)
        Me.cbSoloInternet.TabIndex = 6
        Me.cbSoloInternet.Text = "Sólo Internet"
        Me.cbSoloInternet.UseVisualStyleBackColor = True
        Me.cbSoloInternet.Visible = False
        '
        'tbContrato
        '
        Me.tbContrato.Location = New System.Drawing.Point(165, 31)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(100, 21)
        Me.tbContrato.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(106, 172)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 15)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Ciudad:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(100, 118)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 15)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Número:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(102, 145)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Colonia:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(118, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 15)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Calle:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(100, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nombre:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 303)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(114, 15)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Tipo de Servicio:"
        '
        'cbTipSer
        '
        Me.cbTipSer.DisplayMember = "Concepto"
        Me.cbTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipSer.FormattingEnabled = True
        Me.cbTipSer.Location = New System.Drawing.Point(15, 321)
        Me.cbTipSer.Name = "cbTipSer"
        Me.cbTipSer.Size = New System.Drawing.Size(269, 23)
        Me.cbTipSer.TabIndex = 2
        Me.cbTipSer.ValueMember = "Clv_TipSer"
        '
        'dgvActuales
        '
        Me.dgvActuales.AllowUserToAddRows = False
        Me.dgvActuales.AllowUserToDeleteRows = False
        Me.dgvActuales.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvActuales.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvActuales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvActuales.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDA, Me.CLV_SERVICIOA, Me.SERVICIOA, Me.CONTRATONETA, Me.APARATOA, Me.ULTIMOMESA, Me.ULTIMOANIOA, Me.FECHAULTPAGOA, Me.MENSUALIDADA, Me.DESCUENTOCOMBOA, Me.MENSUALIDADCONDESCUENTOA})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvActuales.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvActuales.Location = New System.Drawing.Point(12, 377)
        Me.dgvActuales.Name = "dgvActuales"
        Me.dgvActuales.ReadOnly = True
        Me.dgvActuales.RowHeadersVisible = False
        Me.dgvActuales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvActuales.Size = New System.Drawing.Size(536, 252)
        Me.dgvActuales.TabIndex = 3
        '
        'IDA
        '
        Me.IDA.DataPropertyName = "ID"
        Me.IDA.HeaderText = "IDA"
        Me.IDA.Name = "IDA"
        Me.IDA.ReadOnly = True
        Me.IDA.Visible = False
        '
        'CLV_SERVICIOA
        '
        Me.CLV_SERVICIOA.DataPropertyName = "CLV_SERVICIO"
        Me.CLV_SERVICIOA.HeaderText = "CLV_SERVICIOA"
        Me.CLV_SERVICIOA.Name = "CLV_SERVICIOA"
        Me.CLV_SERVICIOA.ReadOnly = True
        Me.CLV_SERVICIOA.Visible = False
        '
        'SERVICIOA
        '
        Me.SERVICIOA.DataPropertyName = "SERVICIO"
        Me.SERVICIOA.HeaderText = "Servicio"
        Me.SERVICIOA.Name = "SERVICIOA"
        Me.SERVICIOA.ReadOnly = True
        Me.SERVICIOA.Width = 130
        '
        'CONTRATONETA
        '
        Me.CONTRATONETA.DataPropertyName = "CONTRATONET"
        Me.CONTRATONETA.HeaderText = "CONTRATONETA"
        Me.CONTRATONETA.Name = "CONTRATONETA"
        Me.CONTRATONETA.ReadOnly = True
        Me.CONTRATONETA.Visible = False
        '
        'APARATOA
        '
        Me.APARATOA.DataPropertyName = "APARATO"
        Me.APARATOA.HeaderText = "Aparato"
        Me.APARATOA.Name = "APARATOA"
        Me.APARATOA.ReadOnly = True
        Me.APARATOA.Width = 90
        '
        'ULTIMOMESA
        '
        Me.ULTIMOMESA.DataPropertyName = "ULTIMOMES"
        Me.ULTIMOMESA.HeaderText = "ULTIMOMESA"
        Me.ULTIMOMESA.Name = "ULTIMOMESA"
        Me.ULTIMOMESA.ReadOnly = True
        Me.ULTIMOMESA.Visible = False
        '
        'ULTIMOANIOA
        '
        Me.ULTIMOANIOA.DataPropertyName = "ULTIMOANIO"
        Me.ULTIMOANIOA.HeaderText = "ULTIMOANIOA"
        Me.ULTIMOANIOA.Name = "ULTIMOANIOA"
        Me.ULTIMOANIOA.ReadOnly = True
        Me.ULTIMOANIOA.Visible = False
        '
        'FECHAULTPAGOA
        '
        Me.FECHAULTPAGOA.DataPropertyName = "FECHAULTPAGO"
        Me.FECHAULTPAGOA.HeaderText = "Fecha Ult Pago"
        Me.FECHAULTPAGOA.Name = "FECHAULTPAGOA"
        Me.FECHAULTPAGOA.ReadOnly = True
        Me.FECHAULTPAGOA.Width = 80
        '
        'MENSUALIDADA
        '
        Me.MENSUALIDADA.DataPropertyName = "MENSUALIDAD"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.MENSUALIDADA.DefaultCellStyle = DataGridViewCellStyle2
        Me.MENSUALIDADA.HeaderText = "Mensualidad sin Descuento"
        Me.MENSUALIDADA.Name = "MENSUALIDADA"
        Me.MENSUALIDADA.ReadOnly = True
        Me.MENSUALIDADA.Width = 75
        '
        'DESCUENTOCOMBOA
        '
        Me.DESCUENTOCOMBOA.DataPropertyName = "DESCUENTOCOMBO"
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DESCUENTOCOMBOA.DefaultCellStyle = DataGridViewCellStyle3
        Me.DESCUENTOCOMBOA.HeaderText = "Descuento Combo"
        Me.DESCUENTOCOMBOA.Name = "DESCUENTOCOMBOA"
        Me.DESCUENTOCOMBOA.ReadOnly = True
        Me.DESCUENTOCOMBOA.Width = 75
        '
        'MENSUALIDADCONDESCUENTOA
        '
        Me.MENSUALIDADCONDESCUENTOA.DataPropertyName = "MENSUALIDADCONDESCUENTO"
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.MENSUALIDADCONDESCUENTOA.DefaultCellStyle = DataGridViewCellStyle4
        Me.MENSUALIDADCONDESCUENTOA.HeaderText = "Mensualidad con Descuento"
        Me.MENSUALIDADCONDESCUENTOA.Name = "MENSUALIDADCONDESCUENTOA"
        Me.MENSUALIDADCONDESCUENTOA.ReadOnly = True
        Me.MENSUALIDADCONDESCUENTOA.Width = 75
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(9, 359)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(196, 15)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Servicios Actuales del Cliente"
        '
        'dgvPosibles
        '
        Me.dgvPosibles.AllowUserToAddRows = False
        Me.dgvPosibles.AllowUserToDeleteRows = False
        Me.dgvPosibles.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPosibles.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvPosibles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPosibles.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDP, Me.CLV_SERVICIOP, Me.SERVICIOP, Me.MENSUALIDADP, Me.DESCUENTOCOMBOB, Me.MENSUALIDADCONDESCUENTOP, Me.COBROP})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPosibles.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvPosibles.Location = New System.Drawing.Point(554, 377)
        Me.dgvPosibles.Name = "dgvPosibles"
        Me.dgvPosibles.ReadOnly = True
        Me.dgvPosibles.RowHeadersVisible = False
        Me.dgvPosibles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPosibles.Size = New System.Drawing.Size(442, 252)
        Me.dgvPosibles.TabIndex = 5
        '
        'IDP
        '
        Me.IDP.DataPropertyName = "ID"
        Me.IDP.HeaderText = "IDP"
        Me.IDP.Name = "IDP"
        Me.IDP.ReadOnly = True
        Me.IDP.Visible = False
        '
        'CLV_SERVICIOP
        '
        Me.CLV_SERVICIOP.DataPropertyName = "CLV_SERVICIO"
        Me.CLV_SERVICIOP.HeaderText = "CLV_SERVICIOP"
        Me.CLV_SERVICIOP.Name = "CLV_SERVICIOP"
        Me.CLV_SERVICIOP.ReadOnly = True
        Me.CLV_SERVICIOP.Visible = False
        '
        'SERVICIOP
        '
        Me.SERVICIOP.DataPropertyName = "SERVICIO"
        Me.SERVICIOP.HeaderText = "Servicio"
        Me.SERVICIOP.Name = "SERVICIOP"
        Me.SERVICIOP.ReadOnly = True
        Me.SERVICIOP.Width = 130
        '
        'MENSUALIDADP
        '
        Me.MENSUALIDADP.DataPropertyName = "MENSUALIDAD"
        DataGridViewCellStyle7.Format = "C2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.MENSUALIDADP.DefaultCellStyle = DataGridViewCellStyle7
        Me.MENSUALIDADP.HeaderText = "Mensualidad sin Descuento"
        Me.MENSUALIDADP.Name = "MENSUALIDADP"
        Me.MENSUALIDADP.ReadOnly = True
        Me.MENSUALIDADP.Width = 75
        '
        'DESCUENTOCOMBOB
        '
        Me.DESCUENTOCOMBOB.DataPropertyName = "DESCUENTOCOMBO"
        DataGridViewCellStyle8.Format = "C2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.DESCUENTOCOMBOB.DefaultCellStyle = DataGridViewCellStyle8
        Me.DESCUENTOCOMBOB.HeaderText = "Descuento Combo"
        Me.DESCUENTOCOMBOB.Name = "DESCUENTOCOMBOB"
        Me.DESCUENTOCOMBOB.ReadOnly = True
        Me.DESCUENTOCOMBOB.Width = 70
        '
        'MENSUALIDADCONDESCUENTOP
        '
        Me.MENSUALIDADCONDESCUENTOP.DataPropertyName = "MENSUALIDADCONDESCUENTO"
        DataGridViewCellStyle9.Format = "C2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.MENSUALIDADCONDESCUENTOP.DefaultCellStyle = DataGridViewCellStyle9
        Me.MENSUALIDADCONDESCUENTOP.HeaderText = "Mensualidad con Descuento"
        Me.MENSUALIDADCONDESCUENTOP.Name = "MENSUALIDADCONDESCUENTOP"
        Me.MENSUALIDADCONDESCUENTOP.ReadOnly = True
        Me.MENSUALIDADCONDESCUENTOP.Width = 75
        '
        'COBROP
        '
        Me.COBROP.DataPropertyName = "COBRO"
        DataGridViewCellStyle10.Format = "C2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.COBROP.DefaultCellStyle = DataGridViewCellStyle10
        Me.COBROP.HeaderText = "Cobro"
        Me.COBROP.Name = "COBROP"
        Me.COBROP.ReadOnly = True
        Me.COBROP.Width = 70
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(551, 359)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(194, 15)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Servicios Posibles a Cambiar"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(860, 682)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 13
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnCambioDeServicio
        '
        Me.bnCambioDeServicio.AddNewItem = Nothing
        Me.bnCambioDeServicio.CountItem = Nothing
        Me.bnCambioDeServicio.DeleteItem = Nothing
        Me.bnCambioDeServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnCambioDeServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbCambiar})
        Me.bnCambioDeServicio.Location = New System.Drawing.Point(0, 0)
        Me.bnCambioDeServicio.MoveFirstItem = Nothing
        Me.bnCambioDeServicio.MoveLastItem = Nothing
        Me.bnCambioDeServicio.MoveNextItem = Nothing
        Me.bnCambioDeServicio.MovePreviousItem = Nothing
        Me.bnCambioDeServicio.Name = "bnCambioDeServicio"
        Me.bnCambioDeServicio.PositionItem = Nothing
        Me.bnCambioDeServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnCambioDeServicio.Size = New System.Drawing.Size(1008, 25)
        Me.bnCambioDeServicio.TabIndex = 14
        Me.bnCambioDeServicio.Text = "BindingNavigator1"
        '
        'tsbCambiar
        '
        Me.tsbCambiar.Image = CType(resources.GetObject("tsbCambiar.Image"), System.Drawing.Image)
        Me.tsbCambiar.Name = "tsbCambiar"
        Me.tsbCambiar.Size = New System.Drawing.Size(86, 22)
        Me.tsbCambiar.Text = "&CAMBIAR"
        '
        'FrmCamServCte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.bnCambioDeServicio)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.dgvPosibles)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.dgvActuales)
        Me.Controls.Add(Me.cbTipSer)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.gbDatosDelCliente)
        Me.Name = "FrmCamServCte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Servicios"
        Me.gbDatosDelCliente.ResumeLayout(False)
        Me.gbDatosDelCliente.PerformLayout()
        CType(Me.dgvActuales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPosibles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnCambioDeServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnCambioDeServicio.ResumeLayout(False)
        Me.bnCambioDeServicio.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbDatosDelCliente As System.Windows.Forms.GroupBox
    Friend WithEvents bnBuscar As System.Windows.Forms.Button
    Friend WithEvents tbCiudad As System.Windows.Forms.TextBox
    Friend WithEvents tbColonia As System.Windows.Forms.TextBox
    Friend WithEvents tbNumero As System.Windows.Forms.TextBox
    Friend WithEvents tbCalle As System.Windows.Forms.TextBox
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents cbEsHotel As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents dgvActuales As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dgvPosibles As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnCambioDeServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbCambiar As System.Windows.Forms.ToolStripButton
    Friend WithEvents lbServiciosDelCliente As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents IDA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLV_SERVICIOA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SERVICIOA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONTRATONETA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARATOA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ULTIMOMESA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ULTIMOANIOA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAULTPAGOA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MENSUALIDADA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCUENTOCOMBOA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MENSUALIDADCONDESCUENTOA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLV_SERVICIOP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SERVICIOP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MENSUALIDADP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCUENTOCOMBOB As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MENSUALIDADCONDESCUENTOP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COBROP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbContratoCompania As System.Windows.Forms.TextBox
End Class
