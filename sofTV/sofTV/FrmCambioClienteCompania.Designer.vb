﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambioClienteCompania
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CONTRATOLabel = New System.Windows.Forms.Label()
        Me.NOMBRELabel = New System.Windows.Forms.Label()
        Me.CALLELabel = New System.Windows.Forms.Label()
        Me.COLONIALabel = New System.Windows.Forms.Label()
        Me.NUMEROLabel = New System.Windows.Forms.Label()
        Me.CIUDADLabel = New System.Windows.Forms.Label()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.MuestraServCteResetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraServCteResetTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraServCteResetTableAdapter()
        Me.ResetServCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ResetServCteTableAdapter = New sofTV.DataSetEricTableAdapters.ResetServCteTableAdapter()
        Me.DameClientesActivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClientesActivosTableAdapter = New sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.CALLETextBox = New System.Windows.Forms.TextBox()
        Me.COLONIATextBox = New System.Windows.Forms.TextBox()
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.CIUDADTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxCelular = New System.Windows.Forms.TextBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbContratoCompania = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ButtonAceptar = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.DameSerDELCliTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter()
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ComboBoxCompaniasActual = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxCompaniasPosibles = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServCteResetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResetServCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        Me.CONTRATOLabel.AutoSize = True
        Me.CONTRATOLabel.Location = New System.Drawing.Point(11, 36)
        Me.CONTRATOLabel.Name = "CONTRATOLabel"
        Me.CONTRATOLabel.Size = New System.Drawing.Size(74, 16)
        Me.CONTRATOLabel.TabIndex = 3
        Me.CONTRATOLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        Me.NOMBRELabel.AutoSize = True
        Me.NOMBRELabel.Location = New System.Drawing.Point(11, 62)
        Me.NOMBRELabel.Name = "NOMBRELabel"
        Me.NOMBRELabel.Size = New System.Drawing.Size(71, 16)
        Me.NOMBRELabel.TabIndex = 5
        Me.NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        Me.CALLELabel.AutoSize = True
        Me.CALLELabel.Location = New System.Drawing.Point(11, 88)
        Me.CALLELabel.Name = "CALLELabel"
        Me.CALLELabel.Size = New System.Drawing.Size(52, 16)
        Me.CALLELabel.TabIndex = 7
        Me.CALLELabel.Text = "Calle :"
        '
        'COLONIALabel
        '
        Me.COLONIALabel.AutoSize = True
        Me.COLONIALabel.Location = New System.Drawing.Point(11, 114)
        Me.COLONIALabel.Name = "COLONIALabel"
        Me.COLONIALabel.Size = New System.Drawing.Size(69, 16)
        Me.COLONIALabel.TabIndex = 9
        Me.COLONIALabel.Text = "Colonia :"
        '
        'NUMEROLabel
        '
        Me.NUMEROLabel.AutoSize = True
        Me.NUMEROLabel.Location = New System.Drawing.Point(11, 166)
        Me.NUMEROLabel.Name = "NUMEROLabel"
        Me.NUMEROLabel.Size = New System.Drawing.Size(24, 16)
        Me.NUMEROLabel.TabIndex = 11
        Me.NUMEROLabel.Text = "# :"
        '
        'CIUDADLabel
        '
        Me.CIUDADLabel.AutoSize = True
        Me.CIUDADLabel.Location = New System.Drawing.Point(11, 142)
        Me.CIUDADLabel.Name = "CIUDADLabel"
        Me.CIUDADLabel.Size = New System.Drawing.Size(65, 16)
        Me.CIUDADLabel.TabIndex = 13
        Me.CIUDADLabel.Text = "Ciudad :"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraServCteResetBindingSource
        '
        Me.MuestraServCteResetBindingSource.DataMember = "MuestraServCteReset"
        Me.MuestraServCteResetBindingSource.DataSource = Me.DataSetEric
        '
        'MuestraServCteResetTableAdapter
        '
        Me.MuestraServCteResetTableAdapter.ClearBeforeFill = True
        '
        'ResetServCteBindingSource
        '
        Me.ResetServCteBindingSource.DataMember = "ResetServCte"
        Me.ResetServCteBindingSource.DataSource = Me.DataSetEric
        '
        'ResetServCteTableAdapter
        '
        Me.ResetServCteTableAdapter.ClearBeforeFill = True
        '
        'DameClientesActivosBindingSource
        '
        Me.DameClientesActivosBindingSource.DataMember = "DameClientesActivos"
        Me.DameClientesActivosBindingSource.DataSource = Me.DataSetEric
        '
        'DameClientesActivosTableAdapter
        '
        Me.DameClientesActivosTableAdapter.ClearBeforeFill = True
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(111, 59)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.ReadOnly = True
        Me.NOMBRETextBox.Size = New System.Drawing.Size(427, 22)
        Me.NOMBRETextBox.TabIndex = 6
        Me.NOMBRETextBox.TabStop = False
        '
        'CALLETextBox
        '
        Me.CALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.CALLETextBox.Location = New System.Drawing.Point(111, 85)
        Me.CALLETextBox.Name = "CALLETextBox"
        Me.CALLETextBox.ReadOnly = True
        Me.CALLETextBox.Size = New System.Drawing.Size(257, 22)
        Me.CALLETextBox.TabIndex = 8
        Me.CALLETextBox.TabStop = False
        '
        'COLONIATextBox
        '
        Me.COLONIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "COLONIA", True))
        Me.COLONIATextBox.Location = New System.Drawing.Point(111, 111)
        Me.COLONIATextBox.Name = "COLONIATextBox"
        Me.COLONIATextBox.ReadOnly = True
        Me.COLONIATextBox.Size = New System.Drawing.Size(257, 22)
        Me.COLONIATextBox.TabIndex = 10
        Me.COLONIATextBox.TabStop = False
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Location = New System.Drawing.Point(111, 163)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.ReadOnly = True
        Me.NUMEROTextBox.Size = New System.Drawing.Size(104, 22)
        Me.NUMEROTextBox.TabIndex = 12
        Me.NUMEROTextBox.TabStop = False
        '
        'CIUDADTextBox
        '
        Me.CIUDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CIUDAD", True))
        Me.CIUDADTextBox.Location = New System.Drawing.Point(111, 139)
        Me.CIUDADTextBox.Name = "CIUDADTextBox"
        Me.CIUDADTextBox.ReadOnly = True
        Me.CIUDADTextBox.Size = New System.Drawing.Size(257, 22)
        Me.CIUDADTextBox.TabIndex = 14
        Me.CIUDADTextBox.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(111, 30)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 22)
        Me.TextBox1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBoxCelular)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.tbContratoCompania)
        Me.GroupBox1.Controls.Add(Me.NOMBRETextBox)
        Me.GroupBox1.Controls.Add(Me.CIUDADTextBox)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel)
        Me.GroupBox1.Controls.Add(Me.NUMEROTextBox)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.COLONIATextBox)
        Me.GroupBox1.Controls.Add(Me.CONTRATOLabel)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel)
        Me.GroupBox1.Controls.Add(Me.CALLETextBox)
        Me.GroupBox1.Controls.Add(Me.CALLELabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(28, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(886, 238)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(594, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 18)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Servicios:"
        '
        'TextBoxCelular
        '
        Me.TextBoxCelular.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NUMERO", True))
        Me.TextBoxCelular.Location = New System.Drawing.Point(111, 191)
        Me.TextBoxCelular.Name = "TextBoxCelular"
        Me.TextBoxCelular.ReadOnly = True
        Me.TextBoxCelular.Size = New System.Drawing.Size(104, 22)
        Me.TextBoxCelular.TabIndex = 17
        Me.TextBoxCelular.TabStop = False
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(597, 36)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(258, 183)
        Me.TreeView1.TabIndex = 20
        Me.TreeView1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 194)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Cel :"
        '
        'tbContratoCompania
        '
        Me.tbContratoCompania.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContratoCompania.Location = New System.Drawing.Point(111, 30)
        Me.tbContratoCompania.Name = "tbContratoCompania"
        Me.tbContratoCompania.Size = New System.Drawing.Size(100, 22)
        Me.tbContratoCompania.TabIndex = 15
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(225, 29)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(44, 23)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Enabled = False
        Me.ButtonAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAceptar.Location = New System.Drawing.Point(747, 286)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(136, 36)
        Me.ButtonAceptar.TabIndex = 2
        Me.ButtonAceptar.Text = "CAMBIAR"
        Me.ButtonAceptar.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(747, 348)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.EnforceConstraints = False
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(32, 30)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 15)
        Me.Label13.TabIndex = 95
        Me.Label13.Text = "Actual:"
        '
        'ComboBoxCompaniasActual
        '
        Me.ComboBoxCompaniasActual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompaniasActual.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompaniasActual.FormattingEnabled = True
        Me.ComboBoxCompaniasActual.Location = New System.Drawing.Point(40, 57)
        Me.ComboBoxCompaniasActual.Name = "ComboBoxCompaniasActual"
        Me.ComboBoxCompaniasActual.Size = New System.Drawing.Size(265, 23)
        Me.ComboBoxCompaniasActual.TabIndex = 94
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(335, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 15)
        Me.Label2.TabIndex = 97
        Me.Label2.Text = "Nueva:"
        '
        'ComboBoxCompaniasPosibles
        '
        Me.ComboBoxCompaniasPosibles.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompaniasPosibles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompaniasPosibles.FormattingEnabled = True
        Me.ComboBoxCompaniasPosibles.Location = New System.Drawing.Point(343, 57)
        Me.ComboBoxCompaniasPosibles.Name = "ComboBoxCompaniasPosibles"
        Me.ComboBoxCompaniasPosibles.Size = New System.Drawing.Size(271, 23)
        Me.ComboBoxCompaniasPosibles.TabIndex = 96
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.ComboBoxCompaniasActual)
        Me.GroupBox2.Controls.Add(Me.ComboBoxCompaniasPosibles)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(28, 273)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(651, 129)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cambio de Empresa"
        '
        'FrmCambioClienteCompania
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(926, 425)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmCambioClienteCompania"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Empresa del Cliente"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServCteResetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResetServCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents MuestraServCteResetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServCteResetTableAdapter As sofTV.DataSetEricTableAdapters.MuestraServCteResetTableAdapter
    Friend WithEvents ResetServCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ResetServCteTableAdapter As sofTV.DataSetEricTableAdapters.ResetServCteTableAdapter
    Friend WithEvents DameClientesActivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClientesActivosTableAdapter As sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents tbContratoCompania As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CONTRATOLabel As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel As System.Windows.Forms.Label
    Friend WithEvents CALLELabel As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBoxCelular As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompaniasActual As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompaniasPosibles As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
End Class
