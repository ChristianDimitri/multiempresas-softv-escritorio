﻿Imports System.Data.SqlClient
Public Class FrmCambioClienteCompania

    Dim Clv_TipSer As Integer
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Private TipSer As String = Nothing
    Private Mac As String = Nothing
    Private yaesta As Integer = 0

    Private Sub GuardaDatosBitacora()
        Try

            bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Text, "Se envio un sms", "", "", LocClv_Ciudad)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        BrwSelContratoCambioPlaza.Show()
    End Sub

    Private Sub FrmReset_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            'Me.TextBox1.Text = eContrato
            tbContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
            'CON.Open()
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            'CON.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, eContrato)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            Dim dTable As DataTable = BaseII.ConsultaDT("DameDatosClienteSms")
            If dTable.Rows.Count > 0 Then
                Me.TextBox1.Text = dTable.Rows(0)(0).ToString
                'Me.tbContratoCompania.Text = dTable.Rows(0)(1).ToString
                Me.NOMBRETextBox.Text = dTable.Rows(0)(2).ToString
                Me.CALLETextBox.Text = dTable.Rows(0)(3).ToString
                Me.NUMEROTextBox.Text = dTable.Rows(0)(5).ToString
                Me.COLONIATextBox.Text = dTable.Rows(0)(4).ToString
                Me.CIUDADTextBox.Text = dTable.Rows(0)(6).ToString
                Me.TextBoxCelular.Text = dTable.Rows(0)(9).ToString
                ButtonAceptar.Enabled = True
            End If
            If yaesta = 0 Then
                BuscarCliente()
            End If
            yaesta = 0

        End If

    End Sub

    Private Sub FrmReset_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)

        ButtonAceptar.Enabled = False


        eContrato = 0

        Clv_TipSer = 2

    End Sub
    Private Sub BuscarCliente()



        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, TextBox1.Text)
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        Dim dTable As DataTable = BaseII.ConsultaDT("DameDatosClienteSms")
        If dTable.Rows.Count > 0 Then
            Me.TextBox1.Text = dTable.Rows(0)(0).ToString
            'Me.tbContratoCompania.Text = dTable.Rows(0)(1).ToString
            Me.NOMBRETextBox.Text = dTable.Rows(0)(2).ToString
            Me.CALLETextBox.Text = dTable.Rows(0)(3).ToString
            Me.NUMEROTextBox.Text = dTable.Rows(0)(5).ToString
            Me.COLONIATextBox.Text = dTable.Rows(0)(4).ToString
            Me.CIUDADTextBox.Text = dTable.Rows(0)(6).ToString
            Me.TextBoxCelular.Text = dTable.Rows(0)(9).ToString

            ButtonAceptar.Enabled = True

        Else
            Me.TextBox1.Text = "0"
            Me.tbContratoCompania.Text = ""
            Me.NOMBRETextBox.Text = ""
            Me.CALLETextBox.Text = ""
            Me.NUMEROTextBox.Text = ""
            Me.COLONIATextBox.Text = ""
            Me.CIUDADTextBox.Text = ""
            Me.TextBoxCelular.Text = ""
            Me.TreeView1.Nodes.Clear()
        End If
        Try

            CREAARBOL()
            Llena_companias()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub


    Private Sub Llena_companias()
        Try


            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, TextBox1.Text)
            ComboBoxCompaniasActual.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuarioActual")
            ComboBoxCompaniasActual.DisplayMember = "razon_social"
            ComboBoxCompaniasActual.ValueMember = "id_compania"

            If ComboBoxCompaniasActual.Items.Count > 0 Then
                ComboBoxCompaniasActual.SelectedIndex = 0
            End If
            GloIdCompania = ComboBoxCompaniasActual.SelectedValue
            'ComboBoxCiudades.Text = ""




            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, TextBox1.Text)
            ComboBoxCompaniasPosibles.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuarioPosibles")
            ComboBoxCompaniasPosibles.DisplayMember = "razon_social"
            ComboBoxCompaniasPosibles.ValueMember = "id_compania"

            If ComboBoxCompaniasPosibles.Items.Count > 0 Then
                ComboBoxCompaniasPosibles.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Dim bnd As Integer = 0


    Private Sub tbContratoCompania_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContratoCompania.KeyDown
        If e.KeyValue = Keys.Enter Then
            Try
                Dim con As New SqlConnection(MiConexion)
                con.Open()
                Dim comando As New SqlCommand()
                Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
                GloIdCompania = array(1)
                comando.Connection = con
                comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0).Trim + " and idcompania=" + GloIdCompania.ToString() + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario.ToString() + ")"
                TextBox1.Text = comando.ExecuteScalar.ToString()
                con.Close()
            Catch ex As Exception
                TextBox1.Text = 0
            End Try
            If IsNumeric(Me.TextBox1.Text) = True Then
                If Me.TextBox1.Text > 0 Then
                    BuscarCliente()
                Else
                    ButtonAceptar.Enabled = False
                    Me.TextBox1.Text = "0"
                    Me.tbContratoCompania.Text = ""
                    Me.NOMBRETextBox.Text = ""
                    Me.CALLETextBox.Text = ""
                    Me.NUMEROTextBox.Text = ""
                    Me.COLONIATextBox.Text = ""
                    Me.CIUDADTextBox.Text = ""
                    Me.TextBoxCelular.Text = ""
                    Me.TreeView1.Nodes.Clear()
                End If
            Else
                ButtonAceptar.Enabled = False
                Me.TextBox1.Text = "0"
                Me.tbContratoCompania.Text = ""
                Me.NOMBRETextBox.Text = ""
                Me.CALLETextBox.Text = ""
                Me.NUMEROTextBox.Text = ""
                Me.COLONIATextBox.Text = ""
                Me.CIUDADTextBox.Text = ""
                Me.TextBoxCelular.Text = ""
                Me.TreeView1.Nodes.Clear()

            End If

        End If
    End Sub





    Private Sub ButtonAceptar_Click(sender As Object, e As EventArgs) Handles ButtonAceptar.Click
        If Not IsNumeric(ComboBoxCompaniasPosibles.SelectedValue) Then
            MsgBox("Falta capturar la nueva plaza")
            Exit Sub
        End If

        If Not ComboBoxCompaniasPosibles.SelectedValue > 0 Then
            MsgBox("Falta capturar la nueva plaza")
            Exit Sub
        End If




        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Me.TextBox1.Text)
        BaseII.CreateMyParameter("@idcompaniaNueva", SqlDbType.BigInt, ComboBoxCompaniasPosibles.SelectedValue)
        BaseII.CreateMyParameter("@msj", ParameterDirection.Output, SqlDbType.VarChar, 300)


        BaseII.ProcedimientoOutPut("CambioCompania")

        MsgBox(BaseII.dicoPar("@msj").ToString())

        Me.Close()
    End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.TextBox1.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.TextBox1.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False

            Dim PasaJNet As Boolean = False
            Dim jNet As Integer = -1
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Teléfonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then
                    I = I + 1
                    pasa = False
                End If

            Next
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
            CON.Close()
            'Me.TreeView1.Nodes(0).ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class