﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambioDeTipoDeServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bnBuscar = New System.Windows.Forms.Button()
        Me.cbDeTvADig = New System.Windows.Forms.RadioButton()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.gbServicioTv = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbServicioTv = New System.Windows.Forms.ComboBox()
        Me.nudTvConPago = New System.Windows.Forms.NumericUpDown()
        Me.nudTvSinPago = New System.Windows.Forms.NumericUpDown()
        Me.cbDeDigATv = New System.Windows.Forms.RadioButton()
        Me.bnGuardar = New System.Windows.Forms.Button()
        Me.gbServicioDig = New System.Windows.Forms.GroupBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbServicioDig = New System.Windows.Forms.ComboBox()
        Me.nudExtensiones = New System.Windows.Forms.NumericUpDown()
        Me.nudCajas = New System.Windows.Forms.NumericUpDown()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbContratoCompania = New System.Windows.Forms.TextBox()
        Me.gbServicioTv.SuspendLayout()
        CType(Me.nudTvConPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudTvSinPago, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbServicioDig.SuspendLayout()
        CType(Me.nudExtensiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCajas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnBuscar
        '
        Me.bnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscar.Location = New System.Drawing.Point(178, 82)
        Me.bnBuscar.Name = "bnBuscar"
        Me.bnBuscar.Size = New System.Drawing.Size(35, 23)
        Me.bnBuscar.TabIndex = 3
        Me.bnBuscar.Text = "..."
        Me.bnBuscar.UseVisualStyleBackColor = True
        '
        'cbDeTvADig
        '
        Me.cbDeTvADig.AutoSize = True
        Me.cbDeTvADig.Checked = True
        Me.cbDeTvADig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDeTvADig.Location = New System.Drawing.Point(30, 29)
        Me.cbDeTvADig.Name = "cbDeTvADig"
        Me.cbDeTvADig.Size = New System.Drawing.Size(101, 19)
        Me.cbDeTvADig.TabIndex = 0
        Me.cbDeTvADig.TabStop = True
        Me.cbDeTvADig.Text = "De TV a Dig"
        Me.cbDeTvADig.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(27, 64)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(65, 15)
        Me.CMBLabel1.TabIndex = 2
        Me.CMBLabel1.Text = "Contrato:"
        '
        'tbContrato
        '
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(30, 82)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(142, 21)
        Me.tbContrato.TabIndex = 2
        '
        'gbServicioTv
        '
        Me.gbServicioTv.Controls.Add(Me.Label4)
        Me.gbServicioTv.Controls.Add(Me.Label3)
        Me.gbServicioTv.Controls.Add(Me.Label2)
        Me.gbServicioTv.Controls.Add(Me.cbServicioTv)
        Me.gbServicioTv.Controls.Add(Me.nudTvConPago)
        Me.gbServicioTv.Controls.Add(Me.nudTvSinPago)
        Me.gbServicioTv.Enabled = False
        Me.gbServicioTv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbServicioTv.Location = New System.Drawing.Point(411, 121)
        Me.gbServicioTv.Name = "gbServicioTv"
        Me.gbServicioTv.Size = New System.Drawing.Size(364, 183)
        Me.gbServicioTv.TabIndex = 5
        Me.gbServicioTv.TabStop = False
        Me.gbServicioTv.Text = "Servicio de TV"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 15)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Servicio:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 121)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "TV con Pago:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "TV sin Pago:"
        '
        'cbServicioTv
        '
        Me.cbServicioTv.DisplayMember = "Descripcion"
        Me.cbServicioTv.FormattingEnabled = True
        Me.cbServicioTv.Location = New System.Drawing.Point(80, 35)
        Me.cbServicioTv.Name = "cbServicioTv"
        Me.cbServicioTv.Size = New System.Drawing.Size(278, 23)
        Me.cbServicioTv.TabIndex = 0
        Me.cbServicioTv.ValueMember = "Clv_Servicio"
        '
        'nudTvConPago
        '
        Me.nudTvConPago.Location = New System.Drawing.Point(108, 119)
        Me.nudTvConPago.Name = "nudTvConPago"
        Me.nudTvConPago.Size = New System.Drawing.Size(36, 21)
        Me.nudTvConPago.TabIndex = 2
        '
        'nudTvSinPago
        '
        Me.nudTvSinPago.Location = New System.Drawing.Point(108, 88)
        Me.nudTvSinPago.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudTvSinPago.Name = "nudTvSinPago"
        Me.nudTvSinPago.Size = New System.Drawing.Size(36, 21)
        Me.nudTvSinPago.TabIndex = 1
        '
        'cbDeDigATv
        '
        Me.cbDeDigATv.AutoSize = True
        Me.cbDeDigATv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDeDigATv.Location = New System.Drawing.Point(137, 29)
        Me.cbDeDigATv.Name = "cbDeDigATv"
        Me.cbDeDigATv.Size = New System.Drawing.Size(101, 19)
        Me.cbDeDigATv.TabIndex = 1
        Me.cbDeDigATv.Text = "De Dig a TV"
        Me.cbDeDigATv.UseVisualStyleBackColor = True
        '
        'bnGuardar
        '
        Me.bnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.Location = New System.Drawing.Point(669, 12)
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.bnGuardar.TabIndex = 6
        Me.bnGuardar.Text = "&GUARDAR"
        Me.bnGuardar.UseVisualStyleBackColor = True
        '
        'gbServicioDig
        '
        Me.gbServicioDig.Controls.Add(Me.ComboBox1)
        Me.gbServicioDig.Controls.Add(Me.Label1)
        Me.gbServicioDig.Controls.Add(Me.Label5)
        Me.gbServicioDig.Controls.Add(Me.Label6)
        Me.gbServicioDig.Controls.Add(Me.Label7)
        Me.gbServicioDig.Controls.Add(Me.cbServicioDig)
        Me.gbServicioDig.Controls.Add(Me.nudExtensiones)
        Me.gbServicioDig.Controls.Add(Me.nudCajas)
        Me.gbServicioDig.Enabled = False
        Me.gbServicioDig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbServicioDig.Location = New System.Drawing.Point(30, 121)
        Me.gbServicioDig.Name = "gbServicioDig"
        Me.gbServicioDig.Size = New System.Drawing.Size(364, 183)
        Me.gbServicioDig.TabIndex = 4
        Me.gbServicioDig.TabStop = False
        Me.gbServicioDig.Text = "Servicio Digital"
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(6, 109)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(352, 24)
        Me.ComboBox1.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 16)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Modelo de Caja: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 38)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 15)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Servicio:"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(6, 148)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(166, 20)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Extensiones Análogas:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 66)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 15)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Cajas:"
        '
        'cbServicioDig
        '
        Me.cbServicioDig.DisplayMember = "Descripcion"
        Me.cbServicioDig.FormattingEnabled = True
        Me.cbServicioDig.Location = New System.Drawing.Point(74, 35)
        Me.cbServicioDig.Name = "cbServicioDig"
        Me.cbServicioDig.Size = New System.Drawing.Size(284, 23)
        Me.cbServicioDig.TabIndex = 0
        Me.cbServicioDig.ValueMember = "Clv_Servicio"
        '
        'nudExtensiones
        '
        Me.nudExtensiones.Location = New System.Drawing.Point(172, 148)
        Me.nudExtensiones.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudExtensiones.Name = "nudExtensiones"
        Me.nudExtensiones.Size = New System.Drawing.Size(36, 21)
        Me.nudExtensiones.TabIndex = 2
        '
        'nudCajas
        '
        Me.nudCajas.Location = New System.Drawing.Point(74, 64)
        Me.nudCajas.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudCajas.Name = "nudCajas"
        Me.nudCajas.Size = New System.Drawing.Size(36, 21)
        Me.nudCajas.TabIndex = 1
        Me.nudCajas.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(669, 54)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 7
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(391, 64)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(239, 23)
        Me.ComboBoxCompanias.TabIndex = 108
        Me.ComboBoxCompanias.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(388, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(76, 15)
        Me.Label8.TabIndex = 109
        Me.Label8.Text = "Compañía:"
        Me.Label8.Visible = False
        '
        'tbContratoCompania
        '
        Me.tbContratoCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContratoCompania.Location = New System.Drawing.Point(30, 82)
        Me.tbContratoCompania.Name = "tbContratoCompania"
        Me.tbContratoCompania.Size = New System.Drawing.Size(142, 21)
        Me.tbContratoCompania.TabIndex = 110
        '
        'FrmCambioDeTipoDeServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(817, 323)
        Me.Controls.Add(Me.tbContratoCompania)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.ComboBoxCompanias)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.gbServicioDig)
        Me.Controls.Add(Me.bnGuardar)
        Me.Controls.Add(Me.cbDeDigATv)
        Me.Controls.Add(Me.gbServicioTv)
        Me.Controls.Add(Me.tbContrato)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.cbDeTvADig)
        Me.Controls.Add(Me.bnBuscar)
        Me.Name = "FrmCambioDeTipoDeServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Tipo de Servicio"
        Me.gbServicioTv.ResumeLayout(False)
        Me.gbServicioTv.PerformLayout()
        CType(Me.nudTvConPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudTvSinPago, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbServicioDig.ResumeLayout(False)
        Me.gbServicioDig.PerformLayout()
        CType(Me.nudExtensiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCajas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnBuscar As System.Windows.Forms.Button
    Friend WithEvents cbDeTvADig As System.Windows.Forms.RadioButton
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents gbServicioTv As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbServicioTv As System.Windows.Forms.ComboBox
    Friend WithEvents nudTvConPago As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudTvSinPago As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbDeDigATv As System.Windows.Forms.RadioButton
    Friend WithEvents bnGuardar As System.Windows.Forms.Button
    Friend WithEvents gbServicioDig As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbServicioDig As System.Windows.Forms.ComboBox
    Friend WithEvents nudExtensiones As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudCajas As System.Windows.Forms.NumericUpDown
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbContratoCompania As System.Windows.Forms.TextBox
End Class
