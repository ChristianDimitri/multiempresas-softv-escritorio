﻿
Public Class FrmCapturaGrupoConceptosPoliza

    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click

        Try
            If ConceptoText.Text.Length = 0 Then
                MessageBox.Show("Captura el Concepto.")
                Exit Sub
            End If

            If PosicionText.Text.Length = 0 Then
                MessageBox.Show("Captura la Posición.")
                Exit Sub
            End If

            If IsNumeric(PosicionText.Text) = False Then
                MessageBox.Show("Captura una Posición válida.")
                Exit Sub
            End If

            If tbIdProgramacion.Text.Length = 0 Then
                MessageBox.Show("Captura Id Programación.")
            End If

            If IsNumeric(tbIdProgramacion.Text) = False Then
                MessageBox.Show("Captura Id Programación válido.")
            End If

            DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
            'Dim valida As Integer
            'valida = CInt(PosicionText.Text)
            'If valida >= 0 Then
            '    If ConceptoText.Text.Trim <> "" Then
            '        Me.DialogResult = DialogResult.OK
            '        MsgBox("Se a guardado satisfactoriamente", MsgBoxStyle.Information, "Guardado")
            '        Me.Close()
            '    Else
            '        MsgBox("Ingrese el campo concepto", MsgBoxStyle.Exclamation, "Error")
            '    End If
            'Else
            '    MsgBox("El campo Posicion tiene que ser mayor a 0", MsgBoxStyle.Exclamation, "Error")
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            'MsgBox("El campo Posicion es Numerico", MsgBoxStyle.Exclamation, "Error")
        End Try




    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub

    Private Sub FrmCapturaGrupoConceptosPoliza_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub
End Class