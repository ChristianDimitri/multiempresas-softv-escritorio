﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatalogoIP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim StatusLabel1 As System.Windows.Forms.Label
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim Numero_telLabel As System.Windows.Forms.Label
        Dim Ult_Cliente_AsignadoLabel As System.Windows.Forms.Label
        Dim Ultima_FechaAsigLabel As System.Windows.Forms.Label
        Dim Ultima_Fecha_LiberacionLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCatalogoIP))
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.cbStatus = New System.Windows.Forms.ComboBox()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.tbIP = New System.Windows.Forms.TextBox()
        Me.tbUltimoClienteAsignado = New System.Windows.Forms.TextBox()
        Me.tbUltimaFechaLiberacion = New System.Windows.Forms.TextBox()
        Me.tbUltimaFechaAsignacion = New System.Windows.Forms.TextBox()
        StatusLabel1 = New System.Windows.Forms.Label()
        ClaveLabel = New System.Windows.Forms.Label()
        Numero_telLabel = New System.Windows.Forms.Label()
        Ult_Cliente_AsignadoLabel = New System.Windows.Forms.Label()
        Ultima_FechaAsigLabel = New System.Windows.Forms.Label()
        Ultima_Fecha_LiberacionLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.SuspendLayout()
        Me.CMBPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusLabel1
        '
        StatusLabel1.AutoSize = True
        StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel1.Location = New System.Drawing.Point(55, 124)
        StatusLabel1.Name = "StatusLabel1"
        StatusLabel1.Size = New System.Drawing.Size(59, 16)
        StatusLabel1.TabIndex = 16
        StatusLabel1.Text = "Status :"
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.Location = New System.Drawing.Point(58, 56)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(56, 16)
        ClaveLabel.TabIndex = 3
        ClaveLabel.Text = "Clave :"
        '
        'Numero_telLabel
        '
        Numero_telLabel.AutoSize = True
        Numero_telLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_telLabel.Location = New System.Drawing.Point(14, 92)
        Numero_telLabel.Name = "Numero_telLabel"
        Numero_telLabel.Size = New System.Drawing.Size(96, 16)
        Numero_telLabel.TabIndex = 5
        Numero_telLabel.Text = "Dirección IP:"
        '
        'Ult_Cliente_AsignadoLabel
        '
        Ult_Cliente_AsignadoLabel.AutoSize = True
        Ult_Cliente_AsignadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ult_Cliente_AsignadoLabel.Location = New System.Drawing.Point(368, 115)
        Ult_Cliente_AsignadoLabel.Name = "Ult_Cliente_AsignadoLabel"
        Ult_Cliente_AsignadoLabel.Size = New System.Drawing.Size(182, 16)
        Ult_Cliente_AsignadoLabel.TabIndex = 13
        Ult_Cliente_AsignadoLabel.Text = "Ultimo Cliente Asignado :"
        '
        'Ultima_FechaAsigLabel
        '
        Ultima_FechaAsigLabel.AutoSize = True
        Ultima_FechaAsigLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ultima_FechaAsigLabel.Location = New System.Drawing.Point(338, 56)
        Ultima_FechaAsigLabel.Name = "Ultima_FechaAsigLabel"
        Ultima_FechaAsigLabel.Size = New System.Drawing.Size(212, 16)
        Ultima_FechaAsigLabel.TabIndex = 9
        Ultima_FechaAsigLabel.Text = "Ultima Fecha De Asignación :"
        '
        'Ultima_Fecha_LiberacionLabel
        '
        Ultima_Fecha_LiberacionLabel.AutoSize = True
        Ultima_Fecha_LiberacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ultima_Fecha_LiberacionLabel.Location = New System.Drawing.Point(342, 87)
        Ultima_Fecha_LiberacionLabel.Name = "Ultima_Fecha_LiberacionLabel"
        Ultima_Fecha_LiberacionLabel.Size = New System.Drawing.Size(208, 16)
        Ultima_Fecha_LiberacionLabel.TabIndex = 11
        Ultima_Fecha_LiberacionLabel.Text = "Ultima Fecha De Liberación :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(18, 25)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(92, 16)
        Label1.TabIndex = 19
        Label1.Text = "Distribuidor:"
        Label1.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(571, 204)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(147, 36)
        Me.Button5.TabIndex = 26
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Consulta_Cat_Num_TelefonoBindingNavigator
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.CountItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem})
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Name = "Consulta_Cat_Num_TelefonoBindingNavigator"
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PositionItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Size = New System.Drawing.Size(727, 25)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.TabIndex = 25
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Name = "Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem"
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel1.Controls.Add(Label1)
        Me.CMBPanel1.Controls.Add(Me.ComboBox1)
        Me.CMBPanel1.Controls.Add(Me.CMBTextBox1)
        Me.CMBPanel1.Controls.Add(StatusLabel1)
        Me.CMBPanel1.Controls.Add(Me.cbStatus)
        Me.CMBPanel1.Controls.Add(ClaveLabel)
        Me.CMBPanel1.Controls.Add(Me.tbClave)
        Me.CMBPanel1.Controls.Add(Numero_telLabel)
        Me.CMBPanel1.Controls.Add(Me.tbIP)
        Me.CMBPanel1.Controls.Add(Me.tbUltimoClienteAsignado)
        Me.CMBPanel1.Controls.Add(Ult_Cliente_AsignadoLabel)
        Me.CMBPanel1.Controls.Add(Me.tbUltimaFechaLiberacion)
        Me.CMBPanel1.Controls.Add(Ultima_FechaAsigLabel)
        Me.CMBPanel1.Controls.Add(Ultima_Fecha_LiberacionLabel)
        Me.CMBPanel1.Controls.Add(Me.tbUltimaFechaAsignacion)
        Me.CMBPanel1.Location = New System.Drawing.Point(12, 28)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(706, 170)
        Me.CMBPanel1.TabIndex = 24
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(116, 22)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(173, 24)
        Me.ComboBox1.TabIndex = 20
        Me.ComboBox1.ValueMember = "IdDistribuidor"
        Me.ComboBox1.Visible = False
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(347, 25)
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.Size = New System.Drawing.Size(344, 15)
        Me.CMBTextBox1.TabIndex = 18
        Me.CMBTextBox1.Text = "Fechas De"
        Me.CMBTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cbStatus
        '
        Me.cbStatus.DisplayMember = "Status"
        Me.cbStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbStatus.FormattingEnabled = True
        Me.cbStatus.Location = New System.Drawing.Point(116, 123)
        Me.cbStatus.Name = "cbStatus"
        Me.cbStatus.Size = New System.Drawing.Size(173, 24)
        Me.cbStatus.TabIndex = 17
        Me.cbStatus.ValueMember = "clv_status"
        '
        'tbClave
        '
        Me.tbClave.Enabled = False
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(116, 56)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.Size = New System.Drawing.Size(80, 22)
        Me.tbClave.TabIndex = 4
        '
        'tbIP
        '
        Me.tbIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIP.Location = New System.Drawing.Point(116, 89)
        Me.tbIP.MaxLength = 15
        Me.tbIP.Name = "tbIP"
        Me.tbIP.Size = New System.Drawing.Size(173, 22)
        Me.tbIP.TabIndex = 6
        '
        'tbUltimoClienteAsignado
        '
        Me.tbUltimoClienteAsignado.Enabled = False
        Me.tbUltimoClienteAsignado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUltimoClienteAsignado.Location = New System.Drawing.Point(556, 112)
        Me.tbUltimoClienteAsignado.Name = "tbUltimoClienteAsignado"
        Me.tbUltimoClienteAsignado.Size = New System.Drawing.Size(135, 22)
        Me.tbUltimoClienteAsignado.TabIndex = 14
        '
        'tbUltimaFechaLiberacion
        '
        Me.tbUltimaFechaLiberacion.Enabled = False
        Me.tbUltimaFechaLiberacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUltimaFechaLiberacion.Location = New System.Drawing.Point(556, 84)
        Me.tbUltimaFechaLiberacion.Name = "tbUltimaFechaLiberacion"
        Me.tbUltimaFechaLiberacion.Size = New System.Drawing.Size(135, 22)
        Me.tbUltimaFechaLiberacion.TabIndex = 12
        '
        'tbUltimaFechaAsignacion
        '
        Me.tbUltimaFechaAsignacion.Enabled = False
        Me.tbUltimaFechaAsignacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUltimaFechaAsignacion.Location = New System.Drawing.Point(556, 53)
        Me.tbUltimaFechaAsignacion.Name = "tbUltimaFechaAsignacion"
        Me.tbUltimaFechaAsignacion.Size = New System.Drawing.Size(135, 22)
        Me.tbUltimaFechaAsignacion.TabIndex = 10
        '
        'FrmCatalogoIP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(727, 258)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Consulta_Cat_Num_TelefonoBindingNavigator)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Name = "FrmCatalogoIP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de IPs"
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.ResumeLayout(False)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PerformLayout()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents tbIP As System.Windows.Forms.TextBox
    Friend WithEvents tbUltimoClienteAsignado As System.Windows.Forms.TextBox
    Friend WithEvents tbUltimaFechaLiberacion As System.Windows.Forms.TextBox
    Friend WithEvents tbUltimaFechaAsignacion As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
End Class
