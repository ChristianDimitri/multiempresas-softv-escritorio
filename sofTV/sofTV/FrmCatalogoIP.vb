﻿Public Class FrmCatalogoIP

    Private Sub FrmCatalogoIP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        cbStatus.DataSource = BaseII.ConsultaDT("GetAllStatusIP")

        If BanderaCatalogoIP = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Id", SqlDbType.Int, IdIP)
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("GetCatalogoIpById")
            Dim dr As DataRow
            For Each dr In dt.Rows
                tbClave.Text = IdIP
                tbIP.Text = dr("IP")
                cbStatus.Text = dr("Status")
                tbUltimaFechaAsignacion.Text = dr("UltimaFechaAsignacion")
                tbUltimaFechaLiberacion.Text = dr("UltimaFechaLiberacion")
                tbUltimoClienteAsignado.Text = dr("UltimoClienteAsignado")
            Next

            tbIP.Enabled = True
            cbStatus.Enabled = True
        End If

        If BanderaCatalogoIP = "C" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Id", SqlDbType.Int, IdIP)
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("GetCatalogoIpById")
            Dim dr As DataRow
            For Each dr In dt.Rows
                tbClave.Text = IdIP
                tbIP.Text = dr("IP")
                cbStatus.Text = dr("Status")
                tbUltimaFechaAsignacion.Text = dr("UltimaFechaAsignacion")
                tbUltimaFechaLiberacion.Text = dr("UltimaFechaLiberacion")
                tbUltimoClienteAsignado.Text = dr("UltimoClienteAsignado")
            Next

            tbIP.Enabled = False
            cbStatus.Enabled = False
        End If


        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Click
        Dim YaExiste As Integer = 0

        If tbIP.Text.Trim() = "" Then
            MessageBox.Show("Ingrese la dirección IP")
            Exit Sub
        End If
        If cbStatus.Text.Trim() = "" Then
            MessageBox.Show("Ingrese el status")
            Exit Sub
        End If

        If BanderaCatalogoIP = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IP", SqlDbType.NVarChar, tbIP.Text.Trim())
            BaseII.CreateMyParameter("@Status", SqlDbType.NVarChar, cbStatus.Text.Trim())
            BaseII.CreateMyParameter("@YaExiste", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("InsertIP")
            YaExiste = BaseII.dicoPar("@YaExiste")
            If YaExiste = 1 Then
                MessageBox.Show("Ya existe la dirección IP")
            Else
                MessageBox.Show("Dirección IP agregada correctamente")
            End If
        End If
        If BanderaCatalogoIP = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Id", SqlDbType.Int, tbClave.Text.Trim())
            BaseII.CreateMyParameter("@IP", SqlDbType.NVarChar, tbIP.Text.Trim())
            BaseII.CreateMyParameter("@Status", SqlDbType.NVarChar, cbStatus.Text.Trim())
            BaseII.CreateMyParameter("@UltimaFechaAsignacion", SqlDbType.Date, tbUltimaFechaAsignacion.Text.Trim())
            BaseII.CreateMyParameter("@UltimaFechaLiberacion", SqlDbType.Date, tbUltimaFechaLiberacion.Text.Trim())
            BaseII.CreateMyParameter("@UltimoClienteAsignado", SqlDbType.Int, tbUltimoClienteAsignado.Text.Trim())
            BaseII.Inserta("UpdateIP")
            MessageBox.Show("Dirección IP guardada con exito")
        End If
        Me.Close()

        'tbClave.Text = ""
        'tbIP.Text = ""
        'cbStatus.Text = ""
        'tbUltimaFechaAsignacion.Text = ""
        'tbUltimaFechaLiberacion.Text = ""
        'tbUltimoClienteAsignado.Text = ""
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    'Valida la IP
    Private Sub tbIP_Validated(sender As Object, e As EventArgs) Handles tbIP.Validated
        If tbIP.Text <> "" Then
            Dim a() As String = Split(tbIP.Text.Trim(), ".")
            Dim i As Integer
            If a.Length <> 4 Then
                MsgBox("Dirección IP incorrecta")
                tbIP.Focus()
                Return
            End If
            For i = 0 To 3
                If IsNumeric(a(i)) Then
                    If a(i) > 255 Or a(i) < 0 Then
                        MsgBox("Dirección IP incorrecta")
                        tbIP.Focus()
                        Return
                    End If
                Else
                    MsgBox("Dirección IP incorrecta")
                    tbIP.Focus()
                    Return
                End If
            Next
        End If
    End Sub

    Private Sub FrmCatalogoIP_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        tbClave.Text = ""
        tbIP.Text = ""
        cbStatus.Text = ""
        tbUltimaFechaAsignacion.Text = ""
        tbUltimaFechaLiberacion.Text = ""
        tbUltimoClienteAsignado.Text = ""
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim err As Integer = 0

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdIP", SqlDbType.NVarChar, tbIP.Text.Trim())
        BaseII.CreateMyParameter("@err", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("EliminarIP")
        err = BaseII.dicoPar("@err")
        If err = 0 Then
            MessageBox.Show("Dirección IP eliminada")
        Else
            MessageBox.Show("No se pudo eliminar la Dirección IP")
        End If
    End Sub
End Class