Imports System.Data.SqlClient
Public Class FrmClienteObs

    Private Sub ConRelClienteObs(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelClienteObs", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Obs", SqlDbType.VarChar, 500)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
            Me.TextBox1.Text = parametro2.Value
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub NueRelClienteObs(ByVal Contrato As Long, ByVal Obs As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelClienteObs", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Obs", SqlDbType.VarChar, 500)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Obs
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
            MsgBox(mensaje5, MsgBoxStyle.Information)
            Me.Close()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub BorRelClienteObs(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorRelClienteObs", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = contrato
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
            MsgBox(mensaje6, MsgBoxStyle.Information)
            Me.Close()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ToolStripButtonGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButtonGuardar.Click
        If Me.TextBox1.Text.Length = 0 Then
            MsgBox("Teclea una observación.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        NueRelClienteObs(Contrato, Me.TextBox1.Text)
    End Sub

    Private Sub ToolStripButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButtonEliminar.Click
        BorRelClienteObs(Contrato)
    End Sub

    Private Sub FrmClienteObs_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        ConRelClienteObs(Contrato)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class