﻿Imports System.IO
Imports System.Data.SqlClient
Public Class FrmCluster
    Dim locbndnew As Integer = 0
    Private Sub FrmCluster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            eliminar.Enabled = False
            locbndnew = 1
        End If
        If opcion = "M" Then
            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand()
            conexion.Open()
            comando.Connection = conexion
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "MuestraCluster"
            Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
            p4.Direction = ParameterDirection.Input
            p4.Value = 4
            comando.Parameters.Add(p4)
            Dim p3 As New SqlParameter("@clave", SqlDbType.VarChar)
            p3.Direction = ParameterDirection.Input
            p3.Value = ""
            comando.Parameters.Add(p3)
            Dim p2 As New SqlParameter("@descripcion", SqlDbType.VarChar)
            p2.Direction = ParameterDirection.Input
            p2.Value = ""
            comando.Parameters.Add(p2)
            Dim p1 As New SqlParameter("@clv_cluster", SqlDbType.Int)
            p1.Direction = ParameterDirection.Input
            p1.Value = gloclv_cluster
            comando.Parameters.Add(p1)
            Dim reader As SqlDataReader = comando.ExecuteReader()
            If reader.Read() Then
                tbclv_cluster.Text = reader(0).ToString
                Clv_TxtTextBox.Text = reader(1).ToString
                DescripcionTextBox.Text = reader(2).ToString
            End If
            reader.Close()
            conexion.Close()
            llenacombodgv()
        End If
        If opcion = "C" Then
            DescripcionTextBox.Enabled = False
            Clv_TxtTextBox.Enabled = False
            guardar.Enabled = False
            eliminar.Enabled = False
            Button1.Enabled = False
            Button2.Enabled = False
            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand()
            conexion.Open()
            comando.Connection = conexion
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "MuestraCluster"
            Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
            p4.Direction = ParameterDirection.Input
            p4.Value = 4
            comando.Parameters.Add(p4)
            Dim p3 As New SqlParameter("@clave", SqlDbType.VarChar)
            p3.Direction = ParameterDirection.Input
            p3.Value = ""
            comando.Parameters.Add(p3)
            Dim p2 As New SqlParameter("@descripcion", SqlDbType.VarChar)
            p2.Direction = ParameterDirection.Input
            p2.Value = ""
            comando.Parameters.Add(p2)
            Dim p1 As New SqlParameter("@clv_cluster", SqlDbType.Int)
            p1.Direction = ParameterDirection.Input
            p1.Value = gloclv_cluster
            comando.Parameters.Add(p1)
            Dim reader As SqlDataReader = comando.ExecuteReader()
            If reader.Read() Then
                tbclv_cluster.Text = reader(0).ToString
                Clv_TxtTextBox.Text = reader(1).ToString
                DescripcionTextBox.Text = reader(2).ToString
            End If
            reader.Close()
            conexion.Close()
            llenacombodgv()
        End If
    End Sub

    Private Sub guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles guardar.Click
        If Clv_TxtTextBox.Text = "" Or DescripcionTextBox.Text = "" Then
            MsgBox("Debe llenar todos los campos para poder continuar")
            Exit Sub
        End If
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "InsertUpdateCluster"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        If opcion = "N" Then
            p4.Value = 0
        Else
            p4.Value = 1
        End If
        comando.Parameters.Add(p4)
        Dim p3 As New SqlParameter("@clave", SqlDbType.VarChar)
        p3.Direction = ParameterDirection.Input
        p3.Value = Clv_TxtTextBox.Text.Trim
        comando.Parameters.Add(p3)
        Dim p2 As New SqlParameter("@descripcion", SqlDbType.VarChar)
        p2.Direction = ParameterDirection.Input
        p2.Value = DescripcionTextBox.Text.Trim
        comando.Parameters.Add(p2)
        Dim p1 As New SqlParameter("@clv_cluster", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = tbclv_cluster.Text
        comando.Parameters.Add(p1)
        Dim reader As SqlDataReader = comando.ExecuteReader()
        reader.Read()
        Dim bnd As String = reader(0).ToString
        If bnd = "1" Then
            MsgBox("Datos guardados exitosamente")
            Me.Close()
        Else
            MsgBox("Ya existe la Clave del Cluster, vuelva a intentarlo")
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If SectorComboBox.Items.Count = 0 Then 'Agregar
            Exit Sub
        End If
        If locbndnew = 1 Then
            MsgBox("Debe guardar primero el cluster")
            Exit Sub
        End If
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "QuitarEliminarRelClusterSector"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        p4.Value = 1
        comando.Parameters.Add(p4)
        Dim p1 As New SqlParameter("@clv_cluster", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = tbclv_cluster.Text
        comando.Parameters.Add(p1)
        Dim p2 As New SqlParameter("@clv_sector", SqlDbType.Int)
        p2.Direction = ParameterDirection.Input
        p2.Value = SectorComboBox.SelectedValue
        comando.Parameters.Add(p2)
        comando.ExecuteNonQuery()
        llenacombodgv()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvrelsector.Rows.Count = 0 Then 'Eliminar
            Exit Sub
        End If
        If locbndnew = 1 Then
            MsgBox("Debe guardar primero el cluster")
            Exit Sub
        End If
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "QuitarEliminarRelClusterSector"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        p4.Value = 2
        comando.Parameters.Add(p4)
        Dim p1 As New SqlParameter("@clv_cluster", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = tbclv_cluster.Text
        comando.Parameters.Add(p1)
        Dim p2 As New SqlParameter("@clv_sector", SqlDbType.Int)
        p2.Direction = ParameterDirection.Input
        p2.Value = dgvrelsector.SelectedCells(0).Value
        comando.Parameters.Add(p2)
        comando.ExecuteNonQuery()
        llenacombodgv()
    End Sub
    Private Sub llenacombodgv()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_cluster", SqlDbType.Int, gloclv_cluster)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        dgvrelsector.DataSource = BaseII.ConsultaDT("MuestraRelClusterSector")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_cluster", SqlDbType.Int, gloclv_cluster)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        SectorComboBox.DataSource = BaseII.ConsultaDT("MuestraRelClusterSector")
        SectorComboBox.DisplayMember = "Descripcion"
        SectorComboBox.ValueMember = "Clv_Sector"
    End Sub

    Private Sub eliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles eliminar.Click
        If opcion = "N" Then
            Exit Sub
        End If
        If Clv_TxtTextBox.Text = "" Or DescripcionTextBox.Text = "" Then
            MsgBox("Debe llenar todos los campos para poder continuar")
            Exit Sub
        End If
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "InsertUpdateCluster"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input

        p4.Value = 2

        comando.Parameters.Add(p4)
        Dim p3 As New SqlParameter("@clave", SqlDbType.VarChar)
        p3.Direction = ParameterDirection.Input
        p3.Value = Clv_TxtTextBox.Text.Trim
        comando.Parameters.Add(p3)
        Dim p2 As New SqlParameter("@descripcion", SqlDbType.VarChar)
        p2.Direction = ParameterDirection.Input
        p2.Value = DescripcionTextBox.Text.Trim
        comando.Parameters.Add(p2)
        Dim p1 As New SqlParameter("@clv_cluster", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = tbclv_cluster.Text
        comando.Parameters.Add(p1)
        Dim reader As SqlDataReader = comando.ExecuteReader()
        reader.Read()
        Dim bnd As String = reader(0).ToString
        If bnd = "1" Then
            MsgBox("Cluster eliminado con éxito.")
            Me.Close()
        Else
            MsgBox("El Cluster ya tiene Sectores asignados o ya pertenece a una clave técnica, no se puede eliminar.")
        End If
    End Sub
End Class