Imports System.Data.SqlClient
Public Class FrmComision


    Private Sub FrmComision_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker2.Value = Today
        Me.DateTimePicker2.MaxDate = Today
        Me.DateTimePicker1.Value = Today
        If IdSistema = "AG" Then
            Me.RadioButton1.Enabled = False
        End If

    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub



    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click

        eFechaIni = Me.DateTimePicker1.Value
        eFechaFin = Me.DateTimePicker2.Value


        If Me.RadioButton4.Checked = True Then 'COMISIONES
            eOpVentas = 1
            FrmSelVendedor.Show()

        ElseIf Me.RadioButton5.Checked = True Then 'NUMERO DE VENTAS
            If Me.RadioButton1.Checked = True Then
                eTipSer = 1
                eServicio = "de B�sico"
            ElseIf Me.RadioButton2.Checked = True Then
                eTipSer = 2
                eServicio = "de Internet"
            ElseIf Me.RadioButton3.Checked = True Then
                eTipSer = 3
                eServicio = "de Digital"
            End If
            eOpVentas = 2

            FrmSelVendedor.Show()

        ElseIf Me.RadioButton6.Checked = True Then 'VENTAS POR STATUS
            eOpVentas = 3
            If Me.CheckBox1.Checked = True Then
                eCont = 1
            Else
                eCont = 0
            End If

            If Me.CheckBox2.Checked = True Then
                eInst = 1
            Else
                eInst = 0
            End If

            If Me.CheckBox3.Checked = True Then
                eDesc = 1
            Else
                eDesc = 0
            End If

            If Me.CheckBox4.Checked = True Then
                eSusp = 1
            Else
                eSusp = 0
            End If

            If Me.CheckBox5.Checked = True Then
                eBaja = 1
            Else
                eBaja = 0
            End If

            If Me.CheckBox6.Checked = True Then
                eFuera = 1
            Else
                eFuera = 0
            End If

            If Me.RadioButton1.Checked = True Then
                eTipSer = 1
                eServicio = "de B�sico"
            ElseIf Me.RadioButton2.Checked = True Then
                eTipSer = 2
                eServicio = "de Internet"
            ElseIf Me.RadioButton3.Checked = True Then
                eTipSer = 3
                eServicio = "de Digital"
            End If

            If eCont = 0 And eInst = 0 And eDesc = 0 And eSusp = 0 And eBaja = 0 And eFuera = 0 Then
                MsgBox("Selecciona al Menos un Status para la Venta.", , "Atenci�n")
            Else

                FrmSelVendedor.Show()

            End If


        ElseIf Me.RadioButton7.Checked = True Then 'FOLIOS POR USARSE
            eOpVentas = 4
            eOp = 0
            FrmSelVendedor.Show()

        End If

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        'Me.CheckBox3.Checked = False
        'Me.CheckBox3.Enabled = False
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        'Me.CheckBox3.Checked = False
        'Me.CheckBox3.Enabled = False
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        'Me.CheckBox3.Enabled = True
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        Me.GroupBox2.Enabled = False
        Me.GroupBox3.Enabled = False
    End Sub

    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        Me.GroupBox2.Enabled = True
        Me.GroupBox3.Enabled = False
    End Sub

    Private Sub RadioButton6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        Me.GroupBox2.Enabled = True
        Me.GroupBox3.Enabled = True
    End Sub

    Private Sub RadioButton7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton7.CheckedChanged
        Me.GroupBox2.Enabled = False
        Me.GroupBox3.Enabled = False
    End Sub
End Class