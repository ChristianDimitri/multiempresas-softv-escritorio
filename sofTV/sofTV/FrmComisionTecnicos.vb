﻿Public Class FrmComisionTecnicos

    Private Sub FrmComisionTecnicos_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)

        Usp_MuestraOrdenQueja()
        llenaTipoTCJ()

        If opRanTec = "N" Then
            idRanTec = 0
        End If
        If (opRanTec = "M" Or opRanTec = "C") And idRanTec > 0 Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idComision", SqlDbType.BigInt, idRanTec)
            BaseII.CreateMyParameter("@TipoOQ", ParameterDirection.Output, SqlDbType.VarChar, 50)
            BaseII.CreateMyParameter("@TipoTCJ", ParameterDirection.Output, SqlDbType.VarChar, 50)
            BaseII.CreateMyParameter("@limiteInf", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@limiteSup", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@pago", ParameterDirection.Output, SqlDbType.Money)
            BaseII.ProcedimientoOutPut("Usp_ConRangosComisionTecnicos")
            ComboBox2.SelectedValue = BaseII.dicoPar("@TipoOQ")
            ComboBox1.SelectedValue = BaseII.dicoPar("@TipoTCJ")
            TextBox1.Text = BaseII.dicoPar("@limiteInf")
            TextBox2.Text = BaseII.dicoPar("@limiteSup")
            TextBox3.Text = BaseII.dicoPar("@pago")

            If opRanTec = "C" Then
                TextBox1.Enabled = False
                TextBox2.Enabled = False
                TextBox3.Enabled = False
                ComboBox1.Enabled = False
                ComboBox2.Enabled = False
                'Button1.Enabled = False
                Button5.Enabled = False
            End If

        End If

    End Sub

    Private Sub llenaTipoTCJ()
        Try
            BaseII.limpiaParametros()
            ComboBox1.DataSource = BaseII.ConsultaDT("Usp_MuestraTipoTCJ")
            ComboBox1.DisplayMember = "TipoTCJ"
            ComboBox1.ValueMember = "idTipoTCJ"

            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Usp_MuestraOrdenQueja()
        Try
            BaseII.limpiaParametros()
            ComboBox2.DataSource = BaseII.ConsultaDT("Usp_MuestraOrdenQueja")
            ComboBox2.DisplayMember = "Tipo"
            ComboBox2.ValueMember = "Idtipo"

            If ComboBox2.Items.Count > 0 Then
                ComboBox2.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox1, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox2, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox3, Asc(e.KeyChar), "L"))
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        If TextBox1.Text.Length = 0 Then
            MsgBox("Capture el limite inferior")
            Exit Sub
        End If

        If TextBox2.Text.Length = 0 Then
            MsgBox("Capture el limite Superior")
            Exit Sub
        End If

        If TextBox3.Text.Length = 0 Then
            MsgBox("Capture el Pago")
            Exit Sub
        End If

        If CInt(TextBox1.Text) >= CInt(TextBox2.Text) Then
            MsgBox("El limite inferior no puede ser mayor o igual al limite superior")
            Exit Sub
        End If

        Dim Msg As String = Nothing

        If opRanTec = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@TipoOQ", SqlDbType.VarChar, ComboBox2.SelectedValue, 50)
            BaseII.CreateMyParameter("@TipoTCJ", SqlDbType.VarChar, ComboBox1.SelectedValue, 50)
            BaseII.CreateMyParameter("@limiteInf", SqlDbType.Int, TextBox1.Text)
            BaseII.CreateMyParameter("@limiteSup", SqlDbType.Int, TextBox2.Text)
            BaseII.CreateMyParameter("@pago", SqlDbType.Money, TextBox3.Text)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_NueRangosComisionTecnicos")
            Msg = BaseII.dicoPar("@Msg")
        ElseIf opRanTec = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.BigInt, idRanTec)
            BaseII.CreateMyParameter("@TipoOQ", SqlDbType.VarChar, ComboBox2.SelectedValue, 50)
            BaseII.CreateMyParameter("@TipoTCJ", SqlDbType.VarChar, ComboBox1.SelectedValue, 50)
            BaseII.CreateMyParameter("@limiteInf", SqlDbType.Int, TextBox1.Text)
            BaseII.CreateMyParameter("@limiteSup", SqlDbType.Int, TextBox2.Text)
            BaseII.CreateMyParameter("@pago", SqlDbType.Money, TextBox3.Text)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_ModRangosComisionTecnicos")
            Msg = BaseII.dicoPar("@Msg")
        End If


        If Msg.Length > 0 Then
            MsgBox(Msg, MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("Guardado con exito", MsgBoxStyle.Information)
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class