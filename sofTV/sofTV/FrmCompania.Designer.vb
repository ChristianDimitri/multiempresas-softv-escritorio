﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCompania
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCompania))
        Me.CONSERVICIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.tbidcompania = New System.Windows.Forms.TextBox()
        Me.tbrazonsocial = New System.Windows.Forms.TextBox()
        Me.tbrfc = New System.Windows.Forms.TextBox()
        Me.tbcalle = New System.Windows.Forms.TextBox()
        Me.tbexterior = New System.Windows.Forms.TextBox()
        Me.tbinterior = New System.Windows.Forms.TextBox()
        Me.tbcolonia = New System.Windows.Forms.TextBox()
        Me.tbentrecalles = New System.Windows.Forms.TextBox()
        Me.tblocalidad = New System.Windows.Forms.TextBox()
        Me.tbmunicipio = New System.Windows.Forms.TextBox()
        Me.tbestado = New System.Windows.Forms.TextBox()
        Me.tbpais = New System.Windows.Forms.TextBox()
        Me.tbcodigop = New System.Windows.Forms.TextBox()
        Me.tbtelefono = New System.Windows.Forms.TextBox()
        Me.tbfax = New System.Windows.Forms.TextBox()
        Me.tbemail = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSERVICIOSBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TxtLabel.Location = New System.Drawing.Point(46, 48)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(98, 15)
        Clv_TxtLabel.TabIndex = 14
        Clv_TxtLabel.Text = "ID Compañía :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(46, 80)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(100, 15)
        Label1.TabIndex = 15
        Label1.Text = "Razón Social :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(104, 114)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(42, 15)
        Label2.TabIndex = 16
        Label2.Text = "RFC :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(96, 151)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(48, 15)
        Label3.TabIndex = 17
        Label3.Text = "Calle :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(24, 188)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(120, 15)
        Label4.TabIndex = 18
        Label4.Text = "Número Exterior :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(235, 188)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(116, 15)
        Label5.TabIndex = 19
        Label5.Text = "Número Interior :"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(481, 149)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(91, 15)
        Label6.TabIndex = 20
        Label6.Text = "Entre calles :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(81, 225)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(64, 15)
        Label7.TabIndex = 21
        Label7.Text = "Colonia :"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(66, 293)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(78, 15)
        Label8.TabIndex = 22
        Label8.Text = "Localidad :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(341, 293)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(78, 15)
        Label10.TabIndex = 24
        Label10.Text = "Municipio :"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(85, 328)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(59, 15)
        Label11.TabIndex = 25
        Label11.Text = "Estado :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(376, 328)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(43, 15)
        Label12.TabIndex = 26
        Label12.Text = "País :"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(41, 258)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(104, 15)
        Label13.TabIndex = 27
        Label13.Text = "Código Postal :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(501, 184)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(71, 15)
        Label14.TabIndex = 28
        Label14.Text = "Teléfono :"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(534, 221)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(38, 15)
        Label15.TabIndex = 29
        Label15.Text = "Fax :"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(520, 258)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(52, 15)
        Label16.TabIndex = 30
        Label16.Text = "Email :"
        '
        'CONSERVICIOSBindingNavigator
        '
        Me.CONSERVICIOSBindingNavigator.AddNewItem = Nothing
        Me.CONSERVICIOSBindingNavigator.CountItem = Nothing
        Me.CONSERVICIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSERVICIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONSERVICIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSERVICIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONSERVICIOSBindingNavigator.Name = "CONSERVICIOSBindingNavigator"
        Me.CONSERVICIOSBindingNavigator.PositionItem = Nothing
        Me.CONSERVICIOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSERVICIOSBindingNavigator.Size = New System.Drawing.Size(921, 25)
        Me.CONSERVICIOSBindingNavigator.TabIndex = 13
        Me.CONSERVICIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'tbidcompania
        '
        Me.tbidcompania.BackColor = System.Drawing.Color.White
        Me.tbidcompania.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbidcompania.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbidcompania.Enabled = False
        Me.tbidcompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbidcompania.Location = New System.Drawing.Point(150, 42)
        Me.tbidcompania.MaxLength = 5
        Me.tbidcompania.Name = "tbidcompania"
        Me.tbidcompania.Size = New System.Drawing.Size(100, 21)
        Me.tbidcompania.TabIndex = 31
        Me.tbidcompania.Text = "0"
        '
        'tbrazonsocial
        '
        Me.tbrazonsocial.BackColor = System.Drawing.Color.White
        Me.tbrazonsocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbrazonsocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrazonsocial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrazonsocial.Location = New System.Drawing.Point(150, 74)
        Me.tbrazonsocial.MaxLength = 255
        Me.tbrazonsocial.Name = "tbrazonsocial"
        Me.tbrazonsocial.Size = New System.Drawing.Size(322, 21)
        Me.tbrazonsocial.TabIndex = 32
        '
        'tbrfc
        '
        Me.tbrfc.BackColor = System.Drawing.Color.White
        Me.tbrfc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbrfc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrfc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrfc.Location = New System.Drawing.Point(151, 107)
        Me.tbrfc.MaxLength = 15
        Me.tbrfc.Name = "tbrfc"
        Me.tbrfc.Size = New System.Drawing.Size(322, 21)
        Me.tbrfc.TabIndex = 33
        '
        'tbcalle
        '
        Me.tbcalle.BackColor = System.Drawing.Color.White
        Me.tbcalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcalle.Location = New System.Drawing.Point(151, 145)
        Me.tbcalle.MaxLength = 255
        Me.tbcalle.Name = "tbcalle"
        Me.tbcalle.Size = New System.Drawing.Size(322, 21)
        Me.tbcalle.TabIndex = 34
        '
        'tbexterior
        '
        Me.tbexterior.BackColor = System.Drawing.Color.White
        Me.tbexterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbexterior.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbexterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbexterior.Location = New System.Drawing.Point(150, 182)
        Me.tbexterior.MaxLength = 15
        Me.tbexterior.Name = "tbexterior"
        Me.tbexterior.Size = New System.Drawing.Size(79, 21)
        Me.tbexterior.TabIndex = 35
        '
        'tbinterior
        '
        Me.tbinterior.BackColor = System.Drawing.Color.White
        Me.tbinterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbinterior.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbinterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbinterior.Location = New System.Drawing.Point(357, 182)
        Me.tbinterior.MaxLength = 15
        Me.tbinterior.Name = "tbinterior"
        Me.tbinterior.Size = New System.Drawing.Size(79, 21)
        Me.tbinterior.TabIndex = 36
        '
        'tbcolonia
        '
        Me.tbcolonia.BackColor = System.Drawing.Color.White
        Me.tbcolonia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcolonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcolonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcolonia.Location = New System.Drawing.Point(151, 219)
        Me.tbcolonia.MaxLength = 255
        Me.tbcolonia.Name = "tbcolonia"
        Me.tbcolonia.Size = New System.Drawing.Size(322, 21)
        Me.tbcolonia.TabIndex = 37
        '
        'tbentrecalles
        '
        Me.tbentrecalles.BackColor = System.Drawing.Color.White
        Me.tbentrecalles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbentrecalles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbentrecalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbentrecalles.Location = New System.Drawing.Point(578, 145)
        Me.tbentrecalles.MaxLength = 255
        Me.tbentrecalles.Name = "tbentrecalles"
        Me.tbentrecalles.Size = New System.Drawing.Size(322, 21)
        Me.tbentrecalles.TabIndex = 38
        '
        'tblocalidad
        '
        Me.tblocalidad.BackColor = System.Drawing.Color.White
        Me.tblocalidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tblocalidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblocalidad.Location = New System.Drawing.Point(150, 289)
        Me.tblocalidad.MaxLength = 255
        Me.tblocalidad.Name = "tblocalidad"
        Me.tblocalidad.Size = New System.Drawing.Size(185, 21)
        Me.tblocalidad.TabIndex = 39
        '
        'tbmunicipio
        '
        Me.tbmunicipio.BackColor = System.Drawing.Color.White
        Me.tbmunicipio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbmunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbmunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbmunicipio.Location = New System.Drawing.Point(425, 290)
        Me.tbmunicipio.MaxLength = 255
        Me.tbmunicipio.Name = "tbmunicipio"
        Me.tbmunicipio.Size = New System.Drawing.Size(185, 21)
        Me.tbmunicipio.TabIndex = 40
        '
        'tbestado
        '
        Me.tbestado.BackColor = System.Drawing.Color.White
        Me.tbestado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbestado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbestado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbestado.Location = New System.Drawing.Point(150, 321)
        Me.tbestado.MaxLength = 255
        Me.tbestado.Name = "tbestado"
        Me.tbestado.Size = New System.Drawing.Size(185, 21)
        Me.tbestado.TabIndex = 41
        '
        'tbpais
        '
        Me.tbpais.BackColor = System.Drawing.Color.White
        Me.tbpais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbpais.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbpais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpais.Location = New System.Drawing.Point(425, 322)
        Me.tbpais.MaxLength = 255
        Me.tbpais.Name = "tbpais"
        Me.tbpais.Size = New System.Drawing.Size(185, 21)
        Me.tbpais.TabIndex = 42
        '
        'tbcodigop
        '
        Me.tbcodigop.BackColor = System.Drawing.Color.White
        Me.tbcodigop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcodigop.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcodigop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcodigop.Location = New System.Drawing.Point(151, 252)
        Me.tbcodigop.MaxLength = 5
        Me.tbcodigop.Name = "tbcodigop"
        Me.tbcodigop.Size = New System.Drawing.Size(185, 21)
        Me.tbcodigop.TabIndex = 43
        '
        'tbtelefono
        '
        Me.tbtelefono.BackColor = System.Drawing.Color.White
        Me.tbtelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbtelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbtelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtelefono.Location = New System.Drawing.Point(578, 178)
        Me.tbtelefono.MaxLength = 255
        Me.tbtelefono.Name = "tbtelefono"
        Me.tbtelefono.Size = New System.Drawing.Size(185, 21)
        Me.tbtelefono.TabIndex = 44
        '
        'tbfax
        '
        Me.tbfax.BackColor = System.Drawing.Color.White
        Me.tbfax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbfax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbfax.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbfax.Location = New System.Drawing.Point(578, 215)
        Me.tbfax.MaxLength = 255
        Me.tbfax.Name = "tbfax"
        Me.tbfax.Size = New System.Drawing.Size(185, 21)
        Me.tbfax.TabIndex = 45
        '
        'tbemail
        '
        Me.tbemail.BackColor = System.Drawing.Color.White
        Me.tbemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbemail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbemail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbemail.Location = New System.Drawing.Point(578, 252)
        Me.tbemail.MaxLength = 255
        Me.tbemail.Name = "tbemail"
        Me.tbemail.Size = New System.Drawing.Size(185, 21)
        Me.tbemail.TabIndex = 46
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(764, 363)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 47
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmCompania
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(921, 413)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.tbemail)
        Me.Controls.Add(Me.tbfax)
        Me.Controls.Add(Me.tbtelefono)
        Me.Controls.Add(Me.tbcodigop)
        Me.Controls.Add(Me.tbpais)
        Me.Controls.Add(Me.tbestado)
        Me.Controls.Add(Me.tbmunicipio)
        Me.Controls.Add(Me.tblocalidad)
        Me.Controls.Add(Me.tbentrecalles)
        Me.Controls.Add(Me.tbcolonia)
        Me.Controls.Add(Me.tbinterior)
        Me.Controls.Add(Me.tbexterior)
        Me.Controls.Add(Me.tbcalle)
        Me.Controls.Add(Me.tbrfc)
        Me.Controls.Add(Me.tbrazonsocial)
        Me.Controls.Add(Me.tbidcompania)
        Me.Controls.Add(Label16)
        Me.Controls.Add(Label15)
        Me.Controls.Add(Label14)
        Me.Controls.Add(Label13)
        Me.Controls.Add(Label12)
        Me.Controls.Add(Label11)
        Me.Controls.Add(Label10)
        Me.Controls.Add(Label8)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Clv_TxtLabel)
        Me.Controls.Add(Me.CONSERVICIOSBindingNavigator)
        Me.Name = "FrmCompania"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compañía"
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSERVICIOSBindingNavigator.ResumeLayout(False)
        Me.CONSERVICIOSBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CONSERVICIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbidcompania As System.Windows.Forms.TextBox
    Friend WithEvents tbrazonsocial As System.Windows.Forms.TextBox
    Friend WithEvents tbrfc As System.Windows.Forms.TextBox
    Friend WithEvents tbcalle As System.Windows.Forms.TextBox
    Friend WithEvents tbexterior As System.Windows.Forms.TextBox
    Friend WithEvents tbinterior As System.Windows.Forms.TextBox
    Friend WithEvents tbcolonia As System.Windows.Forms.TextBox
    Friend WithEvents tbentrecalles As System.Windows.Forms.TextBox
    Friend WithEvents tblocalidad As System.Windows.Forms.TextBox
    Friend WithEvents tbmunicipio As System.Windows.Forms.TextBox
    Friend WithEvents tbestado As System.Windows.Forms.TextBox
    Friend WithEvents tbpais As System.Windows.Forms.TextBox
    Friend WithEvents tbcodigop As System.Windows.Forms.TextBox
    Friend WithEvents tbtelefono As System.Windows.Forms.TextBox
    Friend WithEvents tbfax As System.Windows.Forms.TextBox
    Friend WithEvents tbemail As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
