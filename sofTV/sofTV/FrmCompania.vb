﻿Imports System.Data.SqlClient
Public Class FrmCompania

    Private Sub FrmCompania_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
        End If
        If opcion = "C" Then
            BindingNavigatorDeleteItem.Enabled = False
            CONSERVICIOSBindingNavigatorSaveItem.Enabled = False
            tbrazonsocial.Enabled = False
            tbrfc.Enabled = False
            tbcalle.Enabled = False
            tbentrecalles.Enabled = False
            tbexterior.Enabled = False
            tbinterior.Enabled = False
            tbtelefono.Enabled = False
            tbcolonia.Enabled = False
            tbfax.Enabled = False
            tbcodigop.Enabled = False
            tbemail.Enabled = False
            tblocalidad.Enabled = False
            tbmunicipio.Enabled = False
            tbestado.Enabled = False
            tbpais.Enabled = False
            llenacampos()
        End If
        If opcion = "M" Then
            BindingNavigatorDeleteItem.Enabled = True
            llenacampos()
        End If
    End Sub
    Private Sub llenacampos()
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "DameDatosCompaniaLong"
        Dim p1 As New SqlParameter("@idcompania", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = GloIdCompania
        comando.Parameters.Add(p1)
        Dim reader As SqlDataReader = comando.ExecuteReader()
        reader.Read()
        tbidcompania.Text = reader(0).ToString
        tbrazonsocial.Text = reader(1).ToString
        tbrfc.Text = reader(2).ToString
        tbcalle.Text = reader(3).ToString
        tbentrecalles.Text = reader(4).ToString
        tbexterior.Text = reader(5).ToString
        tbinterior.Text = reader(6).ToString
        tbtelefono.Text = reader(7).ToString
        tbcolonia.Text = reader(8).ToString
        tbfax.Text = reader(9).ToString
        tbcodigop.Text = reader(10).ToString
        tbemail.Text = reader(11).ToString
        tblocalidad.Text = reader(12).ToString
        tbmunicipio.Text = reader(13).ToString
        tbestado.Text = reader(14).ToString
        tbpais.Text = reader(15).ToString
        reader.Close()
        conexion.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "EliminaCompania"
        Dim p1 As New SqlParameter("@idcompania", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = GloIdCompania
        comando.Parameters.Add(p1)
        Dim bnddelete As Integer = comando.ExecuteScalar()
        If bnddelete = 0 Then
            MsgBox("Compañía eliminada exitosamente")
        ElseIf bnddelete = 1 Then
            MsgBox("La compañia ya tiene clientes asignados. No se puede eliminar")
        End If
        conexion.Close()
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        If (opcion = "N" Or opcion = "M") And (tbrazonsocial.Text = "" Or tbcalle.Text = "" Or tbcodigop.Text = "" Or tbcolonia.Text = "" Or tbestado.Text = "" Or tbexterior.Text = "" Or tbmunicipio.Text = "" Or tbpais.Text = "" Or tbrfc.Text = "") Then
            MsgBox("Aún faltan campos por completar")
            Exit Sub
        End If
        If opcion = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
            BaseII.CreateMyParameter("@razonsocial", SqlDbType.VarChar, tbrazonsocial.Text)
            BaseII.CreateMyParameter("@rfc", SqlDbType.VarChar, tbrfc.Text)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbcalle.Text)
            BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbentrecalles.Text)
            BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbexterior.Text)
            BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbinterior.Text)
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, tbtelefono.Text)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbcolonia.Text)
            BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, tbfax.Text)
            BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbcodigop.Text)
            BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbemail.Text)
            BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tblocalidad.Text)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbmunicipio.Text)
            BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbestado.Text)
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbpais.Text)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.Inserta("InsertaUpdateCompania")
            MsgBox("Compañía guardada con éxito")
        End If
        If opcion = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
            BaseII.CreateMyParameter("@razonsocial", SqlDbType.VarChar, tbrazonsocial.Text)
            BaseII.CreateMyParameter("@rfc", SqlDbType.VarChar, tbrfc.Text)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbcalle.Text)
            BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbentrecalles.Text)
            BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbexterior.Text)
            BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbinterior.Text)
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, tbtelefono.Text)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbcolonia.Text)
            BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, tbfax.Text)
            BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbcodigop.Text)
            BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbemail.Text)
            BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tblocalidad.Text)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbmunicipio.Text)
            BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbestado.Text)
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbpais.Text)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.Inserta("InsertaUpdateCompania")
            MsgBox("Datos guardados éxitosamente")
        End If
        Me.Close()
    End Sub
End Class