<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConsultaCobro2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.DataGridView2 = New System.Windows.Forms.DataGridView
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Costo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Button3 = New System.Windows.Forms.Button
        Me.LblImporte_Total = New System.Windows.Forms.Label
        Me.CNOPanel10 = New System.Windows.Forms.Panel
        Me.LabelGRan_Total = New System.Windows.Forms.Label
        Me.CMBLabel30 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.CMBLabel29 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.LabelSaldoAnterior = New System.Windows.Forms.Label
        Me.LblCredito_Apagar = New System.Windows.Forms.Label
        Me.LabelTotal = New System.Windows.Forms.Label
        Me.LabelIva = New System.Windows.Forms.Label
        Me.LabelSubTotal = New System.Windows.Forms.Label
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CNOPanel10.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.Costo})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView2.Location = New System.Drawing.Point(12, 29)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(635, 396)
        Me.DataGridView2.TabIndex = 517
        '
        'Descripcion
        '
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle2
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 350
        '
        'Costo
        '
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Costo.DefaultCellStyle = DataGridViewCellStyle3
        Me.Costo.HeaderText = "Costo"
        Me.Costo.Name = "Costo"
        Me.Costo.ReadOnly = True
        Me.Costo.Width = 230
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(871, 507)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(139, 42)
        Me.Button3.TabIndex = 524
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'LblImporte_Total
        '
        Me.LblImporte_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImporte_Total.Location = New System.Drawing.Point(446, 185)
        Me.LblImporte_Total.Name = "LblImporte_Total"
        Me.LblImporte_Total.Size = New System.Drawing.Size(146, 22)
        Me.LblImporte_Total.TabIndex = 527
        Me.LblImporte_Total.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CNOPanel10
        '
        Me.CNOPanel10.BackColor = System.Drawing.Color.LightGray
        Me.CNOPanel10.Controls.Add(Me.LabelGRan_Total)
        Me.CNOPanel10.Controls.Add(Me.CMBLabel30)
        Me.CNOPanel10.Location = New System.Drawing.Point(696, 246)
        Me.CNOPanel10.Name = "CNOPanel10"
        Me.CNOPanel10.Size = New System.Drawing.Size(284, 33)
        Me.CNOPanel10.TabIndex = 535
        '
        'LabelGRan_Total
        '
        Me.LabelGRan_Total.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelGRan_Total.Location = New System.Drawing.Point(115, 6)
        Me.LabelGRan_Total.Name = "LabelGRan_Total"
        Me.LabelGRan_Total.Size = New System.Drawing.Size(146, 22)
        Me.LabelGRan_Total.TabIndex = 523
        Me.LabelGRan_Total.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CMBLabel30
        '
        Me.CMBLabel30.AutoSize = True
        Me.CMBLabel30.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel30.Location = New System.Drawing.Point(3, 6)
        Me.CMBLabel30.Name = "CMBLabel30"
        Me.CMBLabel30.Size = New System.Drawing.Size(107, 20)
        Me.CMBLabel30.TabIndex = 522
        Me.CMBLabel30.Text = "Total a Pagar:"
        Me.CMBLabel30.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label29
        '
        Me.Label29.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(705, 189)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(101, 18)
        Me.Label29.TabIndex = 534
        Me.Label29.Text = "Saldo Anterior:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'CMBLabel29
        '
        Me.CMBLabel29.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel29.Location = New System.Drawing.Point(655, 218)
        Me.CMBLabel29.Name = "CMBLabel29"
        Me.CMBLabel29.Size = New System.Drawing.Size(151, 18)
        Me.CMBLabel29.TabIndex = 533
        Me.CMBLabel29.Text = "Credito por Redondeo :"
        Me.CMBLabel29.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label28
        '
        Me.Label28.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(762, 164)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(44, 18)
        Me.Label28.TabIndex = 532
        Me.Label28.Text = "Total:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label27
        '
        Me.Label27.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(758, 138)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(48, 18)
        Me.Label27.TabIndex = 531
        Me.Label27.Text = "I.V.A.:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(551, 112)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(255, 18)
        Me.Label26.TabIndex = 530
        Me.Label26.Text = "Total de Cargos :"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(485, 257)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 22)
        Me.Label1.TabIndex = 540
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelSaldoAnterior
        '
        Me.LabelSaldoAnterior.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSaldoAnterior.Location = New System.Drawing.Point(813, 185)
        Me.LabelSaldoAnterior.Name = "LabelSaldoAnterior"
        Me.LabelSaldoAnterior.Size = New System.Drawing.Size(146, 22)
        Me.LabelSaldoAnterior.TabIndex = 541
        Me.LabelSaldoAnterior.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LblCredito_Apagar
        '
        Me.LblCredito_Apagar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCredito_Apagar.Location = New System.Drawing.Point(813, 215)
        Me.LblCredito_Apagar.Name = "LblCredito_Apagar"
        Me.LblCredito_Apagar.Size = New System.Drawing.Size(146, 22)
        Me.LblCredito_Apagar.TabIndex = 539
        Me.LblCredito_Apagar.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTotal
        '
        Me.LabelTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTotal.Location = New System.Drawing.Point(812, 160)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(146, 22)
        Me.LabelTotal.TabIndex = 538
        Me.LabelTotal.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelIva
        '
        Me.LabelIva.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelIva.Location = New System.Drawing.Point(813, 134)
        Me.LabelIva.Name = "LabelIva"
        Me.LabelIva.Size = New System.Drawing.Size(146, 22)
        Me.LabelIva.TabIndex = 537
        Me.LabelIva.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelSubTotal
        '
        Me.LabelSubTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSubTotal.Location = New System.Drawing.Point(812, 108)
        Me.LabelSubTotal.Name = "LabelSubTotal"
        Me.LabelSubTotal.Size = New System.Drawing.Size(146, 22)
        Me.LabelSubTotal.TabIndex = 536
        Me.LabelSubTotal.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'FrmConsultaCobro2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1028, 561)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LabelSaldoAnterior)
        Me.Controls.Add(Me.LblCredito_Apagar)
        Me.Controls.Add(Me.LabelTotal)
        Me.Controls.Add(Me.LabelIva)
        Me.Controls.Add(Me.LabelSubTotal)
        Me.Controls.Add(Me.CNOPanel10)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.CMBLabel29)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.LblImporte_Total)
        Me.Controls.Add(Me.Button3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "FrmConsultaCobro2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consultar Cobro"
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CNOPanel10.ResumeLayout(False)
        Me.CNOPanel10.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents LblImporte_Total As System.Windows.Forms.Label
    Friend WithEvents CNOPanel10 As System.Windows.Forms.Panel
    Friend WithEvents LabelGRan_Total As System.Windows.Forms.Label
    Friend WithEvents CMBLabel30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LabelSaldoAnterior As System.Windows.Forms.Label
    Friend WithEvents LblCredito_Apagar As System.Windows.Forms.Label
    Friend WithEvents LabelTotal As System.Windows.Forms.Label
    Friend WithEvents LabelIva As System.Windows.Forms.Label
    Friend WithEvents LabelSubTotal As System.Windows.Forms.Label
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Costo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
