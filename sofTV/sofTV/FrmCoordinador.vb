﻿Public Class FrmCoordinador

    Dim clv_coordinador As Integer = 0
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        If opcionCoo = "N" And clv_coordinador = 0 Then

            If TextBox1.Text = "" Then
                MsgBox("Capture el Nombre del coordinador", MsgBoxStyle.Information)
                Exit Sub
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, TextBox1.Text, 50)
            BaseII.CreateMyParameter("@Fecha", SqlDbType.Date, DateTimePicker1.Value)
            BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, CheckBox1.Checked)
            BaseII.CreateMyParameter("@Cambaceo", SqlDbType.Bit, RadioButton1.Checked)
            BaseII.CreateMyParameter("@Sucursales", SqlDbType.Bit, RadioButton2.Checked)
            BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, ComboBox1.SelectedValue)
            BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.BigInt, ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@clv_coordinador", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("Usp_NueCoordinador")
            clv_coordinador = BaseII.dicoPar("@clv_coordinador")

            MsgBox("Guardado con exito", MsgBoxStyle.Information)

            Panel1.Enabled = True

        ElseIf opcionCoo = "M" And clv_coordinador > 0 Then

            If TextBox1.Text = "" Then
                MsgBox("Capture el Nombre del coordinador", MsgBoxStyle.Information)
                Exit Sub
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, TextBox1.Text, 50)
            BaseII.CreateMyParameter("@Fecha", SqlDbType.Date, DateTimePicker1.Value)
            BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, CheckBox1.Checked)
            BaseII.CreateMyParameter("@Cambaceo", SqlDbType.Bit, RadioButton1.Checked)
            BaseII.CreateMyParameter("@Sucursales", SqlDbType.Bit, RadioButton2.Checked)
            BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, ComboBox1.SelectedValue)
            BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.BigInt, ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@clv_coordinador", SqlDbType.BigInt, clv_coordinador)
            BaseII.Inserta("Usp_ModCoordinador")

            MsgBox("Guardado con exito", MsgBoxStyle.Information)
        End If




    End Sub

    Private Sub FrmCoordinador_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)

        clv_coordinador = GloIdCoordiandor

        If opcionCoo = "N" Then
            Panel1.Enabled = False
            clv_coordinador = 0
            RadioButton1.Checked = True
        End If

        llenaVendedorCoordinador()
        llenaSucursalCoordinador()
        llenalistboxs()

        If (opcionCoo = "M" Or opcionCoo = "C") And GloIdCoordiandor > 0 Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_coordinador", SqlDbType.BigInt, clv_coordinador)
            BaseII.CreateMyParameter("@Nombre", ParameterDirection.Output, SqlDbType.VarChar, 50)
            BaseII.CreateMyParameter("@Fecha", ParameterDirection.Output, SqlDbType.Date)
            BaseII.CreateMyParameter("@Activo", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@Cambaceo", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@Sucursales", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@clv_vendedor", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@clv_sucursal", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("Usp_ConCoordinador")
            TextBox1.Text = BaseII.dicoPar("@Nombre")
            DateTimePicker1.Value = BaseII.dicoPar("@Fecha")
            CheckBox1.Checked = CBool(BaseII.dicoPar("@Activo"))
            RadioButton1.Checked = CBool(BaseII.dicoPar("@Cambaceo"))
            RadioButton2.Checked = CBool(BaseII.dicoPar("@Sucursales"))
            ComboBox1.SelectedValue = CInt(BaseII.dicoPar("@clv_vendedor"))
            ComboBox2.SelectedValue = CInt(BaseII.dicoPar("@clv_sucursal"))

            Panel1.Enabled = True

            If opcionCoo = "C" Then
                TextBox1.Enabled = False
                CheckBox1.Enabled = False
                DateTimePicker1.Enabled = False
                ComboBox1.Enabled = False
                GroupBox1.Enabled = False
                Button1.Enabled = False
                agregar.Enabled = False
                quitar.Enabled = False
            End If

        End If

    End Sub

    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_Coordinador", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, 0)
        loquehay.DataSource = BaseII.ConsultaDT("Usp_RelCoordinadorVendedor")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_Coordinador", SqlDbType.Int, clv_coordinador)
        BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, 0)
        seleccion.DataSource = BaseII.ConsultaDT("Usp_RelCoordinadorVendedor")
    End Sub

    Private Sub llenaVendedorCoordinador()
        Try
            BaseII.limpiaParametros()
            ComboBox1.DataSource = BaseII.ConsultaDT("Usp_MuestraVendedorCoordinador")
            ComboBox1.DisplayMember = "Nombre"
            ComboBox1.ValueMember = "Clv_Vendedor"

            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaSucursalCoordinador()
        Try
            BaseII.limpiaParametros()
            ComboBox2.DataSource = BaseII.ConsultaDT("Usp_MuestraSucursalCoordinador")
            ComboBox2.DisplayMember = "Nombre"
            ComboBox2.ValueMember = "Clv_sucursal"

            If ComboBox2.Items.Count > 0 Then
                ComboBox2.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub agregar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub quitar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub agregar_Click_1(sender As Object, e As EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_Coordinador", SqlDbType.Int, clv_coordinador)
        BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.Inserta("Usp_RelCoordinadorVendedor")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click_1(sender As Object, e As EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_Coordinador", SqlDbType.Int, clv_coordinador)
        BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.Inserta("Usp_RelCoordinadorVendedor")
        llenalistboxs()
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            Label4.visible = False
            ComboBox1.SelectedValue = 0
            ComboBox1.Visible = False

            label8.visible = True
            ComboBox2.Visible = True

            Me.Size = New Size(761, 219)
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            label4.visible = True
            ComboBox1.Visible = True

            label8.visible = False
            ComboBox2.SelectedValue = 0
            ComboBox2.Visible = False

            Me.Size = New Size(761, 591)
        End If
    End Sub
End Class