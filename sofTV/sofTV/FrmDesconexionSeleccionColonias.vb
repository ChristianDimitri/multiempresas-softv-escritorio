﻿Public Class FrmDesconexionSeleccionColonias

    Private Sub MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(ByVal CLV_SESSION As Integer, ByVal OP As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        If OP = 0 Then dgvIzq.DataSource = BaseII.ConsultaDT("MUESTRATBLDESCONEXIONSELECCIONCOLONIAS")
        If OP = 1 Then dgvDer.DataSource = BaseII.ConsultaDT("MUESTRATBLDESCONEXIONSELECCIONCOLONIAS")
    End Sub

    Private Sub INSERTATBLDESCONEXIONSELECCIONCOLONIAS(ByVal CLV_SESSION As Integer, ByVal OP As Integer, ByVal CLV_CIUDAD As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_COLONIA", SqlDbType.Int, CLV_CIUDAD)
        BaseII.Inserta("INSERTATBLDESCONEXIONSELECCIONCOLONIAS")
    End Sub

    Private Sub ELIMINATBLDESCONEXIONSELECCIONCOLONIAS(ByVal CLV_SESSION As Integer, ByVal OP As Integer, ByVal CLV_CIUDAD As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_COLONIA", SqlDbType.Int, CLV_CIUDAD)
        BaseII.Inserta("ELIMINATBLDESCONEXIONSELECCIONCOLONIAS")
    End Sub

    Private Sub DameClv_Session()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        eClv_Session = 0
        eClv_Session = CInt(BaseII.dicoPar("@CLV_SESSION").ToString)
    End Sub

    Private Sub GENERAROrdenesDesconexion(ByVal CLV_SESSION As Integer, ByVal CLV_USUARIO As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, CLV_USUARIO, 10)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.ProcedimientoOutPut("GENERAROrdenesDesconexion")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ")
    End Sub

    Private Sub MUESTRAMensajeGeneracionOrdenes()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("MUESTRAMensajeGeneracionOrdenes")
        lbMensaje.Text = BaseII.dicoPar("@MSJ")
    End Sub

    Private Sub FrmDesconexionSeleccionCiudades_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
        MUESTRAMensajeGeneracionOrdenes()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        If dgvIzq.SelectedCells(0).Value = 0 Then
            Exit Sub
        End If
        INSERTATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0, dgvIzq.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        INSERTATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0, dgvDer.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1, dgvDer.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnGenerar.Click
        Try
            If dgvDer.RowCount = 0 Then
                MessageBox.Show("Selecciona una Colonia.")
                Exit Sub
            End If
            GENERAROrdenesDesconexion(eClv_Session, GloUsuario)
            MessageBox.Show(eMsj)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class