﻿Public Class FrmDetalleClienteBuro

    Public Contrato As Long

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub FrmDetalleClienteBuro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenarDatosCliente()

        UspDesactivaBotones(Me, Me.Name)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub llenarDatosCliente()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
        Dim dt As DataTable = BaseII.ConsultaDT("USP_GetClienteListadoBuro")
        txtContrato.Text = dt.Rows(0)("Contrato").ToString()
        txtNombre.Text = dt.Rows(0)("NOMBRE").ToString()
        txtDe.Text = dt.Rows(0)("RangoIncial").ToString()
        txtA.Text = dt.Rows(0)("RangoFinal").ToString()
        txtClvBuro.Text = dt.Rows(0)("Clv_Buro").ToString()
        txtFechaReporte.Text = CDate(dt.Rows(0)("FechaReporte").ToString()).ToShortDateString()
        If (dt.Rows(0)("FechaRetiro").ToString().Trim.Length <> 0) Then
            txtFechaRetiro.Text = CDate(dt.Rows(0)("FechaRetiro").ToString()).ToShortDateString()
        Else
            txtFechaRetiro.Text = ""
        End If
        txtAdeudoPorMensualidades.Text = dt.Rows(0)("Rentas").ToString()
        txtAdeudoPorPagare.Text = dt.Rows(0)("Pagare").ToString()
        txtTotal.Text = dt.Rows(0)("Total").ToString()
        If (txtFechaRetiro.Text.Trim.Length > 0) Then
            btnQuitarBuro.Visible = False
        End If
    End Sub

    Private Sub btnQuitarBuro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitarBuro.Click
        If (MessageBox.Show("Seguro que desea Sacar a este cliente del Listado de Buro De Credito", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@Usuario", SqlDbType.VarChar, GloUsuario)
            BaseII.CreateMyParameter("@Sistema", SqlDbType.VarChar, "Softv")
            BaseII.CreateMyParameter("@Pantalla", SqlDbType.VarChar, "FrmDetalleClienteBuro")
            BaseII.CreateMyParameter("@Mensaje", SqlDbType.VarChar, "El Usuario Del Sistema " + GloUsuario + " Quito de la lista de Buro al  Cliente Con el Contrato  " + Contrato.ToString() + " y haciendo la corrección en el listado de Buro a 0 pesos en el Adeudo")
            BaseII.CreateMyParameter("@clv_Ciudad", SqlDbType.VarChar, LocClv_Ciudad)
            BaseII.Inserta("USP_QuitarClienteDeBuroGuardaBitacora")
            MessageBox.Show("Cliente Retirado De Buro Exitosamente")
        End If

        Me.Close()
    End Sub
End Class