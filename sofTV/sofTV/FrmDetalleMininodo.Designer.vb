﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetalleMininodo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.TBObs = New System.Windows.Forms.TextBox()
        Me.TBBaja = New System.Windows.Forms.TextBox()
        Me.CMBTextBox28 = New System.Windows.Forms.TextBox()
        Me.CMBoxStatus = New System.Windows.Forms.ComboBox()
        Me.TBInstalacion = New System.Windows.Forms.TextBox()
        Me.CMBLabelAparato = New System.Windows.Forms.Label()
        Me.LblMac = New System.Windows.Forms.TextBox()
        Me.CMBLabel53 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(363, 105)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 15)
        Me.Label9.TabIndex = 99
        Me.Label9.Text = "Baja:"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label31.Location = New System.Drawing.Point(31, 82)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(109, 15)
        Me.Label31.TabIndex = 94
        Me.Label31.Text = "Observaciones :"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label35.Location = New System.Drawing.Point(328, 78)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(75, 15)
        Me.Label35.TabIndex = 92
        Me.Label35.Text = "Activación:"
        '
        'TBObs
        '
        Me.TBObs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBObs.Location = New System.Drawing.Point(34, 105)
        Me.TBObs.MaxLength = 250
        Me.TBObs.Multiline = True
        Me.TBObs.Name = "TBObs"
        Me.TBObs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBObs.Size = New System.Drawing.Size(272, 86)
        Me.TBObs.TabIndex = 95
        '
        'TBBaja
        '
        Me.TBBaja.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBBaja.CausesValidation = False
        Me.TBBaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBBaja.Location = New System.Drawing.Point(409, 103)
        Me.TBBaja.Name = "TBBaja"
        Me.TBBaja.ReadOnly = True
        Me.TBBaja.Size = New System.Drawing.Size(100, 21)
        Me.TBBaja.TabIndex = 98
        Me.TBBaja.TabStop = False
        '
        'CMBTextBox28
        '
        Me.CMBTextBox28.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox28.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox28.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox28.Location = New System.Drawing.Point(324, 44)
        Me.CMBTextBox28.Name = "CMBTextBox28"
        Me.CMBTextBox28.ReadOnly = True
        Me.CMBTextBox28.Size = New System.Drawing.Size(189, 19)
        Me.CMBTextBox28.TabIndex = 96
        Me.CMBTextBox28.TabStop = False
        Me.CMBTextBox28.Text = "Fechas de "
        Me.CMBTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBoxStatus
        '
        Me.CMBoxStatus.Enabled = False
        Me.CMBoxStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CMBoxStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBoxStatus.FormattingEnabled = True
        Me.CMBoxStatus.Location = New System.Drawing.Point(161, 44)
        Me.CMBoxStatus.Name = "CMBoxStatus"
        Me.CMBoxStatus.Size = New System.Drawing.Size(145, 23)
        Me.CMBoxStatus.TabIndex = 97
        Me.CMBoxStatus.TabStop = False
        '
        'TBInstalacion
        '
        Me.TBInstalacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBInstalacion.CausesValidation = False
        Me.TBInstalacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBInstalacion.Location = New System.Drawing.Point(409, 76)
        Me.TBInstalacion.Name = "TBInstalacion"
        Me.TBInstalacion.ReadOnly = True
        Me.TBInstalacion.Size = New System.Drawing.Size(100, 21)
        Me.TBInstalacion.TabIndex = 93
        Me.TBInstalacion.TabStop = False
        '
        'CMBLabelAparato
        '
        Me.CMBLabelAparato.AutoSize = True
        Me.CMBLabelAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelAparato.ForeColor = System.Drawing.Color.OrangeRed
        Me.CMBLabelAparato.Location = New System.Drawing.Point(28, 12)
        Me.CMBLabelAparato.Name = "CMBLabelAparato"
        Me.CMBLabelAparato.Size = New System.Drawing.Size(126, 18)
        Me.CMBLabelAparato.TabIndex = 102
        Me.CMBLabelAparato.Text = "Datos Aparato :"
        Me.CMBLabelAparato.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblMac
        '
        Me.LblMac.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LblMac.CausesValidation = False
        Me.LblMac.Enabled = False
        Me.LblMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMac.Location = New System.Drawing.Point(179, 12)
        Me.LblMac.Multiline = True
        Me.LblMac.Name = "LblMac"
        Me.LblMac.ReadOnly = True
        Me.LblMac.Size = New System.Drawing.Size(231, 20)
        Me.LblMac.TabIndex = 100
        '
        'CMBLabel53
        '
        Me.CMBLabel53.AutoSize = True
        Me.CMBLabel53.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel53.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel53.Location = New System.Drawing.Point(31, 48)
        Me.CMBLabel53.Name = "CMBLabel53"
        Me.CMBLabel53.Size = New System.Drawing.Size(108, 15)
        Me.CMBLabel53.TabIndex = 101
        Me.CMBLabel53.Text = "Status Aparato :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(373, 158)
        Me.Button5.MaximumSize = New System.Drawing.Size(136, 33)
        Me.Button5.MinimumSize = New System.Drawing.Size(136, 33)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 103
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmDetalleMininodo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(546, 225)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CMBTextBox28)
        Me.Controls.Add(Me.TBInstalacion)
        Me.Controls.Add(Me.CMBLabelAparato)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.CMBLabel53)
        Me.Controls.Add(Me.TBObs)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TBBaja)
        Me.Controls.Add(Me.LblMac)
        Me.Controls.Add(Me.CMBoxStatus)
        Me.Controls.Add(Me.Label35)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmDetalleMininodo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detalle Aparato"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TBObs As System.Windows.Forms.TextBox
    Friend WithEvents TBBaja As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents CMBoxStatus As System.Windows.Forms.ComboBox
    Friend WithEvents TBInstalacion As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabelAparato As System.Windows.Forms.Label
    Friend WithEvents LblMac As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel53 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
End Class
