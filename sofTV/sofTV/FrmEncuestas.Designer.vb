<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEncuestas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEncuestas))
        Me.TreeViewIzq = New System.Windows.Forms.TreeView
        Me.TreeViewDer = New System.Windows.Forms.TreeView
        Me.ButtonInsertarUno = New System.Windows.Forms.Button
        Me.ButtonInsertarTodos = New System.Windows.Forms.Button
        Me.ButtonEliminarUno = New System.Windows.Forms.Button
        Me.ButtonEliminarTodos = New System.Windows.Forms.Button
        Me.TextBoxNombre = New System.Windows.Forms.TextBox
        Me.TextBoxDescripcion = New System.Windows.Forms.TextBox
        Me.CheckBoxActiva = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.BindingNavigatorEncuestas = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.TSBEliminar = New System.Windows.Forms.ToolStripButton
        Me.TSBGuardar = New System.Windows.Forms.ToolStripButton
        Me.TSBNuevo = New System.Windows.Forms.ToolStripButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.ButtonSalir = New System.Windows.Forms.Button
        CType(Me.BindingNavigatorEncuestas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigatorEncuestas.SuspendLayout()
        Me.SuspendLayout()
        '
        'TreeViewIzq
        '
        Me.TreeViewIzq.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewIzq.Location = New System.Drawing.Point(29, 175)
        Me.TreeViewIzq.Name = "TreeViewIzq"
        Me.TreeViewIzq.Size = New System.Drawing.Size(430, 459)
        Me.TreeViewIzq.TabIndex = 0
        '
        'TreeViewDer
        '
        Me.TreeViewDer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewDer.Location = New System.Drawing.Point(565, 175)
        Me.TreeViewDer.Name = "TreeViewDer"
        Me.TreeViewDer.Size = New System.Drawing.Size(430, 459)
        Me.TreeViewDer.TabIndex = 1
        '
        'ButtonInsertarUno
        '
        Me.ButtonInsertarUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonInsertarUno.Location = New System.Drawing.Point(481, 318)
        Me.ButtonInsertarUno.Name = "ButtonInsertarUno"
        Me.ButtonInsertarUno.Size = New System.Drawing.Size(67, 23)
        Me.ButtonInsertarUno.TabIndex = 2
        Me.ButtonInsertarUno.Text = ">"
        Me.ButtonInsertarUno.UseVisualStyleBackColor = True
        '
        'ButtonInsertarTodos
        '
        Me.ButtonInsertarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonInsertarTodos.Location = New System.Drawing.Point(481, 347)
        Me.ButtonInsertarTodos.Name = "ButtonInsertarTodos"
        Me.ButtonInsertarTodos.Size = New System.Drawing.Size(67, 23)
        Me.ButtonInsertarTodos.TabIndex = 3
        Me.ButtonInsertarTodos.Text = ">>"
        Me.ButtonInsertarTodos.UseVisualStyleBackColor = True
        '
        'ButtonEliminarUno
        '
        Me.ButtonEliminarUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminarUno.Location = New System.Drawing.Point(481, 420)
        Me.ButtonEliminarUno.Name = "ButtonEliminarUno"
        Me.ButtonEliminarUno.Size = New System.Drawing.Size(67, 23)
        Me.ButtonEliminarUno.TabIndex = 4
        Me.ButtonEliminarUno.Text = "<"
        Me.ButtonEliminarUno.UseVisualStyleBackColor = True
        '
        'ButtonEliminarTodos
        '
        Me.ButtonEliminarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminarTodos.Location = New System.Drawing.Point(481, 449)
        Me.ButtonEliminarTodos.Name = "ButtonEliminarTodos"
        Me.ButtonEliminarTodos.Size = New System.Drawing.Size(67, 23)
        Me.ButtonEliminarTodos.TabIndex = 5
        Me.ButtonEliminarTodos.Text = "<<"
        Me.ButtonEliminarTodos.UseVisualStyleBackColor = True
        '
        'TextBoxNombre
        '
        Me.TextBoxNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNombre.Location = New System.Drawing.Point(376, 48)
        Me.TextBoxNombre.MaxLength = 50
        Me.TextBoxNombre.Name = "TextBoxNombre"
        Me.TextBoxNombre.Size = New System.Drawing.Size(233, 21)
        Me.TextBoxNombre.TabIndex = 6
        '
        'TextBoxDescripcion
        '
        Me.TextBoxDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxDescripcion.Location = New System.Drawing.Point(376, 74)
        Me.TextBoxDescripcion.MaxLength = 250
        Me.TextBoxDescripcion.Name = "TextBoxDescripcion"
        Me.TextBoxDescripcion.Size = New System.Drawing.Size(472, 21)
        Me.TextBoxDescripcion.TabIndex = 7
        '
        'CheckBoxActiva
        '
        Me.CheckBoxActiva.AutoSize = True
        Me.CheckBoxActiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxActiva.Location = New System.Drawing.Point(325, 112)
        Me.CheckBoxActiva.Name = "CheckBoxActiva"
        Me.CheckBoxActiva.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBoxActiva.Size = New System.Drawing.Size(63, 19)
        Me.CheckBoxActiva.TabIndex = 8
        Me.CheckBoxActiva.Text = "Activa"
        Me.CheckBoxActiva.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(312, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 15)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Nombre"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(287, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 15)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Descripción"
        '
        'BindingNavigatorEncuestas
        '
        Me.BindingNavigatorEncuestas.AddNewItem = Nothing
        Me.BindingNavigatorEncuestas.CountItem = Nothing
        Me.BindingNavigatorEncuestas.DeleteItem = Nothing
        Me.BindingNavigatorEncuestas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorEncuestas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSBEliminar, Me.TSBGuardar, Me.TSBNuevo})
        Me.BindingNavigatorEncuestas.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigatorEncuestas.MoveFirstItem = Nothing
        Me.BindingNavigatorEncuestas.MoveLastItem = Nothing
        Me.BindingNavigatorEncuestas.MoveNextItem = Nothing
        Me.BindingNavigatorEncuestas.MovePreviousItem = Nothing
        Me.BindingNavigatorEncuestas.Name = "BindingNavigatorEncuestas"
        Me.BindingNavigatorEncuestas.PositionItem = Nothing
        Me.BindingNavigatorEncuestas.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigatorEncuestas.Size = New System.Drawing.Size(1016, 25)
        Me.BindingNavigatorEncuestas.TabIndex = 27
        Me.BindingNavigatorEncuestas.Text = "BindingNavigator1"
        '
        'TSBEliminar
        '
        Me.TSBEliminar.Image = CType(resources.GetObject("TSBEliminar.Image"), System.Drawing.Image)
        Me.TSBEliminar.Name = "TSBEliminar"
        Me.TSBEliminar.RightToLeftAutoMirrorImage = True
        Me.TSBEliminar.Size = New System.Drawing.Size(88, 22)
        Me.TSBEliminar.Text = "&ELIMINAR"
        '
        'TSBGuardar
        '
        Me.TSBGuardar.Image = CType(resources.GetObject("TSBGuardar.Image"), System.Drawing.Image)
        Me.TSBGuardar.Name = "TSBGuardar"
        Me.TSBGuardar.Size = New System.Drawing.Size(88, 22)
        Me.TSBGuardar.Text = "&GUARDAR"
        '
        'TSBNuevo
        '
        Me.TSBNuevo.Image = CType(resources.GetObject("TSBNuevo.Image"), System.Drawing.Image)
        Me.TSBNuevo.Name = "TSBNuevo"
        Me.TSBNuevo.RightToLeftAutoMirrorImage = True
        Me.TSBNuevo.Size = New System.Drawing.Size(67, 22)
        Me.TSBNuevo.Text = "&NUEVO"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(26, 157)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(149, 15)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Preguntas Sin Asignar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(562, 157)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(142, 15)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Preguntas Asignadas"
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(868, 686)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(136, 36)
        Me.ButtonSalir.TabIndex = 30
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'FrmEncuestas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.BindingNavigatorEncuestas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CheckBoxActiva)
        Me.Controls.Add(Me.TextBoxDescripcion)
        Me.Controls.Add(Me.TextBoxNombre)
        Me.Controls.Add(Me.ButtonEliminarTodos)
        Me.Controls.Add(Me.ButtonEliminarUno)
        Me.Controls.Add(Me.ButtonInsertarTodos)
        Me.Controls.Add(Me.ButtonInsertarUno)
        Me.Controls.Add(Me.TreeViewDer)
        Me.Controls.Add(Me.TreeViewIzq)
        Me.Name = "FrmEncuestas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Encuestas"
        CType(Me.BindingNavigatorEncuestas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigatorEncuestas.ResumeLayout(False)
        Me.BindingNavigatorEncuestas.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TreeViewIzq As System.Windows.Forms.TreeView
    Friend WithEvents TreeViewDer As System.Windows.Forms.TreeView
    Friend WithEvents ButtonInsertarUno As System.Windows.Forms.Button
    Friend WithEvents ButtonInsertarTodos As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminarUno As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminarTodos As System.Windows.Forms.Button
    Friend WithEvents TextBoxNombre As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxActiva As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigatorEncuestas As System.Windows.Forms.BindingNavigator
    Friend WithEvents TSBEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
End Class
