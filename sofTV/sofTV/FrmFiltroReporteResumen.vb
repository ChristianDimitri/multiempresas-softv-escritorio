﻿Public Class FrmFiltroReporteResumen

    Private Sub FrmFiltroReporteResumen_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, 0)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
    End Sub
    Private Sub FrmFiltroReporteResumen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenalbtodos()
        eOpPPE = 6
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub llenalbtodos()
        eClv_Session = LocClv_session
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        lbtodos.DataSource = BaseII.ConsultaDT("MuestraTrabajosResumen")
    End Sub
    Private Sub llenalbseleccion()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        lbseleccion.DataSource = BaseII.ConsultaDT("MuestraSeleccionTrabajos")
    End Sub
    Private Sub btSeleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionaUno.Click
        If lbtodos.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, lbtodos.SelectedValue)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()
    End Sub

    Private Sub btDeseleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDeseleccionaUno.Click
        If lbseleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, lbseleccion.SelectedValue)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()
    End Sub

    Private Sub btSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionarTodos.Click
        If lbtodos.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, 0)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If lbseleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, 0)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()
    End Sub

    Private Sub rbsolicitud_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbsolicitud.CheckedChanged
        If rbsolicitud.Checked Then
            eOpPPE = 6
        End If
    End Sub

    Private Sub dtini_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtini.ValueChanged
        If dtini.Value > dtfin.Value Then
            MsgBox("La fecha inicial no puede ser mayor a la fecha final.")
            dtini.Value = Today
        End If
    End Sub

    Private Sub dtfin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtfin.ValueChanged
        If dtfin.Value < dtini.Value Then
            MsgBox("La fecha final no puede ser menor a la fecha inicial.")
            dtfin.Value = Today
        End If
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        If RadioButton2.Checked = True Then
            Rstatus = 0
        ElseIf RadioButton1.Checked = True Then
            Rstatus = 1
        End If

        eFechaIniPPE = dtini.Value
        eFechaFinPPE = dtfin.Value
        FrmImprimirPPE.Show()
    End Sub

    Private Sub rbejecucion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbejecucion.CheckedChanged
        If rbejecucion.Checked Then
            eOpPPE = 7
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            rbejecucion.Enabled = True
        Else
            rbejecucion.Enabled = False
            rbejecucion.Checked = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            rbejecucion.Checked = False
            rbejecucion.Enabled = False
        Else
            rbejecucion.Enabled = True
        End If
    End Sub
End Class