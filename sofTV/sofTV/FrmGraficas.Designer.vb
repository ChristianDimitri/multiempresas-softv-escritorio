<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGraficas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.Dame_clv_session_ReportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_ReportesTableAdapter = New sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter()
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.ConVentasVendedoresProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConVentasVendedoresProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConVentasVendedoresProTableAdapter()
        Me.NombreListBox = New System.Windows.Forms.ListBox()
        Me.ConVentasVendedoresTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConVentasVendedoresTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConVentasVendedoresTmpTableAdapter()
        Me.NombreListBox1 = New System.Windows.Forms.ListBox()
        Me.InsertarVendedorTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarVendedorTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarVendedorTmpTableAdapter()
        Me.BorrarVendedorTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarVendedorTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarVendedorTmpTableAdapter()
        Me.ConServiciosProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConServiciosProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConServiciosProTableAdapter()
        Me.ConServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConServiciosTmpTableAdapter()
        Me.InsertarServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarServiciosTmpTableAdapter()
        Me.BorrarServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarServiciosTmpTableAdapter()
        Me.ConSucursalesProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSucursalesProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSucursalesProTableAdapter()
        Me.ConSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSucursalesTmpTableAdapter()
        Me.InsertarSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarSucursalesTmpTableAdapter()
        Me.BorrarSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarSucursalesTmpTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.NombreListBox3 = New System.Windows.Forms.ListBox()
        Me.NombreListBox2 = New System.Windows.Forms.ListBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.agregartodociudad = New System.Windows.Forms.Button()
        Me.agregarciudad = New System.Windows.Forms.Button()
        Me.quitar = New System.Windows.Forms.Button()
        Me.quitartodociudad = New System.Windows.Forms.Button()
        Me.seleccionciudad = New System.Windows.Forms.ListBox()
        Me.loquehayciudad = New System.Windows.Forms.ListBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.agregartodocompania = New System.Windows.Forms.Button()
        Me.agregarcompania = New System.Windows.Forms.Button()
        Me.quitarcompania = New System.Windows.Forms.Button()
        Me.quitartodocompania = New System.Windows.Forms.Button()
        Me.seleccioncompania = New System.Windows.Forms.ListBox()
        Me.loquehaycompania = New System.Windows.Forms.ListBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.DescripcionListBox1 = New System.Windows.Forms.ListBox()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.DescripcionListBox = New System.Windows.Forms.ListBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConVentasVendedoresProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConVentasVendedoresTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSucursalesProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clv_session_ReportesBindingSource
        '
        Me.Dame_clv_session_ReportesBindingSource.DataMember = "Dame_clv_session_Reportes"
        Me.Dame_clv_session_ReportesBindingSource.DataSource = Me.DataSetEric2
        '
        'Dame_clv_session_ReportesTableAdapter
        '
        Me.Dame_clv_session_ReportesTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(24, 29)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(305, 24)
        Me.ConceptoComboBox.TabIndex = 0
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'ConVentasVendedoresProBindingSource
        '
        Me.ConVentasVendedoresProBindingSource.DataMember = "ConVentasVendedoresPro"
        Me.ConVentasVendedoresProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConVentasVendedoresProTableAdapter
        '
        Me.ConVentasVendedoresProTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox
        '
        Me.NombreListBox.DataSource = Me.ConVentasVendedoresProBindingSource
        Me.NombreListBox.DisplayMember = "Nombre"
        Me.NombreListBox.FormattingEnabled = True
        Me.NombreListBox.ItemHeight = 16
        Me.NombreListBox.Location = New System.Drawing.Point(13, 22)
        Me.NombreListBox.Name = "NombreListBox"
        Me.NombreListBox.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox.TabIndex = 4
        Me.NombreListBox.ValueMember = "Clv_Vendedor"
        '
        'ConVentasVendedoresTmpBindingSource
        '
        Me.ConVentasVendedoresTmpBindingSource.DataMember = "ConVentasVendedoresTmp"
        Me.ConVentasVendedoresTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConVentasVendedoresTmpTableAdapter
        '
        Me.ConVentasVendedoresTmpTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox1
        '
        Me.NombreListBox1.DataSource = Me.ConVentasVendedoresTmpBindingSource
        Me.NombreListBox1.DisplayMember = "Nombre"
        Me.NombreListBox1.FormattingEnabled = True
        Me.NombreListBox1.ItemHeight = 16
        Me.NombreListBox1.Location = New System.Drawing.Point(415, 19)
        Me.NombreListBox1.Name = "NombreListBox1"
        Me.NombreListBox1.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox1.TabIndex = 6
        Me.NombreListBox1.ValueMember = "Clv_Vendedor"
        '
        'InsertarVendedorTmpBindingSource
        '
        Me.InsertarVendedorTmpBindingSource.DataMember = "InsertarVendedorTmp"
        Me.InsertarVendedorTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarVendedorTmpTableAdapter
        '
        Me.InsertarVendedorTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarVendedorTmpBindingSource
        '
        Me.BorrarVendedorTmpBindingSource.DataMember = "BorrarVendedorTmp"
        Me.BorrarVendedorTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarVendedorTmpTableAdapter
        '
        Me.BorrarVendedorTmpTableAdapter.ClearBeforeFill = True
        '
        'ConServiciosProBindingSource
        '
        Me.ConServiciosProBindingSource.DataMember = "ConServiciosPro"
        Me.ConServiciosProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConServiciosProTableAdapter
        '
        Me.ConServiciosProTableAdapter.ClearBeforeFill = True
        '
        'ConServiciosTmpBindingSource
        '
        Me.ConServiciosTmpBindingSource.DataMember = "ConServiciosTmp"
        Me.ConServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConServiciosTmpTableAdapter
        '
        Me.ConServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'InsertarServiciosTmpBindingSource
        '
        Me.InsertarServiciosTmpBindingSource.DataMember = "InsertarServiciosTmp"
        Me.InsertarServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarServiciosTmpTableAdapter
        '
        Me.InsertarServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarServiciosTmpBindingSource
        '
        Me.BorrarServiciosTmpBindingSource.DataMember = "BorrarServiciosTmp"
        Me.BorrarServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarServiciosTmpTableAdapter
        '
        Me.BorrarServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'ConSucursalesProBindingSource
        '
        Me.ConSucursalesProBindingSource.DataMember = "ConSucursalesPro"
        Me.ConSucursalesProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSucursalesProTableAdapter
        '
        Me.ConSucursalesProTableAdapter.ClearBeforeFill = True
        '
        'ConSucursalesTmpBindingSource
        '
        Me.ConSucursalesTmpBindingSource.DataMember = "ConSucursalesTmp"
        Me.ConSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSucursalesTmpTableAdapter
        '
        Me.ConSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'InsertarSucursalesTmpBindingSource
        '
        Me.InsertarSucursalesTmpBindingSource.DataMember = "InsertarSucursalesTmp"
        Me.InsertarSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarSucursalesTmpTableAdapter
        '
        Me.InsertarSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarSucursalesTmpBindingSource
        '
        Me.BorrarSucursalesTmpBindingSource.DataMember = "BorrarSucursalesTmp"
        Me.BorrarSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarSucursalesTmpTableAdapter
        '
        Me.BorrarSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(307, 104)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(307, 133)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(307, 206)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 17
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(307, 235)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 18
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(308, 110)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 19
        Me.Button5.Text = ">"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(308, 139)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 20
        Me.Button6.Text = ">>"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(308, 201)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 21
        Me.Button7.Text = "<"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(308, 230)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 22
        Me.Button8.Text = "<<"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(39, 29)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(104, 20)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Sucursales"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(182, 29)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(147, 20)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "Depto. de Ventas"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'NombreListBox3
        '
        Me.NombreListBox3.DataSource = Me.ConSucursalesTmpBindingSource
        Me.NombreListBox3.DisplayMember = "Nombre"
        Me.NombreListBox3.FormattingEnabled = True
        Me.NombreListBox3.ItemHeight = 16
        Me.NombreListBox3.Location = New System.Drawing.Point(407, 24)
        Me.NombreListBox3.Name = "NombreListBox3"
        Me.NombreListBox3.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox3.TabIndex = 18
        Me.NombreListBox3.ValueMember = "Clv_Sucursal"
        '
        'NombreListBox2
        '
        Me.NombreListBox2.DataSource = Me.ConSucursalesProBindingSource
        Me.NombreListBox2.DisplayMember = "Nombre"
        Me.NombreListBox2.FormattingEnabled = True
        Me.NombreListBox2.ItemHeight = 16
        Me.NombreListBox2.Location = New System.Drawing.Point(19, 24)
        Me.NombreListBox2.Name = "NombreListBox2"
        Me.NombreListBox2.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox2.TabIndex = 17
        Me.NombreListBox2.ValueMember = "Clv_Sucursal"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Enabled = False
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(10, 207)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(809, 479)
        Me.TabControl1.TabIndex = 5
        Me.TabControl1.TabStop = False
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.agregartodociudad)
        Me.TabPage3.Controls.Add(Me.agregarciudad)
        Me.TabPage3.Controls.Add(Me.quitar)
        Me.TabPage3.Controls.Add(Me.quitartodociudad)
        Me.TabPage3.Controls.Add(Me.seleccionciudad)
        Me.TabPage3.Controls.Add(Me.loquehayciudad)
        Me.TabPage3.Location = New System.Drawing.Point(4, 25)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(801, 450)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Ciudades"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'agregartodociudad
        '
        Me.agregartodociudad.Location = New System.Drawing.Point(363, 189)
        Me.agregartodociudad.Name = "agregartodociudad"
        Me.agregartodociudad.Size = New System.Drawing.Size(75, 23)
        Me.agregartodociudad.TabIndex = 139
        Me.agregartodociudad.Text = ">>"
        Me.agregartodociudad.UseVisualStyleBackColor = True
        '
        'agregarciudad
        '
        Me.agregarciudad.Location = New System.Drawing.Point(363, 160)
        Me.agregarciudad.Name = "agregarciudad"
        Me.agregarciudad.Size = New System.Drawing.Size(75, 23)
        Me.agregarciudad.TabIndex = 138
        Me.agregarciudad.Text = ">"
        Me.agregarciudad.UseVisualStyleBackColor = True
        '
        'quitar
        '
        Me.quitar.Location = New System.Drawing.Point(363, 251)
        Me.quitar.Name = "quitar"
        Me.quitar.Size = New System.Drawing.Size(75, 23)
        Me.quitar.TabIndex = 140
        Me.quitar.Text = "<"
        Me.quitar.UseVisualStyleBackColor = True
        '
        'quitartodociudad
        '
        Me.quitartodociudad.Location = New System.Drawing.Point(363, 280)
        Me.quitartodociudad.Name = "quitartodociudad"
        Me.quitartodociudad.Size = New System.Drawing.Size(75, 23)
        Me.quitartodociudad.TabIndex = 141
        Me.quitartodociudad.Text = "<<"
        Me.quitartodociudad.UseVisualStyleBackColor = True
        '
        'seleccionciudad
        '
        Me.seleccionciudad.DisplayMember = "Nombre"
        Me.seleccionciudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.seleccionciudad.FormattingEnabled = True
        Me.seleccionciudad.ItemHeight = 16
        Me.seleccionciudad.Location = New System.Drawing.Point(480, 95)
        Me.seleccionciudad.Name = "seleccionciudad"
        Me.seleccionciudad.Size = New System.Drawing.Size(250, 260)
        Me.seleccionciudad.TabIndex = 137
        Me.seleccionciudad.ValueMember = "Clv_Ciudad"
        '
        'loquehayciudad
        '
        Me.loquehayciudad.DisplayMember = "Nombre"
        Me.loquehayciudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.loquehayciudad.FormattingEnabled = True
        Me.loquehayciudad.ItemHeight = 16
        Me.loquehayciudad.Location = New System.Drawing.Point(70, 95)
        Me.loquehayciudad.Name = "loquehayciudad"
        Me.loquehayciudad.Size = New System.Drawing.Size(250, 260)
        Me.loquehayciudad.TabIndex = 136
        Me.loquehayciudad.ValueMember = "Clv_Ciudad"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.agregartodocompania)
        Me.TabPage4.Controls.Add(Me.agregarcompania)
        Me.TabPage4.Controls.Add(Me.quitarcompania)
        Me.TabPage4.Controls.Add(Me.quitartodocompania)
        Me.TabPage4.Controls.Add(Me.seleccioncompania)
        Me.TabPage4.Controls.Add(Me.loquehaycompania)
        Me.TabPage4.Location = New System.Drawing.Point(4, 25)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(801, 450)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Compañías"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'agregartodocompania
        '
        Me.agregartodocompania.Location = New System.Drawing.Point(363, 189)
        Me.agregartodocompania.Name = "agregartodocompania"
        Me.agregartodocompania.Size = New System.Drawing.Size(75, 23)
        Me.agregartodocompania.TabIndex = 145
        Me.agregartodocompania.Text = ">>"
        Me.agregartodocompania.UseVisualStyleBackColor = True
        '
        'agregarcompania
        '
        Me.agregarcompania.Location = New System.Drawing.Point(363, 160)
        Me.agregarcompania.Name = "agregarcompania"
        Me.agregarcompania.Size = New System.Drawing.Size(75, 23)
        Me.agregarcompania.TabIndex = 144
        Me.agregarcompania.Text = ">"
        Me.agregarcompania.UseVisualStyleBackColor = True
        '
        'quitarcompania
        '
        Me.quitarcompania.Location = New System.Drawing.Point(363, 251)
        Me.quitarcompania.Name = "quitarcompania"
        Me.quitarcompania.Size = New System.Drawing.Size(75, 23)
        Me.quitarcompania.TabIndex = 146
        Me.quitarcompania.Text = "<"
        Me.quitarcompania.UseVisualStyleBackColor = True
        '
        'quitartodocompania
        '
        Me.quitartodocompania.Location = New System.Drawing.Point(363, 280)
        Me.quitartodocompania.Name = "quitartodocompania"
        Me.quitartodocompania.Size = New System.Drawing.Size(75, 23)
        Me.quitartodocompania.TabIndex = 147
        Me.quitartodocompania.Text = "<<"
        Me.quitartodocompania.UseVisualStyleBackColor = True
        '
        'seleccioncompania
        '
        Me.seleccioncompania.DisplayMember = "razon_social"
        Me.seleccioncompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.seleccioncompania.FormattingEnabled = True
        Me.seleccioncompania.ItemHeight = 16
        Me.seleccioncompania.Location = New System.Drawing.Point(480, 95)
        Me.seleccioncompania.Name = "seleccioncompania"
        Me.seleccioncompania.Size = New System.Drawing.Size(250, 260)
        Me.seleccioncompania.TabIndex = 143
        Me.seleccioncompania.ValueMember = "idcompania"
        '
        'loquehaycompania
        '
        Me.loquehaycompania.DisplayMember = "razon_social"
        Me.loquehaycompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.loquehaycompania.FormattingEnabled = True
        Me.loquehaycompania.ItemHeight = 16
        Me.loquehaycompania.Location = New System.Drawing.Point(70, 95)
        Me.loquehaycompania.Name = "loquehaycompania"
        Me.loquehaycompania.Size = New System.Drawing.Size(250, 260)
        Me.loquehaycompania.TabIndex = 142
        Me.loquehaycompania.ValueMember = "IdCompania"
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.ForeColor = System.Drawing.Color.Black
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(801, 450)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Sucursales / Vendedores"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.NombreListBox2)
        Me.Panel2.Controls.Add(Me.NombreListBox3)
        Me.Panel2.Controls.Add(Me.Button6)
        Me.Panel2.Controls.Add(Me.Button5)
        Me.Panel2.Controls.Add(Me.Button7)
        Me.Panel2.Controls.Add(Me.Button8)
        Me.Panel2.Location = New System.Drawing.Point(40, 33)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(716, 401)
        Me.Panel2.TabIndex = 32
        Me.Panel2.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.NombreListBox1)
        Me.Panel1.Controls.Add(Me.NombreListBox)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Location = New System.Drawing.Point(50, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(693, 391)
        Me.Panel1.TabIndex = 31
        Me.Panel1.Visible = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(801, 450)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Servicios"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.DescripcionListBox1)
        Me.Panel3.Controls.Add(Me.Button12)
        Me.Panel3.Controls.Add(Me.Button9)
        Me.Panel3.Controls.Add(Me.Button11)
        Me.Panel3.Controls.Add(Me.Button10)
        Me.Panel3.Controls.Add(Me.DescripcionListBox)
        Me.Panel3.Location = New System.Drawing.Point(52, 52)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(659, 361)
        Me.Panel3.TabIndex = 27
        '
        'DescripcionListBox1
        '
        Me.DescripcionListBox1.DataSource = Me.ConServiciosTmpBindingSource
        Me.DescripcionListBox1.DisplayMember = "Descripcion"
        Me.DescripcionListBox1.FormattingEnabled = True
        Me.DescripcionListBox1.ItemHeight = 16
        Me.DescripcionListBox1.Location = New System.Drawing.Point(398, 3)
        Me.DescripcionListBox1.Name = "DescripcionListBox1"
        Me.DescripcionListBox1.Size = New System.Drawing.Size(258, 356)
        Me.DescripcionListBox1.TabIndex = 16
        Me.DescripcionListBox1.TabStop = False
        Me.DescripcionListBox1.ValueMember = "Clv_Servicio"
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(294, 209)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(75, 23)
        Me.Button12.TabIndex = 26
        Me.Button12.TabStop = False
        Me.Button12.Text = "<<"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(294, 90)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 23
        Me.Button9.TabStop = False
        Me.Button9.Text = ">"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(294, 180)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(75, 23)
        Me.Button11.TabIndex = 25
        Me.Button11.TabStop = False
        Me.Button11.Text = "<"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(294, 119)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 24
        Me.Button10.TabStop = False
        Me.Button10.Text = ">>"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'DescripcionListBox
        '
        Me.DescripcionListBox.DataSource = Me.ConServiciosProBindingSource
        Me.DescripcionListBox.DisplayMember = "Descripcion"
        Me.DescripcionListBox.FormattingEnabled = True
        Me.DescripcionListBox.ItemHeight = 16
        Me.DescripcionListBox.Location = New System.Drawing.Point(3, 3)
        Me.DescripcionListBox.Name = "DescripcionListBox"
        Me.DescripcionListBox.Size = New System.Drawing.Size(258, 356)
        Me.DescripcionListBox.TabIndex = 15
        Me.DescripcionListBox.TabStop = False
        Me.DescripcionListBox.ValueMember = "Clv_Servicio"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(61, 27)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(134, 22)
        Me.DateTimePicker1.TabIndex = 0
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(245, 27)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(134, 22)
        Me.DateTimePicker2.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(387, 21)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(413, 66)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Rango de Fechas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 16)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Del"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(217, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(22, 16)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Al"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ConceptoComboBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(16, 102)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(353, 66)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Servicio"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CheckBox2)
        Me.GroupBox3.Controls.Add(Me.CheckBox1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(16, 21)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(353, 66)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Reportes"
        '
        'Button13
        '
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.Location = New System.Drawing.Point(542, 165)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(136, 36)
        Me.Button13.TabIndex = 4
        Me.Button13.Text = "&GRAFICAR"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(67, 19)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(239, 26)
        Me.ComboBoxCompanias.TabIndex = 99
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ComboBoxCompanias)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox4.Location = New System.Drawing.Point(387, 93)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(353, 56)
        Me.GroupBox4.TabIndex = 100
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Compañía"
        Me.GroupBox4.Visible = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmGraficas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(831, 699)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button13)
        Me.Name = "FrmGraficas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gráficas de Ventas"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConVentasVendedoresProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConVentasVendedoresTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSucursalesProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents Dame_clv_session_ReportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_ReportesTableAdapter As sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConVentasVendedoresProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConVentasVendedoresProTableAdapter As sofTV.DataSetEric2TableAdapters.ConVentasVendedoresProTableAdapter
    Friend WithEvents NombreListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConVentasVendedoresTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConVentasVendedoresTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConVentasVendedoresTmpTableAdapter
    Friend WithEvents NombreListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents InsertarVendedorTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarVendedorTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarVendedorTmpTableAdapter
    Friend WithEvents BorrarVendedorTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarVendedorTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarVendedorTmpTableAdapter
    Friend WithEvents ConServiciosProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosProTableAdapter As sofTV.DataSetEric2TableAdapters.ConServiciosProTableAdapter
    Friend WithEvents ConServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConServiciosTmpTableAdapter
    Friend WithEvents InsertarServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarServiciosTmpTableAdapter
    Friend WithEvents BorrarServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarServiciosTmpTableAdapter
    Friend WithEvents ConSucursalesProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSucursalesProTableAdapter As sofTV.DataSetEric2TableAdapters.ConSucursalesProTableAdapter
    Friend WithEvents ConSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConSucursalesTmpTableAdapter
    Friend WithEvents InsertarSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarSucursalesTmpTableAdapter
    Friend WithEvents BorrarSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarSucursalesTmpTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents NombreListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents NombreListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents DescripcionListBox As System.Windows.Forms.ListBox
    Friend WithEvents DescripcionListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents agregartodociudad As System.Windows.Forms.Button
    Friend WithEvents agregarciudad As System.Windows.Forms.Button
    Friend WithEvents quitar As System.Windows.Forms.Button
    Friend WithEvents quitartodociudad As System.Windows.Forms.Button
    Friend WithEvents seleccionciudad As System.Windows.Forms.ListBox
    Friend WithEvents loquehayciudad As System.Windows.Forms.ListBox
    Friend WithEvents agregartodocompania As System.Windows.Forms.Button
    Friend WithEvents agregarcompania As System.Windows.Forms.Button
    Friend WithEvents quitarcompania As System.Windows.Forms.Button
    Friend WithEvents quitartodocompania As System.Windows.Forms.Button
    Friend WithEvents seleccioncompania As System.Windows.Forms.ListBox
    Friend WithEvents loquehaycompania As System.Windows.Forms.ListBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
