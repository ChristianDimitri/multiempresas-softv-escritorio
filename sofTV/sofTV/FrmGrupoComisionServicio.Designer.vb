﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGrupoComisionServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DescripcionLabel = New System.Windows.Forms.Label()
        Me.Clv_TxtLabel = New System.Windows.Forms.Label()
        Me.NombreLabel = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBoxPrecioTelemarketing = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxPrecio = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.eliminar = New System.Windows.Forms.ToolStripButton()
        Me.guardar = New System.Windows.Forms.ToolStripButton()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.tbclv_cluster = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.SectorComboBox = New System.Windows.Forms.ComboBox()
        Me.dgvrelsector = New System.Windows.Forms.DataGridView()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_SectorTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvrelsector, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        Me.DescripcionLabel.AutoSize = True
        Me.DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionLabel.Location = New System.Drawing.Point(157, 110)
        Me.DescripcionLabel.Name = "DescripcionLabel"
        Me.DescripcionLabel.Size = New System.Drawing.Size(71, 16)
        Me.DescripcionLabel.TabIndex = 4
        Me.DescripcionLabel.Text = "Nombre :"
        '
        'Clv_TxtLabel
        '
        Me.Clv_TxtLabel.AutoSize = True
        Me.Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtLabel.Location = New System.Drawing.Point(174, 77)
        Me.Clv_TxtLabel.Name = "Clv_TxtLabel"
        Me.Clv_TxtLabel.Size = New System.Drawing.Size(56, 16)
        Me.Clv_TxtLabel.TabIndex = 2
        Me.Clv_TxtLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        Me.NombreLabel.AutoSize = True
        Me.NombreLabel.Location = New System.Drawing.Point(89, 32)
        Me.NombreLabel.Name = "NombreLabel"
        Me.NombreLabel.Size = New System.Drawing.Size(69, 16)
        Me.NombreLabel.TabIndex = 11
        Me.NombreLabel.Text = "Servicio:"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBoxPrecioTelemarketing)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBoxPrecio)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ClaveTextBox)
        Me.GroupBox1.Controls.Add(Me.BindingNavigator1)
        Me.GroupBox1.Controls.Add(Me.DescripcionTextBox)
        Me.GroupBox1.Controls.Add(Me.DescripcionLabel)
        Me.GroupBox1.Controls.Add(Me.Clv_TxtLabel)
        Me.GroupBox1.Controls.Add(Me.tbclv_cluster)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(606, 230)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Grupo"
        '
        'TextBoxPrecioTelemarketing
        '
        Me.TextBoxPrecioTelemarketing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxPrecioTelemarketing.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxPrecioTelemarketing.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPrecioTelemarketing.Location = New System.Drawing.Point(246, 166)
        Me.TextBoxPrecioTelemarketing.Name = "TextBoxPrecioTelemarketing"
        Me.TextBoxPrecioTelemarketing.Size = New System.Drawing.Size(127, 24)
        Me.TextBoxPrecioTelemarketing.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(43, 169)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(184, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Comisión Telemarketing :"
        '
        'TextBoxPrecio
        '
        Me.TextBoxPrecio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxPrecio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPrecio.Location = New System.Drawing.Point(246, 136)
        Me.TextBoxPrecio.Name = "TextBoxPrecio"
        Me.TextBoxPrecio.Size = New System.Drawing.Size(127, 24)
        Me.TextBoxPrecio.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(69, 140)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(159, 16)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Comisión Cambaceo :"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ClaveTextBox.Enabled = False
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(246, 74)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(127, 24)
        Me.ClaveTextBox.TabIndex = 0
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.eliminar, Me.guardar})
        Me.BindingNavigator1.Location = New System.Drawing.Point(3, 18)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(600, 25)
        Me.BindingNavigator1.TabIndex = 2
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'eliminar
        '
        Me.eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.eliminar.Name = "eliminar"
        Me.eliminar.Size = New System.Drawing.Size(74, 22)
        Me.eliminar.Text = "&ELIMINAR"
        Me.eliminar.Visible = False
        '
        'guardar
        '
        Me.guardar.Name = "guardar"
        Me.guardar.Size = New System.Drawing.Size(75, 22)
        Me.guardar.Text = "&GUARDAR"
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(246, 106)
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(287, 24)
        Me.DescripcionTextBox.TabIndex = 1
        '
        'tbclv_cluster
        '
        Me.tbclv_cluster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbclv_cluster.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbclv_cluster.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbclv_cluster.Location = New System.Drawing.Point(246, 106)
        Me.tbclv_cluster.Name = "tbclv_cluster"
        Me.tbclv_cluster.Size = New System.Drawing.Size(127, 24)
        Me.tbclv_cluster.TabIndex = 5
        Me.tbclv_cluster.Text = "0"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.NombreLabel)
        Me.GroupBox2.Controls.Add(Me.SectorComboBox)
        Me.GroupBox2.Controls.Add(Me.dgvrelsector)
        Me.GroupBox2.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.GroupBox2.Controls.Add(Me.Clv_SectorTextBox)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(9, 255)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(606, 388)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Servicios en el grupo"
        '
        'SectorComboBox
        '
        Me.SectorComboBox.DisplayMember = "Nombre"
        Me.SectorComboBox.FormattingEnabled = True
        Me.SectorComboBox.Location = New System.Drawing.Point(92, 52)
        Me.SectorComboBox.Name = "SectorComboBox"
        Me.SectorComboBox.Size = New System.Drawing.Size(401, 24)
        Me.SectorComboBox.TabIndex = 12
        Me.SectorComboBox.ValueMember = "Clv_Colonia"
        '
        'dgvrelsector
        '
        Me.dgvrelsector.AllowUserToAddRows = False
        Me.dgvrelsector.AllowUserToDeleteRows = False
        Me.dgvrelsector.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvrelsector.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvrelsector.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Servicio, Me.descripcion})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvrelsector.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvrelsector.Location = New System.Drawing.Point(92, 83)
        Me.dgvrelsector.Name = "dgvrelsector"
        Me.dgvrelsector.ReadOnly = True
        Me.dgvrelsector.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvrelsector.Size = New System.Drawing.Size(401, 291)
        Me.dgvrelsector.TabIndex = 7
        Me.dgvrelsector.TabStop = False
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.Visible = False
        '
        'descripcion
        '
        Me.descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.descripcion.DataPropertyName = "Descripcion"
        Me.descripcion.HeaderText = "Servicio"
        Me.descripcion.Name = "descripcion"
        Me.descripcion.ReadOnly = True
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(236, 149)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ColoniaTextBox.TabIndex = 11
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'Clv_SectorTextBox
        '
        Me.Clv_SectorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_SectorTextBox.Location = New System.Drawing.Point(220, 150)
        Me.Clv_SectorTextBox.Name = "Clv_SectorTextBox"
        Me.Clv_SectorTextBox.ReadOnly = True
        Me.Clv_SectorTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_SectorTextBox.TabIndex = 1
        Me.Clv_SectorTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(509, 43)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 33)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Agregar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(509, 84)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 33)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Eliminar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(246, 196)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(127, 24)
        Me.TextBox1.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(43, 199)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(161, 16)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Comisión Sucursales :"
        '
        'FrmGrupoComisionServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(635, 652)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmGrupoComisionServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Grupo de Comisiones por Servicio"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvrelsector, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents tbclv_cluster As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SectorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents dgvrelsector As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents DescripcionLabel As System.Windows.Forms.Label
    Friend WithEvents Clv_TxtLabel As System.Windows.Forms.Label
    Friend WithEvents NombreLabel As System.Windows.Forms.Label
    Friend WithEvents TextBoxPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextBoxPrecioTelemarketing As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
