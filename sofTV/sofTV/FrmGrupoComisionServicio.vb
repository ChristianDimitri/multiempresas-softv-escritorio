﻿Imports System.IO
Imports System.Data.SqlClient
Public Class FrmGrupoComisionServicio
    Dim locbndnew As Integer = 0
    Private Sub FrmGrupoComisionServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            eliminar.Enabled = False
            locbndnew = 1
            ClaveTextBox.Text = 0
        End If

        If opcion = "M" Then
            ClaveTextBox.Text = gloIdGrupoComisionServicio
            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand()
            conexion.Open()
            comando.Connection = conexion
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "MuestraGrupoComisionServicio"
            Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
            p4.Direction = ParameterDirection.Input
            p4.Value = 2
            comando.Parameters.Add(p4)
            Dim p3 As New SqlParameter("@clave", SqlDbType.VarChar)
            p3.Direction = ParameterDirection.Input
            p3.Value = ClaveTextBox.Text
            comando.Parameters.Add(p3)
            Dim p2 As New SqlParameter("@descripcion", SqlDbType.VarChar)
            p2.Direction = ParameterDirection.Input
            p2.Value = ""
            comando.Parameters.Add(p2)

            Dim reader As SqlDataReader = comando.ExecuteReader()
            If reader.Read() Then
                ClaveTextBox.Text = reader(0).ToString
                DescripcionTextBox.Text = reader(1).ToString
                TextBoxPrecio.Text = reader(2).ToString
                TextBoxPrecioTelemarketing.Text = reader(3).ToString
                TextBox1.Text = reader(4).ToString
            End If
            reader.Close()
            conexion.Close()


        End If
        If opcion = "C" Then
            ClaveTextBox.Text = gloIdGrupoComisionServicio
            DescripcionTextBox.Enabled = False
            guardar.Enabled = False
            eliminar.Enabled = False
            Button1.Enabled = False
            Button2.Enabled = False
            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand()
            conexion.Open()
            comando.Connection = conexion
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "MuestraGrupoComisionServicio"
            Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
            p4.Direction = ParameterDirection.Input
            p4.Value = 2
            comando.Parameters.Add(p4)
            Dim p3 As New SqlParameter("@clave", SqlDbType.VarChar)
            p3.Direction = ParameterDirection.Input
            p3.Value = ClaveTextBox.Text
            comando.Parameters.Add(p3)
            Dim p2 As New SqlParameter("@descripcion", SqlDbType.VarChar)
            p2.Direction = ParameterDirection.Input
            p2.Value = ""
            comando.Parameters.Add(p2)

            Dim reader As SqlDataReader = comando.ExecuteReader()
            If reader.Read() Then
                ClaveTextBox.Text = reader(0).ToString
                DescripcionTextBox.Text = reader(1).ToString
                TextBoxPrecio.Text = reader(2).ToString
                TextBoxPrecioTelemarketing.Text = reader(3).ToString
                TextBox1.Text = reader(4).ToString
            End If
            reader.Close()
            conexion.Close()
        End If
        llenaListado()
    End Sub

    Private Sub guardar_Click(sender As Object, e As EventArgs) Handles guardar.Click
        If DescripcionTextBox.Text = "" Or TextBoxPrecio.Text = "" Or TextBoxPrecioTelemarketing.Text = "" Then
            MsgBox("Debe llenar todos los campos para poder continuar")
            Exit Sub
        End If
        If opcion = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, DescripcionTextBox.Text, 200)
            BaseII.CreateMyParameter("@Precio", SqlDbType.Money, TextBoxPrecio.Text)
            BaseII.CreateMyParameter("@PrecioTelemarketing", SqlDbType.Money, TextBoxPrecioTelemarketing.Text)
            BaseII.CreateMyParameter("@PrecioSucursales", SqlDbType.Money, TextBox1.Text)
            BaseII.CreateMyParameter("@clave", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("InsertGrupoComisionServicio")
            ClaveTextBox.Text = BaseII.dicoPar("@clave")
            MsgBox("Datos guardados exitosamente")
            opcion = "M"
            locbndnew = 0
        Else
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clave", SqlDbType.Int, ClaveTextBox.Text)
            BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, DescripcionTextBox.Text, 200)
            BaseII.CreateMyParameter("@Precio", SqlDbType.Money, TextBoxPrecio.Text)
            BaseII.CreateMyParameter("@PrecioTelemarketing", SqlDbType.Money, TextBoxPrecioTelemarketing.Text)
            BaseII.CreateMyParameter("@PrecioSucursales", SqlDbType.Money, TextBox1.Text)
            BaseII.Inserta("UpdateGrupoComisionServicio")
            MsgBox("Datos guardados exitosamente")            
        End If
       
    End Sub

    Private Sub llenaListado()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, ClaveTextBox.Text)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        dgvrelsector.DataSource = BaseII.ConsultaDT("MuestraRelGrupoComisionServicio")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, ClaveTextBox.Text)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        SectorComboBox.DataSource = BaseII.ConsultaDT("MuestraRelGrupoComisionServicio")
        SectorComboBox.DisplayMember = "Descripcion"
        SectorComboBox.ValueMember = "Clv_Servicio"
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If SectorComboBox.Items.Count = 0 Then 'Agregar
            Exit Sub
        End If
        If locbndnew = 1 Then
            MsgBox("Primero debe guardar el grupo")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, ClaveTextBox.Text)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, SectorComboBox.SelectedValue)
        
        BaseII.Inserta("AgregaRelGrupoComisionServicio")
        llenaListado()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If dgvrelsector.Rows.Count = 0 Then 'Eliminar
            Exit Sub
        End If
        If locbndnew = 1 Then
            MsgBox("Primero debe guardar el grupo")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, ClaveTextBox.Text)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, dgvrelsector.SelectedCells(0).Value)

        BaseII.Inserta("borraRelGrupoComisionServicio")
        llenaListado()
    End Sub
End Class