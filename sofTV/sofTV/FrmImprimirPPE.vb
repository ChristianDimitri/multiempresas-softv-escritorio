Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic

Public Class FrmImprimirPPE
    Private customersByCityReport As ReportDocument

    Private Sub ConfigureCrystalReports()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim eFecha As String = Nothing
        eFecha = "Del " & eFechaIniPPE & " al " & eFechaFinPPE
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim companias As String = ""
        Dim ciudades As String = ""
        Dim contador As Integer = 0
        Dim reportPath As String = Nothing


        If eOpPPE = 1 Then

            Me.Text = "Reporte de Venta de Pel�culas PPE"


            reportPath = RutaReportes + "\ReportServiciosPPE.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(1, eFechaFinPPE)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"


        ElseIf eOpPPE = 2 Then
            Me.Text = "Bit�cora de Activaci�n de Paquetes"


            reportPath = RutaReportes + "\ReportBitActPaq.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(1, eFechaFinPPE)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FechaInicial", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@FechaFinal", SqlDbType.DateTime, CObj(eFechaFinPPE))

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteBitActPaq")
            DS = BaseII.ConsultaDS("ReporteBitActPaq", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"




        ElseIf eOpPPE = 3 Then

            Me.Text = "Reporte Resumen de Ventas Sucursal"

            Dim conexion As New SqlConnection(MiConexion)

            Dim sBuilder As New StringBuilder("EXEC ReporteResumenVentas '" + eFechaIniPPE + "', '" + eFechaFinPPE + "', " + identificador.ToString)
            'Dim sBuilder As New StringBuilder("EXEC REPORTETEST")
            Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
            Dim dTable As New DataTable

            dAdapter.Fill(dTable)

            reportPath = RutaReportes + "\ReportResumenVentas.rpt"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dTable)
            Dim con As New SqlConnection(MiConexion)
            con.Open()
            Dim comando As New SqlCommand()
            comando.Connection = con
            comando.CommandText = "exec DameCompaniasTituloReporte " + identificador.ToString
            companias = "Compa��as: "
            Dim reader As SqlDataReader = comando.ExecuteReader()
            contador = 0
            While reader.Read()
                If contador = 0 Then
                    companias = companias + reader(0).ToString
                    contador = contador + 1
                Else
                    companias = companias + ", " + reader(0).ToString
                End If
            End While
            reader.Close()
            comando.Dispose()
            comando.Connection = con
            comando.CommandText = "exec DameCiudadesTituloReporte " + identificador.ToString
            reader = comando.ExecuteReader()
            contador = 0
            ciudades = "Ciudades: "
            While reader.Read()
                If contador = 0 Then
                    ciudades = ciudades + reader(0).ToString
                    contador = contador + 1
                Else
                    ciudades = ciudades + ", " + reader(0).ToString
                End If
            End While
            con.Close()
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & companias & "'"




        ElseIf eOpPPE = 4 Then
            Me.Text = "Reporte Resumen de Ventas Vendedores"


            reportPath = RutaReportes + "\ReportResumenVentasVendedores.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(1, eFechaFinPPE)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFinPPE))
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)

            Dim listatablas As New List(Of String)
            listatablas.Add("VentasVendedor")
            DS = BaseII.ConsultaDS("VentasVendedor", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim con As New SqlConnection(MiConexion)
            con.Open()
            Dim comando As New SqlCommand()
            comando.Connection = con
            comando.CommandText = "exec DameCompaniasTituloReporte " + identificador.ToString
            companias = "Compa��as: "
            Dim reader As SqlDataReader = comando.ExecuteReader()
            contador = 0
            While reader.Read()
                If contador = 0 Then
                    companias = companias + reader(0).ToString
                    contador = contador + 1
                Else
                    companias = companias + ", " + reader(0).ToString
                End If
            End While
            reader.Close()
            comando.Dispose()
            comando.Connection = con
            comando.CommandText = "exec DameCiudadesTituloReporte " + identificador.ToString
            reader = comando.ExecuteReader()
            contador = 0
            ciudades = "Ciudades: "
            While reader.Read()
                If contador = 0 Then
                    ciudades = ciudades + reader(0).ToString
                    contador = contador + 1
                Else
                    ciudades = ciudades + ", " + reader(0).ToString
                End If
            End While
            con.Close()
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & companias & "'"
        ElseIf eOpPPE = 5 Then
            eOpPPE = 0
            Me.Text = "Listado de Adelantados"
            Dim dSet As New DataSet
            dSet = LISTADOAdelantados()
            customersByCityReport.Load(RutaReportes + "\LISTADOAdelantados.rpt")
            customersByCityReport.SetDataSource(dSet)
        ElseIf eOpPPE = 6 Then
            Me.Text = "Resumen Ordenes y Quejas Ejecutadas"
            reportPath = RutaReportes + "\ResumenOrdenQueja.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, CObj(eFechaFinPPE))
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@Status", SqlDbType.Int, Rstatus)


            Dim listatablas As New List(Of String)
            listatablas.Add("UspReporteResumenOrdenQueja")
            DS = BaseII.ConsultaDS("ReporteResumenOrdenQueja", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloCiudad & "'"
        ElseIf eOpPPE = 7 Then
            Me.Text = "Resumen Ordenes y Quejas Ejecutadas"
            reportPath = RutaReportes + "\ResumenOrdenQueja.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, CObj(eFechaFinPPE))
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@Status", SqlDbType.Int, Rstatus)

            Dim listatablas As New List(Of String)
            listatablas.Add("UspReporteResumenOrdenQueja")
            DS = BaseII.ConsultaDS("ReporteResumenOrdenQueja", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloCiudad & "'"

        ElseIf eOpPPE = 26 Then
            Me.Text = "Resumen Resumen Clientes"
            reportPath = RutaReportes + "\ResumenClientes.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, GloIdResumenCliente)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteResumenClientes")
            DS = BaseII.ConsultaDS("Usp_ReporteResumenClientes", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 27 Then

            Me.Text = "Resumen Resumen Clientes"
            reportPath = RutaReportes + "\ResumenClientes.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteResumenClientes")
            DS = BaseII.ConsultaDS("usp_Resumenclientes", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 29 Then

            Me.Text = "Reporte Detalle de Ventas"
            reportPath = RutaReportes + "\ReporteDetalleVentas.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.Date, eFechaIni)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.Date, eFechaFin)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteDetalleventas")
            DS = BaseII.ConsultaDS("Usp_ReporteDetalleventas", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 30 Then

            Me.Text = "Reporte Recuperaci�n"
            reportPath = RutaReportes + "\ReporteRecuperacion.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.Date, eFechaIni)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.Date, eFechaFin)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteRecuperacion")
            DS = BaseII.ConsultaDS("Usp_ReporteRecuperacion", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 31 Then

            Me.Text = "Contrato TV"
            reportPath = RutaReportes + "\ReporteContratoTv.rpt"

            Dim dSet As New DataSet
            dSet.Clear()

            Dim tableNameList As New List(Of String)
            tableNameList.Add("Datos")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("Factura")
            tableNameList.Add("Tarifas")
            tableNameList.Add("Tvs")
            tableNameList.Add("XAgenda")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, eContrato)
            dSet = BaseII.ConsultaDS("REPORTEContratoTv", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

       

        ElseIf eOpPPE = 32 Then

            Me.Text = "Contrato Internet"
            reportPath = RutaReportes + "\REPORTEContratoInt.rpt"

            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("Datos")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("Tvs")
            tableNameList.Add("Tarifas")
            tableNameList.Add("Factura")
            tableNameList.Add("XAgenda")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
            dSet = BaseII.ConsultaDS("REPORTEContratoInt", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

        ElseIf eOpPPE = 33 Then
            Me.Text = "Resumen Nuevo"
            reportPath = RutaReportes + "\ResumenNuevo.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, GloIdReporteResumen)

            Dim listatablas As New List(Of String)
            listatablas.Add("Tbl_ResumenNuevo")
            DS = BaseII.ConsultaDS("Usp_ReporteResumenNuevo", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 34 Then

            Me.Text = "Resumen Nuevo"
            reportPath = RutaReportes + "\ResumenNuevo.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            Dim listatablas As New List(Of String)
            listatablas.Add("Tbl_ResumenNuevo")
            DS = BaseII.ConsultaDS("usp_ResumenNuevo", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
        ElseIf eOpPPE = 35 Then

            'Obtenemos Compania y distribuidor del contrato 

            Dim Report_CompaniaCiudad As String = "''"
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Contrato)
            BaseII.CreateMyParameter("@reporte", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("DameReporteDigital_ByContrato")
            Report_CompaniaCiudad = BaseII.dicoPar("@reporte").ToString

            Me.Text = "Contrato Digital"

            If Report_CompaniaCiudad = 1 Then
                reportPath = RutaReportes + "\ReporteContratoDig_Monclova.rpt"
            Else
                reportPath = RutaReportes + "\ReporteContratoDig_CuatroCienegas.rpt"
            End If


            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("Datos")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("Tvs")
            tableNameList.Add("Tarifas")
            tableNameList.Add("Factura")
            tableNameList.Add("XAgenda")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
            dSet = BaseII.ConsultaDS("REPORTEContratoDig", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

        ElseIf eOpPPE = 36 Then

            Me.Text = "Reporte Piratas"
            reportPath = RutaReportes + "\ReportePiratas.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.Date, eFechaIni)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.Date, eFechaFin)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReportePiratas")
            DS = BaseII.ConsultaDS("ReportePiratas", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIni + " A la Fecha: " + eFechaFin
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        End If


        Me.CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmImprimirPPE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ConfigureCrystalReports()
    End Sub

    Private Function LISTADOAdelantados() As DataSet
        BaseII.limpiaParametros()
        Dim tableNameList As New List(Of String)
        tableNameList.Add("LISTADOAdelantados")
        tableNameList.Add("GENERAL")
        Return BaseII.ConsultaDS("LISTADOAdelantados", tableNameList)
    End Function

End Class