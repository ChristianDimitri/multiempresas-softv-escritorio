﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmIntegraciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gridIntegraciones = New System.Windows.Forms.DataGridView()
        Me.bGuardar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbStoreId = New System.Windows.Forms.TextBox()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbIdComercio = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbUsuario = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbPassword = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbPuerto = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbTerminalId = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.bEliminar = New System.Windows.Forms.Button()
        Me.bModificar = New System.Windows.Forms.Button()
        Me.cbComputadora = New System.Windows.Forms.ComboBox()
        Me.tbUrl1 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbUrl2 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        CType(Me.gridIntegraciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridIntegraciones
        '
        Me.gridIntegraciones.AllowUserToAddRows = False
        Me.gridIntegraciones.AllowUserToDeleteRows = False
        Me.gridIntegraciones.AllowUserToResizeRows = False
        Me.gridIntegraciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridIntegraciones.Location = New System.Drawing.Point(15, 158)
        Me.gridIntegraciones.Name = "gridIntegraciones"
        Me.gridIntegraciones.ReadOnly = True
        Me.gridIntegraciones.RowHeadersVisible = False
        Me.gridIntegraciones.Size = New System.Drawing.Size(555, 237)
        Me.gridIntegraciones.TabIndex = 0
        '
        'bGuardar
        '
        Me.bGuardar.Location = New System.Drawing.Point(333, 129)
        Me.bGuardar.Name = "bGuardar"
        Me.bGuardar.Size = New System.Drawing.Size(75, 23)
        Me.bGuardar.TabIndex = 1
        Me.bGuardar.Text = "Guardar"
        Me.bGuardar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Store Id"
        '
        'tbStoreId
        '
        Me.tbStoreId.Location = New System.Drawing.Point(15, 25)
        Me.tbStoreId.Name = "tbStoreId"
        Me.tbStoreId.Size = New System.Drawing.Size(100, 20)
        Me.tbStoreId.TabIndex = 3
        '
        'tbNombre
        '
        Me.tbNombre.Location = New System.Drawing.Point(121, 25)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(150, 20)
        Me.tbNombre.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(118, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nombre"
        '
        'tbIdComercio
        '
        Me.tbIdComercio.Location = New System.Drawing.Point(429, 64)
        Me.tbIdComercio.Name = "tbIdComercio"
        Me.tbIdComercio.Size = New System.Drawing.Size(141, 20)
        Me.tbIdComercio.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(426, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Id Comercio"
        '
        'tbUsuario
        '
        Me.tbUsuario.Location = New System.Drawing.Point(277, 25)
        Me.tbUsuario.Name = "tbUsuario"
        Me.tbUsuario.Size = New System.Drawing.Size(146, 20)
        Me.tbUsuario.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(274, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Usuario"
        '
        'tbPassword
        '
        Me.tbPassword.Location = New System.Drawing.Point(429, 25)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.Size = New System.Drawing.Size(141, 20)
        Me.tbPassword.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(426, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Contraseña"
        '
        'tbPuerto
        '
        Me.tbPuerto.Location = New System.Drawing.Point(15, 64)
        Me.tbPuerto.Name = "tbPuerto"
        Me.tbPuerto.Size = New System.Drawing.Size(100, 20)
        Me.tbPuerto.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Puerto"
        '
        'tbTerminalId
        '
        Me.tbTerminalId.Location = New System.Drawing.Point(121, 64)
        Me.tbTerminalId.Name = "tbTerminalId"
        Me.tbTerminalId.Size = New System.Drawing.Size(133, 20)
        Me.tbTerminalId.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(118, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Terminal Id"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(257, 48)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Computadora"
        '
        'bEliminar
        '
        Me.bEliminar.Location = New System.Drawing.Point(495, 129)
        Me.bEliminar.Name = "bEliminar"
        Me.bEliminar.Size = New System.Drawing.Size(75, 23)
        Me.bEliminar.TabIndex = 18
        Me.bEliminar.Text = "Eliminar"
        Me.bEliminar.UseVisualStyleBackColor = True
        '
        'bModificar
        '
        Me.bModificar.Location = New System.Drawing.Point(414, 129)
        Me.bModificar.Name = "bModificar"
        Me.bModificar.Size = New System.Drawing.Size(75, 23)
        Me.bModificar.TabIndex = 19
        Me.bModificar.Text = "Modificar"
        Me.bModificar.UseVisualStyleBackColor = True
        '
        'cbComputadora
        '
        Me.cbComputadora.DisplayMember = "IpMaquina"
        Me.cbComputadora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbComputadora.FormattingEnabled = True
        Me.cbComputadora.Location = New System.Drawing.Point(260, 64)
        Me.cbComputadora.Name = "cbComputadora"
        Me.cbComputadora.Size = New System.Drawing.Size(163, 21)
        Me.cbComputadora.TabIndex = 20
        Me.cbComputadora.ValueMember = "Clave"
        '
        'tbUrl1
        '
        Me.tbUrl1.Location = New System.Drawing.Point(15, 103)
        Me.tbUrl1.Name = "tbUrl1"
        Me.tbUrl1.Size = New System.Drawing.Size(265, 20)
        Me.tbUrl1.TabIndex = 22
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 87)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(38, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "URL 1"
        '
        'tbUrl2
        '
        Me.tbUrl2.Location = New System.Drawing.Point(286, 103)
        Me.tbUrl2.Name = "tbUrl2"
        Me.tbUrl2.Size = New System.Drawing.Size(284, 20)
        Me.tbUrl2.TabIndex = 24
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(283, 87)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "URL 2"
        '
        'FrmIntegraciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(582, 406)
        Me.Controls.Add(Me.tbUrl2)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.tbUrl1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.cbComputadora)
        Me.Controls.Add(Me.bModificar)
        Me.Controls.Add(Me.bEliminar)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tbTerminalId)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tbPuerto)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbPassword)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbUsuario)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbIdComercio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbStoreId)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bGuardar)
        Me.Controls.Add(Me.gridIntegraciones)
        Me.Name = "FrmIntegraciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Integraciones"
        CType(Me.gridIntegraciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridIntegraciones As System.Windows.Forms.DataGridView
    Friend WithEvents bGuardar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbStoreId As System.Windows.Forms.TextBox
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbIdComercio As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbPuerto As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbTerminalId As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents bEliminar As System.Windows.Forms.Button
    Friend WithEvents bModificar As System.Windows.Forms.Button
    Friend WithEvents cbComputadora As System.Windows.Forms.ComboBox
    Friend WithEvents tbUrl1 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbUrl2 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
End Class
