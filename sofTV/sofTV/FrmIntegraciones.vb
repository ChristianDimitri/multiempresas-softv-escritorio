﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic

Public Class FrmIntegraciones
    Private bandera As String = "N"

    Private Sub FrmIntegraciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        gridIntegraciones.DataSource = BaseII.ConsultaDT("GetAllIntegraciones")

        BaseII.limpiaParametros()
        cbComputadora.DataSource = BaseII.ConsultaDT("GetComputadoras")

    End Sub

    Private Sub bGuardar_Click(sender As Object, e As EventArgs) Handles bGuardar.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@SandboxId", SqlDbType.Int, Convert.ToInt32(tbStoreId.Text.Trim()))
        BaseII.CreateMyParameter("@NombreIntegracion", SqlDbType.NVarChar, tbNombre.Text.Trim(), 250)
        BaseII.CreateMyParameter("@IdComercio", SqlDbType.Int, Convert.ToInt32(tbIdComercio.Text.Trim()))
        BaseII.CreateMyParameter("@Usuario", SqlDbType.NVarChar, tbUsuario.Text.Trim(), 150)
        BaseII.CreateMyParameter("@Contraseña", SqlDbType.NVarChar, tbPassword.Text.Trim(), 250)
        BaseII.CreateMyParameter("@Puerto", SqlDbType.NVarChar, tbPuerto.Text.Trim(), 100)
        BaseII.CreateMyParameter("@TerminalId", SqlDbType.Int, Convert.ToInt32(tbTerminalId.Text.Trim()))
        BaseII.CreateMyParameter("@Computadora", SqlDbType.NVarChar, cbComputadora.Text.Trim(), 150)
        BaseII.CreateMyParameter("@Url1", SqlDbType.NVarChar, tbUrl1.Text.Trim(), 200)
        BaseII.CreateMyParameter("@Url2", SqlDbType.NVarChar, tbUrl2.Text.Trim(), 200)

        If bandera = "N" Then
            BaseII.Inserta("InsertIntegraciones")
        End If
        If bandera = "M" Then
            BaseII.Inserta("UpdateIntegraciones")
        End If

        BaseII.limpiaParametros()
        gridIntegraciones.DataSource = BaseII.ConsultaDT("GetAllIntegraciones")
        bandera = "N"
    End Sub

    Private Sub bModificar_Click(sender As Object, e As EventArgs) Handles bModificar.Click
        If gridIntegraciones.SelectedRows.Count = 0 Then
            bandera = "M"
            tbStoreId.Text = gridIntegraciones("SandboxId", gridIntegraciones.CurrentRow.Index).Value
            tbNombre.Text = gridIntegraciones("NombreIntegracion", gridIntegraciones.CurrentRow.Index).Value
            tbIdComercio.Text = gridIntegraciones("IdComercio", gridIntegraciones.CurrentRow.Index).Value
            tbUsuario.Text = gridIntegraciones("Usuario", gridIntegraciones.CurrentRow.Index).Value
            tbPassword.Text = gridIntegraciones("Contraseña", gridIntegraciones.CurrentRow.Index).Value
            tbPuerto.Text = gridIntegraciones("Puerto", gridIntegraciones.CurrentRow.Index).Value
            tbTerminalId.Text = gridIntegraciones("TerminalId", gridIntegraciones.CurrentRow.Index).Value
            cbComputadora.Text = gridIntegraciones("Computadora", gridIntegraciones.CurrentRow.Index).Value
            tbUrl1.Text = gridIntegraciones("Url1", gridIntegraciones.CurrentRow.Index).Value
            tbUrl2.Text = gridIntegraciones("Url2", gridIntegraciones.CurrentRow.Index).Value
        End If
    End Sub

    Private Sub bEliminar_Click(sender As Object, e As EventArgs) Handles bEliminar.Click
        Dim respuesta As MsgBoxResult = MsgBox("¿Desea eliminar la integracion seleccionada?", MsgBoxStyle.YesNo, "")
        If respuesta = MsgBoxResult.Yes Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@SandboxId", SqlDbType.Int, Convert.ToInt32(gridIntegraciones(0, gridIntegraciones.CurrentRow.Index)))
            BaseII.Inserta("DeleteIntegraciones")

            BaseII.limpiaParametros()
            gridIntegraciones.DataSource = BaseII.ConsultaDT("GetAllIntegraciones")
        End If
    End Sub

    Private Sub LimpiarCampos()
        tbStoreId.Text = ""
        tbNombre.Text = ""
        tbIdComercio.Text = ""
        tbUsuario.Text = ""
        tbPassword.Text = ""
        tbPuerto.Text = ""
        tbTerminalId.Text = ""
        cbComputadora.Text = ""
        tbUrl1.Text = ""
        tbUrl2.Text = ""
    End Sub
End Class