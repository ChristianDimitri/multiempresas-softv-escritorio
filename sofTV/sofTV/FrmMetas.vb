Imports System.Data.SqlClient

Public Class FrmMetas
    Private Bnd As Boolean = False

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmMetas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConGrupoVentasTableAdapter.Connection = CON
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 3)
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            Me.MuestraAniosTableAdapter.Connection = CON
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            CON.Close()
            Consultar()
            Bnd = True

            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Consultar()
        Try
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.ConMetasTableAdapter.Connection = CON2
            Me.ConMetasTableAdapter.Fill(Me.DataSetEric2.ConMetas, CInt(Me.ConceptoComboBox.SelectedValue), CInt(Me.GrupoComboBox.SelectedValue), CInt(Me.AnioComboBox.SelectedValue))
            CON2.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    

    Private Sub GrupoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrupoComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    

    Private Sub ConceptoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    

    Private Sub AnioComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AnioComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConMetasBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConMetasBindingNavigatorSaveItem.Click
        Try
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.ConMetasTableAdapter.Connection = CON3
            Me.ConMetasTableAdapter.Update(Me.DataSetEric2.ConMetas)
            CON3.Close()
            MsgBox(mensaje5)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim R As Integer = 0
        Try
            R = MsgBox("Se Eliminar�. �Deseas Continuar?", MsgBoxStyle.YesNo, "Atenci�n")
            If R = 6 Then
                Dim CON4 As New SqlConnection(MiConexion)
                CON4.Open()
                Me.ConMetasTableAdapter.Connection = CON4
                Me.ConMetasTableAdapter.Delete(CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.Clv_GrupoTextBox.Text), CInt(Me.AnioTextBox.Text))
                CON4.Close()
                MsgBox(mensaje6)
                Consultar()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    

    Private Sub ConMetasDataGridView_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConMetasDataGridView.CellLeave
        Try
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.ConMetasTableAdapter.Connection = CON3
            Me.ConMetasTableAdapter.Update(Me.DataSetEric2.ConMetas)
            CON3.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class