﻿Public Class FrmMetasVenta

    Private Sub guardar_Click(sender As Object, e As EventArgs) Handles guardar.Click
        If (Not IsNumeric(TextBoxCambaceo.Text)) Or (Not IsNumeric(TextBoxCambaceo.Text)) Then
            MsgBox("Datos incorrectos")
            Exit Sub

        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Cambaceo", SqlDbType.Int, TextBoxCambaceo.Text)
        BaseII.CreateMyParameter("@Telemarketing", SqlDbType.Int, TextBoxTelemarketing.Text)
        BaseII.CreateMyParameter("@MetaPlatino", SqlDbType.Int, TextBoxMetaPlatino.Text)
        BaseII.CreateMyParameter("@BonoPlatino", SqlDbType.Money, TextBoxBonoPlatino.Text)
        BaseII.Inserta("UpdateMetasVentasGeneral")
        MsgBox("Datos guardados exitosamente")
    End Sub

    Private Sub FrmMetasVenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaListado()
        llenaDatos()
        Button2.Enabled = False
    End Sub

    Private Sub llenaListado()
        BaseII.limpiaParametros()
        dgvrelsector.DataSource = BaseII.ConsultaDT("MuestraMetasSucursal")
    End Sub
    Private Sub llenaDatos()
        BaseII.limpiaParametros()       
        BaseII.CreateMyParameter("@Cambaceo", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Telemarketing", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@MetaPlatino", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@BonoPlatino", ParameterDirection.Output, SqlDbType.Money)
        BaseII.ProcedimientoOutPut("MuestraeMetasVentasGeneral")
        TextBoxCambaceo.Text = BaseII.dicoPar("@Cambaceo").ToString
        TextBoxTelemarketing.Text = BaseII.dicoPar("@Telemarketing").ToString
        TextBoxMetaPlatino.Text = BaseII.dicoPar("@MetaPlatino").ToString
        TextBoxBonoPlatino.Text = BaseII.dicoPar("@BonoPlatino").ToString
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not IsNumeric(dgvrelsector.SelectedCells(0).Value) Then
            Exit Sub
        End If
        TextBoxMetaSucursal.Text = dgvrelsector.SelectedCells(2).Value
        dgvrelsector.Enabled = False
        Button2.Enabled = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Not IsNumeric(TextBoxMetaSucursal.Text) Then
            Exit Sub

        End If


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_sucursal", SqlDbType.Int, dgvrelsector.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@Meta", SqlDbType.Int, TextBoxMetaSucursal.Text)
        BaseII.Inserta("UpdateMetasSucursall")
        dgvrelsector.Enabled = True
        Button2.Enabled = False
        llenaListado()
        TextBoxMetaSucursal.Text = ""
    End Sub
End Class