Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmModificaCnrPPE

    Private antComando As String
    Private antResultado As Integer
    Public antFechaH As String
    Public Consecutivo As Long

    Private Sub FrmModificaCnrPPE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dtpFechaH.MinDate = DateTime.Today
        antComando = Me.lblComando.Text
        antResultado = Me.lblResultado.Text
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ConCNRDigBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConCNRDigBindingNavigatorSaveItem.Click
        Me.GuardaCambios(Me.Consecutivo, Me.cbComando.SelectedItem.ToString, Convert.ToInt32(Me.cbResultado.SelectedItem.ToString), Me.dtpFechaH.Value)
        MsgBox("Se han hecho los cambios con exito", MsgBoxStyle.Information)
        Me.Close()
        Me.guardaBit()
        FrmCnrPPE.llenaGrid(1, 0, 0, "", 0, 0, DateTime.Today, DateTime.Today)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub GuardaCambios(ByVal consec As Long, ByVal comando As String, ByVal resultado As Integer, ByVal fecha As DateTime)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("ActualizaComandoPPE", con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Consecutivo", SqlDbType.BigInt))
        com.Parameters("@Consecutivo").Value = consec
        com.Parameters.Add(New SqlParameter("@Comando", SqlDbType.VarChar, 15))
        com.Parameters("@Comando").Value = comando
        com.Parameters.Add(New SqlParameter("@Resultado", SqlDbType.Int))
        com.Parameters("@Resultado").Value = resultado
        com.Parameters.Add(New SqlParameter("@FechaHabilitar", SqlDbType.DateTime))
        com.Parameters("@FechaHabilitar").Value = fecha
        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub guardaBit()
        bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_PPE:" + CStr(eConsecutivo), "Comando: " + Me.antComando, "Comando: " + Me.cbComando.SelectedItem.ToString, LocClv_Ciudad)
        bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_PPE:" + CStr(eConsecutivo), "Resultado :" + Me.antResultado.ToString, "Resultado :" + Me.cbResultado.SelectedItem.ToString, LocClv_Ciudad)
        bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_PPE:" + CStr(eConsecutivo), "Fecha Habilitar: " + Me.antFechaH, "Fecha Habilitar: " + Me.dtpFechaH.Value.ToShortDateString, LocClv_Ciudad)
    End Sub

End Class