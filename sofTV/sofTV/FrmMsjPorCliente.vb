Imports System.Data.SqlClient
Public Class FrmMsjPorCliente
    Dim mensaje As String = ""
    Dim respuesta As Integer = 0
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 0
            End If

            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub FrmMsjPorCliente_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            Dim CONE As New SqlConnection(MiConexion)
            Me.TextBox1.Text = eContrato
            tbcontratocompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
            CONE.Open()
            'Me.DameClientesActivosTableAdapter.Connection = CONE
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetLidia.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            '@Contrato Bigint,@NOMBRE VARCHAR(150),@CALLE VARCHAR(150),@NUMERO VARCHAR(100),@CIUDAD VARCHAR(100),@OP INT, @idcompania
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, eContrato)
            BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@ciudad", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            Dim dTable As DataTable = BaseII.ConsultaDT("DameClientesActivos")
            If dTable.Rows.Count > 0 Then
                Me.TextBox1.Text = dTable.Rows(0)(0).ToString
                'Me.tbcontratocompania.Text = dTable.Rows(0)(1).ToString
                Me.NOMBRETextBox.Text = dTable.Rows(0)(2).ToString
                Me.CALLETextBox.Text = dTable.Rows(0)(3).ToString
                Me.NUMEROTextBox.Text = dTable.Rows(0)(5).ToString
                Me.COLONIATextBox.Text = dTable.Rows(0)(4).ToString
                Me.CIUDADTextBox.Text = dTable.Rows(0)(6).ToString
                eContrato = Me.TextBox1.Text


                Me.MuestraServCteResetTableAdapter.Connection = CONE
                'Me.MuestraServCteResetTableAdapter.Fill(Me.DataSetLidia.MuestraServCteReset, eContrato, 3, respuesta, mensaje)
            End If
            '@Contrato Bigint,@Clv_TipSer int,@Res int output,@Msg varchar(150) output, @idcompania int
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, eContrato)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Res", SqlDbType.Int, respuesta)
            BaseII.CreateMyParameter("@Msg", SqlDbType.VarChar, mensaje, 150)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            MuestraServCteResetDataGridView.DataSource = BaseII.ConsultaDT("MuestraServCteReset")
            Dim Comando = New SqlClient.SqlCommand
            With Comando
                .Connection = CONE
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .CommandText = "MuestraServCteReset"

                '' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Contrato", SqlDbType.Int)
                Dim prm1 As New SqlParameter("@Clv_TipSer", SqlDbType.VarChar, 250)
                Dim prm2 As New SqlParameter("@Res", SqlDbType.VarChar, 50)
                Dim prm3 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
                Dim prm4 As New SqlParameter("@idcompania", SqlDbType.VarChar, 250)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output
                prm4.Direction = ParameterDirection.Input
                prm.Value = eContrato
                prm1.Value = 3
                prm2.Value = respuesta
                prm3.Value = mensaje
                prm4.Value = GloIdCompania
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                Dim i As Integer = Comando.ExecuteNonQuery()
                respuesta = prm2.Value
                mensaje = prm3.Value
            End With
            CONE.Close()
            eContrato = 0
            If respuesta = 1 Then
                MsgBox(mensaje)
            End If
        End If
    End Sub

    Private Sub FrmMsjPorCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_companias()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
       
        eContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim cone1 As New SqlClient.SqlConnection(MiConexion)
        If Me.TextBox2.Text.Trim.Length = 0 Then
            MsgBox("Capture un Mensaje", MsgBoxStyle.Information, "Atenci�n")
            Exit Sub
        ElseIf Me.TextBox1.Text.Length = 0 Then
            MsgBox("Favor de Escoger un Cliente", MsgBoxStyle.Information, "Atenci�n")
        Else
            cone1.Open()
            Me.Mensaje_Por_ClienteTableAdapter.Connection = cone1
            Me.Mensaje_Por_ClienteTableAdapter.Fill(Me.DataSetLidia.Mensaje_Por_Cliente, Me.TextBox1.Text, Me.TextBox2.Text)
            cone1.Close()
            MsgBox("El Mensaje Ha Sido Enviado con �xito", MsgBoxStyle.Information, "Proceso Terminado con �xito")
            bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Name, "Se Mando Un Mensaje A Un Cliente", "", Me.TextBox2.Text, LocClv_Ciudad)
            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, tbcontratocompania.KeyDown
        If e.KeyValue = Keys.Enter Then
            Dim CONE As New SqlConnection(MiConexion)
            'Me.TextBox1.Text = eContrato
            CONE.Open()
            Dim array As String()
            If tbcontratocompania.TextLength > 0 Then
                Try
                    array = tbcontratocompania.Text.Trim.Split("-")
                    GloIdCompania = array(1)
                Catch ex As Exception
                    MsgBox("Contrato inv�lido. Int�ntelo nuevamente.")
                    Exit Sub
                End Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, array(0))
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "", 250)
                BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, "", 250)
                BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, "", 250)
                BaseII.CreateMyParameter("@ciudad", SqlDbType.VarChar, "", 250)
                BaseII.CreateMyParameter("@op", SqlDbType.Int, 4)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Dim dTable As DataTable = BaseII.ConsultaDT("DameClientesActivos")
                If dTable.Rows.Count > 0 Then
                    Me.TextBox1.Text = dTable.Rows(0)(0).ToString
                    'Me.tbcontratocompania.Text = dTable.Rows(0)(1).ToString
                    Me.NOMBRETextBox.Text = dTable.Rows(0)(2).ToString
                    Me.CALLETextBox.Text = dTable.Rows(0)(3).ToString
                    Me.NUMEROTextBox.Text = dTable.Rows(0)(5).ToString
                    Me.COLONIATextBox.Text = dTable.Rows(0)(4).ToString
                    Me.CIUDADTextBox.Text = dTable.Rows(0)(6).ToString
                    eContrato = dTable.Rows(0)(0)


                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, TextBox1.Text)
                    BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 3)
                    BaseII.CreateMyParameter("@Res", SqlDbType.Int, respuesta)
                    BaseII.CreateMyParameter("@Msg", SqlDbType.VarChar, mensaje, 150)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                    MuestraServCteResetDataGridView.DataSource = BaseII.ConsultaDT("MuestraServCteReset")
                End If
                eContrato = 0
                CONE.Close()
                If respuesta = 1 Then
                    MsgBox(mensaje)
                End If
            End If
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Me.TextBox1.Text = ""
            Me.tbcontratocompania.Text = ""
            Me.NOMBRETextBox.Text = ""
            Me.CALLETextBox.Text = ""
            Me.NUMEROTextBox.Text = ""
            Me.COLONIATextBox.Text = ""
            Me.CIUDADTextBox.Text = ""
            eContrato = 0


            
            '@Contrato Bigint,@Clv_TipSer int,@Res int output,@Msg varchar(150) output, @idcompania int
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Res", SqlDbType.Int, respuesta)
            BaseII.CreateMyParameter("@Msg", SqlDbType.VarChar, mensaje, 150)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            MuestraServCteResetDataGridView.DataSource = BaseII.ConsultaDT("MuestraServCteReset")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MuestraServCteResetBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MuestraServCteResetBindingSource.CurrentChanged

    End Sub
End Class