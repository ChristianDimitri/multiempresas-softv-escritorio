Imports System.Data.SqlClient
Public Class FrmMuestraDireccion
    Private Sub FrmMuestraDireccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Locbnd2clientes = True Then
            Locbnd2clientes = False
        End If
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        Me.Muestra_Valida_DireccionTableAdapter.Connection = CON
        Me.Muestra_Valida_DireccionTableAdapter.Fill(Me.ProcedimientosArnoldo4.Muestra_Valida_Direccion, Locclv_session2)
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Locbndguardar = False
        Locbnd2clientes = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Locbndguardar = True
        Locbnd2clientes = True
        Me.Close()
    End Sub
End Class