<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaqueteAdicional
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_PaqAdiLabel As System.Windows.Forms.Label
        Dim PaqueteAdicionalLabel As System.Windows.Forms.Label
        Dim ContratacionLabel As System.Windows.Forms.Label
        Dim MensualidadLabel As System.Windows.Forms.Label
        Dim ContratacionLabel1 As System.Windows.Forms.Label
        Dim MensualidadLabel1 As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim DescripcionLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPaqueteAdicional))
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ConPaqueteAdicionalBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ConPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Clv_PaqAdiTextBox = New System.Windows.Forms.TextBox
        Me.Clv_Tipo_Paquete_AdiocionalTextBox = New System.Windows.Forms.TextBox
        Me.TipoCobroTextBox = New System.Windows.Forms.TextBox
        Me.NumeroTextBox = New System.Windows.Forms.TextBox
        Me.ContratacionTextBox = New System.Windows.Forms.TextBox
        Me.MensualidadTextBox = New System.Windows.Forms.TextBox
        Me.ConPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ConPaqueteAdicionalTableAdapter
        Me.ModPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ModPaqueteAdicionalTableAdapter
        Me.BorPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.BorPaqueteAdicionalTableAdapter
        Me.ConDetPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConDetPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ConDetPaqueteAdicionalTableAdapter
        Me.ConDetPaqueteAdicionalDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NueDetPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueDetPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.NueDetPaqueteAdicionalTableAdapter
        Me.ModDetPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModDetPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ModDetPaqueteAdicionalTableAdapter
        Me.BorDetPaqueteActivadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorDetPaqueteActivadoTableAdapter = New sofTV.DataSetEric2TableAdapters.BorDetPaqueteActivadoTableAdapter
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox
        Me.ContratacionTextBox1 = New System.Windows.Forms.TextBox
        Me.MensualidadTextBox1 = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.DescripcionComboBox1 = New System.Windows.Forms.ComboBox
        Me.MuestraServiciosEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.NuePaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NuePaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.NuePaqueteAdicionalTableAdapter
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.PaqueteAdicionalTextBox = New System.Windows.Forms.TextBox
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox
        Me.MuestraTipo_Paquetes_AdicionalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.Tipo_CobroTextBox = New System.Windows.Forms.TextBox
        Me.MuestraTipo_Paquetes_AdicionalesTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipo_Paquetes_AdicionalesTableAdapter
        Me.MuestraServiciosEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraServiciosEricTableAdapter
        Clv_PaqAdiLabel = New System.Windows.Forms.Label
        PaqueteAdicionalLabel = New System.Windows.Forms.Label
        ContratacionLabel = New System.Windows.Forms.Label
        MensualidadLabel = New System.Windows.Forms.Label
        ContratacionLabel1 = New System.Windows.Forms.Label
        MensualidadLabel1 = New System.Windows.Forms.Label
        DescripcionLabel = New System.Windows.Forms.Label
        DescripcionLabel1 = New System.Windows.Forms.Label
        CType(Me.ConPaqueteAdicionalBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConPaqueteAdicionalBindingNavigator.SuspendLayout()
        CType(Me.ConPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConDetPaqueteAdicionalDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorDetPaqueteActivadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.MuestraServiciosEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NuePaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipo_Paquetes_AdicionalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_PaqAdiLabel
        '
        Clv_PaqAdiLabel.AutoSize = True
        Clv_PaqAdiLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_PaqAdiLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_PaqAdiLabel.Location = New System.Drawing.Point(162, 36)
        Clv_PaqAdiLabel.Name = "Clv_PaqAdiLabel"
        Clv_PaqAdiLabel.Size = New System.Drawing.Size(50, 15)
        Clv_PaqAdiLabel.TabIndex = 2
        Clv_PaqAdiLabel.Text = "Clave :"
        '
        'PaqueteAdicionalLabel
        '
        PaqueteAdicionalLabel.AutoSize = True
        PaqueteAdicionalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteAdicionalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PaqueteAdicionalLabel.Location = New System.Drawing.Point(2, 63)
        PaqueteAdicionalLabel.Name = "PaqueteAdicionalLabel"
        PaqueteAdicionalLabel.Size = New System.Drawing.Size(210, 15)
        PaqueteAdicionalLabel.TabIndex = 8
        PaqueteAdicionalLabel.Text = "Nombre del Paquete Adicional :"
        '
        'ContratacionLabel
        '
        ContratacionLabel.AutoSize = True
        ContratacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratacionLabel.Location = New System.Drawing.Point(746, 177)
        ContratacionLabel.Name = "ContratacionLabel"
        ContratacionLabel.Size = New System.Drawing.Size(92, 15)
        ContratacionLabel.TabIndex = 12
        ContratacionLabel.Text = "Contratación:"
        '
        'MensualidadLabel
        '
        MensualidadLabel.AutoSize = True
        MensualidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensualidadLabel.Location = New System.Drawing.Point(878, 177)
        MensualidadLabel.Name = "MensualidadLabel"
        MensualidadLabel.Size = New System.Drawing.Size(94, 15)
        MensualidadLabel.TabIndex = 14
        MensualidadLabel.Text = "Mensualidad:"
        '
        'ContratacionLabel1
        '
        ContratacionLabel1.AutoSize = True
        ContratacionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratacionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ContratacionLabel1.Location = New System.Drawing.Point(13, 7)
        ContratacionLabel1.Name = "ContratacionLabel1"
        ContratacionLabel1.Size = New System.Drawing.Size(92, 15)
        ContratacionLabel1.TabIndex = 25
        ContratacionLabel1.Text = "Contratación:"
        '
        'MensualidadLabel1
        '
        MensualidadLabel1.AutoSize = True
        MensualidadLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensualidadLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        MensualidadLabel1.Location = New System.Drawing.Point(145, 7)
        MensualidadLabel1.Name = "MensualidadLabel1"
        MensualidadLabel1.Size = New System.Drawing.Size(94, 15)
        MensualidadLabel1.TabIndex = 27
        MensualidadLabel1.Text = "Mensualidad:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(35, 90)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(177, 15)
        DescripcionLabel.TabIndex = 20
        DescripcionLabel.Text = "Tarifa de Larga Distancia :"
        AddHandler DescripcionLabel.Click, AddressOf Me.DescripcionLabel_Click
        '
        'DescripcionLabel1
        '
        DescripcionLabel1.AutoSize = True
        DescripcionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel1.Location = New System.Drawing.Point(14, 7)
        DescripcionLabel1.Name = "DescripcionLabel1"
        DescripcionLabel1.Size = New System.Drawing.Size(62, 15)
        DescripcionLabel1.TabIndex = 0
        DescripcionLabel1.Text = "Servicio:"
        '
        'ConPaqueteAdicionalBindingNavigator
        '
        Me.ConPaqueteAdicionalBindingNavigator.AddNewItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.BindingSource = Me.ConPaqueteAdicionalBindingSource
        Me.ConPaqueteAdicionalBindingNavigator.CountItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.DeleteItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConPaqueteAdicionalBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConPaqueteAdicionalBindingNavigatorSaveItem})
        Me.ConPaqueteAdicionalBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConPaqueteAdicionalBindingNavigator.MoveFirstItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.MoveLastItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.MoveNextItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.MovePreviousItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.Name = "ConPaqueteAdicionalBindingNavigator"
        Me.ConPaqueteAdicionalBindingNavigator.PositionItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConPaqueteAdicionalBindingNavigator.Size = New System.Drawing.Size(702, 25)
        Me.ConPaqueteAdicionalBindingNavigator.TabIndex = 0
        Me.ConPaqueteAdicionalBindingNavigator.Text = "BindingNavigator1"
        '
        'ConPaqueteAdicionalBindingSource
        '
        Me.ConPaqueteAdicionalBindingSource.DataMember = "ConPaqueteAdicional"
        Me.ConPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(82, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConPaqueteAdicionalBindingNavigatorSaveItem
        '
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConPaqueteAdicionalBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Name = "ConPaqueteAdicionalBindingNavigatorSaveItem"
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Size = New System.Drawing.Size(83, 22)
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_PaqAdiTextBox
        '
        Me.Clv_PaqAdiTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_PaqAdiTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Clv_PaqAdi", True))
        Me.Clv_PaqAdiTextBox.Enabled = False
        Me.Clv_PaqAdiTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_PaqAdiTextBox.Location = New System.Drawing.Point(218, 33)
        Me.Clv_PaqAdiTextBox.Name = "Clv_PaqAdiTextBox"
        Me.Clv_PaqAdiTextBox.ReadOnly = True
        Me.Clv_PaqAdiTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_PaqAdiTextBox.TabIndex = 3
        Me.Clv_PaqAdiTextBox.TabStop = False
        '
        'Clv_Tipo_Paquete_AdiocionalTextBox
        '
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Clv_Tipo_Paquete_Adiocional", True))
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Location = New System.Drawing.Point(736, 5)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Name = "Clv_Tipo_Paquete_AdiocionalTextBox"
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.ReadOnly = True
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Size = New System.Drawing.Size(10, 21)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.TabIndex = 5
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.TabStop = False
        '
        'TipoCobroTextBox
        '
        Me.TipoCobroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "TipoCobro", True))
        Me.TipoCobroTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoCobroTextBox.Location = New System.Drawing.Point(752, 4)
        Me.TipoCobroTextBox.Name = "TipoCobroTextBox"
        Me.TipoCobroTextBox.ReadOnly = True
        Me.TipoCobroTextBox.Size = New System.Drawing.Size(10, 21)
        Me.TipoCobroTextBox.TabIndex = 7
        Me.TipoCobroTextBox.TabStop = False
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Numero", True))
        Me.NumeroTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroTextBox.Location = New System.Drawing.Point(218, 122)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(126, 21)
        Me.NumeroTextBox.TabIndex = 11
        '
        'ContratacionTextBox
        '
        Me.ContratacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratacionTextBox.Location = New System.Drawing.Point(749, 195)
        Me.ContratacionTextBox.Name = "ContratacionTextBox"
        Me.ContratacionTextBox.Size = New System.Drawing.Size(126, 21)
        Me.ContratacionTextBox.TabIndex = 13
        '
        'MensualidadTextBox
        '
        Me.MensualidadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensualidadTextBox.Location = New System.Drawing.Point(881, 195)
        Me.MensualidadTextBox.Name = "MensualidadTextBox"
        Me.MensualidadTextBox.Size = New System.Drawing.Size(126, 21)
        Me.MensualidadTextBox.TabIndex = 15
        '
        'ConPaqueteAdicionalTableAdapter
        '
        Me.ConPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ModPaqueteAdicionalBindingSource
        '
        Me.ModPaqueteAdicionalBindingSource.DataMember = "ModPaqueteAdicional"
        Me.ModPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'ModPaqueteAdicionalTableAdapter
        '
        Me.ModPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'BorPaqueteAdicionalBindingSource
        '
        Me.BorPaqueteAdicionalBindingSource.DataMember = "BorPaqueteAdicional"
        Me.BorPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'BorPaqueteAdicionalTableAdapter
        '
        Me.BorPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ConDetPaqueteAdicionalBindingSource
        '
        Me.ConDetPaqueteAdicionalBindingSource.DataMember = "ConDetPaqueteAdicional"
        Me.ConDetPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'ConDetPaqueteAdicionalTableAdapter
        '
        Me.ConDetPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ConDetPaqueteAdicionalDataGridView
        '
        Me.ConDetPaqueteAdicionalDataGridView.AllowUserToAddRows = False
        Me.ConDetPaqueteAdicionalDataGridView.AllowUserToDeleteRows = False
        Me.ConDetPaqueteAdicionalDataGridView.AutoGenerateColumns = False
        Me.ConDetPaqueteAdicionalDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConDetPaqueteAdicionalDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.ConDetPaqueteAdicionalDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5})
        Me.ConDetPaqueteAdicionalDataGridView.DataSource = Me.ConDetPaqueteAdicionalBindingSource
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConDetPaqueteAdicionalDataGridView.DefaultCellStyle = DataGridViewCellStyle14
        Me.ConDetPaqueteAdicionalDataGridView.Location = New System.Drawing.Point(64, 193)
        Me.ConDetPaqueteAdicionalDataGridView.Name = "ConDetPaqueteAdicionalDataGridView"
        Me.ConDetPaqueteAdicionalDataGridView.ReadOnly = True
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConDetPaqueteAdicionalDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.ConDetPaqueteAdicionalDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConDetPaqueteAdicionalDataGridView.Size = New System.Drawing.Size(515, 229)
        Me.ConDetPaqueteAdicionalDataGridView.TabIndex = 16
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 200
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clave"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_PaqAdi"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_PaqAdi"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Contratacion"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contratación"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 130
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Mensualidad"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Mensualidad"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 130
        '
        'NueDetPaqueteAdicionalBindingSource
        '
        Me.NueDetPaqueteAdicionalBindingSource.DataMember = "NueDetPaqueteAdicional"
        Me.NueDetPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'NueDetPaqueteAdicionalTableAdapter
        '
        Me.NueDetPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ModDetPaqueteAdicionalBindingSource
        '
        Me.ModDetPaqueteAdicionalBindingSource.DataMember = "ModDetPaqueteAdicional"
        Me.ModDetPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'ModDetPaqueteAdicionalTableAdapter
        '
        Me.ModDetPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'BorDetPaqueteActivadoBindingSource
        '
        Me.BorDetPaqueteActivadoBindingSource.DataMember = "BorDetPaqueteActivado"
        Me.BorDetPaqueteActivadoBindingSource.DataSource = Me.DataSetEric2
        '
        'BorDetPaqueteActivadoTableAdapter
        '
        Me.BorDetPaqueteActivadoTableAdapter.ClearBeforeFill = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(597, 1)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 20
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(613, 1)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_ServicioTextBox.TabIndex = 24
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'ContratacionTextBox1
        '
        Me.ContratacionTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Contratacion", True))
        Me.ContratacionTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratacionTextBox1.Location = New System.Drawing.Point(629, 1)
        Me.ContratacionTextBox1.Name = "ContratacionTextBox1"
        Me.ContratacionTextBox1.ReadOnly = True
        Me.ContratacionTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.ContratacionTextBox1.TabIndex = 26
        Me.ContratacionTextBox1.TabStop = False
        '
        'MensualidadTextBox1
        '
        Me.MensualidadTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Mensualidad", True))
        Me.MensualidadTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensualidadTextBox1.Location = New System.Drawing.Point(645, 1)
        Me.MensualidadTextBox1.Name = "MensualidadTextBox1"
        Me.MensualidadTextBox1.ReadOnly = True
        Me.MensualidadTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.MensualidadTextBox1.TabIndex = 28
        Me.MensualidadTextBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.ConDetPaqueteAdicionalDataGridView)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Location = New System.Drawing.Point(30, 216)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(655, 428)
        Me.Panel1.TabIndex = 33
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(52, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(165, 15)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Costo con Plan Tarifario:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.TextBox2)
        Me.Panel3.Controls.Add(ContratacionLabel1)
        Me.Panel3.Controls.Add(Me.TextBox1)
        Me.Panel3.Controls.Add(MensualidadLabel1)
        Me.Panel3.Location = New System.Drawing.Point(306, 53)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(289, 53)
        Me.Panel3.TabIndex = 43
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(148, 25)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(126, 21)
        Me.TextBox2.TabIndex = 41
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(16, 25)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(126, 21)
        Me.TextBox1.TabIndex = 40
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(DescripcionLabel1)
        Me.Panel2.Controls.Add(Me.DescripcionComboBox1)
        Me.Panel2.Location = New System.Drawing.Point(55, 53)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(245, 53)
        Me.Panel2.TabIndex = 42
        '
        'DescripcionComboBox1
        '
        Me.DescripcionComboBox1.DataSource = Me.MuestraServiciosEricBindingSource
        Me.DescripcionComboBox1.DisplayMember = "Descripcion"
        Me.DescripcionComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox1.FormattingEnabled = True
        Me.DescripcionComboBox1.Location = New System.Drawing.Point(17, 23)
        Me.DescripcionComboBox1.Name = "DescripcionComboBox1"
        Me.DescripcionComboBox1.Size = New System.Drawing.Size(225, 23)
        Me.DescripcionComboBox1.TabIndex = 1
        Me.DescripcionComboBox1.ValueMember = "Clv_Servicio"
        '
        'MuestraServiciosEricBindingSource
        '
        Me.MuestraServiciosEricBindingSource.DataMember = "MuestraServiciosEric"
        Me.MuestraServiciosEricBindingSource.DataSource = Me.DataSetEric2
        '
        'Button5
        '
        Me.Button5.Enabled = False
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(448, 132)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(78, 32)
        Me.Button5.TabIndex = 38
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(363, 132)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(78, 32)
        Me.Button4.TabIndex = 37
        Me.Button4.Text = "&Eliminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Enabled = False
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(279, 132)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(78, 32)
        Me.Button3.TabIndex = 36
        Me.Button3.Text = "&Guardar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Enabled = False
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(193, 133)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(78, 32)
        Me.Button2.TabIndex = 35
        Me.Button2.Text = "&Modificar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(109, 133)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(78, 31)
        Me.Button1.TabIndex = 34
        Me.Button1.Text = "&Nuevo"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(549, 655)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 39
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'NuePaqueteAdicionalBindingSource
        '
        Me.NuePaqueteAdicionalBindingSource.DataMember = "NuePaqueteAdicional"
        Me.NuePaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'NuePaqueteAdicionalTableAdapter
        '
        Me.NuePaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.NumericUpDown1)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.TextBox3)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.PaqueteAdicionalTextBox)
        Me.Panel4.Controls.Add(DescripcionLabel)
        Me.Panel4.Controls.Add(Me.DescripcionComboBox)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(PaqueteAdicionalLabel)
        Me.Panel4.Controls.Add(Clv_PaqAdiLabel)
        Me.Panel4.Controls.Add(Me.NumeroTextBox)
        Me.Panel4.Controls.Add(Me.Clv_PaqAdiTextBox)
        Me.Panel4.Location = New System.Drawing.Point(30, 28)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(655, 188)
        Me.Panel4.TabIndex = 40
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConPaqueteAdicionalBindingSource, "Minutos", True))
        Me.NumericUpDown1.Location = New System.Drawing.Point(556, 120)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(53, 20)
        Me.NumericUpDown1.TabIndex = 49
        Me.NumericUpDown1.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(422, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 15)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Minutos Incluidos :"
        Me.Label5.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(162, 152)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 15)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Costo :"
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Costo", True))
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(218, 149)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(110, 21)
        Me.TextBox3.TabIndex = 46
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(52, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(127, 15)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Paquete Adicional:"
        '
        'PaqueteAdicionalTextBox
        '
        Me.PaqueteAdicionalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "PaqueteAdicional", True))
        Me.PaqueteAdicionalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaqueteAdicionalTextBox.Location = New System.Drawing.Point(218, 60)
        Me.PaqueteAdicionalTextBox.Name = "PaqueteAdicionalTextBox"
        Me.PaqueteAdicionalTextBox.Size = New System.Drawing.Size(207, 21)
        Me.PaqueteAdicionalTextBox.TabIndex = 23
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataSource = Me.MuestraTipo_Paquetes_AdicionalesBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(218, 87)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(207, 23)
        Me.DescripcionComboBox.TabIndex = 21
        Me.DescripcionComboBox.ValueMember = "Clv_Tipo_Paquete_Adiocional"
        '
        'MuestraTipo_Paquetes_AdicionalesBindingSource
        '
        Me.MuestraTipo_Paquetes_AdicionalesBindingSource.DataMember = "MuestraTipo_Paquetes_Adicionales"
        Me.MuestraTipo_Paquetes_AdicionalesBindingSource.DataSource = Me.DataSetEric2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(151, 125)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Xxxxx:"
        '
        'Tipo_CobroTextBox
        '
        Me.Tipo_CobroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipo_Paquetes_AdicionalesBindingSource, "Tipo_Cobro", True))
        Me.Tipo_CobroTextBox.Location = New System.Drawing.Point(768, 5)
        Me.Tipo_CobroTextBox.Name = "Tipo_CobroTextBox"
        Me.Tipo_CobroTextBox.ReadOnly = True
        Me.Tipo_CobroTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Tipo_CobroTextBox.TabIndex = 22
        Me.Tipo_CobroTextBox.TabStop = False
        '
        'MuestraTipo_Paquetes_AdicionalesTableAdapter
        '
        Me.MuestraTipo_Paquetes_AdicionalesTableAdapter.ClearBeforeFill = True
        '
        'MuestraServiciosEricTableAdapter
        '
        Me.MuestraServiciosEricTableAdapter.ClearBeforeFill = True
        '
        'FrmPaqueteAdicional
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(702, 710)
        Me.Controls.Add(Me.ConPaqueteAdicionalBindingNavigator)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MensualidadTextBox1)
        Me.Controls.Add(Me.TipoCobroTextBox)
        Me.Controls.Add(Me.MensualidadTextBox)
        Me.Controls.Add(MensualidadLabel)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.ContratacionTextBox)
        Me.Controls.Add(Me.Clv_Tipo_Paquete_AdiocionalTextBox)
        Me.Controls.Add(ContratacionLabel)
        Me.Controls.Add(Me.Tipo_CobroTextBox)
        Me.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Controls.Add(Me.ContratacionTextBox1)
        Me.MaximizeBox = False
        Me.Name = "FrmPaqueteAdicional"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paquete Adicional"
        CType(Me.ConPaqueteAdicionalBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConPaqueteAdicionalBindingNavigator.ResumeLayout(False)
        Me.ConPaqueteAdicionalBindingNavigator.PerformLayout()
        CType(Me.ConPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConDetPaqueteAdicionalDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorDetPaqueteActivadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.MuestraServiciosEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NuePaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipo_Paquetes_AdicionalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ConPaqueteAdicionalTableAdapter
    Friend WithEvents ConPaqueteAdicionalBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConPaqueteAdicionalBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_PaqAdiTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_Tipo_Paquete_AdiocionalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TipoCobroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MensualidadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ModPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ModPaqueteAdicionalTableAdapter
    Friend WithEvents BorPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.BorPaqueteAdicionalTableAdapter
    Friend WithEvents ConDetPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConDetPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ConDetPaqueteAdicionalTableAdapter
    Friend WithEvents ConDetPaqueteAdicionalDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents NueDetPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueDetPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.NueDetPaqueteAdicionalTableAdapter
    Friend WithEvents ModDetPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModDetPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ModDetPaqueteAdicionalTableAdapter
    Friend WithEvents BorDetPaqueteActivadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorDetPaqueteActivadoTableAdapter As sofTV.DataSetEric2TableAdapters.BorDetPaqueteActivadoTableAdapter
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratacionTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents MensualidadTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents NuePaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NuePaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.NuePaqueteAdicionalTableAdapter
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MuestraTipo_Paquetes_AdicionalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipo_Paquetes_AdicionalesTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipo_Paquetes_AdicionalesTableAdapter
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraServiciosEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraServiciosEricTableAdapter
    Friend WithEvents DescripcionComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Tipo_CobroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PaqueteAdicionalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
End Class
