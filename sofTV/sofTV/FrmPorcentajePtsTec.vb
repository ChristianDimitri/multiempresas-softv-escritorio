﻿Public Class FrmPorcentajePtsTec

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox1, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox2, Asc(e.KeyChar), "N"))
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        If (CInt(TextBox1.Text) + CInt(TextBox2.Text)) <> 100 Then
            MsgBox("Los campos deben sumar 100%")
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Jefe", SqlDbType.Int, CInt(TextBox1.Text))
        BaseII.CreateMyParameter("@Ayuda", SqlDbType.Int, CInt(TextBox2.Text))
        BaseII.Inserta("PorcentajePtsTec")

        MsgBox("Guardado con éxito")
        Me.Close()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmPorcentajePtsTec_Load(sender As Object, e As EventArgs) Handles Me.Load

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Jefe", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Ayuda", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ConPorcentajePtsTec")
        TextBox1.Text = BaseII.dicoPar("@Jefe").ToString()
        TextBox2.Text = BaseII.dicoPar("@Ayuda").ToString()

    End Sub
End Class