Imports System.Data.SqlClient
Imports System.Text
Public Class FrmPreguntas

    Dim eIDRespuesta As Integer = 0

    Private Sub ConPreguntas(ByVal IDPregunta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConPreguntas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDPregunta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Pregunta", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@IdTipoRespuesta", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Activa", SqlDbType.Bit)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            TextBoxPregunta.Text = parametro2.Value.ToString()
            ConTiposRespuestas(CInt(parametro3.Value.ToString()), 1)
            CheckBoxActiva.Checked = parametro4.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NuePreguntas(ByVal Pregunta As String, ByVal IDTipoRespuesta As Integer, ByVal Activa As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NuePreguntas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Pregunta", SqlDbType.VarChar, 250)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Pregunta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDTipoRespuesta", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDTipoRespuesta
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Activa", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Activa
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eIDPregunta = CInt(parametro4.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ModPreguntas(ByVal IDPregunta As Integer, ByVal Pregunta As String, ByVal IDTipoRespuesta As Integer, ByVal Activa As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModPreguntas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDPregunta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Pregunta", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Pregunta
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@IDTipoRespuesta", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = IDTipoRespuesta
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Activa", SqlDbType.Bit)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Activa
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorPreguntas(ByVal IDPregunta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorPreguntas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDPregunta
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConRespuestas(ByVal IDPregunta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConRespuestas ")
        strSQL.Append(CStr(IDPregunta))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim x As Integer = 0

        Try
            dataAdapter.Fill(dataTable)
            Me.TreeViewRespuesta.Nodes.Clear()

            For Each Row As DataRow In dataTable.Rows
                Me.TreeViewRespuesta.Nodes.Add(Row("Respuesta").ToString())
                Me.TreeViewRespuesta.Nodes(x).Tag = Row("IDRespuesta").ToString()
                x += 1
            Next

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub NueRespuestas(ByVal IDPregunta As Integer, ByVal Respuesta As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRespuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDPregunta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Respuesta", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Respuesta
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ModRespuestas(ByVal IDRespuesta As Integer, ByVal Respuesta As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModRespuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDRespuesta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDRespuesta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Respuesta", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Respuesta
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorRespuestas(ByVal IDRespuesta As Integer, ByVal IDPregunta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorRespuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDRespuesta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDRespuesta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDPregunta
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Op", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Op
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Close()
        End Try
    End Sub

    Private Sub ConTiposRespuestas(ByVal IDTipoRespuesta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConTiposRespuestas ")
        strSQL.Append(CStr(IDTipoRespuesta) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBoxTipoRespuesta.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Function ValidaBorRespuestas(ByVal IDPregunta As Integer, ByVal IDTipoRespuesta As Integer) As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaBorRespuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDPregunta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDTipoRespuesta", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDTipoRespuesta
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Bnd", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Return parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Private Sub AgregarRespuestasExistentes(ByVal IDPregunta As Integer, ByVal IDPreguntaAux As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("AgregarRespuestasExistentes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDPregunta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDPreguntaAux", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDPreguntaAux
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Bloquear(ByVal Op As Boolean)
        TextBoxRespuesta.Enabled = Op
        TreeViewRespuesta.Enabled = Op
        ButtonAgregar.Enabled = Op
        ButtonEliminar.Enabled = Op
        ButtonExistentes.Enabled = Op
    End Sub

    Private Sub LimpiarObjetos()
        TextBoxPregunta.Clear()
        TextBoxRespuesta.Clear()
        TreeViewRespuesta.Nodes.Clear()
    End Sub

    Private Sub TreeViewRespuesta_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewRespuesta.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eIDRespuesta = e.Node.Tag
        End If
    End Sub

    Private Sub FrmPreguntas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBndRespuesta = True Then
            eBndRespuesta = False
            If eIDPreguntaAux = 0 Then
                Exit Sub
            End If
            AgregarRespuestasExistentes(eIDPregunta, eIDPreguntaAux)
            ConRespuestas(eIDPregunta)
        End If
    End Sub

    Private Sub FrmPreguntas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If eOpcion = "N" Then
            ConTiposRespuestas(0, 0)
            Bloquear(False)
            TSBNuevo.Enabled = False
            TSBEliminar.Enabled = False
        ElseIf eOpcion = "C" Or eOpcion = "M" Then
            ConPreguntas(eIDPregunta)
            ConRespuestas(eIDPregunta)
        End If

        If eOpcion = "C" Then
            BindingNavigatorPreguntas.Enabled = False
            TextBoxPregunta.Enabled = False
            ComboBoxTipoRespuesta.Enabled = False
            Bloquear(False)
        ElseIf eOpcion = "M" Then
            ComboBoxTipoRespuesta.Enabled = False
        End If
        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub ButtonAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregar.Click
        If TextBoxRespuesta.Text.Length = 0 Then
            MsgBox("Captura una respuesta.", MsgBoxStyle.Information)
            Exit Sub
        End If
        NueRespuestas(eIDPregunta, TextBoxRespuesta.Text)
        ConRespuestas(eIDPregunta)
        TextBoxRespuesta.Clear()
    End Sub

    Private Sub ButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminar.Click
        If IsNumeric(eIDRespuesta) = False Then
            MsgBox("Seleccion una respuesta.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If ValidaEliminarRespuestas(eIDRespuesta).Length <> 0 Then
            MsgBox(ValidaEliminarRespuestas(eIDRespuesta), MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        BorRespuestas(eIDRespuesta, 0, 0)
        ConRespuestas(eIDPregunta)
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub ComboBoxTipoRespuesta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxTipoRespuesta.SelectedIndexChanged

        If eOpcion = "N" Then
            eIDPregunta = 0
            Exit Sub
        End If

        If IsNumeric(ComboBoxTipoRespuesta.SelectedValue) = False Then
            Exit Sub
        End If

        If ValidaBorRespuestas(eIDPregunta, ComboBoxTipoRespuesta.SelectedValue) = True Then
            Bloquear(True)
        Else
            Bloquear(False)
            'If eIDRespuesta = 0 Then BorRespuestas(0, eIDPregunta, 1)
        End If

    End Sub

    Private Sub ButtonExistentes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonExistentes.Click
        eBndRespuesta = False
        eIDPreguntaAux = 0
        FrmRespuestas.Show()
    End Sub

    Private Sub TSBNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBNuevo.Click
        eOpcion = "N"
        TSBNuevo.Enabled = False
        TSBEliminar.Enabled = False
        ComboBoxTipoRespuesta.Enabled = True
        LimpiarObjetos()
        Bloquear(False)
    End Sub

    Private Sub TSBGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBGuardar.Click
        If TextBoxPregunta.Text.Length = 0 Then
            MsgBox("Captura una pregunta.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If eOpcion = "M" Then
            ModPreguntas(eIDPregunta, TextBoxPregunta.Text, ComboBoxTipoRespuesta.SelectedValue, CheckBoxActiva.Checked)
        ElseIf eOpcion = "N" Then
            eIDPregunta = 0
            NuePreguntas(TextBoxPregunta.Text, ComboBoxTipoRespuesta.SelectedValue, CheckBoxActiva.Checked)
            Bloquear(ValidaBorRespuestas(eIDPregunta, ComboBoxTipoRespuesta.SelectedValue))
            ConRespuestas(eIDPregunta)
            TSBNuevo.Enabled = True
            TSBEliminar.Enabled = True
            ComboBoxTipoRespuesta.Enabled = False
            eOpcion = "M"
        End If

        eBnd = True
        MsgBox(mensaje5, MsgBoxStyle.Information)
    End Sub

    Private Sub TSBEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBEliminar.Click
        If eOpcion <> "M" Then
            Exit Sub
        End If

        If ValidaEliminarPreguntas(eIDPregunta).Length <> 0 Then
            MsgBox(ValidaEliminarPreguntas(eIDPregunta), MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Dim res As Integer
        res = MsgBox("�Deseas eliminar la Pregunta con sus Respuestas?", MsgBoxStyle.YesNo)
        If res = 6 Then
            BorPreguntas(eIDPregunta)
            BorRespuestas(0, eIDPregunta, 1)
            eBnd = True
            MsgBox(mensaje6, MsgBoxStyle.Information)
            Me.Close()
        End If

    End Sub

    Function ValidaEliminarPreguntas(ByVal IDPregunta As Integer) As String
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaEliminarPreguntas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDPregunta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Return parametro3.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Function ValidaEliminarRespuestas(ByVal IDRespuesta) As String
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaEliminarRespuestas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDRespuesta", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDRespuesta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Return parametro3.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Function


    Private Sub TextBoxRespuesta_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxRespuesta.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBoxRespuesta, Asc(LCase(e.KeyChar)), "S")))
    End Sub
End Class