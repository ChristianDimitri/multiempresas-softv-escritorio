﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRangosComisionCambaceo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBRangoInferiorLabel = New System.Windows.Forms.Label()
        Me.CMBRangoSuperiorLabel = New System.Windows.Forms.Label()
        Me.CveRangoTextBox = New System.Windows.Forms.TextBox()
        Me.RangoInferiorTextBox = New System.Windows.Forms.TextBox()
        Me.RangoSuperiorTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBoxPago = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ConCatalogoDeRangosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        CType(Me.ConCatalogoDeRangosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConCatalogoDeRangosBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBRangoInferiorLabel
        '
        Me.CMBRangoInferiorLabel.AutoSize = True
        Me.CMBRangoInferiorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBRangoInferiorLabel.Location = New System.Drawing.Point(70, 50)
        Me.CMBRangoInferiorLabel.Name = "CMBRangoInferiorLabel"
        Me.CMBRangoInferiorLabel.Size = New System.Drawing.Size(119, 15)
        Me.CMBRangoInferiorLabel.TabIndex = 4
        Me.CMBRangoInferiorLabel.Text = "Rango Inferior %:"
        '
        'CMBRangoSuperiorLabel
        '
        Me.CMBRangoSuperiorLabel.AutoSize = True
        Me.CMBRangoSuperiorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBRangoSuperiorLabel.Location = New System.Drawing.Point(69, 79)
        Me.CMBRangoSuperiorLabel.Name = "CMBRangoSuperiorLabel"
        Me.CMBRangoSuperiorLabel.Size = New System.Drawing.Size(128, 15)
        Me.CMBRangoSuperiorLabel.TabIndex = 6
        Me.CMBRangoSuperiorLabel.Text = "Rango Superior %:"
        '
        'CveRangoTextBox
        '
        Me.CveRangoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CveRangoTextBox.Location = New System.Drawing.Point(371, 136)
        Me.CveRangoTextBox.Name = "CveRangoTextBox"
        Me.CveRangoTextBox.ReadOnly = True
        Me.CveRangoTextBox.Size = New System.Drawing.Size(10, 21)
        Me.CveRangoTextBox.TabIndex = 3
        Me.CveRangoTextBox.TabStop = False
        Me.CveRangoTextBox.Visible = False
        '
        'RangoInferiorTextBox
        '
        Me.RangoInferiorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RangoInferiorTextBox.Location = New System.Drawing.Point(196, 47)
        Me.RangoInferiorTextBox.Name = "RangoInferiorTextBox"
        Me.RangoInferiorTextBox.Size = New System.Drawing.Size(226, 21)
        Me.RangoInferiorTextBox.TabIndex = 100
        '
        'RangoSuperiorTextBox
        '
        Me.RangoSuperiorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RangoSuperiorTextBox.Location = New System.Drawing.Point(196, 74)
        Me.RangoSuperiorTextBox.Name = "RangoSuperiorTextBox"
        Me.RangoSuperiorTextBox.Size = New System.Drawing.Size(226, 21)
        Me.RangoSuperiorTextBox.TabIndex = 101
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(335, 144)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 130
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBoxPago
        '
        Me.TextBoxPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPago.Location = New System.Drawing.Point(196, 101)
        Me.TextBoxPago.Name = "TextBoxPago"
        Me.TextBoxPago.Size = New System.Drawing.Size(226, 21)
        Me.TextBoxPago.TabIndex = 132
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(70, 106)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 15)
        Me.Label1.TabIndex = 131
        Me.Label1.Text = "Pago:"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(72, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConCatalogoDeRangosBindingNavigatorSaveItem
        '
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem.Name = "ConCatalogoDeRangosBindingNavigatorSaveItem"
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem.Size = New System.Drawing.Size(72, 22)
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ConCatalogoDeRangosBindingNavigator
        '
        Me.ConCatalogoDeRangosBindingNavigator.AddNewItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.CountItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.DeleteItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConCatalogoDeRangosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConCatalogoDeRangosBindingNavigatorSaveItem})
        Me.ConCatalogoDeRangosBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConCatalogoDeRangosBindingNavigator.MoveFirstItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.MoveLastItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.MoveNextItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.MovePreviousItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.Name = "ConCatalogoDeRangosBindingNavigator"
        Me.ConCatalogoDeRangosBindingNavigator.PositionItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConCatalogoDeRangosBindingNavigator.Size = New System.Drawing.Size(483, 25)
        Me.ConCatalogoDeRangosBindingNavigator.TabIndex = 120
        Me.ConCatalogoDeRangosBindingNavigator.TabStop = True
        Me.ConCatalogoDeRangosBindingNavigator.Text = "BindingNavigator1"
        '
        'FrmRangosComisionCambaceo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(483, 192)
        Me.Controls.Add(Me.TextBoxPago)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RangoSuperiorTextBox)
        Me.Controls.Add(Me.CMBRangoSuperiorLabel)
        Me.Controls.Add(Me.RangoInferiorTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.CMBRangoInferiorLabel)
        Me.Controls.Add(Me.ConCatalogoDeRangosBindingNavigator)
        Me.Controls.Add(Me.CveRangoTextBox)
        Me.Name = "FrmRangosComisionCambaceo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rango Pago Comisiones del Coordinador de Cambaceo"
        CType(Me.ConCatalogoDeRangosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConCatalogoDeRangosBindingNavigator.ResumeLayout(False)
        Me.ConCatalogoDeRangosBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CveRangoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoInferiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoSuperiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents CMBRangoInferiorLabel As System.Windows.Forms.Label
    Friend WithEvents CMBRangoSuperiorLabel As System.Windows.Forms.Label
    Friend WithEvents TextBoxPago As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConCatalogoDeRangosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConCatalogoDeRangosBindingNavigator As System.Windows.Forms.BindingNavigator
End Class
