﻿Public Class FrmRangosComisionCambaceo

    Private Sub FrmRangosComisionCambaceo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If eOpcion = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
        End If

        If eOpcion = "M" Then
            RangoInferiorTextBox.Text = eRangoInferior
            RangoSuperiorTextBox.Text = eRangoSuperior
            TextBoxPago.Text = ePrecio

            RangoInferiorTextBox.Enabled = False
            RangoSuperiorTextBox.Enabled = False

        End If

        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub ConCatalogoDeRangosBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles ConCatalogoDeRangosBindingNavigatorSaveItem.Click
        If RangoInferiorTextBox.Text = "" Or RangoSuperiorTextBox.Text = "" Or TextBoxPago.Text = "" Then
            MsgBox("Debe llenar todos los campos para poder continuar")
            Exit Sub
        End If
        If CInt(RangoInferiorTextBox.Text) >= CInt(RangoSuperiorTextBox.Text) Then
            MsgBox("Error, el limite superior debe ser mayor inferior")
            Exit Sub
        End If

        If eOpcion = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@LimInferior", SqlDbType.SmallInt, RangoInferiorTextBox.Text)
            BaseII.CreateMyParameter("@LimSuperior", SqlDbType.SmallInt, RangoSuperiorTextBox.Text)
            BaseII.CreateMyParameter("@Pago", SqlDbType.Money, TextBoxPago.Text)
            BaseII.CreateMyParameter("@banInserto", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("InsertRangoComisionCambaceo")
            If CBool(BaseII.dicoPar("@banInserto").ToString) Then
                MsgBox("Datos guardados exitosamente")
                Me.Close()
            Else
                MsgBox("Ya existe un rango que cubre esos valores")
            End If

        Else
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, eCveRango)
            BaseII.CreateMyParameter("@Pago", SqlDbType.Money, TextBoxPago.Text)
            BaseII.Inserta("UpdateRangoComisionCambaceo")
            MsgBox("Datos guardados exitosamente")
            Me.Close()
            'MsgBox("Datos guardados exitosamente")
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, eCveRango)
        BaseII.Inserta("DeleteRangoComisionCambaceo")
        MsgBox("Eliminado exitosamente")
        Me.Close()
    End Sub
End Class