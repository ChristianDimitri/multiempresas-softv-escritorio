Imports System.Data.SqlClient

Public Class FrmReactivarContrato
    Dim eRes As Integer
    Dim eMsg As String = Nothing
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmReactivarContrato_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If pasa = 1 Then
            If eContratoRec > 0 Then
                Me.TextBox1.Text = eContratoRec
                eContratoRec = 0
            End If
        ElseIf pasa = 2 Then
            Me.TextBox1.Clear()
        End If
    End Sub

    Private Sub FrmReactivarContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Reactivar()
    End Sub
    Private Sub Reactivar()
        If IsNumeric(Me.TextBox1.Text) = True And GloClv_Servicio > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Text, "", Me.CMBLabel1.Text, GloSucursal, LocClv_Ciudad)
            Me.ReactivaContratoTableAdapter1.Connection = CON
            Me.ReactivaContratoTableAdapter1.Fill(Me.DataSetLidia.ReactivaContrato, CLng(Me.TextBox1.Text), eRes, eMsg, GloClv_Servicio, GloClv_TipSer, eCabModPropio)
            CON.Close()
            GloClv_Servicio = 0
            MsgBox(eMsg)
            pasa = 0
            Me.TextBox1.Clear()
            Me.Close()
        ElseIf GloClv_Servicio = 0 And IsNumeric(Me.TextBox1.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameClientesInactivosTableAdapter.Connection = CON
            Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, Me.TextBox1.Text, "", "", "", "", 0)
            If IsNumeric(Me.TextBox2.Text) = True Then
                Dim resp As MsgBoxResult = MsgBoxResult.Cancel
                resp = MsgBox("El Contrato NO Tiene Asignado un Servicio, � Desea Hacerlo ? ", MsgBoxStyle.YesNo)
                If resp = MsgBoxResult.Yes Then
                    tempocontrato = Me.TextBox1.Text
                    FrmSelPaquete_Reactiva.Show()
                ElseIf resp = MsgBoxResult.No Then
                    Me.TextBox1.Clear()
                End If
            Else
                MsgBox("El Contrato Seleccionado Ya se Encuentra Activado")
            End If
        Else
            MsgBox("Teclee Un Contrato Por Favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Reactivar()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eContratoRec = 0
        BrwSelContratoInactivos.Show()
    End Sub

End Class