﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRecontratacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRecontratacion))
        Me.tcServicios = New System.Windows.Forms.TabControl()
        Me.tpTV = New System.Windows.Forms.TabPage()
        Me.lblTV = New System.Windows.Forms.Label()
        Me.panelTV = New System.Windows.Forms.Panel()
        Me.nudConPago = New System.Windows.Forms.NumericUpDown()
        Me.nudSinPago = New System.Windows.Forms.NumericUpDown()
        Me.btnTVCancelar = New System.Windows.Forms.Button()
        Me.btnTVAceptar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.comboSerTV = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bnTV = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarTV = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarTV = New System.Windows.Forms.ToolStripButton()
        Me.tvTV = New System.Windows.Forms.TreeView()
        Me.tpNET = New System.Windows.Forms.TabPage()
        Me.lblNET = New System.Windows.Forms.Label()
        Me.panelCNET = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.comboTipoSer = New System.Windows.Forms.ComboBox()
        Me.btnCNETCancelar = New System.Windows.Forms.Button()
        Me.btnCNETAceptar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.comboTipoAs = New System.Windows.Forms.ComboBox()
        Me.comboTipoCa = New System.Windows.Forms.ComboBox()
        Me.panelSNET = New System.Windows.Forms.Panel()
        Me.btnSNETCancelar = New System.Windows.Forms.Button()
        Me.btnSNETAceptar = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.comboSerNET = New System.Windows.Forms.ComboBox()
        Me.bnNETServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarSNET = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarSNET = New System.Windows.Forms.ToolStripButton()
        Me.bnNETCablemodem = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarNET = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarNET = New System.Windows.Forms.ToolStripButton()
        Me.tvNET = New System.Windows.Forms.TreeView()
        Me.tpDIG = New System.Windows.Forms.TabPage()
        Me.CMBNumExt = New System.Windows.Forms.Label()
        Me.nudExt = New System.Windows.Forms.NumericUpDown()
        Me.lblDIG = New System.Windows.Forms.Label()
        Me.panelDIG = New System.Windows.Forms.Panel()
        Me.btnDIGCancelar = New System.Windows.Forms.Button()
        Me.btnDIGAceptar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.comboSerDIG = New System.Windows.Forms.ComboBox()
        Me.bnDIGServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarSDIG = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarSDIG = New System.Windows.Forms.ToolStripButton()
        Me.bnDIGTarjeta = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarTDIG = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarTDIG = New System.Windows.Forms.ToolStripButton()
        Me.tvDIG = New System.Windows.Forms.TreeView()
        Me.tpTel = New System.Windows.Forms.TabPage()
        Me.bnServicioTel = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarServicio = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarServicio = New System.Windows.Forms.ToolStripButton()
        Me.bnPaqueteTel = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tbsEliminarPaquete = New System.Windows.Forms.ToolStripButton()
        Me.tbsAgregarPaquete = New System.Windows.Forms.ToolStripButton()
        Me.bnPlanTel = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarPlan = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregaPlan = New System.Windows.Forms.ToolStripButton()
        Me.pnlTelefoniaAdic = New System.Windows.Forms.Panel()
        Me.btnCancelarAdic = New System.Windows.Forms.Button()
        Me.btnAceptarAdic = New System.Windows.Forms.Button()
        Me.lbTelAdic = New System.Windows.Forms.Label()
        Me.comboSerTelAdic = New System.Windows.Forms.ComboBox()
        Me.lblTel = New System.Windows.Forms.Label()
        Me.tvTel = New System.Windows.Forms.TreeView()
        Me.pnlTelefonia = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.lblMuestra = New System.Windows.Forms.Label()
        Me.comboSerTel = New System.Windows.Forms.ComboBox()
        Me.btnNETCancelar = New System.Windows.Forms.Button()
        Me.btnNETAceptar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.comboSerNET2023 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxSoloInternet = New System.Windows.Forms.CheckBox()
        Me.LabelCelular = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.LabelCiudad = New System.Windows.Forms.Label()
        Me.LabelColonia = New System.Windows.Forms.Label()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelCalle = New System.Windows.Forms.Label()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBoxContrato = New System.Windows.Forms.TextBox()
        Me.ButtonBuscar = New System.Windows.Forms.Button()
        Me.bnRecontratar = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbRecon = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBoxTotalaPagar = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.TextBoxContratoCompania = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.tcServicios.SuspendLayout()
        Me.tpTV.SuspendLayout()
        Me.panelTV.SuspendLayout()
        CType(Me.nudConPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudSinPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnTV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTV.SuspendLayout()
        Me.tpNET.SuspendLayout()
        Me.panelCNET.SuspendLayout()
        Me.panelSNET.SuspendLayout()
        CType(Me.bnNETServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnNETServicio.SuspendLayout()
        CType(Me.bnNETCablemodem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnNETCablemodem.SuspendLayout()
        Me.tpDIG.SuspendLayout()
        CType(Me.nudExt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelDIG.SuspendLayout()
        CType(Me.bnDIGServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGServicio.SuspendLayout()
        CType(Me.bnDIGTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGTarjeta.SuspendLayout()
        Me.tpTel.SuspendLayout()
        CType(Me.bnServicioTel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnServicioTel.SuspendLayout()
        CType(Me.bnPaqueteTel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPaqueteTel.SuspendLayout()
        CType(Me.bnPlanTel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPlanTel.SuspendLayout()
        Me.pnlTelefoniaAdic.SuspendLayout()
        Me.pnlTelefonia.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.bnRecontratar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnRecontratar.SuspendLayout()
        Me.SuspendLayout()
        '
        'tcServicios
        '
        Me.tcServicios.Controls.Add(Me.tpTV)
        Me.tcServicios.Controls.Add(Me.tpNET)
        Me.tcServicios.Controls.Add(Me.tpDIG)
        Me.tcServicios.Controls.Add(Me.tpTel)
        Me.tcServicios.Enabled = False
        Me.tcServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcServicios.Location = New System.Drawing.Point(12, 299)
        Me.tcServicios.Name = "tcServicios"
        Me.tcServicios.SelectedIndex = 0
        Me.tcServicios.Size = New System.Drawing.Size(798, 368)
        Me.tcServicios.TabIndex = 0
        '
        'tpTV
        '
        Me.tpTV.Controls.Add(Me.lblTV)
        Me.tpTV.Controls.Add(Me.panelTV)
        Me.tpTV.Controls.Add(Me.bnTV)
        Me.tpTV.Controls.Add(Me.tvTV)
        Me.tpTV.Location = New System.Drawing.Point(4, 24)
        Me.tpTV.Name = "tpTV"
        Me.tpTV.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTV.Size = New System.Drawing.Size(790, 340)
        Me.tpTV.TabIndex = 0
        Me.tpTV.Text = "Televisión"
        Me.tpTV.UseVisualStyleBackColor = True
        '
        'lblTV
        '
        Me.lblTV.ForeColor = System.Drawing.Color.Red
        Me.lblTV.Location = New System.Drawing.Point(3, 313)
        Me.lblTV.Name = "lblTV"
        Me.lblTV.Size = New System.Drawing.Size(666, 24)
        Me.lblTV.TabIndex = 18
        '
        'panelTV
        '
        Me.panelTV.Controls.Add(Me.nudConPago)
        Me.panelTV.Controls.Add(Me.nudSinPago)
        Me.panelTV.Controls.Add(Me.btnTVCancelar)
        Me.panelTV.Controls.Add(Me.btnTVAceptar)
        Me.panelTV.Controls.Add(Me.Label3)
        Me.panelTV.Controls.Add(Me.comboSerTV)
        Me.panelTV.Controls.Add(Me.Label2)
        Me.panelTV.Controls.Add(Me.Label1)
        Me.panelTV.Location = New System.Drawing.Point(18, 89)
        Me.panelTV.Name = "panelTV"
        Me.panelTV.Size = New System.Drawing.Size(750, 178)
        Me.panelTV.TabIndex = 14
        Me.panelTV.Visible = False
        '
        'nudConPago
        '
        Me.nudConPago.Location = New System.Drawing.Point(196, 79)
        Me.nudConPago.Name = "nudConPago"
        Me.nudConPago.Size = New System.Drawing.Size(71, 21)
        Me.nudConPago.TabIndex = 9
        '
        'nudSinPago
        '
        Me.nudSinPago.Location = New System.Drawing.Point(196, 51)
        Me.nudSinPago.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudSinPago.Name = "nudSinPago"
        Me.nudSinPago.Size = New System.Drawing.Size(71, 21)
        Me.nudSinPago.TabIndex = 8
        '
        'btnTVCancelar
        '
        Me.btnTVCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnTVCancelar.Name = "btnTVCancelar"
        Me.btnTVCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnTVCancelar.TabIndex = 7
        Me.btnTVCancelar.Text = "&Cancelar"
        Me.btnTVCancelar.UseVisualStyleBackColor = True
        '
        'btnTVAceptar
        '
        Me.btnTVAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnTVAceptar.Name = "btnTVAceptar"
        Me.btnTVAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnTVAceptar.TabIndex = 6
        Me.btnTVAceptar.Text = "&Aceptar"
        Me.btnTVAceptar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(124, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Servicio: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerTV
        '
        Me.comboSerTV.DisplayMember = "Descripcion"
        Me.comboSerTV.FormattingEnabled = True
        Me.comboSerTV.Location = New System.Drawing.Point(196, 22)
        Me.comboSerTV.Name = "comboSerTV"
        Me.comboSerTV.Size = New System.Drawing.Size(455, 23)
        Me.comboSerTV.TabIndex = 4
        Me.comboSerTV.ValueMember = "Clv_Servicio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(100, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "TV con pago:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(104, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "TV sin pago:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'bnTV
        '
        Me.bnTV.AddNewItem = Nothing
        Me.bnTV.CountItem = Nothing
        Me.bnTV.DeleteItem = Nothing
        Me.bnTV.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTV.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarTV, Me.tsbAgregarTV})
        Me.bnTV.Location = New System.Drawing.Point(3, 3)
        Me.bnTV.MoveFirstItem = Nothing
        Me.bnTV.MoveLastItem = Nothing
        Me.bnTV.MoveNextItem = Nothing
        Me.bnTV.MovePreviousItem = Nothing
        Me.bnTV.Name = "bnTV"
        Me.bnTV.PositionItem = Nothing
        Me.bnTV.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTV.Size = New System.Drawing.Size(784, 25)
        Me.bnTV.TabIndex = 13
        Me.bnTV.Text = "BindingNavigator1"
        '
        'tsbEliminarTV
        '
        Me.tsbEliminarTV.Image = CType(resources.GetObject("tsbEliminarTV.Image"), System.Drawing.Image)
        Me.tsbEliminarTV.Name = "tsbEliminarTV"
        Me.tsbEliminarTV.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarTV.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarTV.Text = "Eliminar Servicio"
        '
        'tsbAgregarTV
        '
        Me.tsbAgregarTV.Image = CType(resources.GetObject("tsbAgregarTV.Image"), System.Drawing.Image)
        Me.tsbAgregarTV.Name = "tsbAgregarTV"
        Me.tsbAgregarTV.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarTV.Text = "Agregar Servicio"
        '
        'tvTV
        '
        Me.tvTV.Location = New System.Drawing.Point(6, 64)
        Me.tvTV.Name = "tvTV"
        Me.tvTV.Size = New System.Drawing.Size(778, 246)
        Me.tvTV.TabIndex = 4
        '
        'tpNET
        '
        Me.tpNET.Controls.Add(Me.lblNET)
        Me.tpNET.Controls.Add(Me.panelCNET)
        Me.tpNET.Controls.Add(Me.panelSNET)
        Me.tpNET.Controls.Add(Me.bnNETServicio)
        Me.tpNET.Controls.Add(Me.bnNETCablemodem)
        Me.tpNET.Controls.Add(Me.tvNET)
        Me.tpNET.Location = New System.Drawing.Point(4, 24)
        Me.tpNET.Name = "tpNET"
        Me.tpNET.Padding = New System.Windows.Forms.Padding(3)
        Me.tpNET.Size = New System.Drawing.Size(790, 340)
        Me.tpNET.TabIndex = 1
        Me.tpNET.Text = "Internet"
        Me.tpNET.UseVisualStyleBackColor = True
        '
        'lblNET
        '
        Me.lblNET.ForeColor = System.Drawing.Color.Red
        Me.lblNET.Location = New System.Drawing.Point(3, 313)
        Me.lblNET.Name = "lblNET"
        Me.lblNET.Size = New System.Drawing.Size(666, 24)
        Me.lblNET.TabIndex = 17
        '
        'panelCNET
        '
        Me.panelCNET.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelCNET.Controls.Add(Me.Label17)
        Me.panelCNET.Controls.Add(Me.comboTipoSer)
        Me.panelCNET.Controls.Add(Me.btnCNETCancelar)
        Me.panelCNET.Controls.Add(Me.btnCNETAceptar)
        Me.panelCNET.Controls.Add(Me.Label7)
        Me.panelCNET.Controls.Add(Me.Label6)
        Me.panelCNET.Controls.Add(Me.comboTipoAs)
        Me.panelCNET.Controls.Add(Me.comboTipoCa)
        Me.panelCNET.Location = New System.Drawing.Point(20, 76)
        Me.panelCNET.Name = "panelCNET"
        Me.panelCNET.Size = New System.Drawing.Size(753, 178)
        Me.panelCNET.TabIndex = 15
        Me.panelCNET.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(49, 79)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(94, 15)
        Me.Label17.TabIndex = 15
        Me.Label17.Text = "Tipo Servicio:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboTipoSer
        '
        Me.comboTipoSer.DisplayMember = "Concepto"
        Me.comboTipoSer.FormattingEnabled = True
        Me.comboTipoSer.Location = New System.Drawing.Point(149, 76)
        Me.comboTipoSer.Name = "comboTipoSer"
        Me.comboTipoSer.Size = New System.Drawing.Size(398, 23)
        Me.comboTipoSer.TabIndex = 14
        Me.comboTipoSer.ValueMember = "Clv_TipSerInternet"
        '
        'btnCNETCancelar
        '
        Me.btnCNETCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnCNETCancelar.Name = "btnCNETCancelar"
        Me.btnCNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnCNETCancelar.TabIndex = 13
        Me.btnCNETCancelar.Text = "&Cancelar"
        Me.btnCNETCancelar.UseVisualStyleBackColor = True
        '
        'btnCNETAceptar
        '
        Me.btnCNETAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnCNETAceptar.Name = "btnCNETAceptar"
        Me.btnCNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnCNETAceptar.TabIndex = 12
        Me.btnCNETAceptar.Text = "&Aceptar"
        Me.btnCNETAceptar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(30, 50)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(113, 15)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Tipo Asignación:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tipo Cablemodem:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboTipoAs
        '
        Me.comboTipoAs.DisplayMember = "Tipo"
        Me.comboTipoAs.FormattingEnabled = True
        Me.comboTipoAs.Location = New System.Drawing.Point(149, 47)
        Me.comboTipoAs.Name = "comboTipoAs"
        Me.comboTipoAs.Size = New System.Drawing.Size(398, 23)
        Me.comboTipoAs.TabIndex = 9
        Me.comboTipoAs.ValueMember = "Id"
        '
        'comboTipoCa
        '
        Me.comboTipoCa.DisplayMember = "Tipo"
        Me.comboTipoCa.FormattingEnabled = True
        Me.comboTipoCa.Location = New System.Drawing.Point(149, 16)
        Me.comboTipoCa.Name = "comboTipoCa"
        Me.comboTipoCa.Size = New System.Drawing.Size(398, 23)
        Me.comboTipoCa.TabIndex = 8
        Me.comboTipoCa.ValueMember = "ID"
        '
        'panelSNET
        '
        Me.panelSNET.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelSNET.Controls.Add(Me.btnSNETCancelar)
        Me.panelSNET.Controls.Add(Me.btnSNETAceptar)
        Me.panelSNET.Controls.Add(Me.Label11)
        Me.panelSNET.Controls.Add(Me.comboSerNET)
        Me.panelSNET.Location = New System.Drawing.Point(20, 96)
        Me.panelSNET.Name = "panelSNET"
        Me.panelSNET.Size = New System.Drawing.Size(753, 178)
        Me.panelSNET.TabIndex = 16
        Me.panelSNET.Visible = False
        '
        'btnSNETCancelar
        '
        Me.btnSNETCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnSNETCancelar.Name = "btnSNETCancelar"
        Me.btnSNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnSNETCancelar.TabIndex = 13
        Me.btnSNETCancelar.Text = "&Cancelar"
        Me.btnSNETCancelar.UseVisualStyleBackColor = True
        '
        'btnSNETAceptar
        '
        Me.btnSNETAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnSNETAceptar.Name = "btnSNETAceptar"
        Me.btnSNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnSNETAceptar.TabIndex = 12
        Me.btnSNETAceptar.Text = "&Aceptar"
        Me.btnSNETAceptar.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(124, 47)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(66, 15)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Servicio: "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerNET
        '
        Me.comboSerNET.DisplayMember = "Descripcion"
        Me.comboSerNET.FormattingEnabled = True
        Me.comboSerNET.Location = New System.Drawing.Point(196, 42)
        Me.comboSerNET.Name = "comboSerNET"
        Me.comboSerNET.Size = New System.Drawing.Size(497, 23)
        Me.comboSerNET.TabIndex = 6
        Me.comboSerNET.ValueMember = "Clv_Servicio"
        '
        'bnNETServicio
        '
        Me.bnNETServicio.AddNewItem = Nothing
        Me.bnNETServicio.CountItem = Nothing
        Me.bnNETServicio.DeleteItem = Nothing
        Me.bnNETServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNETServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarSNET, Me.tsbAgregarSNET})
        Me.bnNETServicio.Location = New System.Drawing.Point(3, 28)
        Me.bnNETServicio.MoveFirstItem = Nothing
        Me.bnNETServicio.MoveLastItem = Nothing
        Me.bnNETServicio.MoveNextItem = Nothing
        Me.bnNETServicio.MovePreviousItem = Nothing
        Me.bnNETServicio.Name = "bnNETServicio"
        Me.bnNETServicio.PositionItem = Nothing
        Me.bnNETServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnNETServicio.Size = New System.Drawing.Size(784, 25)
        Me.bnNETServicio.TabIndex = 16
        Me.bnNETServicio.Text = "BindingNavigator1"
        '
        'tsbEliminarSNET
        '
        Me.tsbEliminarSNET.Image = CType(resources.GetObject("tsbEliminarSNET.Image"), System.Drawing.Image)
        Me.tsbEliminarSNET.Name = "tsbEliminarSNET"
        Me.tsbEliminarSNET.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarSNET.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarSNET.Text = "Eliminar Servicio"
        '
        'tsbAgregarSNET
        '
        Me.tsbAgregarSNET.Image = CType(resources.GetObject("tsbAgregarSNET.Image"), System.Drawing.Image)
        Me.tsbAgregarSNET.Name = "tsbAgregarSNET"
        Me.tsbAgregarSNET.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarSNET.Text = "Agregar Servicio"
        '
        'bnNETCablemodem
        '
        Me.bnNETCablemodem.AddNewItem = Nothing
        Me.bnNETCablemodem.CountItem = Nothing
        Me.bnNETCablemodem.DeleteItem = Nothing
        Me.bnNETCablemodem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNETCablemodem.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarNET, Me.tsbAgregarNET})
        Me.bnNETCablemodem.Location = New System.Drawing.Point(3, 3)
        Me.bnNETCablemodem.MoveFirstItem = Nothing
        Me.bnNETCablemodem.MoveLastItem = Nothing
        Me.bnNETCablemodem.MoveNextItem = Nothing
        Me.bnNETCablemodem.MovePreviousItem = Nothing
        Me.bnNETCablemodem.Name = "bnNETCablemodem"
        Me.bnNETCablemodem.PositionItem = Nothing
        Me.bnNETCablemodem.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnNETCablemodem.Size = New System.Drawing.Size(784, 25)
        Me.bnNETCablemodem.TabIndex = 14
        Me.bnNETCablemodem.Text = "BindingNavigator1"
        '
        'tsbEliminarNET
        '
        Me.tsbEliminarNET.Image = CType(resources.GetObject("tsbEliminarNET.Image"), System.Drawing.Image)
        Me.tsbEliminarNET.Name = "tsbEliminarNET"
        Me.tsbEliminarNET.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarNET.Size = New System.Drawing.Size(156, 22)
        Me.tsbEliminarNET.Text = "Eliminar Cablemodem"
        '
        'tsbAgregarNET
        '
        Me.tsbAgregarNET.Image = CType(resources.GetObject("tsbAgregarNET.Image"), System.Drawing.Image)
        Me.tsbAgregarNET.Name = "tsbAgregarNET"
        Me.tsbAgregarNET.Size = New System.Drawing.Size(158, 22)
        Me.tsbAgregarNET.Text = "Agregar Cablemodem"
        '
        'tvNET
        '
        Me.tvNET.Location = New System.Drawing.Point(6, 64)
        Me.tvNET.Name = "tvNET"
        Me.tvNET.Size = New System.Drawing.Size(778, 235)
        Me.tvNET.TabIndex = 0
        '
        'tpDIG
        '
        Me.tpDIG.Controls.Add(Me.CMBNumExt)
        Me.tpDIG.Controls.Add(Me.nudExt)
        Me.tpDIG.Controls.Add(Me.lblDIG)
        Me.tpDIG.Controls.Add(Me.panelDIG)
        Me.tpDIG.Controls.Add(Me.bnDIGServicio)
        Me.tpDIG.Controls.Add(Me.bnDIGTarjeta)
        Me.tpDIG.Controls.Add(Me.tvDIG)
        Me.tpDIG.Location = New System.Drawing.Point(4, 24)
        Me.tpDIG.Name = "tpDIG"
        Me.tpDIG.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDIG.Size = New System.Drawing.Size(790, 340)
        Me.tpDIG.TabIndex = 2
        Me.tpDIG.Text = "Digital"
        Me.tpDIG.UseVisualStyleBackColor = True
        '
        'CMBNumExt
        '
        Me.CMBNumExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CMBNumExt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CMBNumExt.Location = New System.Drawing.Point(70, 280)
        Me.CMBNumExt.Name = "CMBNumExt"
        Me.CMBNumExt.Size = New System.Drawing.Size(243, 22)
        Me.CMBNumExt.TabIndex = 84
        Me.CMBNumExt.Text = "Número de Extensiones Analógicas:"
        Me.CMBNumExt.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'nudExt
        '
        Me.nudExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.nudExt.Location = New System.Drawing.Point(319, 288)
        Me.nudExt.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudExt.Name = "nudExt"
        Me.nudExt.Size = New System.Drawing.Size(49, 22)
        Me.nudExt.TabIndex = 85
        '
        'lblDIG
        '
        Me.lblDIG.ForeColor = System.Drawing.Color.Red
        Me.lblDIG.Location = New System.Drawing.Point(3, 313)
        Me.lblDIG.Name = "lblDIG"
        Me.lblDIG.Size = New System.Drawing.Size(666, 24)
        Me.lblDIG.TabIndex = 18
        '
        'panelDIG
        '
        Me.panelDIG.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelDIG.Controls.Add(Me.btnDIGCancelar)
        Me.panelDIG.Controls.Add(Me.btnDIGAceptar)
        Me.panelDIG.Controls.Add(Me.Label9)
        Me.panelDIG.Controls.Add(Me.comboSerDIG)
        Me.panelDIG.Location = New System.Drawing.Point(6, 89)
        Me.panelDIG.Name = "panelDIG"
        Me.panelDIG.Size = New System.Drawing.Size(778, 178)
        Me.panelDIG.TabIndex = 16
        Me.panelDIG.Visible = False
        '
        'btnDIGCancelar
        '
        Me.btnDIGCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnDIGCancelar.Name = "btnDIGCancelar"
        Me.btnDIGCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGCancelar.TabIndex = 9
        Me.btnDIGCancelar.Text = "&Cancelar"
        Me.btnDIGCancelar.UseVisualStyleBackColor = True
        '
        'btnDIGAceptar
        '
        Me.btnDIGAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnDIGAceptar.Name = "btnDIGAceptar"
        Me.btnDIGAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGAceptar.TabIndex = 8
        Me.btnDIGAceptar.Text = "&Aceptar"
        Me.btnDIGAceptar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 54)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 15)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Servicio: "
        '
        'comboSerDIG
        '
        Me.comboSerDIG.DisplayMember = "Descripcion"
        Me.comboSerDIG.FormattingEnabled = True
        Me.comboSerDIG.Location = New System.Drawing.Point(85, 51)
        Me.comboSerDIG.Name = "comboSerDIG"
        Me.comboSerDIG.Size = New System.Drawing.Size(610, 23)
        Me.comboSerDIG.TabIndex = 6
        Me.comboSerDIG.ValueMember = "Clv_Servicio"
        '
        'bnDIGServicio
        '
        Me.bnDIGServicio.AddNewItem = Nothing
        Me.bnDIGServicio.CountItem = Nothing
        Me.bnDIGServicio.DeleteItem = Nothing
        Me.bnDIGServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarSDIG, Me.tsbAgregarSDIG})
        Me.bnDIGServicio.Location = New System.Drawing.Point(3, 28)
        Me.bnDIGServicio.MoveFirstItem = Nothing
        Me.bnDIGServicio.MoveLastItem = Nothing
        Me.bnDIGServicio.MoveNextItem = Nothing
        Me.bnDIGServicio.MovePreviousItem = Nothing
        Me.bnDIGServicio.Name = "bnDIGServicio"
        Me.bnDIGServicio.PositionItem = Nothing
        Me.bnDIGServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGServicio.Size = New System.Drawing.Size(784, 25)
        Me.bnDIGServicio.TabIndex = 15
        Me.bnDIGServicio.Text = "BindingNavigator1"
        '
        'tsbEliminarSDIG
        '
        Me.tsbEliminarSDIG.Image = CType(resources.GetObject("tsbEliminarSDIG.Image"), System.Drawing.Image)
        Me.tsbEliminarSDIG.Name = "tsbEliminarSDIG"
        Me.tsbEliminarSDIG.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarSDIG.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarSDIG.Text = "Eliminar Servicio"
        '
        'tsbAgregarSDIG
        '
        Me.tsbAgregarSDIG.Image = CType(resources.GetObject("tsbAgregarSDIG.Image"), System.Drawing.Image)
        Me.tsbAgregarSDIG.Name = "tsbAgregarSDIG"
        Me.tsbAgregarSDIG.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarSDIG.Text = "Agregar Servicio"
        '
        'bnDIGTarjeta
        '
        Me.bnDIGTarjeta.AddNewItem = Nothing
        Me.bnDIGTarjeta.CountItem = Nothing
        Me.bnDIGTarjeta.DeleteItem = Nothing
        Me.bnDIGTarjeta.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGTarjeta.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarTDIG, Me.tsbAgregarTDIG})
        Me.bnDIGTarjeta.Location = New System.Drawing.Point(3, 3)
        Me.bnDIGTarjeta.MoveFirstItem = Nothing
        Me.bnDIGTarjeta.MoveLastItem = Nothing
        Me.bnDIGTarjeta.MoveNextItem = Nothing
        Me.bnDIGTarjeta.MovePreviousItem = Nothing
        Me.bnDIGTarjeta.Name = "bnDIGTarjeta"
        Me.bnDIGTarjeta.PositionItem = Nothing
        Me.bnDIGTarjeta.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGTarjeta.Size = New System.Drawing.Size(784, 25)
        Me.bnDIGTarjeta.TabIndex = 14
        Me.bnDIGTarjeta.Text = "BindingNavigator1"
        '
        'tsbEliminarTDIG
        '
        Me.tsbEliminarTDIG.Image = CType(resources.GetObject("tsbEliminarTDIG.Image"), System.Drawing.Image)
        Me.tsbEliminarTDIG.Name = "tsbEliminarTDIG"
        Me.tsbEliminarTDIG.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarTDIG.Size = New System.Drawing.Size(121, 22)
        Me.tsbEliminarTDIG.Text = "Eliminar Tarjeta"
        '
        'tsbAgregarTDIG
        '
        Me.tsbAgregarTDIG.Image = CType(resources.GetObject("tsbAgregarTDIG.Image"), System.Drawing.Image)
        Me.tsbAgregarTDIG.Name = "tsbAgregarTDIG"
        Me.tsbAgregarTDIG.Size = New System.Drawing.Size(123, 22)
        Me.tsbAgregarTDIG.Text = "Agregar Tarjeta"
        '
        'tvDIG
        '
        Me.tvDIG.Location = New System.Drawing.Point(9, 64)
        Me.tvDIG.Name = "tvDIG"
        Me.tvDIG.Size = New System.Drawing.Size(772, 183)
        Me.tvDIG.TabIndex = 0
        '
        'tpTel
        '
        Me.tpTel.Controls.Add(Me.bnServicioTel)
        Me.tpTel.Controls.Add(Me.bnPaqueteTel)
        Me.tpTel.Controls.Add(Me.bnPlanTel)
        Me.tpTel.Controls.Add(Me.pnlTelefoniaAdic)
        Me.tpTel.Controls.Add(Me.lblTel)
        Me.tpTel.Controls.Add(Me.tvTel)
        Me.tpTel.Controls.Add(Me.pnlTelefonia)
        Me.tpTel.Location = New System.Drawing.Point(4, 24)
        Me.tpTel.Name = "tpTel"
        Me.tpTel.Size = New System.Drawing.Size(790, 340)
        Me.tpTel.TabIndex = 3
        Me.tpTel.Text = "Telefonía"
        Me.tpTel.UseVisualStyleBackColor = True
        '
        'bnServicioTel
        '
        Me.bnServicioTel.AddNewItem = Nothing
        Me.bnServicioTel.CountItem = Nothing
        Me.bnServicioTel.DeleteItem = Nothing
        Me.bnServicioTel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnServicioTel.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarServicio, Me.tsbAgregarServicio})
        Me.bnServicioTel.Location = New System.Drawing.Point(0, 50)
        Me.bnServicioTel.MoveFirstItem = Nothing
        Me.bnServicioTel.MoveLastItem = Nothing
        Me.bnServicioTel.MoveNextItem = Nothing
        Me.bnServicioTel.MovePreviousItem = Nothing
        Me.bnServicioTel.Name = "bnServicioTel"
        Me.bnServicioTel.PositionItem = Nothing
        Me.bnServicioTel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnServicioTel.Size = New System.Drawing.Size(790, 25)
        Me.bnServicioTel.TabIndex = 30
        Me.bnServicioTel.Text = "BindingNavigator3"
        '
        'tsbEliminarServicio
        '
        Me.tsbEliminarServicio.Image = CType(resources.GetObject("tsbEliminarServicio.Image"), System.Drawing.Image)
        Me.tsbEliminarServicio.Name = "tsbEliminarServicio"
        Me.tsbEliminarServicio.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarServicio.Size = New System.Drawing.Size(187, 22)
        Me.tsbEliminarServicio.Text = "Eliminar Servicios Digitales"
        '
        'tsbAgregarServicio
        '
        Me.tsbAgregarServicio.Image = CType(resources.GetObject("tsbAgregarServicio.Image"), System.Drawing.Image)
        Me.tsbAgregarServicio.Name = "tsbAgregarServicio"
        Me.tsbAgregarServicio.Size = New System.Drawing.Size(189, 22)
        Me.tsbAgregarServicio.Text = "Agregar Servicios Digitales"
        '
        'bnPaqueteTel
        '
        Me.bnPaqueteTel.AddNewItem = Nothing
        Me.bnPaqueteTel.CountItem = Nothing
        Me.bnPaqueteTel.DeleteItem = Nothing
        Me.bnPaqueteTel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPaqueteTel.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbsEliminarPaquete, Me.tbsAgregarPaquete})
        Me.bnPaqueteTel.Location = New System.Drawing.Point(0, 25)
        Me.bnPaqueteTel.MoveFirstItem = Nothing
        Me.bnPaqueteTel.MoveLastItem = Nothing
        Me.bnPaqueteTel.MoveNextItem = Nothing
        Me.bnPaqueteTel.MovePreviousItem = Nothing
        Me.bnPaqueteTel.Name = "bnPaqueteTel"
        Me.bnPaqueteTel.PositionItem = Nothing
        Me.bnPaqueteTel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnPaqueteTel.Size = New System.Drawing.Size(790, 25)
        Me.bnPaqueteTel.TabIndex = 29
        Me.bnPaqueteTel.Text = "BindingNavigator1"
        '
        'tbsEliminarPaquete
        '
        Me.tbsEliminarPaquete.Image = CType(resources.GetObject("tbsEliminarPaquete.Image"), System.Drawing.Image)
        Me.tbsEliminarPaquete.Name = "tbsEliminarPaquete"
        Me.tbsEliminarPaquete.RightToLeftAutoMirrorImage = True
        Me.tbsEliminarPaquete.Size = New System.Drawing.Size(188, 22)
        Me.tbsEliminarPaquete.Text = "Eliminar Paquete Adicional"
        '
        'tbsAgregarPaquete
        '
        Me.tbsAgregarPaquete.Image = CType(resources.GetObject("tbsAgregarPaquete.Image"), System.Drawing.Image)
        Me.tbsAgregarPaquete.Name = "tbsAgregarPaquete"
        Me.tbsAgregarPaquete.Size = New System.Drawing.Size(190, 22)
        Me.tbsAgregarPaquete.Text = "Agregar Paquete Adicional"
        '
        'bnPlanTel
        '
        Me.bnPlanTel.AddNewItem = Nothing
        Me.bnPlanTel.CountItem = Nothing
        Me.bnPlanTel.DeleteItem = Nothing
        Me.bnPlanTel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPlanTel.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarPlan, Me.tsbAgregaPlan})
        Me.bnPlanTel.Location = New System.Drawing.Point(0, 0)
        Me.bnPlanTel.MoveFirstItem = Nothing
        Me.bnPlanTel.MoveLastItem = Nothing
        Me.bnPlanTel.MoveNextItem = Nothing
        Me.bnPlanTel.MovePreviousItem = Nothing
        Me.bnPlanTel.Name = "bnPlanTel"
        Me.bnPlanTel.PositionItem = Nothing
        Me.bnPlanTel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnPlanTel.Size = New System.Drawing.Size(790, 25)
        Me.bnPlanTel.TabIndex = 28
        Me.bnPlanTel.Text = "BindingNavigator1"
        '
        'tsbEliminarPlan
        '
        Me.tsbEliminarPlan.Image = CType(resources.GetObject("tsbEliminarPlan.Image"), System.Drawing.Image)
        Me.tsbEliminarPlan.Name = "tsbEliminarPlan"
        Me.tsbEliminarPlan.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarPlan.Size = New System.Drawing.Size(158, 22)
        Me.tsbEliminarPlan.Text = "Eliminar Plan Tarifario"
        '
        'tsbAgregaPlan
        '
        Me.tsbAgregaPlan.Image = CType(resources.GetObject("tsbAgregaPlan.Image"), System.Drawing.Image)
        Me.tsbAgregaPlan.Name = "tsbAgregaPlan"
        Me.tsbAgregaPlan.Size = New System.Drawing.Size(160, 22)
        Me.tsbAgregaPlan.Text = "Agregar Plan Tarifario"
        '
        'pnlTelefoniaAdic
        '
        Me.pnlTelefoniaAdic.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlTelefoniaAdic.Controls.Add(Me.btnCancelarAdic)
        Me.pnlTelefoniaAdic.Controls.Add(Me.btnAceptarAdic)
        Me.pnlTelefoniaAdic.Controls.Add(Me.lbTelAdic)
        Me.pnlTelefoniaAdic.Controls.Add(Me.comboSerTelAdic)
        Me.pnlTelefoniaAdic.Location = New System.Drawing.Point(6, 115)
        Me.pnlTelefoniaAdic.Name = "pnlTelefoniaAdic"
        Me.pnlTelefoniaAdic.Size = New System.Drawing.Size(778, 175)
        Me.pnlTelefoniaAdic.TabIndex = 26
        Me.pnlTelefoniaAdic.Visible = False
        '
        'btnCancelarAdic
        '
        Me.btnCancelarAdic.Location = New System.Drawing.Point(333, 131)
        Me.btnCancelarAdic.Name = "btnCancelarAdic"
        Me.btnCancelarAdic.Size = New System.Drawing.Size(99, 27)
        Me.btnCancelarAdic.TabIndex = 13
        Me.btnCancelarAdic.Text = "&Cancelar"
        Me.btnCancelarAdic.UseVisualStyleBackColor = True
        '
        'btnAceptarAdic
        '
        Me.btnAceptarAdic.Location = New System.Drawing.Point(228, 131)
        Me.btnAceptarAdic.Name = "btnAceptarAdic"
        Me.btnAceptarAdic.Size = New System.Drawing.Size(99, 27)
        Me.btnAceptarAdic.TabIndex = 12
        Me.btnAceptarAdic.Text = "&Aceptar"
        Me.btnAceptarAdic.UseVisualStyleBackColor = True
        '
        'lbTelAdic
        '
        Me.lbTelAdic.AutoSize = True
        Me.lbTelAdic.Location = New System.Drawing.Point(6, 67)
        Me.lbTelAdic.Name = "lbTelAdic"
        Me.lbTelAdic.Size = New System.Drawing.Size(127, 15)
        Me.lbTelAdic.TabIndex = 10
        Me.lbTelAdic.Text = "Paquete Adicional:"
        Me.lbTelAdic.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerTelAdic
        '
        Me.comboSerTelAdic.DisplayMember = "Descripcion"
        Me.comboSerTelAdic.FormattingEnabled = True
        Me.comboSerTelAdic.Location = New System.Drawing.Point(139, 64)
        Me.comboSerTelAdic.Name = "comboSerTelAdic"
        Me.comboSerTelAdic.Size = New System.Drawing.Size(617, 23)
        Me.comboSerTelAdic.TabIndex = 8
        Me.comboSerTelAdic.ValueMember = "Clv_Servicio"
        '
        'lblTel
        '
        Me.lblTel.ForeColor = System.Drawing.Color.Red
        Me.lblTel.Location = New System.Drawing.Point(3, 311)
        Me.lblTel.Name = "lblTel"
        Me.lblTel.Size = New System.Drawing.Size(666, 24)
        Me.lblTel.TabIndex = 27
        '
        'tvTel
        '
        Me.tvTel.Location = New System.Drawing.Point(6, 87)
        Me.tvTel.Name = "tvTel"
        Me.tvTel.Size = New System.Drawing.Size(778, 217)
        Me.tvTel.TabIndex = 24
        '
        'pnlTelefonia
        '
        Me.pnlTelefonia.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlTelefonia.Controls.Add(Me.btnCancelar)
        Me.pnlTelefonia.Controls.Add(Me.btnAceptar)
        Me.pnlTelefonia.Controls.Add(Me.lblMuestra)
        Me.pnlTelefonia.Controls.Add(Me.comboSerTel)
        Me.pnlTelefonia.Location = New System.Drawing.Point(9, 115)
        Me.pnlTelefonia.Name = "pnlTelefonia"
        Me.pnlTelefonia.Size = New System.Drawing.Size(772, 178)
        Me.pnlTelefonia.TabIndex = 25
        Me.pnlTelefonia.Visible = False
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnCancelar.TabIndex = 13
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnAceptar.TabIndex = 12
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'lblMuestra
        '
        Me.lblMuestra.AutoSize = True
        Me.lblMuestra.Location = New System.Drawing.Point(92, 30)
        Me.lblMuestra.Name = "lblMuestra"
        Me.lblMuestra.Size = New System.Drawing.Size(98, 15)
        Me.lblMuestra.TabIndex = 10
        Me.lblMuestra.Text = "Plan Tarifario:"
        Me.lblMuestra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerTel
        '
        Me.comboSerTel.DisplayMember = "Descripcion"
        Me.comboSerTel.FormattingEnabled = True
        Me.comboSerTel.Location = New System.Drawing.Point(196, 22)
        Me.comboSerTel.Name = "comboSerTel"
        Me.comboSerTel.Size = New System.Drawing.Size(483, 23)
        Me.comboSerTel.TabIndex = 8
        Me.comboSerTel.ValueMember = "Clv_Servicio"
        '
        'btnNETCancelar
        '
        Me.btnNETCancelar.Location = New System.Drawing.Point(319, 174)
        Me.btnNETCancelar.Name = "btnNETCancelar"
        Me.btnNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnNETCancelar.TabIndex = 9
        Me.btnNETCancelar.Text = "&Cancelar"
        Me.btnNETCancelar.UseVisualStyleBackColor = True
        '
        'btnNETAceptar
        '
        Me.btnNETAceptar.Location = New System.Drawing.Point(214, 174)
        Me.btnNETAceptar.Name = "btnNETAceptar"
        Me.btnNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnNETAceptar.TabIndex = 8
        Me.btnNETAceptar.Text = "&Aceptar"
        Me.btnNETAceptar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(133, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Servicio: "
        '
        'comboSerNET2023
        '
        Me.comboSerNET2023.FormattingEnabled = True
        Me.comboSerNET2023.Location = New System.Drawing.Point(205, 67)
        Me.comboSerNET2023.Name = "comboSerNET2023"
        Me.comboSerNET2023.Size = New System.Drawing.Size(202, 21)
        Me.comboSerNET2023.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(70, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 23)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Contrato:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBoxSoloInternet)
        Me.GroupBox1.Controls.Add(Me.LabelCelular)
        Me.GroupBox1.Controls.Add(Me.LabelTelefono)
        Me.GroupBox1.Controls.Add(Me.LabelCiudad)
        Me.GroupBox1.Controls.Add(Me.LabelColonia)
        Me.GroupBox1.Controls.Add(Me.LabelNumero)
        Me.GroupBox1.Controls.Add(Me.LabelCalle)
        Me.GroupBox1.Controls.Add(Me.LabelNombre)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 81)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(791, 135)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'CheckBoxSoloInternet
        '
        Me.CheckBoxSoloInternet.AutoSize = True
        Me.CheckBoxSoloInternet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSoloInternet.Enabled = False
        Me.CheckBoxSoloInternet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxSoloInternet.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBoxSoloInternet.Location = New System.Drawing.Point(469, 20)
        Me.CheckBoxSoloInternet.Name = "CheckBoxSoloInternet"
        Me.CheckBoxSoloInternet.Size = New System.Drawing.Size(108, 19)
        Me.CheckBoxSoloInternet.TabIndex = 16
        Me.CheckBoxSoloInternet.Text = "Sólo Internet"
        Me.CheckBoxSoloInternet.UseVisualStyleBackColor = True
        Me.CheckBoxSoloInternet.Visible = False
        '
        'LabelCelular
        '
        Me.LabelCelular.Location = New System.Drawing.Point(150, 96)
        Me.LabelCelular.Name = "LabelCelular"
        Me.LabelCelular.Size = New System.Drawing.Size(300, 19)
        Me.LabelCelular.TabIndex = 15
        Me.LabelCelular.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTelefono
        '
        Me.LabelTelefono.Location = New System.Drawing.Point(562, 96)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(223, 19)
        Me.LabelTelefono.TabIndex = 14
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCiudad
        '
        Me.LabelCiudad.Location = New System.Drawing.Point(562, 77)
        Me.LabelCiudad.Name = "LabelCiudad"
        Me.LabelCiudad.Size = New System.Drawing.Size(223, 19)
        Me.LabelCiudad.TabIndex = 13
        Me.LabelCiudad.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelColonia
        '
        Me.LabelColonia.Location = New System.Drawing.Point(150, 77)
        Me.LabelColonia.Name = "LabelColonia"
        Me.LabelColonia.Size = New System.Drawing.Size(300, 19)
        Me.LabelColonia.TabIndex = 12
        Me.LabelColonia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNumero
        '
        Me.LabelNumero.Location = New System.Drawing.Point(150, 58)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(300, 19)
        Me.LabelNumero.TabIndex = 11
        Me.LabelNumero.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCalle
        '
        Me.LabelCalle.Location = New System.Drawing.Point(150, 39)
        Me.LabelCalle.Name = "LabelCalle"
        Me.LabelCalle.Size = New System.Drawing.Size(300, 19)
        Me.LabelCalle.TabIndex = 10
        Me.LabelCalle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNombre
        '
        Me.LabelNombre.Location = New System.Drawing.Point(150, 20)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(300, 19)
        Me.LabelNombre.TabIndex = 9
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(51, 96)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 19)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Celular: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(456, 96)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(100, 19)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Teléfono: "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label12.Location = New System.Drawing.Point(456, 77)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 19)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Ciudad: "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label13.Location = New System.Drawing.Point(51, 77)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 19)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Colonia: "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(51, 58)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(101, 19)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "#: "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label15.Location = New System.Drawing.Point(51, 39)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 19)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Calle: "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(51, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 19)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Nombre: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TextBoxContrato
        '
        Me.TextBoxContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContrato.Location = New System.Drawing.Point(156, 45)
        Me.TextBoxContrato.Name = "TextBoxContrato"
        Me.TextBoxContrato.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxContrato.TabIndex = 19
        '
        'ButtonBuscar
        '
        Me.ButtonBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBuscar.Location = New System.Drawing.Point(262, 43)
        Me.ButtonBuscar.Name = "ButtonBuscar"
        Me.ButtonBuscar.Size = New System.Drawing.Size(43, 23)
        Me.ButtonBuscar.TabIndex = 18
        Me.ButtonBuscar.Text = "..."
        Me.ButtonBuscar.UseVisualStyleBackColor = True
        '
        'bnRecontratar
        '
        Me.bnRecontratar.AddNewItem = Nothing
        Me.bnRecontratar.CountItem = Nothing
        Me.bnRecontratar.DeleteItem = Nothing
        Me.bnRecontratar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnRecontratar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbRecon})
        Me.bnRecontratar.Location = New System.Drawing.Point(0, 0)
        Me.bnRecontratar.MoveFirstItem = Nothing
        Me.bnRecontratar.MoveLastItem = Nothing
        Me.bnRecontratar.MoveNextItem = Nothing
        Me.bnRecontratar.MovePreviousItem = Nothing
        Me.bnRecontratar.Name = "bnRecontratar"
        Me.bnRecontratar.PositionItem = Nothing
        Me.bnRecontratar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnRecontratar.Size = New System.Drawing.Size(829, 25)
        Me.bnRecontratar.TabIndex = 22
        Me.bnRecontratar.Text = "BindingNavigator1"
        '
        'tsbRecon
        '
        Me.tsbRecon.Image = CType(resources.GetObject("tsbRecon.Image"), System.Drawing.Image)
        Me.tsbRecon.Name = "tsbRecon"
        Me.tsbRecon.Size = New System.Drawing.Size(101, 22)
        Me.tsbRecon.Text = "Recontratar"
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(674, 670)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 23
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(16, 233)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(129, 16)
        Me.Label18.TabIndex = 24
        Me.Label18.Text = "Adeudo a Pagar :"
        '
        'TextBoxTotalaPagar
        '
        Me.TextBoxTotalaPagar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxTotalaPagar.Enabled = False
        Me.TextBoxTotalaPagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTotalaPagar.ForeColor = System.Drawing.Color.Red
        Me.TextBoxTotalaPagar.Location = New System.Drawing.Point(157, 233)
        Me.TextBoxTotalaPagar.Multiline = True
        Me.TextBoxTotalaPagar.Name = "TextBoxTotalaPagar"
        Me.TextBoxTotalaPagar.ReadOnly = True
        Me.TextBoxTotalaPagar.Size = New System.Drawing.Size(640, 60)
        Me.TextBoxTotalaPagar.TabIndex = 25
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'TextBoxContratoCompania
        '
        Me.TextBoxContratoCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContratoCompania.Location = New System.Drawing.Point(157, 45)
        Me.TextBoxContratoCompania.Name = "TextBoxContratoCompania"
        Me.TextBoxContratoCompania.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxContratoCompania.TabIndex = 26
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(469, 43)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(86, 16)
        Me.Label19.TabIndex = 107
        Me.Label19.Text = "Compañía :"
        Me.Label19.Visible = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(561, 40)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(239, 23)
        Me.ComboBoxCompanias.TabIndex = 106
        Me.ComboBoxCompanias.Visible = False
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'FrmRecontratacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(829, 709)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.ComboBoxCompanias)
        Me.Controls.Add(Me.TextBoxContratoCompania)
        Me.Controls.Add(Me.TextBoxTotalaPagar)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.bnRecontratar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TextBoxContrato)
        Me.Controls.Add(Me.ButtonBuscar)
        Me.Controls.Add(Me.tcServicios)
        Me.Name = "FrmRecontratacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recontratación"
        Me.tcServicios.ResumeLayout(False)
        Me.tpTV.ResumeLayout(False)
        Me.tpTV.PerformLayout()
        Me.panelTV.ResumeLayout(False)
        Me.panelTV.PerformLayout()
        CType(Me.nudConPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudSinPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnTV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTV.ResumeLayout(False)
        Me.bnTV.PerformLayout()
        Me.tpNET.ResumeLayout(False)
        Me.tpNET.PerformLayout()
        Me.panelCNET.ResumeLayout(False)
        Me.panelCNET.PerformLayout()
        Me.panelSNET.ResumeLayout(False)
        Me.panelSNET.PerformLayout()
        CType(Me.bnNETServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnNETServicio.ResumeLayout(False)
        Me.bnNETServicio.PerformLayout()
        CType(Me.bnNETCablemodem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnNETCablemodem.ResumeLayout(False)
        Me.bnNETCablemodem.PerformLayout()
        Me.tpDIG.ResumeLayout(False)
        Me.tpDIG.PerformLayout()
        CType(Me.nudExt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelDIG.ResumeLayout(False)
        Me.panelDIG.PerformLayout()
        CType(Me.bnDIGServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGServicio.ResumeLayout(False)
        Me.bnDIGServicio.PerformLayout()
        CType(Me.bnDIGTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGTarjeta.ResumeLayout(False)
        Me.bnDIGTarjeta.PerformLayout()
        Me.tpTel.ResumeLayout(False)
        Me.tpTel.PerformLayout()
        CType(Me.bnServicioTel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnServicioTel.ResumeLayout(False)
        Me.bnServicioTel.PerformLayout()
        CType(Me.bnPaqueteTel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPaqueteTel.ResumeLayout(False)
        Me.bnPaqueteTel.PerformLayout()
        CType(Me.bnPlanTel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPlanTel.ResumeLayout(False)
        Me.bnPlanTel.PerformLayout()
        Me.pnlTelefoniaAdic.ResumeLayout(False)
        Me.pnlTelefoniaAdic.PerformLayout()
        Me.pnlTelefonia.ResumeLayout(False)
        Me.pnlTelefonia.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.bnRecontratar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnRecontratar.ResumeLayout(False)
        Me.bnRecontratar.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tcServicios As System.Windows.Forms.TabControl
    Friend WithEvents tpTV As System.Windows.Forms.TabPage
    Friend WithEvents tpNET As System.Windows.Forms.TabPage
    Friend WithEvents tpDIG As System.Windows.Forms.TabPage
    Friend WithEvents tvTV As System.Windows.Forms.TreeView
    Friend WithEvents tvNET As System.Windows.Forms.TreeView
    Friend WithEvents tvDIG As System.Windows.Forms.TreeView
    Friend WithEvents bnTV As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarTV As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarTV As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnNETCablemodem As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnDIGServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarSDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarSDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnDIGTarjeta As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarTDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarTDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents panelTV As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents comboSerTV As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnTVCancelar As System.Windows.Forms.Button
    Friend WithEvents btnTVAceptar As System.Windows.Forms.Button
    Friend WithEvents panelCNET As System.Windows.Forms.Panel
    Friend WithEvents btnCNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnCNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents comboTipoAs As System.Windows.Forms.ComboBox
    Friend WithEvents comboTipoCa As System.Windows.Forms.ComboBox
    Friend WithEvents panelDIG As System.Windows.Forms.Panel
    Friend WithEvents btnDIGCancelar As System.Windows.Forms.Button
    Friend WithEvents btnDIGAceptar As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents comboSerDIG As System.Windows.Forms.ComboBox
    Friend WithEvents btnNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents comboSerNET2023 As System.Windows.Forms.ComboBox
    Friend WithEvents bnNETServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarSNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarSNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents panelSNET As System.Windows.Forms.Panel
    Friend WithEvents btnSNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnSNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents comboSerNET As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCelular As System.Windows.Forms.Label
    Friend WithEvents LabelTelefono As System.Windows.Forms.Label
    Friend WithEvents LabelCiudad As System.Windows.Forms.Label
    Friend WithEvents LabelColonia As System.Windows.Forms.Label
    Friend WithEvents LabelNumero As System.Windows.Forms.Label
    Friend WithEvents LabelCalle As System.Windows.Forms.Label
    Friend WithEvents LabelNombre As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBoxContrato As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBuscar As System.Windows.Forms.Button
    Friend WithEvents lblTV As System.Windows.Forms.Label
    Friend WithEvents lblNET As System.Windows.Forms.Label
    Friend WithEvents lblDIG As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents comboTipoSer As System.Windows.Forms.ComboBox
    Friend WithEvents bnRecontratar As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbRecon As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents nudConPago As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudSinPago As System.Windows.Forms.NumericUpDown
    Friend WithEvents tpTel As System.Windows.Forms.TabPage
    Friend WithEvents bnServicioTel As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarServicio As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarServicio As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnPaqueteTel As System.Windows.Forms.BindingNavigator
    Friend WithEvents tbsEliminarPaquete As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbsAgregarPaquete As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnPlanTel As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarPlan As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregaPlan As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnlTelefoniaAdic As System.Windows.Forms.Panel
    Friend WithEvents btnCancelarAdic As System.Windows.Forms.Button
    Friend WithEvents btnAceptarAdic As System.Windows.Forms.Button
    Friend WithEvents lbTelAdic As System.Windows.Forms.Label
    Friend WithEvents comboSerTelAdic As System.Windows.Forms.ComboBox
    Friend WithEvents lblTel As System.Windows.Forms.Label
    Friend WithEvents pnlTelefonia As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents lblMuestra As System.Windows.Forms.Label
    Friend WithEvents comboSerTel As System.Windows.Forms.ComboBox
    Friend WithEvents tvTel As System.Windows.Forms.TreeView
    Friend WithEvents CMBNumExt As System.Windows.Forms.Label
    Friend WithEvents nudExt As System.Windows.Forms.NumericUpDown
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextBoxTotalaPagar As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TextBoxContratoCompania As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
