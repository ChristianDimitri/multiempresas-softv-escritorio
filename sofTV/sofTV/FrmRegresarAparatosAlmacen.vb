﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmRegresarAparatosAlmacen

    Dim x, y As Integer

    Private Sub MUESTRADevolucionAparatosAlmacen(ByVal Op As Integer, ByVal Clv_Orden As Integer, ByVal Serie As String, ByVal Folio As Integer, ByVal TipoAparato As String, ByVal MacCablemodem As String, ByVal Clv_Usuario As String)
        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, CObj(Op))
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.Int, CObj(Clv_Orden))
        BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, CObj(Serie), 10)
        BaseII.CreateMyParameter("@FOLIO", SqlDbType.Int, CObj(Folio))
        BaseII.CreateMyParameter("@TIPOAPARATO", SqlDbType.VarChar, CObj(TipoAparato), 1)
        BaseII.CreateMyParameter("@MACCABLEMODEM", SqlDbType.VarChar, CObj(MacCablemodem), 50)
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, CObj(Clv_Usuario), 10)
        dgvD.DataSource = BaseII.ConsultaDT("MUESTRADevolucionAparatosAlmacen")
        If dgvD.RowCount = 0 Then Exit Sub
        dTable = MUESTRAMarcaAparatos()
        Marca.ValueMember = "NoArticulo"
        Marca.DisplayMember = "Descripcion"
        Marca.DataSource = dTable
        For x = 0 To dgvD.RowCount - 1
            Try
                dgvD.Item("MARCA", x).Value = dgvD.Item("MARCAACTUAL", x).Value


                If dgvD.Item("MARCA", x).Value = "" Then
                    dgvD.Item("MARCA", x).Value = "---"
                End If
            Catch ex As Exception

            End Try
        Next
    End Sub

    Private Function MUESTRAMarcaAparatos() As DataTable
        BaseII.limpiaParametros()
        Return BaseII.ConsultaDT("MUESTRAMarcaAparatos")
    End Function

    Private Sub MUESTRATipoAparatoDevolucion()
        Try

        
        BaseII.limpiaParametros()
            cbTipoAparato.DataSource = BaseII.ConsultaDT("MUESTRATipoAparatoDevolucion")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MUESTRAUsuarioDevolucion()
        Try

        
        BaseII.limpiaParametros()
            cbUsuario.DataSource = BaseII.ConsultaDT("MUESTRAUsuarioDevolucion")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PROCESODevolucionAparatosAlmacen(ByVal CLV_ORDEN As Integer, ByVal TipoAparato As String, ByVal Clv_Cablemodem As Integer, ByVal MacCablemodem As String, ByVal EstadoAparato As Boolean, ByVal Usuario As String, ByVal PROVIENE As String, ByVal NOARTICULO As Integer)
        Try

        
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.Int, CObj(CLV_ORDEN))
        BaseII.CreateMyParameter("@TIPOAPARATO", SqlDbType.VarChar, CObj(TipoAparato), 1)
        BaseII.CreateMyParameter("@CLV_CABLEMODEM", SqlDbType.Int, CObj(Clv_Cablemodem))
        BaseII.CreateMyParameter("@MACCABLEMODEM", SqlDbType.VarChar, CObj(MacCablemodem), 50)
        BaseII.CreateMyParameter("@ESTADOAPARATO", SqlDbType.Bit, EstadoAparato)
        BaseII.CreateMyParameter("@USUARIO", SqlDbType.VarChar, CObj(Usuario), 10)
        BaseII.CreateMyParameter("@PROVIENE", SqlDbType.VarChar, PROVIENE, 50)
        BaseII.CreateMyParameter("@NOARTICULO", SqlDbType.Int, NOARTICULO)
            BaseII.Inserta("PROCESODevolucionAparatosAlmacen")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub NoSeEncontroRegistro()
        If dgvD.RowCount = 0 Then
            MessageBox.Show("No se encontró ningún registro.")
            MUESTRADevolucionAparatosAlmacen(0, 0, "", 0, "", "", "")
        End If
    End Sub

    Private Sub tbOrden_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbOrden.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbOrden.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbOrden.Text) = False Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(1, tbOrden.Text, "", 0, "", "", "")
        NoSeEncontroRegistro
    End Sub

    Private Sub bnOrdenBuscar_Click(sender As System.Object, e As System.EventArgs) Handles bnOrdenBuscar.Click
        If tbOrden.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbOrden.Text) = False Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(1, tbOrden.Text, "", 0, "", "", "")
        NoSeEncontroRegistro
    End Sub

    Private Sub tbSerie_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbSerie.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbSerie.Text.Length = 0 Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(2, 0, tbSerie.Text, 0, "", "", "")
        NoSeEncontroRegistro()
    End Sub

    Private Sub tbFolio_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbFolio.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbFolio.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbFolio.Text) = False Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(2, 0, "", tbFolio.Text, "", "", "")
        NoSeEncontroRegistro()
    End Sub

    Private Sub bnSerieFolioBuscar_Click(sender As System.Object, e As System.EventArgs) Handles bnSerieFolioBuscar.Click
        If tbSerie.Text.Length = 0 Then Exit Sub
        If tbFolio.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbFolio.Text) = False Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(2, 0, tbSerie.Text, tbFolio.Text, "", "", "")
        NoSeEncontroRegistro()
    End Sub

    Private Sub bnTipoAparatoBuscar_Click(sender As System.Object, e As System.EventArgs) Handles bnTipoAparatoBuscar.Click
        If cbTipoAparato.Text.Length = 0 Then Exit Sub
        If cbTipoAparato.SelectedValue.ToString.Length = 0 Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(3, 0, "", 0, cbTipoAparato.SelectedValue, "", "")
        NoSeEncontroRegistro()
    End Sub

    Private Sub tbMac_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbMac.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbMac.Text.Length = 0 Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(4, 0, "", 0, "", tbMac.Text, "")
        NoSeEncontroRegistro()
    End Sub

    Private Sub bnMacBuscar_Click(sender As System.Object, e As System.EventArgs) Handles bnMacBuscar.Click
        If tbMac.Text.Length = 0 Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(4, 0, "", 0, "", tbMac.Text, "")
        NoSeEncontroRegistro()
    End Sub

    Private Sub bnUsuarioBuscar_Click(sender As System.Object, e As System.EventArgs) Handles bnUsuarioBuscar.Click
        If cbUsuario.Text.Length = 0 Then Exit Sub
        If cbUsuario.SelectedValue.ToString.Length = 0 Then Exit Sub
        MUESTRADevolucionAparatosAlmacen(5, 0, "", 0, "", "", cbUsuario.SelectedValue)
        NoSeEncontroRegistro()
    End Sub

    Private Sub bnRegresar_Click(sender As System.Object, e As System.EventArgs) Handles bnRegresar.Click

        If dgvD.RowCount = 0 Then
            MessageBox.Show("Selecciona al menos un aparato.")
            Exit Sub
        End If

        y = 0
        For x = 0 To dgvD.RowCount - 1
            If dgvD.Item("REGRESARALALMACEN", x).Value = True Then
                PROCESODevolucionAparatosAlmacen(dgvD.Item("Clv_Orden", x).Value, dgvD.Item("TIPOAPARATO", x).Value, dgvD.Item("CLV_CABLEMODEM", x).Value, dgvD.Item("MACCABLEMODEM", x).Value, dgvD.Item("ESTADOAPARATO", x).Value, GloUsuario, dgvD.Item("PROVIENEDE", x).Value, dgvD.Item("MARCA", x).Value)
                y += 1
            End If
        Next

        If y = 0 Then
            MessageBox.Show("Selecciona al menos un aparato.")
        ElseIf y = 1 Then
            MessageBox.Show("El aparato se regresó con éxito al Almacén.")
            MUESTRADevolucionAparatosAlmacen(0, 0, "", 0, "", "", "")
        ElseIf y > 1 Then
            MessageBox.Show("Los aparatos se regresaron con éxito al Almacén.")
            MUESTRADevolucionAparatosAlmacen(0, 0, "", 0, "", "", "")
        End If

    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmRegresarAparatosAlmacen_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Try

        
        MUESTRADevolucionAparatosAlmacen(0, 0, "", 0, "", "", "")
        MUESTRATipoAparatoDevolucion()
        MUESTRAUsuarioDevolucion()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvD_Sorted(sender As Object, e As EventArgs) Handles dgvD.Sorted
        Try
            Dim dTable As New DataTable
            dTable = MUESTRAMarcaAparatos()
            Marca.ValueMember = "NoArticulo"
            Marca.DisplayMember = "Descripcion"
            Marca.DataSource = dTable
            For x = 0 To dgvD.RowCount - 1
                Try
                    dgvD.Item("MARCA", x).Value = dgvD.Item("MARCAACTUAL", x).Value


                    If dgvD.Item("MARCA", x).Value = "" Then
                        dgvD.Item("MARCA", x).Value = "---"
                    End If
                Catch ex As Exception

                End Try
            Next
        Catch ex As Exception

        End Try
    End Sub
End Class
