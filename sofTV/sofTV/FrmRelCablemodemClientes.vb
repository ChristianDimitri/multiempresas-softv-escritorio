Imports System.Data.SqlClient

Public Class FrmRelCablemodemClientes

    Public Sub CREAARBOL()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim I As Integer = 0
            Dim X As Integer = 0
            If GLOTRABAJO = "ICABM" Then
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 0)
            ElseIf GLOTRABAJO = "BCABM" Then
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 15)
            ElseIf GLOTRABAJO = "CCABM" Then
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 16)
            End If
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.MUESTRACONTNETTableAdapter.Connection = CON
                Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                Me.TreeView1.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET.Rows
                    Me.TreeView1.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()
            Me.TreeView1.Select()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub CREAARBOL2()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            If GLOTRABAJO = "ICABM" Or GLOTRABAJO = "BCABM" Then
                Me.MUESTRAICAM_porSOLTableAdapter.Connection = CON
                Me.MUESTRAICAM_porSOLTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOL, GloDetClave, gloClv_Orden, 1)
            ElseIf GLOTRABAJO = "CCABM" Then
                Me.MUESTRAICAM_porSOLTableAdapter.Connection = CON
                Me.MUESTRAICAM_porSOLTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOL, GloDetClave, gloClv_Orden, 2)
            End If
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRAICAM_porSOL.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.MUESTRACONTNETTableAdapter.Connection = CON
                Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                Me.TreeView2.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET.Rows
                    Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView2.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmRelCablemodemClientes_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        CON.Close()
        GloBndTrabajo = True
        GloBloqueaDetalle = True
    End Sub



    Private Sub FrmRelCablemodemClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CREAARBOL()
        Me.CREAARBOL2()
        If GLOTRABAJO = "ICABM" Then
            Me.Text = "Instalación de Cablemodems"
            Me.Label3.Text = "Cablemodems del Cliente por Asignar"
            Me.Label4.Text = "Instalar y Asignar a estos Cablemodems"
        ElseIf GLOTRABAJO = "BCABM" Then
            Me.Text = "Baja de Cablemodems"
            Me.Label3.Text = "Cablemodems del Cliente Activos"
            Me.Label4.Text = "Cablemodems del Cliente Ejecutados a Baja"
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.Button3.Enabled = False
            Me.Button4.Enabled = False
        End If
    End Sub



    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "ICABM" Then
                Me.NUEICABM_SOLTableAdapter.Connection = CON
                Me.NUEICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEICABM_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0)
            ElseIf GLOTRABAJO = "BCABM" Then
                Me.NUEICABM_SOLTableAdapter.Connection = CON
                Me.NUEICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEICABM_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 2)
            ElseIf GLOTRABAJO = "CCABM" Then
                Me.NUEICABM_SOLTableAdapter.Connection = CON
                Me.NUEICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEICABM_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 4)
            ElseIf GLOTRABAJO = "BPAQU" Then
                Me.GuardaMotivoCanServTableAdapter.Connection = CON
                'Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 2, Me.Contratonet.Text, , 0)
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "ICABM" Then
                If IsNumeric(Me.Contratonet1.Text) = True Then
                    Me.BorICABM_SOLTableAdapter.Connection = CON
                    Me.BorICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borICABM_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, 0)
                End If
            ElseIf GLOTRABAJO = "CCABM" Then
                If IsNumeric(Me.Contratonet1.Text) = True Then
                    Me.BorICABM_SOLTableAdapter.Connection = CON
                    Me.BorICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borICABM_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, 2)
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "ICABM" Then
                Me.BorICABM_SOLTableAdapter.Connection = CON
                Me.BorICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borICABM_SOL, GloDetClave, gloClv_Orden, 0, 1)
            ElseIf GLOTRABAJO = "CCABM" Then
                Me.BorICABM_SOLTableAdapter.Connection = CON
                Me.BorICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borICABM_SOL, GloDetClave, gloClv_Orden, 0, 3)
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "ICABM" Then
                Me.NUEICABM_SOLTableAdapter.Connection = CON
                Me.NUEICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEICABM_SOL, GloDetClave, gloClv_Orden, 0, 1)
            ElseIf GLOTRABAJO = "BCABM" Then
                Me.NUEICABM_SOLTableAdapter.Connection = CON
                Me.NUEICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEICABM_SOL, GloDetClave, gloClv_Orden, 0, 3)
            ElseIf GLOTRABAJO = "CCABM" Then
                Me.NUEICABM_SOLTableAdapter.Connection = CON
                Me.NUEICABM_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEICABM_SOL, GloDetClave, gloClv_Orden, 0, 5)
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            Me.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    
End Class