Imports System.Data.SqlClient
Public Class FrmRepAtenTel

    Private Sub Llena_companias()
        Try


            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FrmRepAtenTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBndAtenTelGraf = True Then
            eBndAtenTelGraf = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Dame_clv_session_ReportesTableAdapter.Connection = CON
            Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEric2.Dame_clv_session_Reportes, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub FrmRepAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Llena_companias()

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
        CON.Close()
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If ComboBoxCompanias.SelectedIndex <= 0 Then

        '    MsgBox("Seleccione la Compa��a", MsgBoxStyle.Information, "Importante")
        '    Exit Sub
        'End If
        'IdCompania
        eIdCompania = ComboBoxCompanias.SelectedValue

        If Me.RadioButton1.Checked = True Then
            eOpVentas = 50
        ElseIf Me.RadioButton2.Checked = True Then
            eOpVentas = 51
        ElseIf Me.RadioButton3.Checked = True Then
            eOpVentas = 52
        End If

        eTipSer = CInt(Me.ConceptoComboBox.SelectedValue)
        eServicio = Me.ConceptoComboBox.Text
        eFechaIni = Me.DateTimePicker1.Value
        eFechaFin = Me.DateTimePicker2.Value

        If eOpVentas = 52 Then
            'FrmSelUsuariosE.Show()
            varfrmselcompania = "hastacompania1"
            FrmSelCiudadJ.Show()
        Else
            'FrmSelTrabajosE.Show()
            varfrmselcompania = "hastacompania2"
            FrmSelCiudadJ.Show()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class