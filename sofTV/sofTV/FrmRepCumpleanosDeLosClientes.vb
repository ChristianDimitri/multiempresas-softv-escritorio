﻿Public Class FrmRepCumpleanosDeLosClientes

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub FrmRepCumpleanosDeLosClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Llena_companias()
        dtpFechaIni.Value = DateTime.Today
        dtpFechaFin.Value = DateTime.Today
        dtpFechaFin.MaxDate = Today
        dtpFechaFin.MinDate = dtpFechaIni.Value
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            
            GloIdCompania = ComboBoxCompanias.SelectedValue


        Catch ex As Exception

        End Try
    End Sub

    Private Sub GroupBoxCompania_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBoxCompania.Enter

    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        dtpFechaIni.MaxDate = dtpFechaFin.Value
    End Sub

    Private Sub dtpFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        dtpFechaFin.MinDate = dtpFechaIni.Value
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click
        'If Me.ComboBoxCompanias.SelectedIndex <= 0 Then
        '    MsgBox("Selecciona una Compañía")
        '    Exit Sub
        'End If
        NombreCompania = ComboBoxCompanias.Text
        eOpVentas = 102
        eFechaIni = dtpFechaIni.Value
        eFechaFin = dtpFechaFin.Value
        FrmImprimirComision.Show()
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class