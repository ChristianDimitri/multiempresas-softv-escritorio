Public Class FrmRepPPV

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.Checked = True Then
            Me.GroupBox1.Enabled = True
            eOP1 = 1
        Else
            Me.GroupBox1.Enabled = False
            eOP1 = 0
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.Checked = True Then
            Me.GroupBox2.Enabled = True
            eOP2 = 1
        Else
            Me.GroupBox2.Enabled = False
            eOP2 = 0
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.TextBox1.Text) = False Then
            eContrato = 0
        Else
            eContrato = Me.TextBox1.Text
        End If
        eFechaIni = Me.DateTimePicker1.Value
        eFechaFin = Me.DateTimePicker2.Value
        eOP3 = 0
        eOpVentas = 54
        FrmImprimirComision.Show()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub FrmRepPPV_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class