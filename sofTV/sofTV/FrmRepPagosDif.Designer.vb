﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepPagosDif
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbTipSer = New System.Windows.Forms.ComboBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cbPrimero = New System.Windows.Forms.CheckBox
        Me.cbSegundo = New System.Windows.Forms.CheckBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cbStatusT = New System.Windows.Forms.CheckBox
        Me.cbStatusI = New System.Windows.Forms.CheckBox
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.btnSalir = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbTipSer)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(318, 61)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tipo de Servicio"
        '
        'cbTipSer
        '
        Me.cbTipSer.DisplayMember = "Concepto"
        Me.cbTipSer.FormattingEnabled = True
        Me.cbTipSer.Location = New System.Drawing.Point(6, 19)
        Me.cbTipSer.Name = "cbTipSer"
        Me.cbTipSer.Size = New System.Drawing.Size(303, 23)
        Me.cbTipSer.TabIndex = 3
        Me.cbTipSer.ValueMember = "Clv_TipSer"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbPrimero)
        Me.GroupBox2.Controls.Add(Me.cbSegundo)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 79)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(318, 51)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Periodos"
        '
        'cbPrimero
        '
        Me.cbPrimero.AutoSize = True
        Me.cbPrimero.Location = New System.Drawing.Point(65, 20)
        Me.cbPrimero.Name = "cbPrimero"
        Me.cbPrimero.Size = New System.Drawing.Size(77, 19)
        Me.cbPrimero.TabIndex = 3
        Me.cbPrimero.Text = "Primero"
        Me.cbPrimero.UseVisualStyleBackColor = True
        '
        'cbSegundo
        '
        Me.cbSegundo.AutoSize = True
        Me.cbSegundo.Location = New System.Drawing.Point(165, 20)
        Me.cbSegundo.Name = "cbSegundo"
        Me.cbSegundo.Size = New System.Drawing.Size(83, 19)
        Me.cbSegundo.TabIndex = 4
        Me.cbSegundo.Text = "Segundo"
        Me.cbSegundo.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cbStatusT)
        Me.GroupBox3.Controls.Add(Me.cbStatusI)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(18, 146)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(312, 52)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Status"
        '
        'cbStatusT
        '
        Me.cbStatusT.AutoSize = True
        Me.cbStatusT.Location = New System.Drawing.Point(137, 20)
        Me.cbStatusT.Name = "cbStatusT"
        Me.cbStatusT.Size = New System.Drawing.Size(166, 19)
        Me.cbStatusT.TabIndex = 6
        Me.cbStatusT.Text = "Suspensión Temporal"
        Me.cbStatusT.UseVisualStyleBackColor = True
        '
        'cbStatusI
        '
        Me.cbStatusI.AutoSize = True
        Me.cbStatusI.Location = New System.Drawing.Point(34, 20)
        Me.cbStatusI.Name = "cbStatusI"
        Me.cbStatusI.Size = New System.Drawing.Size(85, 19)
        Me.cbStatusI.TabIndex = 5
        Me.cbStatusI.Text = "Instalado"
        Me.cbStatusI.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(18, 222)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 3
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(194, 222)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 4
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'FrmRepPagosDif
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(347, 282)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmRepPagosDif"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Repote Pagos Diferidos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents cbPrimero As System.Windows.Forms.CheckBox
    Friend WithEvents cbSegundo As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cbStatusT As System.Windows.Forms.CheckBox
    Friend WithEvents cbStatusI As System.Windows.Forms.CheckBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class
