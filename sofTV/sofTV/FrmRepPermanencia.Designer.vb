<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepPermanencia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ComboBoxMesIni = New System.Windows.Forms.ComboBox
        Me.ComboBoxAnioIni = New System.Windows.Forms.ComboBox
        Me.ComboBoxMesFin = New System.Windows.Forms.ComboBox
        Me.ComboBoxAnioFin = New System.Windows.Forms.ComboBox
        Me.ComboBoxTipSer = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ComboBoxMesIni
        '
        Me.ComboBoxMesIni.DisplayMember = "Mes"
        Me.ComboBoxMesIni.FormattingEnabled = True
        Me.ComboBoxMesIni.Location = New System.Drawing.Point(88, 29)
        Me.ComboBoxMesIni.Name = "ComboBoxMesIni"
        Me.ComboBoxMesIni.Size = New System.Drawing.Size(88, 23)
        Me.ComboBoxMesIni.TabIndex = 0
        Me.ComboBoxMesIni.ValueMember = "Clv_Mes"
        '
        'ComboBoxAnioIni
        '
        Me.ComboBoxAnioIni.DisplayMember = "Anio"
        Me.ComboBoxAnioIni.FormattingEnabled = True
        Me.ComboBoxAnioIni.Items.AddRange(New Object() {"2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"})
        Me.ComboBoxAnioIni.Location = New System.Drawing.Point(182, 29)
        Me.ComboBoxAnioIni.Name = "ComboBoxAnioIni"
        Me.ComboBoxAnioIni.Size = New System.Drawing.Size(101, 23)
        Me.ComboBoxAnioIni.TabIndex = 1
        Me.ComboBoxAnioIni.ValueMember = "Anio"
        '
        'ComboBoxMesFin
        '
        Me.ComboBoxMesFin.DisplayMember = "Mes"
        Me.ComboBoxMesFin.FormattingEnabled = True
        Me.ComboBoxMesFin.Location = New System.Drawing.Point(88, 81)
        Me.ComboBoxMesFin.Name = "ComboBoxMesFin"
        Me.ComboBoxMesFin.Size = New System.Drawing.Size(88, 23)
        Me.ComboBoxMesFin.TabIndex = 2
        Me.ComboBoxMesFin.ValueMember = "Clv_Mes"
        '
        'ComboBoxAnioFin
        '
        Me.ComboBoxAnioFin.DisplayMember = "Anio"
        Me.ComboBoxAnioFin.FormattingEnabled = True
        Me.ComboBoxAnioFin.Items.AddRange(New Object() {"2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"})
        Me.ComboBoxAnioFin.Location = New System.Drawing.Point(182, 81)
        Me.ComboBoxAnioFin.Name = "ComboBoxAnioFin"
        Me.ComboBoxAnioFin.Size = New System.Drawing.Size(101, 23)
        Me.ComboBoxAnioFin.TabIndex = 3
        Me.ComboBoxAnioFin.ValueMember = "Anio"
        '
        'ComboBoxTipSer
        '
        Me.ComboBoxTipSer.DisplayMember = "Concepto"
        Me.ComboBoxTipSer.FormattingEnabled = True
        Me.ComboBoxTipSer.Location = New System.Drawing.Point(6, 20)
        Me.ComboBoxTipSer.Name = "ComboBoxTipSer"
        Me.ComboBoxTipSer.Size = New System.Drawing.Size(335, 23)
        Me.ComboBoxTipSer.TabIndex = 4
        Me.ComboBoxTipSer.ValueMember = "Clv_TipSer"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(63, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "al"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBoxTipSer)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 22)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(347, 60)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tipo de Servicio"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.ComboBoxAnioIni)
        Me.GroupBox2.Controls.Add(Me.ComboBoxMesIni)
        Me.GroupBox2.Controls.Add(Me.ComboBoxMesFin)
        Me.GroupBox2.Controls.Add(Me.ComboBoxAnioFin)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 88)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(347, 136)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Fechas de Contrataciones"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(53, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 15)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Del"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(52, 246)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(194, 246)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'FrmRepPermanencia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(374, 305)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmRepPermanencia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte de Permanencia"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ComboBoxMesIni As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxAnioIni As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxMesFin As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxAnioFin As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
