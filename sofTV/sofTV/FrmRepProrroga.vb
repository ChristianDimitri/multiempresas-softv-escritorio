﻿Public Class FrmRepProrroga

    Private Sub FrmRepProrroga_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        dtpFechaIni.Value = Today
        dtpFechaFin.Value = Today
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If rbSin.Checked = True Then
            OpProrroga = 0
        ElseIf rbCon.Checked = True Then
            OpProrroga = 1
        End If
        eFechaIni = dtpFechaIni.Value
        eFechaFin = dtpFechaFin.Value
        eOpVentas = 99
        FrmImprimirComision.Show()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub dtpFechaIni_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        dtpFechaFin.MinDate = dtpFechaIni.Value
    End Sub

    Private Sub dtpFechaFin_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        dtpFechaIni.MaxDate = dtpFechaFin.Value
    End Sub
End Class