﻿Public Class FrmReporteCajas

    Private Sub FrmReporteCajas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click
        If cbInstalados.Checked = False And cbDesconectados.Checked = False And cbSuspendidos.Checked = False And cbBajas.Checked = False And cbPendientes.Checked = False Then
            MsgBox("Debe seleccionar por lo menos un Status", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "No ha seleccionado Status")
        Else
            eInst = 0
            eDesc = 0
            eSusp = 0
            eBaja = 0
            ePend = 0
            eNumeroCajas = 0
            If cbInstalados.Checked = True Then
                eInst = 1
            End If
            If cbDesconectados.Checked = True Then
                eDesc = 1
            End If
            If cbSuspendidos.Checked = True Then
                eSusp = 1
            End If
            If cbBajas.Checked = True Then
                eBaja = 1
            End If
            If cbPendientes.Checked = True Then
                ePend = 1
            End If

            eNumeroCajas = nudNumeroCajas.Value

            eOpVentas = 105
            FrmImprimirComision.Show()
        End If
    End Sub
End Class