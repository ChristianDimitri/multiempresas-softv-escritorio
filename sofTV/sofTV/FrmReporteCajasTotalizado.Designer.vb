﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReporteCajasTotalizado
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbPendientes = New System.Windows.Forms.CheckBox()
        Me.CMBSeleccionaStatus = New System.Windows.Forms.Label()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.cbBaja = New System.Windows.Forms.CheckBox()
        Me.cbSuspendidos = New System.Windows.Forms.CheckBox()
        Me.cbDesconectados = New System.Windows.Forms.CheckBox()
        Me.cbInstalados = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'cbPendientes
        '
        Me.cbPendientes.AutoSize = True
        Me.cbPendientes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPendientes.Location = New System.Drawing.Point(42, 204)
        Me.cbPendientes.Name = "cbPendientes"
        Me.cbPendientes.Size = New System.Drawing.Size(223, 17)
        Me.cbPendientes.TabIndex = 16
        Me.cbPendientes.Text = "Pendientes de regresar a almacén "
        Me.cbPendientes.UseVisualStyleBackColor = True
        '
        'CMBSeleccionaStatus
        '
        Me.CMBSeleccionaStatus.AutoSize = True
        Me.CMBSeleccionaStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSeleccionaStatus.Location = New System.Drawing.Point(26, 29)
        Me.CMBSeleccionaStatus.Name = "CMBSeleccionaStatus"
        Me.CMBSeleccionaStatus.Size = New System.Drawing.Size(170, 15)
        Me.CMBSeleccionaStatus.TabIndex = 15
        Me.CMBSeleccionaStatus.Text = "SELECCIONE EL STATUS"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(166, 237)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 14
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnAceptar
        '
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(16, 237)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 13
        Me.bnAceptar.Text = "&ACEPTAR"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'cbBaja
        '
        Me.cbBaja.AutoSize = True
        Me.cbBaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBaja.Location = New System.Drawing.Point(42, 171)
        Me.cbBaja.Name = "cbBaja"
        Me.cbBaja.Size = New System.Drawing.Size(62, 19)
        Me.cbBaja.TabIndex = 12
        Me.cbBaja.Text = "Bajas"
        Me.cbBaja.UseVisualStyleBackColor = True
        '
        'cbSuspendidos
        '
        Me.cbSuspendidos.AutoSize = True
        Me.cbSuspendidos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSuspendidos.Location = New System.Drawing.Point(42, 137)
        Me.cbSuspendidos.Name = "cbSuspendidos"
        Me.cbSuspendidos.Size = New System.Drawing.Size(109, 19)
        Me.cbSuspendidos.TabIndex = 11
        Me.cbSuspendidos.Text = "Suspendidos"
        Me.cbSuspendidos.UseVisualStyleBackColor = True
        '
        'cbDesconectados
        '
        Me.cbDesconectados.AutoSize = True
        Me.cbDesconectados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDesconectados.Location = New System.Drawing.Point(42, 99)
        Me.cbDesconectados.Name = "cbDesconectados"
        Me.cbDesconectados.Size = New System.Drawing.Size(124, 19)
        Me.cbDesconectados.TabIndex = 10
        Me.cbDesconectados.Text = "Desconectados"
        Me.cbDesconectados.UseVisualStyleBackColor = True
        '
        'cbInstalados
        '
        Me.cbInstalados.AutoSize = True
        Me.cbInstalados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInstalados.Location = New System.Drawing.Point(42, 67)
        Me.cbInstalados.Name = "cbInstalados"
        Me.cbInstalados.Size = New System.Drawing.Size(96, 19)
        Me.cbInstalados.TabIndex = 9
        Me.cbInstalados.Text = "Instalados "
        Me.cbInstalados.UseVisualStyleBackColor = True
        '
        'FrmReporteCajasTotalizado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(319, 302)
        Me.Controls.Add(Me.cbPendientes)
        Me.Controls.Add(Me.CMBSeleccionaStatus)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.cbBaja)
        Me.Controls.Add(Me.cbSuspendidos)
        Me.Controls.Add(Me.cbDesconectados)
        Me.Controls.Add(Me.cbInstalados)
        Me.Name = "FrmReporteCajasTotalizado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Cajas Totalizado"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbPendientes As System.Windows.Forms.CheckBox
    Friend WithEvents CMBSeleccionaStatus As System.Windows.Forms.Label
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents cbBaja As System.Windows.Forms.CheckBox
    Friend WithEvents cbSuspendidos As System.Windows.Forms.CheckBox
    Friend WithEvents cbDesconectados As System.Windows.Forms.CheckBox
    Friend WithEvents cbInstalados As System.Windows.Forms.CheckBox
End Class
