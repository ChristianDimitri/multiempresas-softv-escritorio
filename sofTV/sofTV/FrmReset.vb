Imports System.Data.SqlClient
Public Class FrmReset
    Dim Clv_TipSer As Integer
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Private TipSer As String = Nothing
    Private Mac As String = Nothing
    Private yaesta As Integer = 0
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GuardaDatosBitacora()
        Try
            Dim Cont As Integer = 0
            If Me.RadioButton1.Checked = True Then
                TipSer = Me.RadioButton4.Text
            Else
                TipSer = Me.RadioButton3.Text
            End If
            
            Mac = Me.Clv_CableModemTextBox.Text
            bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Text, "Se Reseteo Aparato", "", "Servicio: " + TipSer + " / Clave de Tarjeta � CableModem: " + Mac, LocClv_Ciudad)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        BrwSelContrato.Show()
    End Sub

    Private Sub FrmReset_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            'Me.TextBox1.Text = eContrato
            tbContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
            'CON.Open()
            'Me.DameClientesActivosTableAdapter.Connection = CON
            'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            'CON.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, eContrato)
            BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@ciudad", SqlDbType.VarChar, "", 250)
            BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            Dim dTable As DataTable = BaseII.ConsultaDT("DameClientesActivos")
            If dTable.Rows.Count > 0 Then
                Me.TextBox1.Text = dTable.Rows(0)(0).ToString
                'Me.tbContratoCompania.Text = dTable.Rows(0)(1).ToString
                Me.NOMBRETextBox.Text = dTable.Rows(0)(2).ToString
                Me.CALLETextBox.Text = dTable.Rows(0)(3).ToString
                Me.NUMEROTextBox.Text = dTable.Rows(0)(5).ToString
                Me.COLONIATextBox.Text = dTable.Rows(0)(4).ToString
                Me.CIUDADTextBox.Text = dTable.Rows(0)(6).ToString
            End If
            If yaesta = 0 Then
                BuscarCliente()
            End If
            yaesta = 0

        End If

    End Sub

    Private Sub FrmReset_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        'Llena_companias()
        If GloHabilitadosNET = False Then
            Me.RadioButton3.Visible = False
        Else
            RadioButton3.Visible = True
            RadioButton3.Checked = True
        End If


        If GloHabilitadosDIG = False Then

            RadioButton4.Visible = False
        Else
            If RadioButton3.Checked = False Then
                RadioButton4.Checked = True
            Else
                RadioButton4.Checked = False
            End If
            RadioButton4.Visible = True
        End If
        eContrato = 0
        If Me.RadioButton4.Checked = True Then
            Clv_TipSer = 3
        End If
        If Me.RadioButton3.Checked = True Then
            Clv_TipSer = 2
        End If
    End Sub
    Private Sub BuscarCliente()
        
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        'CON.Close()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, TextBox1.Text)
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "", 250)
        BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, "", 250)
        BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, "", 250)
        BaseII.CreateMyParameter("@ciudad", SqlDbType.VarChar, "", 250)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        Dim dTable As DataTable = BaseII.ConsultaDT("DameClientesActivos")
        If dTable.Rows.Count > 0 Then
            Me.TextBox1.Text = dTable.Rows(0)(0).ToString
            'Me.tbContratoCompania.Text = dTable.Rows(0)(1).ToString
            Me.NOMBRETextBox.Text = dTable.Rows(0)(2).ToString
            Me.CALLETextBox.Text = dTable.Rows(0)(3).ToString
            Me.NUMEROTextBox.Text = dTable.Rows(0)(5).ToString
            Me.COLONIATextBox.Text = dTable.Rows(0)(4).ToString
            Me.CIUDADTextBox.Text = dTable.Rows(0)(6).ToString

        Else
            Me.TextBox1.Text = "0"
            Me.tbContratoCompania.Text = ""
            Me.NOMBRETextBox.Text = ""
            Me.CALLETextBox.Text = ""
            Me.NUMEROTextBox.Text = ""
            Me.COLONIATextBox.Text = ""
            Me.CIUDADTextBox.Text = ""
        End If
        Try

            BuscarServ()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub BuscarServ()
        Dim CON As New SqlConnection(MiConexion)
        eRes = 0
        eMsg = ""
        CON.Open()
        'Me.MuestraServCteResetTableAdapter.Connection = CON
        'Me.MuestraServCteResetTableAdapter.Fill(Me.DataSetEric.MuestraServCteReset, Me.TextBox1.Text, Clv_TipSer, eRes, eMsg)
        CON.Close()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Me.TextBox1.Text)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Res", SqlDbType.Int, eRes)
        BaseII.CreateMyParameter("@Msg", SqlDbType.VarChar, eMsg, 150)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        MuestraServCteResetDataGridView.DataSource = BaseII.ConsultaDT("MuestraServCteReset")
        If MuestraServCteResetDataGridView.Rows.Count > 0 Then
            ContratoTextBox.Text = MuestraServCteResetDataGridView.SelectedCells(0).Value
            Clv_CableModemTextBox.Text = MuestraServCteResetDataGridView.SelectedCells(1).Value
        End If
        Dim CONE As New SqlConnection(MiConexion)
        CONE.Open()
        Dim Comando = New SqlClient.SqlCommand
        With Comando
            .Connection = CONE
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            .CommandText = "MuestraServCteReset"

            '' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Contrato", SqlDbType.Int)
            Dim prm1 As New SqlParameter("@Clv_TipSer", SqlDbType.VarChar, 250)
            Dim prm2 As New SqlParameter("@Res", SqlDbType.VarChar, 50)
            Dim prm3 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
            Dim prm4 As New SqlParameter("@idcompania", SqlDbType.VarChar, 250)
            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Output
            prm3.Direction = ParameterDirection.Output
            prm4.Direction = ParameterDirection.Input
            prm.Value = Me.TextBox1.Text
            prm1.Value = Clv_TipSer
            prm2.Value = eRes
            prm3.Value = eMsg
            prm4.Value = GloIdCompania
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            Dim i As Integer = Comando.ExecuteNonQuery()
            eRes = prm2.Value
            eMsg = prm3.Value
        End With
        CONE.Close()
        If eRes = 1 Then
            yaesta = 1
            MsgBox(eMsg)
        End If

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged, RadioButton4.CheckedChanged
        Me.Button4.Visible = False
        If Me.RadioButton4.Checked = True Then
            Clv_TipSer = 3
            If IsNumeric(Me.TextBox1.Text) = True Then
                BuscarCliente()
            End If
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged, RadioButton3.CheckedChanged
        Me.Button4.Visible = False
        If Me.RadioButton3.Checked = True Then
            Clv_TipSer = 2
            If IsNumeric(Me.TextBox1.Text) = True Then
                BuscarCliente()
            End If

        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.ResetServCteTableAdapter.Connection = CON
            Me.ResetServCteTableAdapter.Fill(Me.DataSetEric.ResetServCte, Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, Clv_TipSer, eRes, eMsg)
            CON.Close()
            GuardaDatosBitacora()
            MsgBox(eMsg)


        Else
            MsgBox("Selecciona Un Aparato a Resetear", , "Atenci�n")
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress, tbContratoCompania.KeyPress
        'If Asc(e.KeyChar) = 13 Then
        '    If ComboBoxCompanias.SelectedValue = 0 Then
        '        MsgBox("Selecciona una Compa��a")
        '        Exit Sub
        '    End If
        '    If IsNumeric(Me.TextBox1.Text) = True Then
        '        BuscarCliente()
        '    End If
        'End If
    End Sub

    
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
    End Sub

    Private Sub ResetPrimer_Modelo(ByVal mcontrato As Long, ByVal mClv_Cablemodem As Long, ByVal MClv_Tipser As Integer)
        Dim Con45 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim Res As Integer = 0
        Dim Msg As String = ""
        Try
            cmd = New SqlClient.SqlCommand()
            Con45.Open()
            With cmd
                .CommandText = "ResetServCtePrimer_Modelo"
                .CommandTimeout = 0
                .Connection = Con45
                .CommandType = CommandType.StoredProcedure

                '@Contrato Bigint,@Clv_CableModem Bigint,@Clv_TipSer int,@Res int output,@Msg varchar(150) output

                Dim prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = mcontrato
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Clv_CableModem", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = mClv_Cablemodem
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = MClv_Tipser
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Res", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@Msg", SqlDbType.VarChar, 50)
                prm5.Direction = ParameterDirection.Output
                prm5.Value = ""
                .Parameters.Add(prm5)

                Dim i As Integer = cmd.ExecuteNonQuery()
                Res = prm4.Value
                Msg = prm5.Value
            End With
            Con45.Close()
        Catch ex As Exception
            If Con45.State <> ConnectionState.Closed Then Con45.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            ResetPrimer_Modelo(Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, Clv_TipSer)
            GuardaDatosBitacora()
            MsgBox("Se realizo con Exito")
        Else
            MsgBox("Selecciona Un Aparato a Resetear", , "Atenci�n")
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Me.TextBox1.Text = ""
        Me.tbContratoCompania.Text = ""
        Me.NOMBRETextBox.Text = ""
        Me.CALLETextBox.Text = ""
        Me.NUMEROTextBox.Text = ""
        Me.COLONIATextBox.Text = ""
        Me.CIUDADTextBox.Text = ""

        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            tbContratoCompania.Text = ""
            TextBox1.Text = "0"
            BuscarCliente()
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub tbContratoCompania_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbContratoCompania.TextChanged

    End Sub
    Dim bnd As Integer = 0
    Private Sub TextBox1_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown

    End Sub

    Private Sub tbContratoCompania_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContratoCompania.KeyDown
        If e.KeyValue = Keys.Enter Then
            Try
                Dim con As New SqlConnection(MiConexion)
                con.Open()
                Dim comando As New SqlCommand()
                Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
                GloIdCompania = array(1)
                comando.Connection = con
                comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0).Trim + " and idcompania=" + GloIdCompania.ToString()
                TextBox1.Text = comando.ExecuteScalar.ToString()
                con.Close()
            Catch ex As Exception

            End Try
            If IsNumeric(Me.TextBox1.Text) = True Then

                BuscarCliente()

            End If

        End If
    End Sub

    Private Sub MuestraServCteResetDataGridView_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MuestraServCteResetDataGridView.SelectionChanged
        If MuestraServCteResetDataGridView.Rows.Count > 0 Then
            Try
                ContratoTextBox.Text = MuestraServCteResetDataGridView.SelectedCells(0).Value
                Clv_CableModemTextBox.Text = MuestraServCteResetDataGridView.SelectedCells(1).Value
            Catch ex As Exception

            End Try
        End If
        
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            Clv_TipSer = 2
            BuscarCliente()
        Else
            Clv_TipSer = 3
            BuscarCliente()
        End If
    End Sub
End Class