﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRoboAparatos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Orden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Cablemodem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MacCableModem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contratonet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_TipSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Robado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Orden, Me.Clave, Me.Clv_Cablemodem, Me.MacCableModem, Me.Contratonet, Me.Clv_TipSer, Me.Robado})
        Me.DataGridView1.Location = New System.Drawing.Point(22, 24)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(303, 372)
        Me.DataGridView1.TabIndex = 0
        '
        'Clv_Orden
        '
        Me.Clv_Orden.DataPropertyName = "Clv_Orden"
        Me.Clv_Orden.HeaderText = "Clv_Orden"
        Me.Clv_Orden.Name = "Clv_Orden"
        Me.Clv_Orden.Visible = False
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.Visible = False
        '
        'Clv_Cablemodem
        '
        Me.Clv_Cablemodem.DataPropertyName = "Clv_Cablemodem"
        Me.Clv_Cablemodem.HeaderText = "Clv_Cablemodem"
        Me.Clv_Cablemodem.Name = "Clv_Cablemodem"
        Me.Clv_Cablemodem.Visible = False
        '
        'MacCableModem
        '
        Me.MacCableModem.DataPropertyName = "MacCableModem"
        Me.MacCableModem.HeaderText = "Aparato"
        Me.MacCableModem.Name = "MacCableModem"
        Me.MacCableModem.Width = 200
        '
        'Contratonet
        '
        Me.Contratonet.DataPropertyName = "Contratonet"
        Me.Contratonet.HeaderText = "Contratonet"
        Me.Contratonet.Name = "Contratonet"
        Me.Contratonet.Visible = False
        '
        'Clv_TipSer
        '
        Me.Clv_TipSer.DataPropertyName = "Clv_TipSer"
        Me.Clv_TipSer.HeaderText = "Clv_TipSer"
        Me.Clv_TipSer.Name = "Clv_TipSer"
        Me.Clv_TipSer.Visible = False
        '
        'Robado
        '
        Me.Robado.DataPropertyName = "Robado"
        Me.Robado.HeaderText = "Sin Recuperar"
        Me.Robado.Name = "Robado"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(106, 412)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmRoboAparatos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(345, 467)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "FrmRoboAparatos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aparatos sin Recuperar"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Clv_Orden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Cablemodem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MacCableModem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contratonet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_TipSer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Robado As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
