Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.IO

Public Class FrmSelCablemodemsDisponibles


    Private Sub ChecaRequiereWan()
        If IsNumeric(ComboBox2.SelectedValue) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_Cablemodem", SqlDbType.BigInt, ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@RequiereMacWan", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("ChecaRequiereMacWan")
            GloRequiereMacWan = CBool(BaseII.dicoPar("@RequiereMacWan").ToString)
        End If
        If GloRequiereMacWan Then
            LabelWan.Visible = True
            TextBoxWan.Visible = True
        Else
            LabelWan.Visible = False
            TextBoxWan.Visible = False
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndClv_CablemodemSel = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If GloClv_TipSer = 3 Then
            If GLOTRABAJO = "CAPAR" Then
                If IsNumeric(Me.ComboBox2.SelectedValue) = False Then
                    MsgBox("�Seleccione al menos una tarjeta!", MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
                If Me.ComboBox2.Text.Length = 0 Then
                    MsgBox("�Seleccione al menos una tarjeta!", MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
            End If


            If GLOTRABAJO <> "CAPAR" Then
                If IsNumeric(Me.CajasComboBox.SelectedValue) = False Then
                    MsgBox("�Seleccione al menos una caja!", MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
                If Me.CajasComboBox.Text.Length = 0 Then
                    MsgBox("�Seleccione al menos una caja!", MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
            End If
        End If

        If RequiereMacWan And TextBoxWan.Text.Length = 0 Then
            MsgBox("�Requiere la direcci�n MAC WAN!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If RequiereMacWan And TextBoxWan.Text.Length <> 12 Then
            MsgBox("�Formato incorrecto de la direcci�n MAC WAN!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If IsNumeric(Me.CajasComboBox.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
            GloClvCajaDig = Me.CajasComboBox.Text
            GloBndClv_CablemodemSel = True
        ElseIf IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
            GloClvCajaDig = Me.CajasComboBox.Text
            GloBndClv_CablemodemSel = True
            GloMacWan = TextBoxWan.Text
            GloMacLan = ComboBox2.Text
        Else
            GloClv_CablemodemSel = 0
            GloClvCajaDig = 0
            GloMacCablemodemSel = ""
            GloBndClv_CablemodemSel = False
            If IdSistema <> "LO" And IdSistema <> "YU" Then
                MsgBox("No se Seleccionado un Cablemodem", MsgBoxStyle.Information)
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("No se Seleccionado un ATA", MsgBoxStyle.Information)
            End If
        End If
        If GloClv_TipSer = 3 Then
            If GLOTRABAJO <> "CAPAR" Then
                If IsNumeric(Me.CajasComboBox.SelectedValue) = True Then
                    GloClvCajaDig = Me.CajasComboBox.Text
                    GloBndClv_CablemodemSel = True
                Else
                    MsgBox("No ha seleccionado una caja digital", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        End If
        Me.Close()
    End Sub

    Private Sub FrmSelCablemodemsDisponibles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        GloRequiereMacWan = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If GloClv_TipSer = 2 Then
            Label5.Text = "Cablemodems Disponibles :"
            If GLOTRABAJO = "CCABM" Or GLOTRABAJO = "ICABM" Then
                MUESTRACABLEMODEMS_disponibles_PorServicio()
            Else
                Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 0, Locclv_tec)
            End If
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 0, Locclv_tec)
            Me.Label1.Visible = False
            Me.CajasComboBox.Visible = False
            CON.Close()
            ChecaRequiereWan()
            consultaMacWanPorOrden()
        ElseIf GloClv_TipSer = 3 Then
            If GLOTRABAJO = "CCAJA" Then
                Me.ComboBox2.Visible = False
                Me.CajasComboBox.Visible = True
                Me.Label5.Visible = False
                Me.Label1.Visible = True
                MuestraCajasDisponibles()
            ElseIf GLOTRABAJO = "CAPAR" Then
                Me.ComboBox2.Visible = True
                Me.CajasComboBox.Visible = False
                Me.Label5.Visible = True
                Me.Label1.Visible = False
            ElseIf GLOTRABAJO = "IAPAR" Then
                Me.ComboBox2.Visible = True
            End If
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 3, Locclv_tec)
            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "RIAPA" Or GLOTRABAJO = "CCAJA" Then
                Me.CajasComboBox.Visible = True
                MuestraCajasDisponibles()
                'Else
                '    Me.CajasComboBox.Visible = False
            End If
        End If
            CON.Close()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
        End If
    End Sub

    Private Sub MuestraCajasDisponibles()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC MuestraCajasDisponibles ")
        StrSql.Append(CStr(Locclv_tec) & ",")
        StrSql.Append(CStr(ContratoNetCajasDig))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        'Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            'BS.DataSource = DT
            Me.CajasComboBox.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MUESTRACABLEMODEMS_disponibles_PorServicio()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC MUESTRACABLEMODEMS_disponibles_PorServicio ")
        StrSql.Append(CStr("0") & ",")
        StrSql.Append(CStr(Locclv_tec) & ",")
        StrSql.Append(CStr(GloContratonetOrdenR))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        'Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            'BS.DataSource = DT
            Me.ComboBox2.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ComboBox2_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedValueChanged
        ChecaRequiereWan()
        consultaMacWanPorOrden()
    End Sub

    Private Sub consultaMacWanPorOrden()
        If IsNumeric(ComboBox2.SelectedValue) And GloRequiereMacWan Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, GloDetClave)
            BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@MacWan", ParameterDirection.Output, SqlDbType.VarChar, 50)
            BaseII.ProcedimientoOutPut("consultaMacWanPorOrden")
            TextBoxWan.Text = BaseII.dicoPar("@MacWan").ToString
        End If

    End Sub
End Class