﻿Public Class FrmSelCiudadesMovimientoCartera
    Dim a As Integer
    Dim b As Integer
    Private Sub FrmSelCiudadesMovimientoCartera_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        cbCiudades.DisplayMember = "cds"
        cbCiudades.ValueMember = "clave_cds"
        cbCiudades.DataSource = BaseII.ConsultaDT("ElegirCuidadesMovCartera")
    End Sub
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        bgMovCartera = False
        Me.Close()
    End Sub
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        cadenaCiudades = cbCiudades.SelectedValue
        BndMovimientoCartera = False
        BrwMovimientosCartera.Show()
        Me.Close()
    End Sub
End Class