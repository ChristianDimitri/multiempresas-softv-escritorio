﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSelCliente

    'Private Sub BUSCACLIENTES(ByVal OP As Integer)
    '    Try
    '        Dim CON3 As New SqlConnection(MiConexion)
    '        CON3.Open()
    '        If OP = 0 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            If IsNumeric(Me.bcONTRATO.Text) = True Then
    '                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
    '            Else
    '                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
    '            End If
    '            Me.bcONTRATO.Clear()
    '        ElseIf OP = 1 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType(Me.BNOMBRE.Text, String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
    '            'Me.BNOMBRE.Clear()
    '        ElseIf OP = 2 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType(Me.BCALLE.Text, String)), (CType(Me.BNUMERO.Text, String)), (CType(Me.BCIUDAD.Text, String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        ElseIf OP = 3 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)

    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        End If
    '        CON3.Close()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub
    Dim loccon As Integer = 0
    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Try
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            If OP = 0 Then
                If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
                    OP = OP + 10
                End If
                If loccon > 0 Then
                    'Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
                    'Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
                    BusCliPorContratoSeparado(loccon, "", "", "", "", "", "", OP, GloClv_TipSer, 0)
                Else
                    'Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
                    'Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
                    BusCliPorContratoSeparado(0, "", "", "", "", "", "", OP, GloClv_TipSer, 0)
                End If
                Me.bcONTRATO.Clear()
            ElseIf OP = 1 Then
                If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
                    OP = OP + 10
                End If
                'Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
                'Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType(Me.BNOMBRE.Text, String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
                'Me.BNOMBRE.Clear()
                BusCliPorContratoSeparado(0, Me.BNOMBRE.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", OP, GloClv_TipSer, 0)
            ElseIf OP = 2 Then
                If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
                    OP = OP + 10
                End If
                'Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
                'Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType(Me.BCALLE.Text, String)), (CType(Me.BNUMERO.Text, String)), (CType(Me.BCIUDAD.Text, String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
                'Me.BCALLE.Clear()
                'Me.BNUMERO.Clear()
                'Me.BCIUDAD.Clear()
                BusCliPorContratoSeparado(0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, Me.BCIUDAD.Text, OP, GloClv_TipSer, Me.cmbColonias.SelectedValue)
            ElseIf OP = 3 Then
                If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
                    OP = OP + 10
                End If
                'Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
                'Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
                BusCliPorContratoSeparado(0, "", "", "", "", "", "", OP, GloClv_TipSer, 0)
            ElseIf OP = 9 Then
                GloClv_TipSer = 999 'Or GloClv_TipSer = 1001 Then
                OP = OP - 6
                'End If
                'Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
                'Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer)
                BusCliPorContratoSeparado(0, "", "", "", "", "", "", OP, GloClv_TipSer, 0)
                'Me.BCALLE.Clear()
                'Me.BNUMERO.Clear()
                'Me.BCIUDAD.Clear()
            ElseIf OP = 5 Then
                BusCliPorContratoSeparado(0, "", "", "", "", "", "", OP, GloClv_TipSer, 0)
            End If
            CON3.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BusCliPorContratoSeparado(ByVal ContratoCli As Long, ByVal NombreCli As String, ByVal APaternoCli As String, ByVal AMaternoCli As String, _
                                          ByVal CalleCli As String, ByVal NumeroCli As String, ByVal CiudadCli As String, ByVal OpCli As Integer, ByVal Clv_TipSerCli As Integer, ByVal clvColonia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder
        Dim tarjeta As String = ""
        Dim stb As String = ""
        If TxtTarjeta.Text = "" Then
            tarjeta = "''"
        Else
            tarjeta = "'" + TxtTarjeta.Text.Trim + "'"
        End If
        If TxtSetUpBox.Text = "" Then
            stb = "''"
        Else
            stb = "'" + TxtSetUpBox.Text.Trim + "'"
        End If
        'op=3   0, "", "", "", "", "", "", OP, GloClv_TipSer, Me.cmbColonias.SelectedValue
        'op=0 busca contratocompania
        StrSQL.Append("EXEC uspBuscaContratoSeparado2 ")
        StrSQL.Append(CStr(ContratoCli) & ",")
        StrSQL.Append("'" & NombreCli & "',")
        StrSQL.Append("'" & APaternoCli & "',")
        StrSQL.Append("'" & AMaternoCli & "',")
        StrSQL.Append("'" & CalleCli & "',")
        StrSQL.Append("'" & NumeroCli & "',")
        StrSQL.Append("'" & CiudadCli & "',")
        StrSQL.Append(CStr(OpCli) & ",")
        StrSQL.Append(CStr(Clv_TipSerCli) & ",")
        StrSQL.Append(CStr(clvColonia) & ",")
        StrSQL.Append(CStr(GloIdCompania) & ",")
        StrSQL.Append(CStr(stb) & ",")
        StrSQL.Append(CStr(tarjeta))
        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
            Try
                DataGridView1.Columns("ContratoBueno").Visible = False
                CONTRATOLabel1.Text = CStr(Me.DataGridView1.SelectedCells(10).Value)
                NOMBRELabel1.Text = CStr(Me.DataGridView1.SelectedCells(1).Value) + " " + CStr(Me.DataGridView1.SelectedCells(2).Value) + " " + CStr(Me.DataGridView1.SelectedCells(3).Value) + " "
                CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
                NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(6).Value)
                COLONIALabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
                ContratoCompaniaLabel.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
            Catch ex As Exception

            End Try
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            GLOCONTRATOSEL = Me.CONTRATOLabel1.Text
            Dim array As String() = Me.ContratoCompaniaLabel.Text.Split("-")
            eContratoCompania = array(0)
            GloIdCompania = array(1)
            If GloClv_TipSer = 1000 Or 1001 Then
                LocbndProceso = True
            End If
            If Locformulario = 1 Then
                Locbndcontrato1 = True
            ElseIf Locformulario = 2 Then
                Locbndcontrato2 = True
            ElseIf Locformulario = 3 Then
                Locbndcontrato3 = True
            End If
            'If GLOCONTRATOSEL > 0 Then
            '    FrmServiciosPPE.DameContrato()
            'End If
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        LocbndProceso1 = True
        Me.Close()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            cmbColonias.SelectedIndex = 0
            If bcONTRATO.Text.Contains("-") Then
                Dim array As String()
                array = bcONTRATO.Text.Trim.Split("-")
                loccon = array(0).Trim
                If array(1).Length = 0 Then
                    Exit Sub
                End If
                GloIdCompania = array(1).Trim
                Me.BUSCACLIENTES(0)
            Else
                loccon = bcONTRATO.Text
                GloIdCompania = 999
                Me.BUSCACLIENTES(0)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.BUSCACLIENTES(1)
    End Sub


    Private Sub BrwClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GLOCONTRATOSEL = 0
        If IdSistema = "VA" Or IdSistema = "LO" Or IdSistema = "YU" Then
            FrmSelCliente2.Show()
            Me.Close()
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla según sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        CON.Close()
        llenaComboColonias()
        Me.BUSCACLIENTES(3)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub bcONTRATO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bcONTRATO.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros y guión
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Or val = 45 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            Try
                If bcONTRATO.Text.Contains("-") Then
                    Dim array As String()
                    array = bcONTRATO.Text.Trim.Split("-")
                    loccon = array(0).Trim
                    If array(1).Length = 0 Then
                        Exit Sub
                    End If
                    GloIdCompania = array(1).Trim
                    Me.BUSCACLIENTES(0)
                Else
                    loccon = bcONTRATO.Text
                    GloIdCompania = 999
                    Me.BUSCACLIENTES(0)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Me.BUSCACLIENTES(2)
    End Sub

    Private Sub BNOMBRE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(1)
            'Me.BNOMBRE.Text = ""
        End If
    End Sub

    Private Sub BNOMBRE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BCALLE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCALLE.TextChanged

    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BNUMERO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNUMERO.TextChanged

    End Sub

    Private Sub BCIUDAD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCIUDAD.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub



    Private Sub CONTRATOLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.TextChanged
        Try
            CREAARBOL()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SOLOINTERNETCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOLOINTERNETCheckBox.CheckedChanged
        If Me.SOLOINTERNETCheckBox.Checked = False Then
            Me.SOLOINTERNETCheckBox.Enabled = False
        Else
            Me.SOLOINTERNETCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ESHOTELCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESHOTELCheckBox.CheckedChanged
        If Me.ESHOTELCheckBox.Checked = False Then
            Me.ESHOTELCheckBox.Enabled = False
        Else
            Me.ESHOTELCheckBox.Enabled = True
        End If
    End Sub


    Private Sub ServicioListBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.bcONTRATO.Focus()
    End Sub




    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON.Close()
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        Try
            CONTRATOLabel1.Text = CStr(Me.DataGridView1.SelectedCells(10).Value)
            NOMBRELabel1.Text = CStr(Me.DataGridView1.SelectedCells(1).Value) + " " + CStr(Me.DataGridView1.SelectedCells(2).Value) + " " + CStr(Me.DataGridView1.SelectedCells(3).Value) + " "
            CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
            NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(6).Value)
            COLONIALabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
            ContratoCompaniaLabel.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
        Catch ex As Exception

        End Try
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            GLOCONTRATOSEL = Me.CONTRATOLabel1.Text
            Dim array As String() = Me.ContratoCompaniaLabel.Text.Split("-")
            eContratoCompania = array(0)
            GloIdCompania = array(1)
            If GloClv_TipSer = 1000 Or 1001 Then
                LocbndProceso = True
            End If
            If Locformulario = 1 Then
                Locbndcontrato1 = True
            ElseIf Locformulario = 2 Then
                Locbndcontrato2 = True
            ElseIf Locformulario = 3 Then
                Locbndcontrato3 = True
            End If
            'If GLOCONTRATOSEL > 0 Then
            '    FrmServiciosPPE.DameContrato()
            'End If
            Me.Close()
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub


    Private Sub Button7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        cmbColonias.SelectedIndex = 0
        BUSCACLIENTES(1)
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            CONTRATOLabel1.Text = CStr(Me.DataGridView1.SelectedCells(10).Value)
            NOMBRELabel1.Text = CStr(Me.DataGridView1.SelectedCells(1).Value) + " " + CStr(Me.DataGridView1.SelectedCells(2).Value) + " " + CStr(Me.DataGridView1.SelectedCells(3).Value) + " "
            CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
            NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(6).Value)
            COLONIALabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
            ContratoCompaniaLabel.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BNOMBRE_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BNOMBRE.KeyDown, APaternoTextBox.KeyDown, AMaternoTextBox.KeyDown
        If e.KeyCode = Keys.Enter Then
            Button7_Click_1(sender, e)
        End If
    End Sub

#Region "COMBO COLONIAS"
    Private Sub llenaComboColonias()
        Dim colonias As New BaseIII
        Try
            colonias.limpiaParametros()
            Me.cmbColonias.DataSource = colonias.ConsultaDT("uspConsultaColonias")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            CONTRATOLabel1.Text = CStr(Me.DataGridView1.SelectedCells(10).Value)
            NOMBRELabel1.Text = CStr(Me.DataGridView1.SelectedCells(1).Value) + " " + CStr(Me.DataGridView1.SelectedCells(2).Value) + " " + CStr(Me.DataGridView1.SelectedCells(3).Value) + " "
            CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
            NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(6).Value)
            COLONIALabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
            ContratoCompaniaLabel.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        If TxtSetUpBox.Text = "" And TxtTarjeta.Text = "" Then
            Exit Sub
        End If
        BUSCACLIENTES(5)
    End Sub

    Private Sub TxtSetUpBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtSetUpBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TxtSetUpBox.Text = "" And TxtTarjeta.Text = "" Then
                Exit Sub
            End If
            BUSCACLIENTES(5)
        End If
    End Sub

    Private Sub TxtTarjeta_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtTarjeta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TxtSetUpBox.Text = "" And TxtTarjeta.Text = "" Then
                Exit Sub
            End If
            BUSCACLIENTES(5)
        End If
    End Sub
End Class