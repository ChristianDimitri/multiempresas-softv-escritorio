﻿Public Class FrmSelCompania

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try

            GloIdCompania = ComboBoxCompanias.SelectedValue

        Catch ex As Exception

        End Try
    End Sub

    Private Sub FrmSelCompania_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Llena_companias()
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click
        If Me.ComboBoxCompanias.SelectedIndex <= 0 Then
            MsgBox("Selecciona una Compañía")
            Exit Sub
        End If
        bgMovCartera = True
        NombreCompania = ComboBoxCompanias.Text
        'If OpCompania = 0 Then
        '    FrmTipoClientes.Show()
        'End If
        'If OpCompania = 1 Then

        '    FrmImprimirComision.Show()
        'End If
        
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class