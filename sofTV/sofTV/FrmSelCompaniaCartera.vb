﻿Imports System.Data.SqlClient
Public Class FrmSelCompaniaCartera

    Private Sub FrmSelCompaniaCartera_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub

    Private Sub FrmSelCompaniaCartera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If varfrmselcompania = "movimientoscartera" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            Dim comando As New SqlCommand()
            comando.Connection = conexion
            comando.CommandText = " exec DameIdentificador"
            identificador = comando.ExecuteScalar()
            conexion.Close()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.Inserta("ProcedureSeleccionCompania")
        Else
            BaseII.limpiaParametros()
            If varfrmselcompania = "ResumenOrdenQueja" Or varfrmselcompania = "ReporteClientesDesconexion" Or varfrmselcompania = "CajasTotalizado" Or varfrmselcompania = "pendientesderealizar" Or varfrmselcompania = "hastacompaniareprecontratacion" Or varfrmselcompania = "hastacompaniamensualidades" Or varfrmselcompania = "normalrecordatorios" Or varfrmselcompania = "hastacompaniapruebaint" Or varfrmselcompania = "hastacompaniacumpleanos" Or varfrmselcompania = "hastacompaniaimprimircomision" Or varfrmselcompania = "hastacompaniaselsucursal" Or varfrmselcompania = "hastacompaniasresventas" Or varfrmselcompania = "normal" Or varfrmselcompania = "hastacolonias" Or varfrmselcompania = "hastacompania1" Or varfrmselcompania = "hastacompania2" Or varfrmselcompania = "hastacompaniaselvendedor" Or varfrmselcompania = "hastacompaniaselusuario" Or varfrmselcompania = "hastacompaniaselusuarioventas" Or varfrmselcompania = "hastacompaniasreportefolios" Or varfrmselcompania = "UbicacionCajas" Or varfrmselcompania = "CajasDetallado" Or varfrmselcompania = "ReporteCajas" Then
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, -1)
            Else
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
                Dim conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim comando As New SqlCommand()
                comando.Connection = conexion
                comando.CommandText = " exec DameIdentificador"
                identificador = comando.ExecuteScalar()
                conexion.Close()
            End If
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.Inserta("ProcedureSeleccionCompania")
        End If
        
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompania")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompania")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bgMovCartera = False
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count > 0 Then
            If varfrmselcompania.Equals("cartera") Then              
                If BndDesPagAde = True Then
                    BndDesPagAde = False
                    FrmSelFechaDesPagAde.Show()
                Else
                    'If IdSistema = "AG" Then
                    '    FrmSelPeriodoCartera.Show()
                    'Else
                    'execar = True
                    'End If
                    FrmSelPeriodo.Show()
                End If
            End If
            
            If varfrmselcompania = "opcion1varios" Then
                FrmSelServRep.Show()
            End If
            If varfrmselcompania = "normal" Then
                FrmSelColoniaJ.Show()
            ElseIf varfrmselcompania = "hastacolonias" Then
                FrmSelColoniaJ.Show()
            ElseIf varfrmselcompania = "hastacompania1" Then
                FrmSelUsuariosE.Show()
            ElseIf varfrmselcompania = "hastacompania2" Then
                FrmSelTrabajosE.Show()
            ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
                'FrmSelVendedor.Show()
                FrmSelVendeRecup.Show()
            ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
                FrmSelUsuario.Show()
            ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
                'FrmSelUsuariosVentas.Show()
                FrmSelVendeRecup.Show()
            ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
                FormReporteFolios.Show()
            ElseIf varfrmselcompania = "hastacompaniasresventas" Then
                FrmSelFechasPPE.Show()
            ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
                FrmSelSucursal.Show()
            ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
                FrmImprimirComision.Show()
            ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
                FrmRepCumpleanosDeLosClientes.Show()
            ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
                FrmRepPruebaInternet.Show()
            ElseIf varfrmselcompania = "normalrecordatorios" Then
                FrmSelColoniaJ.Show()
            ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
                FrmImprimirComision.Show()
            ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
                SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
                SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
                SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
                SoftvMod.VariablesGlobales.MiConexion = MiConexion
                SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
                SoftvMod.VariablesGlobales.IdCompania = identificador
                Dim frm As New SoftvMod.FrmRepRecontratacion
                frm.ShowDialog()
            ElseIf varfrmselcompania = "movimientoscartera" Then
                FrmSelCiudadJ.Show()
            ElseIf varfrmselcompania = "pendientesderealizar" Then
                FrmImprimirComision.Show()
            End If
            If varfrmselcompania = "UbicacionCajas" Then
                FrmImprimirComision.Show()
            End If
            If varfrmselcompania = "CajasDetallado" Then
                FrmReporteCajaDetallado.Show()
            End If
            If varfrmselcompania = "CajasTotalizado" Then
                FrmReporteCajasTotalizado.Show()
            End If
            If varfrmselcompania = "ReporteCajas" Then
                FrmReporteCajas.Show()
            End If
            If varfrmselcompania = "ResumenOrdenQueja" Then
                FrmFiltroReporteResumen.Show()
            End If
            If varfrmselcompania = "ReporteClientesDesconexion" Then
                FrmSelOpRepDescoClien.Show()
            End If
            Me.Close()
        End If
    End Sub
End Class