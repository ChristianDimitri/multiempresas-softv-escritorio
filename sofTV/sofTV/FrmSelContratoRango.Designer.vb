<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelContratoRango
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_TipSerLabel As System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.CMBPanel1 = New System.Windows.Forms.Panel
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.DataSetLidia = New sofTV.DataSetLidia
        Me.DimeTipSer_CualEsPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DimeTipSer_CualEsPrincipalTableAdapter = New sofTV.DataSetLidiaTableAdapters.DimeTipSer_CualEsPrincipalTableAdapter
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox
        Clv_TipSerLabel = New System.Windows.Forms.Label
        Me.CMBPanel1.SuspendLayout()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimeTipSer_CualEsPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_TipSerLabel
        '
        Clv_TipSerLabel.AutoSize = True
        Clv_TipSerLabel.Location = New System.Drawing.Point(17, 302)
        Clv_TipSerLabel.Name = "Clv_TipSerLabel"
        Clv_TipSerLabel.Size = New System.Drawing.Size(62, 13)
        Clv_TipSerLabel.TabIndex = 15
        Clv_TipSerLabel.Text = "Clv Tip Ser:"
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(161, 89)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(78, 21)
        Me.TextBox2.TabIndex = 8
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(161, 42)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(78, 21)
        Me.TextBox1.TabIndex = 6
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(33, 182)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(193, 182)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "&CERRAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(15, 42)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(136, 20)
        Me.CMBLabel1.TabIndex = 12
        Me.CMBLabel1.Text = "Contrato Inicial:"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(15, 90)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(128, 20)
        Me.CMBLabel2.TabIndex = 13
        Me.CMBLabel2.Text = "Contrato Final:"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.Controls.Add(Me.Button2)
        Me.CMBPanel1.Controls.Add(Me.Button3)
        Me.CMBPanel1.Controls.Add(Me.CMBLabel2)
        Me.CMBPanel1.Controls.Add(Me.CMBLabel1)
        Me.CMBPanel1.Controls.Add(Me.TextBox2)
        Me.CMBPanel1.Controls.Add(Me.TextBox1)
        Me.CMBPanel1.Location = New System.Drawing.Point(14, 12)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(334, 144)
        Me.CMBPanel1.TabIndex = 14
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(245, 88)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(30, 22)
        Me.Button2.TabIndex = 15
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(245, 41)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(30, 22)
        Me.Button3.TabIndex = 14
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = False
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DimeTipSer_CualEsPrincipalBindingSource
        '
        Me.DimeTipSer_CualEsPrincipalBindingSource.DataMember = "DimeTipSer_CualEsPrincipal"
        Me.DimeTipSer_CualEsPrincipalBindingSource.DataSource = Me.DataSetLidia
        '
        'DimeTipSer_CualEsPrincipalTableAdapter
        '
        Me.DimeTipSer_CualEsPrincipalTableAdapter.ClearBeforeFill = True
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DimeTipSer_CualEsPrincipalBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(85, 299)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_TipSerTextBox.TabIndex = 16
        '
        'FrmSelContratoRango
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 235)
        Me.Controls.Add(Clv_TipSerLabel)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.Name = "FrmSelContratoRango"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección Rango de Contratos"
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimeTipSer_CualEsPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents DimeTipSer_CualEsPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimeTipSer_CualEsPrincipalTableAdapter As sofTV.DataSetLidiaTableAdapters.DimeTipSer_CualEsPrincipalTableAdapter
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
End Class
