﻿Public Class FrmSelMesyAnio

    Private Sub FrmSelMesyAnio_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If eOpVentas = 139 Then
            CheckBox1.Visible = True
        End If

        colorea(Me, Me.Name)
        llenaMes()
    End Sub

    Private Sub llenaMes()
        Dim Table1 As DataTable
        Table1 = New DataTable("TableName")

        Dim column1 As DataColumn = New DataColumn("Clv_Mes")
        column1.DataType = System.Type.GetType("System.Int32")
        Dim column2 As DataColumn = New DataColumn("Nombre")
        column2.DataType = System.Type.GetType("System.String")

        Table1.Columns.Add(column1)
        Table1.Columns.Add(column2)

   

        Table1.Rows.Add(1, "Enero")
        Table1.Rows.Add(2, "Febrero")
        Table1.Rows.Add(3, "Marzo")
        Table1.Rows.Add(4, "Abril")
        Table1.Rows.Add(5, "Mayo")
        Table1.Rows.Add(6, "Junio")
        Table1.Rows.Add(7, "Julio")
        Table1.Rows.Add(8, "Agosto")
        Table1.Rows.Add(9, "Septiembre")
        Table1.Rows.Add(10, "Octubre")
        Table1.Rows.Add(11, "Noviembre")
        Table1.Rows.Add(12, "Diciembre")
        ComboBox1.ValueMember = "Clv_Mes"
        ComboBox1.DisplayMember = "Nombre"

        ComboBox1.DataSource = Table1
        ComboBox1.SelectedValue = 1
       
    End Sub
 
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not IsNumeric(ComboBox2.Text) Then
            MsgBox("Selecciona el año")
            Exit Sub
        End If
        If Not IsNumeric(ComboBox1.SelectedValue) Then
            MsgBox("Selecciona el mes")
            Exit Sub
        End If

        If eOpVentas <> 138 And eOpVentas <> 139 Then

            eOpVentas = 129

            If RepReporteComision = 1 And TRepReporteComision = 1 Then
                eOpVentas = 130
            ElseIf RepReporteComision = 2 And TRepReporteComision = 1 Then
                eOpVentas = 131
            ElseIf RepReporteComision = 3 And TRepReporteComision = 1 Then
                eOpVentas = 132
            ElseIf RepReporteComision = 4 And TRepReporteComision = 1 Then
                eOpVentas = 133
            ElseIf RepReporteComision = 1 And TRepReporteComision = 2 Then
                eOpVentas = 134
            ElseIf RepReporteComision = 2 And TRepReporteComision = 2 Then
                eOpVentas = 135
            ElseIf RepReporteComision = 3 And TRepReporteComision = 2 Then
                eOpVentas = 136
            ElseIf RepReporteComision = 4 And TRepReporteComision = 2 Then
                eOpVentas = 137
            End If
        End If

        If eOpVentas = 139 And CheckBox1.Checked = True Then
            eOpVentas = 140
        End If

        AnioReporteComision = ComboBox2.Text
        MesReporteComision = ComboBox1.SelectedValue
        FrmImprimirComision.Show()
        Me.Close()
    End Sub
End Class