﻿Imports System.Data.SqlClient
Public Class FrmSelServicios

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Try
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                GloGuardarNet = False
                If GloClv_TipSer <> 5 Then
                    GloClv_Servicio = ComboBox1.SelectedValue
                    locGloClv_servicioPromocion = GloClv_Servicio
                Else
                    GloClv_servTel = Me.ComboBox1.SelectedValue
                    locGloClv_servicioPromocion = GloClv_servTel
                End If

                'If UCase(Pantalla) = UCase("FrmClientes") Then
                '    If GloClv_Servicio > 0 Then
                '        Me.Validate()
                '        Me.ConsultaclientesnetTableAdapter1.Insert(Contrato, "C", 0, 0, False, False, 0, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 0, "", False, LoContratonet)
                '        Me.ConsultaclientesnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
                '        Me.ConsultacontnetTableAdapter1.Insert(LoContratonet, GloClv_Servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", LoClv_Unicanet)
                '        Me.ConsultacontnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
                '        FrmClientes.creaarbol()
                '        FrmClientes.Panel5.Visible = False
                '        FrmClientes.Panel6.Visible = True
                '        FrmClientes.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
                '        FrmClientes.CREAARBOL()

                '        GloClv_Servicio = 0
                '    End If
                If GloClv_TipSer = 2 Then
                    If Equip_tel = True Then
                        numtel = True
                    End If
                    GloBndSer = True
                ElseIf GloClv_TipSer = 5 Then
                    globndTel = True
                End If
                Me.Close()
                frmctr.Activar()
            Else
                MsgBox("No a Seleccionado un Servicio", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_Servicio = 0
        GloClv_servTel = 0
        Me.Close()
    End Sub

    Private Sub FrmSelServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        locGloClv_servicioPromocion = 0
        frmctr.MdiParent = FrmClientes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'Me.MuestraServiciosTableAdapter.Connection = CON
            'Me.MuestraServiciosTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Long)))
            MuestraServiciosBase2(GloClv_TipSer, GloIdCompania)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MuestraServiciosBase2(ByVal tipser As Integer, ByVal idcompania As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, tipser)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, idcompania)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        ComboBox1.DataSource = BaseII.ConsultaDT("MuestraServiciosBase2")
    End Sub


End Class