Imports System.Data.SqlClient
Public Class FrmSelSucursal


    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        If NombreListBox1.Items.Count > 0 Then
            If eOpVentas = 21 Or eOpVentas = 23 Then
                FrmSelServicioE.Show()
                Me.Close()
            End If
        End If
        
    End Sub

    Private Sub FrmSelSucursal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        Me.Dame_clv_session_ReportesTableAdapter.Connection = CON
        Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEric2.Dame_clv_session_Reportes, eClv_Session)
        Me.ConSucursalesProTableAdapter.Connection = CON
        Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 0)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.NombreListBox.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.InsertarSucursalesTmpTableAdapter.Connection = CON
            Me.InsertarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.InsertarSucursalesTmp, CInt(Me.NombreListBox.SelectedValue), eClv_Session, 0)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.NombreListBox.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.InsertarSucursalesTmpTableAdapter.Connection = CON
            Me.InsertarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.InsertarSucursalesTmp, 0, eClv_Session, 1)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.NombreListBox1.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorrarSucursalesTmpTableAdapter.Connection = CON
            Me.BorrarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.BorrarSucursalesTmp, CInt(Me.NombreListBox1.SelectedValue), eClv_Session, 0)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.NombreListBox1.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorrarSucursalesTmpTableAdapter.Connection = CON
            Me.BorrarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.BorrarSucursalesTmp, 0, eClv_Session, 1)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            CON.Close()
        End If
    End Sub
End Class