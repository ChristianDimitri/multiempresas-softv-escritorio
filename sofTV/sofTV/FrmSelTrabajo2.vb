Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelTrabajo2
    Dim ban As Boolean = False
    Dim bndreporteTec As Boolean = True
    Private Sub FrmSelTrabajo2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim con As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        MuestraTipOrdQuejaPrincipal()
        MuestraTipSerPrincipal()
        MuestraSelecciona_QuejaSerTmpNuevo(LocClv_session, 0, 0)
        'MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)

    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        'MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub
    Private Sub MuestraTipOrdQuejaPrincipal()
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraTipOrdQuejaPrincipal ")


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ComboBox1.DataSource = binding


        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try

    End Sub

    Private Sub MuestraTipSerPrincipal()
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraTipSerPrincipal ")


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ComboBox4.DataSource = binding

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try

    End Sub
    Private Sub MuestraSelecciona_QuejaSerTmpNuevo(ByVal Clv_session As Long, ByVal clv_tipserv As Integer, ByVal tipo As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraSelecciona_QuejaSerTmpNuevo ")
        str.Append(CStr(Clv_session) & ", ")
        str.Append(CStr(clv_tipserv) & ", ")
        str.Append(CStr(tipo))


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.TextBox1.Text = CStr(Me.ComboBox1.SelectedValue)
            Me.TextBox2.Text = CStr(Me.ComboBox4.SelectedValue)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub
    Private Sub MuestraSelecciona_Trabajo_tmpConsulta(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraSelecciona_Trabajo_tmpConsulta ")
        str.Append(CStr(clv_session))
        


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ListBox1.DataSource = binding
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub
    Private Sub Borra_Seleccion_trabajo(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Borra_Seleccion_trabajo ")
        str.Append(CStr(clv_session))



        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub
    Private Sub Insertauno_Seleccion_Trabajo(ByVal clv_session As Long, ByVal clv_trabajo As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Insertauno_Seleccion_Trabajo ")
        str.Append(CStr(clv_session) & ", ")
        str.Append(CStr(clv_trabajo))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub
    Private Sub Insertauno_Seleccion_Trabajo_tmp(ByVal clv_session As Long, ByVal clv_trabajo As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Insertauno_Seleccion_Trabajo_tmp ")
        str.Append(CStr(clv_session) & ", ")
        str.Append(CStr(clv_trabajo))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub
    Private Sub MuestraSelecciona_TrabajoConsulta(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraSelecciona_TrabajoConsulta ")
        str.Append(CStr(clv_session))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ListBox2.DataSource = binding
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try


    End Sub
    Private Sub InsertaTOSeleccion_trabajo(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec InsertaTOSeleccion_trabajo ")
        str.Append(CStr(clv_session))


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub
    Private Sub InsertaTOSeleccion_trabajo_tmp(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec InsertaTOSeleccion_trabajo_tmp ")
        str.Append(CStr(clv_session))


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        'If bndreporteTec = True Then
        Borra_Seleccion_trabajo(LocClv_session)
        'End If
        MuestraSelecciona_QuejaSerTmpNuevo(LocClv_session, Me.ComboBox4.SelectedValue, Me.ComboBox1.SelectedValue)
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then  'And Me.ComboBox1.SelectedValue <> 2 Then
            MuestraSelecciona_TrabajoConsulta(LocClv_session)
            MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)


            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            ListBox4.DataSource = BaseII.ConsultaDT("MuestraSelecciona_ClasificacionConsulta")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            ListBox3.DataSource = BaseII.ConsultaDT("MuestraSelecciona_Clasificacion_tmpConsulta")

        End If

    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            If bndreporteTec = True Then
                Borra_Seleccion_trabajo(LocClv_session)
            End If
            MuestraSelecciona_QuejaSerTmpNuevo(LocClv_session, Me.ComboBox4.SelectedValue, Me.ComboBox1.SelectedValue)
            MuestraSelecciona_TrabajoConsulta(LocClv_session)
            MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)

        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Insertauno_Seleccion_Trabajo(LocClv_session, CLng(Me.ListBox1.SelectedValue))
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
      
    End Sub

   
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Insertauno_Seleccion_Trabajo_tmp(LocClv_session, CLng(Me.ListBox2.SelectedValue))
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        InsertaTOSeleccion_trabajo(LocClv_session)
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        InsertaTOSeleccion_trabajo_tmp(LocClv_session)
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim cont As Integer = 0
        GloClv_TipSer = ComboBox4.SelectedValue
        cont = Me.ListBox2.Items.Count
        If cont = 0 And ListBox4.Items.Count = 0 Then
            MsgBox("Seleccione Al Menos un Trabajo o Clasificación de Problema", MsgBoxStyle.Information)
        Else
            FrmSelFechas2.Show()
            bndreporteTec = False
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub llenaClasificacion()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        ListBox4.DataSource = BaseII.ConsultaDT("MuestraSelecciona_ClasificacionConsulta")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        ListBox3.DataSource = BaseII.ConsultaDT("MuestraSelecciona_Clasificacion_tmpConsulta")
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If ListBox3.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@clvProblema", SqlDbType.Int, ListBox3.SelectedValue)
        BaseII.Inserta("InsertaUno_Seleccion_Clasificacion")
        llenaClasificacion()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If ListBox3.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.Inserta("InsertaToSeleccion_clasificacion")
        llenaClasificacion()
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If ListBox4.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@clvProblema", SqlDbType.Int, ListBox4.SelectedValue)
        BaseII.Inserta("Insertauno_Seleccion_Clasificacion_tmp")
        llenaClasificacion()
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        If ListBox4.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.Inserta("InsertaTOSeleccion_clasificacion_tmp")
        llenaClasificacion()
    End Sub
End Class