Imports System.Data.SqlClient
Public Class FrmSelUsuariosVentas

    Private Sub FrmSelUsuariosVentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If varfrmselcompania <> "hastacompaniaselusuarioventas" Then
            Me.Dame_clv_session_ReportesTableAdapter.Connection = CON
            Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEric2.Dame_clv_session_Reportes, eClv_Session)
        End If
        Me.ConSeleccionaUsuariosProTableAdapter.Connection = CON
        Me.ConSeleccionaUsuariosProTableAdapter.Fill(Me.DataSetEric2.ConSeleccionaUsuariosPro, eClv_Session, 0)
        CON.Close()
    End Sub

    Private Sub RefrescarPro()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSeleccionaUsuariosProTableAdapter.Connection = CON
        Me.ConSeleccionaUsuariosProTableAdapter.Fill(Me.DataSetEric2.ConSeleccionaUsuariosPro, eClv_Session, 1)
        CON.Close()
    End Sub

    Private Sub RefrescarTmp()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSeleccionaUsuariosTmpTableAdapter.Connection = CON
        Me.ConSeleccionaUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.ConSeleccionaUsuariosTmp, eClv_Session)
        CON.Close()
    End Sub

    Private Sub InsertarUno()
        If Me.NombreListBox1.Items.Count = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertarSeleccionaUsuariosTmpTableAdapter.Connection = CON
        Me.InsertarSeleccionaUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarSeleccionaUsuariosTmp, CInt(Me.Clv_GrupoTextBox1.Text), CInt(Me.ClaveTextBox1.Text), eClv_Session, 0)
        CON.Close()
    End Sub

    Private Sub InsertarTodos()
        If Me.NombreListBox1.Items.Count = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertarSeleccionaUsuariosTmpTableAdapter.Connection = CON
        Me.InsertarSeleccionaUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarSeleccionaUsuariosTmp, 0, 0, eClv_Session, 1)
        CON.Close()
    End Sub

    Private Sub BorrarUno()
        If Me.NombreListBox.Items.Count = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BorrarSeleccionaUsuariosTmpTableAdapter.Connection = CON
        Me.BorrarSeleccionaUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarSeleccionaUsuariosTmp, CInt(Me.Clv_GrupoTextBox.Text), CInt(Me.ClaveTextBox.Text), eClv_Session, 0)
        CON.Close()
    End Sub

    Private Sub BorrarTodos()
        If Me.NombreListBox.Items.Count = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BorrarSeleccionaUsuariosTmpTableAdapter.Connection = CON
        Me.BorrarSeleccionaUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarSeleccionaUsuariosTmp, 0, 0, eClv_Session, 1)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        InsertarUno()
        RefrescarPro()
        RefrescarTmp()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        InsertarTodos()
        RefrescarPro()
        RefrescarTmp()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        BorrarUno()
        RefrescarPro()
        RefrescarTmp()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        BorrarTodos()
        RefrescarPro()
        RefrescarTmp()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.NombreListBox.Items.Count > 0 Then
            If eOpVentas = 58 Then
                FrmImprimirComision.Show()
            Else
                FrmSelServicioE.Show()
            End If
            Me.Close()
        End If
    End Sub
End Class