﻿Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelVendeRecup

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        rSelvendedor = CheckBox1.Checked
        rSelrecuperador = CheckBox2.Checked
        If rSelvendedor = True Or rSelrecuperador = True Then
            If varfrmselcompania = "hastacompaniaselvendedor" Then
                insertaSelvenderecupera()
                FrmSelVendedor.Show()
            ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
                insertaSelvenderecupera()
                FrmSelUsuariosVentas.Show()
            ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
                insertaSelvenderecupera()
                FrmSelUsuario.Show()
            End If
            Me.Close()
        Else
            MsgBox("selecciona un tipo de Vendedor")
            Exit Sub
        End If
    End Sub

    Private Sub insertaSelvenderecupera()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@Vendedor", SqlDbType.Bit, rSelvendedor)
        BaseII.CreateMyParameter("@Recuperador", SqlDbType.Bit, rSelrecuperador)
        BaseII.Inserta("insertaSelvendeRecupera")
    End Sub

    Private Sub FrmSelVendeRecup_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        DameClvSession()
    End Sub

    Private Sub DameClvSession()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = CLng(parametro.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
End Class