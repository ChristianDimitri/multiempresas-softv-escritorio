﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSeleccionaIP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label1 As System.Windows.Forms.Label
        Me.bAceptar = New System.Windows.Forms.Button()
        Me.cbIP = New System.Windows.Forms.ComboBox()
        Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.Transparent
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.OrangeRed
        Label1.Location = New System.Drawing.Point(21, 35)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(194, 18)
        Label1.TabIndex = 52
        Label1.Text = "Seleccionar Dirección IP"
        '
        'bAceptar
        '
        Me.bAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.bAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bAceptar.ForeColor = System.Drawing.Color.Black
        Me.bAceptar.Location = New System.Drawing.Point(257, 88)
        Me.bAceptar.Name = "bAceptar"
        Me.bAceptar.Size = New System.Drawing.Size(136, 33)
        Me.bAceptar.TabIndex = 59
        Me.bAceptar.Text = "&ACEPTAR"
        Me.bAceptar.UseVisualStyleBackColor = False
        '
        'cbIP
        '
        Me.cbIP.DisplayMember = "IP"
        Me.cbIP.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbIP.FormattingEnabled = True
        Me.cbIP.Location = New System.Drawing.Point(21, 58)
        Me.cbIP.Name = "cbIP"
        Me.cbIP.Size = New System.Drawing.Size(372, 23)
        Me.cbIP.TabIndex = 53
        Me.cbIP.ValueMember = "IdIP"
        '
        'FrmSeleccionaIP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 146)
        Me.Controls.Add(Me.bAceptar)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.cbIP)
        Me.Name = "FrmSeleccionaIP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona IP"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bAceptar As System.Windows.Forms.Button
    Friend WithEvents cbIP As System.Windows.Forms.ComboBox
End Class
