﻿Public Class FrmSeleccionaIP

    Private Sub FrmSeleccionaIP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        cbIP.DataSource = BaseII.ConsultaDT("GetIPsDisponibles")

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub bAceptar_Click(sender As Object, e As EventArgs) Handles bAceptar.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ContratoNet", SqlDbType.Int, GloContratonet)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@IdIP", SqlDbType.Int, cbIP.SelectedValue)
        BaseII.Inserta("AsignarIPManual")

        frmInternet2.lDireccionIP.Text = cbIP.Text
        If frmInternet2.lDireccionIP.Text <> "" Then
            frmInternet2.bAsignarIP.Visible = False
        End If


        Me.Close()
    End Sub
End Class