﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSeriesFolio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Clv_VendedorLabel As System.Windows.Forms.Label
        Me.CMBSerie = New System.Windows.Forms.Label()
        Me.Cmb_serie = New System.Windows.Forms.ComboBox()
        Me.CMBfolio = New System.Windows.Forms.Label()
        Me.Txt_Serie = New System.Windows.Forms.TextBox()
        Me.Btn_Imprimir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Btn_Salir = New System.Windows.Forms.Button()
        Me.lbTipo = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Clv_VendedorLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Clv_VendedorLabel
        '
        Clv_VendedorLabel.AutoSize = True
        Clv_VendedorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Clv_VendedorLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Clv_VendedorLabel.Location = New System.Drawing.Point(2, 23)
        Clv_VendedorLabel.Name = "Clv_VendedorLabel"
        Clv_VendedorLabel.Size = New System.Drawing.Size(76, 15)
        Clv_VendedorLabel.TabIndex = 17
        Clv_VendedorLabel.Text = "Vendedor :"
        '
        'CMBSerie
        '
        Me.CMBSerie.AutoSize = True
        Me.CMBSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSerie.Location = New System.Drawing.Point(29, 47)
        Me.CMBSerie.Name = "CMBSerie"
        Me.CMBSerie.Size = New System.Drawing.Size(45, 15)
        Me.CMBSerie.TabIndex = 0
        Me.CMBSerie.Text = "Serie:"
        '
        'Cmb_serie
        '
        Me.Cmb_serie.FormattingEnabled = True
        Me.Cmb_serie.Location = New System.Drawing.Point(80, 47)
        Me.Cmb_serie.Name = "Cmb_serie"
        Me.Cmb_serie.Size = New System.Drawing.Size(269, 21)
        Me.Cmb_serie.TabIndex = 2
        '
        'CMBfolio
        '
        Me.CMBfolio.AutoSize = True
        Me.CMBfolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBfolio.Location = New System.Drawing.Point(56, 84)
        Me.CMBfolio.Name = "CMBfolio"
        Me.CMBfolio.Size = New System.Drawing.Size(222, 15)
        Me.CMBfolio.TabIndex = 2
        Me.CMBfolio.Text = "¿Cuantos folios deseas imprimir?"
        '
        'Txt_Serie
        '
        Me.Txt_Serie.Location = New System.Drawing.Point(90, 132)
        Me.Txt_Serie.Name = "Txt_Serie"
        Me.Txt_Serie.Size = New System.Drawing.Size(98, 20)
        Me.Txt_Serie.TabIndex = 3
        '
        'Btn_Imprimir
        '
        Me.Btn_Imprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Imprimir.Location = New System.Drawing.Point(126, 217)
        Me.Btn_Imprimir.Name = "Btn_Imprimir"
        Me.Btn_Imprimir.Size = New System.Drawing.Size(107, 42)
        Me.Btn_Imprimir.TabIndex = 7
        Me.Btn_Imprimir.Text = "&Imprimir"
        Me.Btn_Imprimir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Btn_Salir
        '
        Me.Btn_Salir.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Salir.Location = New System.Drawing.Point(242, 217)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(107, 42)
        Me.Btn_Salir.TabIndex = 8
        Me.Btn_Salir.Text = "&Salir"
        Me.Btn_Salir.UseVisualStyleBackColor = True
        '
        'lbTipo
        '
        Me.lbTipo.AutoSize = True
        Me.lbTipo.Location = New System.Drawing.Point(228, 71)
        Me.lbTipo.Name = "lbTipo"
        Me.lbTipo.Size = New System.Drawing.Size(0, 13)
        Me.lbTipo.TabIndex = 6
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(207, 132)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(97, 20)
        Me.TextBox1.TabIndex = 4
        Me.TextBox1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(87, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 15)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Ventas:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(204, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Recuperación:"
        Me.Label2.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(90, 173)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(46, 20)
        Me.TextBox2.TabIndex = 5
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(142, 173)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(46, 20)
        Me.TextBox3.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(87, 155)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 15)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Tv:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(139, 155)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 15)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Int:"
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(80, 20)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(269, 21)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "Clv_Vendedor"
        '
        'FrmSeriesFolio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 272)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Clv_VendedorLabel)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.lbTipo)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Controls.Add(Me.Btn_Imprimir)
        Me.Controls.Add(Me.Txt_Serie)
        Me.Controls.Add(Me.CMBfolio)
        Me.Controls.Add(Me.Cmb_serie)
        Me.Controls.Add(Me.CMBSerie)
        Me.MaximizeBox = False
        Me.Name = "FrmSeriesFolio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Series  Folio"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBSerie As System.Windows.Forms.Label
    Friend WithEvents Cmb_serie As System.Windows.Forms.ComboBox
    Friend WithEvents CMBfolio As System.Windows.Forms.Label
    Friend WithEvents Txt_Serie As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Imprimir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Btn_Salir As System.Windows.Forms.Button
    Friend WithEvents lbTipo As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
End Class
