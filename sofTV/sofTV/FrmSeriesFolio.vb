﻿Imports System.Data
Imports System.Data.SqlClient
Imports sofTV.Base

Public Class FrmSeriesFolio

    Public serie As String, folios As Integer, foliosRecu As Integer, foliosTv As Integer, foliosInt As Integer
    Dim consulta As New CBase

    Private Sub FrmSeriesFolio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LlenaVendedor()
        'LlenaCombo()
    End Sub

    Private Sub LlenaVendedor()

        BaseII.limpiaParametros()
        Dim dt As DataTable = BaseII.ConsultaDT("SP_Vendedores")

        If dt.Rows.Count > 0 Then
            ComboBox1.DataSource = dt
            ComboBox1.DisplayMember = "Nombre"
            ComboBox1.ValueMember = "Clv_Vendedor"
        End If

    End Sub

    Private Sub LlenaCombo()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, ComboBox1.SelectedValue)
        Dim dt As DataTable = BaseII.ConsultaDT("SP_SerieFolio")



        If dt.Rows.Count > 0 Then
            Cmb_serie.DataSource = dt
            Cmb_serie.DisplayMember = "Serie"
            Cmb_serie.ValueMember = "Clave"
        End If



    End Sub

    Private Sub InsertarSerie(ByVal vserie As String, ByVal vfolios As Integer, ByVal vfoliosRecu As Integer)
        Try
            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim cmm As New SqlCommand()
                cmm.Connection = conexion
                cmm.CommandText = "SP_InsertarSerieFolio"
                cmm.CommandType = CommandType.StoredProcedure

                cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
                cmm.Parameters.Add("@folio", SqlDbType.Int)

                cmm.Parameters("@serie").Value = vserie
                cmm.Parameters("@folio").Value = (vfolios + vfoliosRecu)

                cmm.ExecuteNonQuery()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub InsertFolio(ByVal series As String, ByVal folios As Integer, ByVal foliosRecu As Integer, ByVal foliosint As Integer)
        Try
            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim cmm As New SqlCommand()
                cmm.Connection = conexion
                cmm.CommandText = "SpInsertNewTbl_Folios"
                cmm.CommandType = CommandType.StoredProcedure

                cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
                cmm.Parameters.Add("@folio", SqlDbType.Int)
                cmm.Parameters.Add("@folioRecu", SqlDbType.Int)
                cmm.Parameters.Add("@folioInt", SqlDbType.Int)

                cmm.Parameters("@serie").Value = series
                cmm.Parameters("@folio").Value = folios
                cmm.Parameters("@folioRecu").Value = foliosRecu
                cmm.Parameters("@folioInt").Value = foliosint

                cmm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            MsgBox(ex)
        End Try
    End Sub

    Private Sub Btn_Imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Imprimir.Click
        If Cmb_serie.SelectedValue <> 0 And (IsNumeric(Txt_Serie.Text) Or IsNumeric(TextBox1.Text)) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, Cmb_serie.Text)
            BaseII.CreateMyParameter("@res", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("ValidaFoliosImprimir")
            Dim aux As Integer = 0
            Dim aux2 As Integer = 10
            Dim res As Integer = BaseII.dicoPar("@res")
            If TipoSerie = 1 Then 'Cobro, maximo 15
                If res >= 15 Or CInt(Txt_Serie.Text) > 15 Then
                    MsgBox("No se pueden tener más de 15 folios impresos para cobro.")
                    Exit Sub
                End If
                If (CInt(Txt_Serie.Text) + res) > 15 Then
                    MsgBox("Solo puede tener 15 folios disponibles para cobro. Se imprimirán " + (15 - res).ToString)
                    Txt_Serie.Text = (15 - res).ToString
                End If
            End If
            If TipoSerie = 2 Then 'Venta, maximo 7
                If res >= 7 Or CInt(Txt_Serie.Text) > 7 Then
                    MsgBox("No se pueden tener más de 7 folios impresos para venta.")
                    Exit Sub
                End If
                aux = CInt(Txt_Serie.Text) + res
                If (aux) > 7 Then
                    MsgBox("Solo puede tener 7 folios disponibles para venta. Se imprimirán " + (7 - res).ToString)
                    Txt_Serie.Text = (7 - res).ToString
                End If
            End If
            serie = Cmb_serie.GetItemText(Cmb_serie.SelectedItem).ToString()
            'folios = Convert.ToInt32(Txt_Serie.Text.ToString())
            If IsNumeric(Txt_Serie.Text) = False Then
                folios = 0
            Else
                folios = Convert.ToInt32(Txt_Serie.Text.ToString())
            End If
            If IsNumeric(TextBox1.Text) = False Then
                foliosRecu = 0
            Else
                foliosRecu = Convert.ToInt32(TextBox1.Text.ToString())
            End If
            If IsNumeric(TextBox2.Text) = False Then
                foliosTv = 0
            Else
                foliosTv = Convert.ToInt32(TextBox2.Text.ToString())
            End If
            If IsNumeric(TextBox3.Text) = False Then
                foliosInt = 0
            Else
                foliosInt = Convert.ToInt32(TextBox3.Text.ToString())
            End If

            If (foliosTv > 0 Or foliosInt > 0) And folios = 0 Then
                MsgBox("Los folios de Tv e Internet son unicamente para ventas, capture cantidad de folios en el apartado Ventas", MsgBoxStyle.Information)
                Exit Sub
            ElseIf ((foliosTv > 0 Or foliosInt > 0) And folios > 0) Then
                If (foliosTv + foliosInt) <> folios Then
                    MsgBox("La suma de folios de Tv e Internet debe ser igual a los folios de Ventas", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If



            InsertFolio(serie, foliosTv, foliosRecu, foliosInt)

            InsertarSerie(serie, folios, foliosRecu)

            GloSeries = serie
            GlofoliosTv = foliosTv
            GlofoliosInt = foliosInt

            'REPORTEPreContrato(0, serie)
            If TipoSerie = 1 Then
                LocGloOpRep = 20
            Else

                If folios > 0 Then
                    'LocGloOpRep = 22
                    'FrmImprimirFac.ShowDialog()
                    If foliosTv > 0 Then
                        LocGloOpRep = 27
                        FrmImprimirFac.ShowDialog()
                    End If
                    If foliosInt > 0 Then
                        LocGloOpRep = 28
                        FrmImprimirFac.ShowDialog()
                    End If
                End If

                If foliosRecu > 0 Then
                    LocGloOpRep = 25
                    FrmImprimirFac.ShowDialog()
                End If
            End If
        Else
            MsgBox("Verifique que la cantidad de folios sea numérico o seleccione una serie", MsgBoxStyle.Information)
        End If

        LocGloOpRep = 0
        Me.Close()
    End Sub

    Private Sub Btn_Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub REPORTEPreContrato(ByVal OP As Integer, ByVal SERIE As String)
        Dim dTable As New DataTable
        Dim rDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, SERIE, 50)
        dTable = BaseII.ConsultaDT("REPORTEPreContrato")

        rDocument = New CrystalDecisions.CrystalReports.Engine.ReportDocument
        rDocument.Load(RutaReportes + "\REPORTEPreContrato.rpt")
        rDocument.SetDataSource(dTable)

        If dTable.Rows.Count = 0 Then Exit Sub

        MessageBox.Show("Se imprimirá la parte frontal de " + dTable.Rows.Count.ToString + " precontratos.", "Atención", MessageBoxButtons.OK)

        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS
        rDocument.PrintToPrinter(1, False, 0, 0)

        MessageBox.Show("Se imprimirá la parte posterior de " + dTable.Rows.Count.ToString + " precontratos.", "Atención", MessageBoxButtons.OK)

        rDocument = New CrystalDecisions.CrystalReports.Engine.ReportDocument
        rDocument.Load(RutaReportes + "\REPORTEPreContratoPosterior.rpt")
        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS

        For Each e As DataRow In dTable.Rows
            rDocument.PrintToPrinter(1, False, 0, 0)
        Next


    End Sub
    Dim TipoSerie As Integer
    Private Sub Cmb_serie_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cmb_serie.SelectedIndexChanged
        Try
            '        BaseII.limpiaParametros()
            '        BaseII.CreateMyParameter("@clave", SqlDbType.Int, Cmb_serie.SelectedValue)
            '        BaseII.CreateMyParameter("@tipo", ParameterDirection.Output, SqlDbType.Int)
            '        BaseII.ProcedimientoOutPut("DameTipoSerie")
            '        Dim tipo As Integer = BaseII.dicoPar("@tipo")
            '        If tipo = 1 Then
            '            lbTipo.Visible = True
            '            lbTipo.Text = "*Serie de cobro"
            '            TipoSerie = 1
            '        ElseIf tipo = 2 Then
            '            lbTipo.Visible = True
            '            lbTipo.Text = "*Serie de venta"
            '            TipoSerie = 2
            '        Else
            '            lbTipo.Visible = False
            '        End If


            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clave", SqlDbType.Int, Cmb_serie.SelectedValue)
            BaseII.CreateMyParameter("@Recuperador", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("DimesiesRecuperador")
            esRecuperador = BaseII.dicoPar("@Recuperador")
            If esRecuperador = True Then
                Label2.Visible = True
                TextBox1.Visible = True
            Else
                Label2.Visible = False
                TextBox1.Visible = False
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        LlenaCombo()
    End Sub
End Class