Imports System.Net
Imports System.IO
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmSms
    Dim cont As Integer
    Private req As WebRequest = Nothing
    Private rsp As WebResponse = Nothing
    Private cuentaMensajes As Integer = 0
    Private contratado As Integer = 0
    Private suspendido As Integer = 0
    Private cancelado As Integer = 0
    Private Temporales As Integer = 0
    Private instalado As Integer = 0
    Private desconectado As Integer = 0
    Private fueraArea As Integer = 0
    Private periodo1 As Integer = 0
    Private periodo2 As Integer = 0


    Private Sub FrmSms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'ProcedimientosArnoldo2.Muestra_MotCanc_Reporte' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_MotCanc_ReporteTableAdapter.Connection = CON
        Me.Muestra_MotCanc_ReporteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_MotCanc_Reporte)
        CON.Close()

        ProgressBar1.Minimum = 0
        ProgressBar1.Value = 0
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

        GroupBoxUltPago.Enabled = False
        CheckBoxPorPagar.Checked = False

        GroupBoxCancelados.Enabled = False

        Dim table As New DataTable
        table.Columns.Add("MesInt", GetType(Integer))
        table.Columns.Add("MesVarchar", GetType(String))
        table.Rows.Add(1, "Enero")
        table.Rows.Add(2, "Febrero")
        table.Rows.Add(3, "Marzo")
        table.Rows.Add(4, "Abril")
        table.Rows.Add(5, "Mayo")
        table.Rows.Add(6, "Junio")
        table.Rows.Add(7, "Julio")
        table.Rows.Add(8, "Agosto")
        table.Rows.Add(9, "Septiembre")
        table.Rows.Add(10, "Octubre")
        table.Rows.Add(11, "Noviembre")
        table.Rows.Add(12, "Diciembre")

        cbUltimoMes.DataSource = table
        cbUltimoMes.DisplayMember = "MesVarchar"
        cbUltimoMes.ValueMember = "MesInt"



    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If Me.CheckBox1.Checked = False And Me.CheckBox3.Checked = False And Me.CheckBox6.Checked = False And Me.CheckBox4.Checked = False And Me.CheckBox5.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox7.Checked = False Then
            MsgBox("Selecciona al menos un Status.", , "Atenci�n")
            Exit Sub
        End If
        If TextBox1.Text.Length = 0 Then
            MsgBox("Falta capturar el mensaje.", , "Atenci�n")
            Exit Sub
        End If

        If CheckBoxPorPagar.Checked Then
            If Not IsNumeric(cbUltimoMes.SelectedValue) Then
                MsgBox("Falta capturar el �lltimo mes.", , "Atenci�n")
                Exit Sub
            End If

            If Not IsNumeric(tbUltimoAno.Text) Then
                MsgBox("Falta capturar el �lltimo a�o.", , "Atenci�n")
                Exit Sub
            End If

        End If

        Button2.Enabled = False
        Button3.Enabled = False

        If CheckBox1.Checked = True Then
            contratado = 1
        End If
        If CheckBox5.Checked = True Then
            suspendido = 1
        End If
        If CheckBox2.Checked = True Then
            cancelado = 1
        End If
        If CheckBox7.Checked = True Then
            Temporales = 1
        End If
        If CheckBox3.Checked = True Then
            instalado = 1
        End If
        If CheckBox4.Checked = True Then
            desconectado = 1
        End If
        If CheckBox6.Checked = True Then
            fueraArea = 1
        End If
        If CheckBox8.Checked = True Then
            periodo1 = 1
        End If
        If CheckBox9.Checked = True Then
            periodo2 = 1
        End If
        EnviarSms()
    End Sub



    Private Function obtenerStatus(ByVal file As String) As String
        Dim reader As StreamReader = New StreamReader(file)
        Dim ret As String = reader.ReadToEnd
        reader.Close()
        Dim posI As Integer = ret.IndexOf("<status>") + 8
        Dim posF As Integer = ret.IndexOf("</status>")
        Dim status As String = ret.Substring(posI, posF - posI)
        Return status
    End Function

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub





    Private Sub EnviarSms()
        Dim tabla As DataTable = New DataTable
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim mensaje As String = TextBox1.Text.ToString
        Try
            'Dim com As SqlCommand = New SqlCommand("MandarSMSmasivo", con)
            'con.Open()
            'com.CommandType = CommandType.StoredProcedure
            'com.CommandTimeout = 0
            'com.Parameters.Add(New SqlParameter("@Clv_Session", Convert.ToInt64(LocClv_session)))
            'com.Parameters.Add(New SqlParameter("@Identificador", Convert.ToInt64(identificador)))
            'com.Parameters.Add(New SqlParameter("@Con", contratado))
            'com.Parameters.Add(New SqlParameter("@Ins", instalado))
            'com.Parameters.Add(New SqlParameter("@Sus", suspendido))
            'com.Parameters.Add(New SqlParameter("@Des", cancelado))
            'com.Parameters.Add(New SqlParameter("@Baj", desconectado))
            'com.Parameters.Add(New SqlParameter("@Fue", fueraArea))
            'com.Parameters.Add(New SqlParameter("@Tem", Temporales))
            'com.Parameters.Add(New SqlParameter("@Per1", periodo1))
            'com.Parameters.Add(New SqlParameter("@Per2", periodo2))
            'com.Parameters.Add(New SqlParameter("@Mensaje", mensaje))

            'Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            'da.Fill(tabla)

            'If tabla.Rows.Count > 0 Then
            '    Try
            '        Dim ins As SqlCommand = New SqlCommand("INSERT INTO Master_Mensaje (Fecha, Mensaje) VALUES (getdate(), '" + mensaje + "')", con)
            '        ins.CommandType = CommandType.Text
            '        ins.ExecuteNonQuery()
            '    Catch ex As Exception
            '        Windows.Forms.MessageBox.Show(ex.Message, "Error")
            '    End Try
            'End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@Con", SqlDbType.Bit, contratado)
            BaseII.CreateMyParameter("@Ins", SqlDbType.Bit, instalado)
            BaseII.CreateMyParameter("@Sus", SqlDbType.Bit, suspendido)
            BaseII.CreateMyParameter("@Des", SqlDbType.Bit, desconectado)
            BaseII.CreateMyParameter("@Baj", SqlDbType.Bit, cancelado)
            BaseII.CreateMyParameter("@Fue", SqlDbType.Bit, fueraArea)
            BaseII.CreateMyParameter("@Tem", SqlDbType.Bit, Temporales)
            BaseII.CreateMyParameter("@Mensaje", SqlDbType.VarChar, mensaje, 500)
            BaseII.CreateMyParameter("@Identificador", SqlDbType.BigInt, identificador)


            BaseII.CreateMyParameter("@PorPagar", SqlDbType.Bit, CheckBoxPorPagar.Checked)
            If CheckBoxPorPagar.Checked Then
                BaseII.CreateMyParameter("@UltimoMes", SqlDbType.Int, CInt(cbUltimoMes.SelectedValue))
                BaseII.CreateMyParameter("@ultimoAnio", SqlDbType.Int, CInt(tbUltimoAno.Text))
            End If
            BaseII.CreateMyParameter("@MotCan", SqlDbType.Int, ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@FecIniCan", SqlDbType.Date, DateTimePicker1.Value)
            BaseII.CreateMyParameter("@FecFinCan", SqlDbType.Date, DateTimePicker2.Value)

            tabla = BaseII.ConsultaDT("MandarSMSmasivo")

        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.Message, "Error")
            'Finally
            '    con.Close()
        End Try
        If tabla.Rows.Count = 0 Then
            Windows.Forms.MessageBox.Show("No se encontraron clientes. Favor de intentar de nuevo.", "Error")
        Else
            'cont = tabla.Rows.Count
            ' Me.Post("GIGACABLE", "GIGA07", tabla, mensaje)
            'Me.Post("asd", "asd", tabla, mensaje)
            MsgBox("Se enviaran " + tabla.Rows.Count.ToString + " mensajes", MsgBoxStyle.MsgBoxRight)
        End If
        Me.Close()
    End Sub






    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked Then
            GroupBoxUltPago.Enabled = True
        Else
            GroupBoxUltPago.Enabled = False
            CheckBoxPorPagar.Checked = False
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged

        If CheckBox2.Checked Then
            GroupBoxCancelados.Enabled = True
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Muestra_MotCanc_ReporteTableAdapter.Connection = CON
            Me.Muestra_MotCanc_ReporteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_MotCanc_Reporte)
            CON.Close()
        Else
            GroupBoxCancelados.Enabled = False
        End If
    End Sub
End Class