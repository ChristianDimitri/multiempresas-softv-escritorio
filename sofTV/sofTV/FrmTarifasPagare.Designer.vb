﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarifasPagare
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DIA_FINALLabel = New System.Windows.Forms.Label()
        Me.DIA_INICIALLabel = New System.Windows.Forms.Label()
        Me.Periodo_InicialLabel = New System.Windows.Forms.Label()
        Me.Periodo_FinalLabel = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TipServCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.HDRadioBut = New System.Windows.Forms.RadioButton()
        Me.NormalRadioBut = New System.Windows.Forms.RadioButton()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CBoxSERVICIO = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Periodo_InicialDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Periodo_FinalDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.DIA_INICIALNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.DIA_FINALNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LabelRenta3 = New System.Windows.Forms.Label()
        Me.LabelRenta2 = New System.Windows.Forms.Label()
        Me.LabelRenta1 = New System.Windows.Forms.Label()
        Me.LabelRenta0 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CostoText = New System.Windows.Forms.TextBox()
        Me.RentaPrincipalText = New System.Windows.Forms.TextBox()
        Me.RentaAdicionalText = New System.Windows.Forms.TextBox()
        Me.RentaAdicionalText2 = New System.Windows.Forms.TextBox()
        Me.RentaAdicionalText3 = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.DIA_INICIALNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DIA_FINALNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'DIA_FINALLabel
        '
        Me.DIA_FINALLabel.AutoSize = True
        Me.DIA_FINALLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DIA_FINALLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.DIA_FINALLabel.Location = New System.Drawing.Point(160, 79)
        Me.DIA_FINALLabel.Name = "DIA_FINALLabel"
        Me.DIA_FINALLabel.Size = New System.Drawing.Size(73, 15)
        Me.DIA_FINALLabel.TabIndex = 11
        Me.DIA_FINALLabel.Text = "Día Final :"
        '
        'DIA_INICIALLabel
        '
        Me.DIA_INICIALLabel.AutoSize = True
        Me.DIA_INICIALLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DIA_INICIALLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.DIA_INICIALLabel.Location = New System.Drawing.Point(3, 79)
        Me.DIA_INICIALLabel.Name = "DIA_INICIALLabel"
        Me.DIA_INICIALLabel.Size = New System.Drawing.Size(80, 15)
        Me.DIA_INICIALLabel.TabIndex = 9
        Me.DIA_INICIALLabel.Text = "Día Inicial :"
        '
        'Periodo_InicialLabel
        '
        Me.Periodo_InicialLabel.AutoSize = True
        Me.Periodo_InicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_InicialLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Periodo_InicialLabel.Location = New System.Drawing.Point(12, 28)
        Me.Periodo_InicialLabel.Name = "Periodo_InicialLabel"
        Me.Periodo_InicialLabel.Size = New System.Drawing.Size(77, 15)
        Me.Periodo_InicialLabel.TabIndex = 600
        Me.Periodo_InicialLabel.Text = "Apartir de :"
        '
        'Periodo_FinalLabel
        '
        Me.Periodo_FinalLabel.AutoSize = True
        Me.Periodo_FinalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_FinalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Periodo_FinalLabel.Location = New System.Drawing.Point(142, 28)
        Me.Periodo_FinalLabel.Name = "Periodo_FinalLabel"
        Me.Periodo_FinalLabel.Size = New System.Drawing.Size(52, 15)
        Me.Periodo_FinalLabel.TabIndex = 601
        Me.Periodo_FinalLabel.Text = "Hasta :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label7.Location = New System.Drawing.Point(47, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(124, 15)
        Me.Label7.TabIndex = 600
        Me.Label7.Text = "Costo del Equipo :"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 36)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(109, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Tipo Servicio :"
        '
        'TipServCombo
        '
        Me.TipServCombo.DisplayMember = "CONCEPTO"
        Me.TipServCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipServCombo.FormattingEnabled = True
        Me.TipServCombo.Location = New System.Drawing.Point(124, 35)
        Me.TipServCombo.Name = "TipServCombo"
        Me.TipServCombo.Size = New System.Drawing.Size(484, 23)
        Me.TipServCombo.TabIndex = 0
        Me.TipServCombo.ValueMember = "CLV_TIPSER"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(525, 639)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(110, 16)
        Me.CMBLabel3.TabIndex = 3
        Me.CMBLabel3.Text = "Tipo Paquete :"
        Me.CMBLabel3.Visible = False
        '
        'HDRadioBut
        '
        Me.HDRadioBut.AutoSize = True
        Me.HDRadioBut.Location = New System.Drawing.Point(571, 638)
        Me.HDRadioBut.Name = "HDRadioBut"
        Me.HDRadioBut.Size = New System.Drawing.Size(41, 17)
        Me.HDRadioBut.TabIndex = 3
        Me.HDRadioBut.Text = "HD"
        Me.HDRadioBut.UseVisualStyleBackColor = True
        Me.HDRadioBut.Visible = False
        '
        'NormalRadioBut
        '
        Me.NormalRadioBut.AutoSize = True
        Me.NormalRadioBut.Checked = True
        Me.NormalRadioBut.Location = New System.Drawing.Point(604, 639)
        Me.NormalRadioBut.Name = "NormalRadioBut"
        Me.NormalRadioBut.Size = New System.Drawing.Size(58, 17)
        Me.NormalRadioBut.TabIndex = 2
        Me.NormalRadioBut.TabStop = True
        Me.NormalRadioBut.Text = "Normal"
        Me.NormalRadioBut.UseVisualStyleBackColor = True
        Me.NormalRadioBut.Visible = False
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(32, 165)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(86, 16)
        Me.CMBLabel4.TabIndex = 5
        Me.CMBLabel4.Text = "Setup Box :"
        '
        'SaveButton
        '
        Me.SaveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(124, 294)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(255, 35)
        Me.SaveButton.TabIndex = 3
        Me.SaveButton.Text = "Guardar &Relación con el Equipo"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'ExitButton
        '
        Me.ExitButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitButton.Location = New System.Drawing.Point(816, 682)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(140, 35)
        Me.ExitButton.TabIndex = 4
        Me.ExitButton.Text = "&Cerrar"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(549, 640)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 16)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Compañía :"
        Me.Label1.Visible = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(124, 81)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(484, 23)
        Me.ComboBoxCompanias.TabIndex = 0
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'CBoxSERVICIO
        '
        Me.CBoxSERVICIO.DisplayMember = "SERVICIO"
        Me.CBoxSERVICIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBoxSERVICIO.FormattingEnabled = True
        Me.CBoxSERVICIO.Location = New System.Drawing.Point(124, 124)
        Me.CBoxSERVICIO.Name = "CBoxSERVICIO"
        Me.CBoxSERVICIO.Size = New System.Drawing.Size(484, 23)
        Me.CBoxSERVICIO.TabIndex = 1
        Me.CBoxSERVICIO.ValueMember = "CLV_SERVICIO"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(45, 127)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 16)
        Me.Label4.TabIndex = 115
        Me.Label4.Text = "Servicio :"
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(124, 165)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(484, 123)
        Me.CheckedListBox1.TabIndex = 2
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Panel3)
        Me.Panel4.Controls.Add(Me.Panel2)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Enabled = False
        Me.Panel4.Location = New System.Drawing.Point(0, 335)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(956, 340)
        Me.Panel4.TabIndex = 625
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel7)
        Me.Panel3.Controls.Add(Me.Panel1)
        Me.Panel3.Location = New System.Drawing.Point(12, 9)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(307, 311)
        Me.Panel3.TabIndex = 627
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Periodo_InicialDateTimePicker)
        Me.Panel7.Controls.Add(Me.Periodo_FinalDateTimePicker)
        Me.Panel7.Controls.Add(Me.CMBTextBox1)
        Me.Panel7.Controls.Add(Me.DIA_FINALLabel)
        Me.Panel7.Controls.Add(Me.DIA_INICIALLabel)
        Me.Panel7.Controls.Add(Me.Periodo_InicialLabel)
        Me.Panel7.Controls.Add(Me.Periodo_FinalLabel)
        Me.Panel7.Controls.Add(Me.DIA_INICIALNumericUpDown)
        Me.Panel7.Controls.Add(Me.DIA_FINALNumericUpDown)
        Me.Panel7.Location = New System.Drawing.Point(3, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(296, 108)
        Me.Panel7.TabIndex = 117
        Me.Panel7.TabStop = True
        '
        'Periodo_InicialDateTimePicker
        '
        Me.Periodo_InicialDateTimePicker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_InicialDateTimePicker.CustomFormat = "MMM/yyyy"
        Me.Periodo_InicialDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_InicialDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Periodo_InicialDateTimePicker.Location = New System.Drawing.Point(24, 46)
        Me.Periodo_InicialDateTimePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.Periodo_InicialDateTimePicker.Name = "Periodo_InicialDateTimePicker"
        Me.Periodo_InicialDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Periodo_InicialDateTimePicker.TabIndex = 0
        Me.Periodo_InicialDateTimePicker.Value = New Date(2007, 4, 18, 0, 0, 0, 0)
        '
        'Periodo_FinalDateTimePicker
        '
        Me.Periodo_FinalDateTimePicker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_FinalDateTimePicker.CustomFormat = "MMM/yyyy"
        Me.Periodo_FinalDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_FinalDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Periodo_FinalDateTimePicker.Location = New System.Drawing.Point(155, 46)
        Me.Periodo_FinalDateTimePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.Periodo_FinalDateTimePicker.Name = "Periodo_FinalDateTimePicker"
        Me.Periodo_FinalDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Periodo_FinalDateTimePicker.TabIndex = 1
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(6, 5)
        Me.CMBTextBox1.Multiline = True
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.Size = New System.Drawing.Size(305, 20)
        Me.CMBTextBox1.TabIndex = 622
        Me.CMBTextBox1.TabStop = False
        Me.CMBTextBox1.Text = "Periodo de Vigencia :"
        Me.CMBTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DIA_INICIALNumericUpDown
        '
        Me.DIA_INICIALNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DIA_INICIALNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DIA_INICIALNumericUpDown.Location = New System.Drawing.Point(84, 77)
        Me.DIA_INICIALNumericUpDown.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.DIA_INICIALNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DIA_INICIALNumericUpDown.Name = "DIA_INICIALNumericUpDown"
        Me.DIA_INICIALNumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.DIA_INICIALNumericUpDown.TabIndex = 2
        Me.DIA_INICIALNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'DIA_FINALNumericUpDown
        '
        Me.DIA_FINALNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DIA_FINALNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DIA_FINALNumericUpDown.Location = New System.Drawing.Point(239, 77)
        Me.DIA_FINALNumericUpDown.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.DIA_FINALNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DIA_FINALNumericUpDown.Name = "DIA_FINALNumericUpDown"
        Me.DIA_FINALNumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.DIA_FINALNumericUpDown.TabIndex = 3
        Me.DIA_FINALNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.LabelRenta3)
        Me.Panel1.Controls.Add(Me.LabelRenta2)
        Me.Panel1.Controls.Add(Me.LabelRenta1)
        Me.Panel1.Controls.Add(Me.LabelRenta0)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.CostoText)
        Me.Panel1.Controls.Add(Me.RentaPrincipalText)
        Me.Panel1.Controls.Add(Me.RentaAdicionalText)
        Me.Panel1.Controls.Add(Me.RentaAdicionalText2)
        Me.Panel1.Controls.Add(Me.RentaAdicionalText3)
        Me.Panel1.Location = New System.Drawing.Point(3, 114)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(296, 189)
        Me.Panel1.TabIndex = 122
        Me.Panel1.TabStop = True
        '
        'LabelRenta3
        '
        Me.LabelRenta3.AutoSize = True
        Me.LabelRenta3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelRenta3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelRenta3.Location = New System.Drawing.Point(22, 134)
        Me.LabelRenta3.Name = "LabelRenta3"
        Me.LabelRenta3.Size = New System.Drawing.Size(145, 15)
        Me.LabelRenta3.TabIndex = 626
        Me.LabelRenta3.Text = "Renta Adicional 3ra. :"
        '
        'LabelRenta2
        '
        Me.LabelRenta2.AutoSize = True
        Me.LabelRenta2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelRenta2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelRenta2.Location = New System.Drawing.Point(23, 108)
        Me.LabelRenta2.Name = "LabelRenta2"
        Me.LabelRenta2.Size = New System.Drawing.Size(148, 15)
        Me.LabelRenta2.TabIndex = 625
        Me.LabelRenta2.Text = "Renta Adicional 2da. :"
        '
        'LabelRenta1
        '
        Me.LabelRenta1.AutoSize = True
        Me.LabelRenta1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelRenta1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelRenta1.Location = New System.Drawing.Point(26, 82)
        Me.LabelRenta1.Name = "LabelRenta1"
        Me.LabelRenta1.Size = New System.Drawing.Size(145, 15)
        Me.LabelRenta1.TabIndex = 624
        Me.LabelRenta1.Text = "Renta Adicional 1ra. :"
        '
        'LabelRenta0
        '
        Me.LabelRenta0.AutoSize = True
        Me.LabelRenta0.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelRenta0.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelRenta0.Location = New System.Drawing.Point(57, 57)
        Me.LabelRenta0.Name = "LabelRenta0"
        Me.LabelRenta0.Size = New System.Drawing.Size(114, 15)
        Me.LabelRenta0.TabIndex = 623
        Me.LabelRenta0.Text = "Renta Principal :"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.White
        Me.TextBox1.Location = New System.Drawing.Point(6, 5)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(305, 20)
        Me.TextBox1.TabIndex = 622
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "Tarifas :"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CostoText
        '
        Me.CostoText.Location = New System.Drawing.Point(177, 31)
        Me.CostoText.Name = "CostoText"
        Me.CostoText.Size = New System.Drawing.Size(102, 20)
        Me.CostoText.TabIndex = 0
        Me.CostoText.Text = "0"
        '
        'RentaPrincipalText
        '
        Me.RentaPrincipalText.Location = New System.Drawing.Point(177, 56)
        Me.RentaPrincipalText.Name = "RentaPrincipalText"
        Me.RentaPrincipalText.Size = New System.Drawing.Size(102, 20)
        Me.RentaPrincipalText.TabIndex = 1
        Me.RentaPrincipalText.Text = "0"
        '
        'RentaAdicionalText
        '
        Me.RentaAdicionalText.Location = New System.Drawing.Point(177, 81)
        Me.RentaAdicionalText.Name = "RentaAdicionalText"
        Me.RentaAdicionalText.Size = New System.Drawing.Size(102, 20)
        Me.RentaAdicionalText.TabIndex = 2
        Me.RentaAdicionalText.Text = "0"
        '
        'RentaAdicionalText2
        '
        Me.RentaAdicionalText2.Location = New System.Drawing.Point(177, 107)
        Me.RentaAdicionalText2.Name = "RentaAdicionalText2"
        Me.RentaAdicionalText2.Size = New System.Drawing.Size(102, 20)
        Me.RentaAdicionalText2.TabIndex = 3
        Me.RentaAdicionalText2.Text = "0"
        '
        'RentaAdicionalText3
        '
        Me.RentaAdicionalText3.Location = New System.Drawing.Point(177, 133)
        Me.RentaAdicionalText3.Name = "RentaAdicionalText3"
        Me.RentaAdicionalText3.Size = New System.Drawing.Size(102, 20)
        Me.RentaAdicionalText3.TabIndex = 4
        Me.RentaAdicionalText3.Text = "0"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Location = New System.Drawing.Point(332, 49)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(614, 271)
        Me.Panel2.TabIndex = 626
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(605, 260)
        Me.DataGridView1.TabIndex = 1
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator)
        Me.Panel5.Location = New System.Drawing.Point(332, 9)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(617, 34)
        Me.Panel5.TabIndex = 0
        '
        'CONREL_TARIFADOS_SERVICIOSBindingNavigator
        '
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.AddNewItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.CountItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.DeleteItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorAddNewItem, Me.ToolStripButton4, Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem, Me.ToolStripButton2, Me.ToolStripButton3})
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Name = "CONREL_TARIFADOS_SERVICIOSBindingNavigator"
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.PositionItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Size = New System.Drawing.Size(617, 25)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.TabIndex = 627
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorAddNewItem.ForeColor = System.Drawing.Color.Black
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(110, 22)
        Me.BindingNavigatorAddNewItem.Text = "&Agregar Nuevo"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton4.Enabled = False
        Me.ToolStripButton4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton4.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(71, 22)
        Me.ToolStripButton4.Text = "&Modificar"
        '
        'CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem
        '
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.ForeColor = System.Drawing.Color.Black
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Name = "CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem"
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(64, 22)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Text = "G&uardar"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Enabled = False
        Me.ToolStripButton2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(61, 22)
        Me.ToolStripButton2.Text = "E&liminar"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton3.Enabled = False
        Me.ToolStripButton3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton3.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(68, 22)
        Me.ToolStripButton3.Text = "Ca&ncelar"
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(29, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 16)
        Me.Label2.TabIndex = 627
        Me.Label2.Text = "Compañia :"
        '
        'FrmTarifasPagare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(968, 729)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.CheckedListBox1)
        Me.Controls.Add(Me.CBoxSERVICIO)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ComboBoxCompanias)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.TipServCombo)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.HDRadioBut)
        Me.Controls.Add(Me.NormalRadioBut)
        Me.Controls.Add(Me.CMBLabel3)
        Me.MaximizeBox = False
        Me.Name = "FrmTarifasPagare"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarifas Pagaré"
        Me.TopMost = True
        Me.Panel4.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.DIA_INICIALNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DIA_FINALNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.ResumeLayout(False)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TipServCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents HDRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents NormalRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CBoxSERVICIO As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Periodo_InicialDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Periodo_FinalDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DIA_INICIALNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents DIA_FINALNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LabelRenta3 As System.Windows.Forms.Label
    Friend WithEvents LabelRenta2 As System.Windows.Forms.Label
    Friend WithEvents LabelRenta1 As System.Windows.Forms.Label
    Friend WithEvents LabelRenta0 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CostoText As System.Windows.Forms.TextBox
    Friend WithEvents RentaPrincipalText As System.Windows.Forms.TextBox
    Friend WithEvents RentaAdicionalText As System.Windows.Forms.TextBox
    Friend WithEvents RentaAdicionalText2 As System.Windows.Forms.TextBox
    Friend WithEvents RentaAdicionalText3 As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents CONREL_TARIFADOS_SERVICIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents DIA_FINALLabel As System.Windows.Forms.Label
    Friend WithEvents DIA_INICIALLabel As System.Windows.Forms.Label
    Friend WithEvents Periodo_InicialLabel As System.Windows.Forms.Label
    Friend WithEvents Periodo_FinalLabel As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
