﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.IO

Public Class FrmTarifasPagare
    Private MyList As ArrayList
    Private GloIdCostoAP As Long = 0
    Private GloIdRelVigencia As Long = 0
    Private LocMoveOpcion As String = ""

    Private Sub LlenaServicios(LocClv_TipSer As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, LocClv_TipSer)
        CBoxSERVICIO.DataSource = BaseII.ConsultaDT("MuestraServiciosRentas")
        ChecaTelefonosDisponibles()
        CBoxSERVICIO.DisplayMember = "SERVICIO"
        CBoxSERVICIO.ValueMember = "CLV_SERVICIO"
    End Sub

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            'ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 1 Then
                ComboBoxCompanias.SelectedValue = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try

    End Sub

    Private Sub FrmTarifasPagare_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If OpPagare = "M" Then
            GloIdCostoAP = IdPagare
            Panel4.Enabled = True
        ElseIf OpPagare = "N" Then
            GloIdCostoAP = 0
            Panel4.Enabled = False
        End If


        'MuestraMarcasAparatosPagare()
        ' MuestraMarcasAparatosPagareList()


        Llena_companias()
        If OpPagare = "N" Then
            MuestraTipoServicioPagare(2)
            MuestraServiciosRentas()
            ConsultaEquiposMarcadosList()
            'Me.ComboBoxCompanias.Enabled = True
            Me.TipServCombo.Enabled = True
            'Me.NormalRadioBut.Enabled = False
            'Me.HDRadioBut.Enabled = False
            '' Me.AparatoCombo.Enabled = True
            Me.CBoxSERVICIO.Enabled = True

            Panel5.Enabled = True
            Panel3.Enabled = False
            Panel2.Enabled = False
            Panel4.Enabled = False
        ElseIf OpPagare = "M" Then
            GloIdCostoAP = IdPagare
            MuestraTipoServicioPagare(2)
            MuestraServiciosRentas()
            ConCostosPagare()
            LlenaGridVigencias(GloIdCostoAP)
            ConsultaEquiposMarcadosList()
            'Me.ComboBoxCompanias.Enabled = False
            Me.TipServCombo.Enabled = False
            'Me.NormalRadioBut.Enabled = False
            'Me.HDRadioBut.Enabled = False
            ''Me.AparatoCombo.Enabled = False
            Me.CBoxSERVICIO.Enabled = False

            Panel5.Enabled = True
            Panel3.Enabled = False
            Panel2.Enabled = True
            Panel4.Enabled = True
        End If

    End Sub

    Private Sub MuestraTipoServicioPagare(ByVal OPCION As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraTipoServicioPagare ")
        StrSQL.Append(CStr(OPCION))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            TipServCombo.DataSource = BS.DataSource
            If TipServCombo.Items.Count > 0 Then
                LlenaServicios(TipServCombo.SelectedValue)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    'Private Sub MuestraMarcasAparatosPagare()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim StrSQL As New StringBuilder

    '    StrSQL.Append("EXEC MuestraMarcasAparatosPagare ")
    '    StrSQL.Append(CStr(TipServCombo.SelectedValue) & ", ")
    '    If Me.NormalRadioBut.Checked Then
    '        StrSQL.Append(CStr("1"))
    '    ElseIf Me.HDRadioBut.Checked Then
    '        StrSQL.Append(CStr("2"))
    '    Else
    '        StrSQL.Append(CStr("0"))
    '    End If

    '    Dim DT As New DataTable
    '    Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
    '    Dim BS As New BindingSource

    '    Try
    '        CON.Open()
    '        DA.Fill(DT)
    '        BS.DataSource = DT
    '        AparatoCombo.DataSource = BS.DataSource
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Information)
    '    Finally
    '        CON.Close()
    '        CON.Dispose()
    '    End Try

    'End Sub
    Private Sub DameVigenciaTarifas(LocIdCostoAP As Long, LocIdRelVigencia As Long)
        Dim SqlReader As SqlDataReader
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdCostoAP", SqlDbType.BigInt, LocIdCostoAP)
        BaseII.CreateMyParameter("@IdRelVigencia", SqlDbType.BigInt, LocIdRelVigencia)
        SqlReader = BaseII.ConsultaReader("SP_ConsultaRel_CostoArticuloPagareConVigenciasIndividual")
        While (SqlReader.Read)
            GloIdRelVigencia = SqlReader(0).ToString()
            GloIdCostoAP = SqlReader(1).ToString()
            DIA_INICIALNumericUpDown.Value = SqlReader(2).ToString()
            DIA_FINALNumericUpDown.Value = SqlReader(3).ToString()
            Periodo_InicialDateTimePicker.Value = SqlReader(4).ToString()
            Periodo_FinalDateTimePicker.Value = SqlReader(5).ToString()
            CostoText.Text = SqlReader(6).ToString()
            RentaPrincipalText.Text = SqlReader(7).ToString()
            RentaAdicionalText.Text = SqlReader(8).ToString()
            RentaAdicionalText2.Text = SqlReader(9).ToString()
            RentaAdicionalText3.Text = SqlReader(10).ToString()
        End While

    End Sub



    Private Function SP_GuardaCostosPagareNew(locId As Long,
locTIPSER As Integer,
    locOPCION As String,
    locClv_Servicio As Integer
) As String
        SP_GuardaCostosPagareNew = ""
        GloIdCostoAP = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ID", SqlDbType.BigInt, locId)
        BaseII.CreateMyParameter("@TIPSER ", SqlDbType.BigInt, locTIPSER)
        BaseII.CreateMyParameter("@OPCION", SqlDbType.VarChar, locOPCION, 1)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.BigInt, locClv_Servicio)
        BaseII.CreateMyParameter("@IdCostoAp", ParameterDirection.Output, SqlDbType.BigInt)
        BaseII.CreateMyParameter("@IdCompania", SqlDbType.BigInt, ComboBoxCompanias.SelectedValue)
        BaseII.ProcedimientoOutPut("SP_GuardaCostosPagareNew")
        'Checamos si tiene estado cuenta generado
        SP_GuardaCostosPagareNew = BaseII.dicoPar("@Msj")
        GloIdCostoAP = BaseII.dicoPar("@IdCostoAp")

        If GloIdCostoAP > 0 Then
            LimpiaSeleccionEquipos()
            GuardaEquiposSeleccinadosAll()
        End If




    End Function
    Private Function InsertarCostoPagare(locIdCostoAP As Long,
locDIA_INICIAL As Integer,
     locDIA_FINAL As Integer,
     locPeriodo_Inicial As DateTime,
     locPeriodo_Final As DateTime,
     locCostoArticulo As Decimal,
     locRentaPrincipal As Decimal,
     locRentaAdicional As Decimal,
     locRentaAdicional2 As Decimal,
     locRentaAdicional3 As Decimal
) As String
        InsertarCostoPagare = ""
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@IdCostoAP", SqlDbType.BigInt, locIdCostoAP)
        BaseII.CreateMyParameter("@DIA_INICIAL", SqlDbType.Int, locDIA_INICIAL)
        BaseII.CreateMyParameter("@DIA_FINAL", SqlDbType.Int, locDIA_FINAL)
        BaseII.CreateMyParameter("@Periodo_Inicial", SqlDbType.DateTime, locPeriodo_Inicial)
        BaseII.CreateMyParameter("@Periodo_Final", SqlDbType.DateTime, locPeriodo_Final)
        BaseII.CreateMyParameter("@Vigente", SqlDbType.Bit, True)
        BaseII.CreateMyParameter("@CostoArticulo", SqlDbType.Money, locCostoArticulo)
        BaseII.CreateMyParameter("@RentaPrincipal", SqlDbType.Money, locRentaPrincipal)
        BaseII.CreateMyParameter("@RentaAdicional", SqlDbType.Money, locRentaAdicional)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@RentaAdicional2", SqlDbType.Money, locRentaAdicional2)
        BaseII.CreateMyParameter("@RentaAdicional3", SqlDbType.Money, locRentaAdicional3)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.ProcedimientoOutPut("SP_InsertaRel_CostoArticuloPagareConVigencias")
        'Checamos si tiene estado cuenta generado
        InsertarCostoPagare = BaseII.dicoPar("@Msj")
    End Function

    Private Function ModificarCostoPagare(locIdCostoAP As Long,
                                          locIdRelVigencia As Long,
locDIA_INICIAL As Integer,
    locDIA_FINAL As Integer,
    locPeriodo_Inicial As DateTime,
    locPeriodo_Final As DateTime,
    locCostoArticulo As Decimal,
    locRentaPrincipal As Decimal,
    locRentaAdicional As Decimal,
    locRentaAdicional2 As Decimal,
    locRentaAdicional3 As Decimal
) As String
        ModificarCostoPagare = ""
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@IdCostoAP", SqlDbType.BigInt, locIdCostoAP)
        BaseII.CreateMyParameter("@IdRelVigencia", SqlDbType.BigInt, locIdRelVigencia)
        BaseII.CreateMyParameter("@DIA_INICIAL", SqlDbType.BigInt, locDIA_INICIAL)
        BaseII.CreateMyParameter("@DIA_FINAL", SqlDbType.BigInt, locDIA_FINAL)
        BaseII.CreateMyParameter("@Periodo_Inicial", SqlDbType.DateTime, locPeriodo_Inicial)
        BaseII.CreateMyParameter("@Periodo_Final", SqlDbType.DateTime, locPeriodo_Final)
        BaseII.CreateMyParameter("@Vigente", SqlDbType.Bit, True)
        BaseII.CreateMyParameter("@CostoArticulo", SqlDbType.Money, locCostoArticulo)
        BaseII.CreateMyParameter("@RentaPrincipal", SqlDbType.Money, locRentaPrincipal)
        BaseII.CreateMyParameter("@RentaAdicional", SqlDbType.Money, locRentaAdicional)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@RentaAdicional2", SqlDbType.Money, locRentaAdicional2)
        BaseII.CreateMyParameter("@RentaAdicional3", SqlDbType.Money, locRentaAdicional3)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.ProcedimientoOutPut("SP_ModificarRel_CostoArticuloPagareConVigencias")
        'Checamos si tiene estado cuenta generado
        ModificarCostoPagare = BaseII.dicoPar("@Msj")
    End Function

    Private Function Sp_ValidaVigencias(locIdCostoAP As Long,
                                          locIdRelVigencia As Long,
locDIA_INICIAL As Integer,
    locDIA_FINAL As Integer,
    locPeriodo_Inicial As DateTime,
    locPeriodo_Final As DateTime
) As String
        Sp_ValidaVigencias = ""
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@DiaIni", SqlDbType.BigInt, locDIA_INICIAL)
        BaseII.CreateMyParameter("@DiaFin", SqlDbType.BigInt, locDIA_FINAL)
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, locPeriodo_Inicial)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, locPeriodo_Final)
        BaseII.CreateMyParameter("@IdCostoAP", SqlDbType.BigInt, locIdCostoAP)
        BaseII.CreateMyParameter("@IdRelVigencia", SqlDbType.BigInt, locIdRelVigencia)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.ProcedimientoOutPut("Sp_ValidaVigencias")
        'Checamos si tiene estado cuenta generado
        Sp_ValidaVigencias = BaseII.dicoPar("@Msj")
    End Function


    Private Function EliminarCostoPagare(locIdCostoAP As Long,
                                         locIdRelVigencia As Long) As String
        EliminarCostoPagare = ""
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@IdCostoAP", SqlDbType.BigInt, locIdCostoAP)
        BaseII.CreateMyParameter("@IdRelVigencia", SqlDbType.BigInt, locIdRelVigencia)

        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.ProcedimientoOutPut("SP_EliminarRel_CostoArticuloPagareConVigenciasIndividual")
        'Checamos si tiene estado cuenta generado
        EliminarCostoPagare = BaseII.dicoPar("@Msj")
    End Function


    Private Sub ConsultaEquiposMarcadosList()
        Try


            'MyList = New ArrayList
            'CheckedListBox1.Items.Clear()
            'Dim SqlReader As SqlDataReader
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@TipoServicio", SqlDbType.Int, TipServCombo.SelectedValue)
            'BaseII.CreateMyParameter("@TipoPaquete", SqlDbType.Int, 1)
            'BaseII.CreateMyParameter("@IdCostoAP", SqlDbType.BigInt, GloIdCostoAP)
            'SqlReader = BaseII.ConsultaReader("ConsultaEquipoMarcado")
            'While (SqlReader.Read)
            '    CheckedListBox1.Items.Add(SqlReader(1).ToString, CBool(SqlReader(2).ToString))
            '    MyList.Add(SqlReader(0).ToString)
            '    End While


            MyList = New ArrayList
            CheckedListBox1.Items.Clear()
            Dim CON As New SqlConnection(MiConexion)
            Dim StrSQL As New StringBuilder
            Dim SqlReader As SqlDataReader



            StrSQL.Append("EXEC ConsultaEquipoMarcado ")
            StrSQL.Append(CStr(TipServCombo.SelectedValue) & ", ")
            If Me.NormalRadioBut.Checked Then
                StrSQL.Append(CStr("1") & ", ")
            ElseIf Me.HDRadioBut.Checked Then
                StrSQL.Append(CStr("2") & ", ")
            Else
                StrSQL.Append(CStr("0") & ", ")
            End If

            StrSQL.Append(CStr(GloIdCostoAP))

            'Dim DT As New DataTable
            'Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
            Dim CMD As New SqlCommand(StrSQL.ToString(), CON)
            'Dim BS As New BindingSource

            Try
                CON.Open()
                SqlReader = CMD.ExecuteReader
                While (SqlReader.Read)
                    CheckedListBox1.Items.Add(SqlReader(1).ToString, CBool(SqlReader(2).ToString))
                    MyList.Add(SqlReader(0).ToString)
                End While
                'CheckedListBox1.Items.Add("prueba", False)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            Finally
                CON.Close()
                CON.Dispose()
            End Try


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub


    Private Sub MuestraMarcasAparatosPagareList()

        MyList = New ArrayList
        CheckedListBox1.Items.Clear()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder
        Dim SqlReader As SqlDataReader



        StrSQL.Append("EXEC MuestraMarcasAparatosPagareParaList ")
        StrSQL.Append(CStr(TipServCombo.SelectedValue) & ", ")
        If Me.NormalRadioBut.Checked Then
            StrSQL.Append(CStr("1") & ", ")
        ElseIf Me.HDRadioBut.Checked Then
            StrSQL.Append(CStr("2") & ", ")
        Else
            StrSQL.Append(CStr("0") & ", ")
        End If

        StrSQL.Append(CStr("0"))

        'Dim DT As New DataTable
        'Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim CMD As New SqlCommand(StrSQL.ToString(), CON)
        'Dim BS As New BindingSource

        Try
            CON.Open()
            SqlReader = CMD.ExecuteReader
            While (SqlReader.Read)
                CheckedListBox1.Items.Add(SqlReader(1).ToString, CBool(SqlReader(2).ToString))
                MyList.Add(SqlReader(0).ToString)
            End While
            'CheckedListBox1.Items.Add("prueba", False)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub

    Private Sub GuardaCostosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GuardaCostosPagare", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ID", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = IdPagare
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@TIPSER", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Me.TipServCombo.SelectedValue
        CMD.Parameters.Add(PRM2)

        Dim PRM4 As New SqlParameter("@IDARTICULO", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = 0
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@COSTOARTICULO", SqlDbType.Money)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = 0
        CMD.Parameters.Add(PRM5)

        Dim PRM8 As New SqlParameter("@RENTAPRINCIPAL", SqlDbType.Money)
        PRM8.Direction = ParameterDirection.Input
        PRM8.Value = 0
        CMD.Parameters.Add(PRM8)

        If IsNumeric(RentaAdicionalText.Text) = False Then RentaAdicionalText.Text = 0
        Dim PRM9 As New SqlParameter("@RENTAADICIONAL", SqlDbType.Money)
        PRM9.Direction = ParameterDirection.Input
        PRM9.Value = 0
        CMD.Parameters.Add(PRM9)

        Dim PRM6 As New SqlParameter("@OPCION", SqlDbType.VarChar, 1)
        PRM6.Direction = ParameterDirection.Input
        PRM6.Value = OpPagare
        CMD.Parameters.Add(PRM6)

        Dim PRM7 As New SqlParameter("@EXISTENCIA", SqlDbType.Bit)
        PRM7.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM7)

        Dim PRM10 As New SqlParameter("@idcompania", SqlDbType.Int)
        PRM10.Direction = ParameterDirection.Input
        PRM10.Value = 1 'ComboBoxCompanias.SelectedValue
        CMD.Parameters.Add(PRM10)

        If IsNumeric(RentaAdicionalText2.Text) = False Then RentaAdicionalText2.Text = 0
        Dim PRM92 As New SqlParameter("@RENTAADICIONAL2", SqlDbType.Money)
        PRM92.Direction = ParameterDirection.Input
        PRM92.Value = 0
        CMD.Parameters.Add(PRM92)

        If IsNumeric(RentaAdicionalText3.Text) = False Then RentaAdicionalText3.Text = 0
        Dim PRM93 As New SqlParameter("@RENTAADICIONAL3", SqlDbType.Money)
        PRM93.Direction = ParameterDirection.Input
        PRM93.Value = 0
        CMD.Parameters.Add(PRM93)

        Dim PRM110 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        PRM110.Direction = ParameterDirection.Input
        PRM110.Value = Me.CBoxSERVICIO.SelectedValue
        CMD.Parameters.Add(PRM110)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM7.Value = True Then
                MsgBox("El registro ya fue dado de alta anteriormente", MsgBoxStyle.Information)
                Exit Sub
            Else
                MsgBox("Registro almacenado Satisfactoriamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub


    Public Function SP_EliminarRel_CostoArticuloPagareConVigenciasIndividual(locIdCostoAP As Long, LocIdRelVigencia As Long) As String
        Dim CON As New SqlConnection(MiConexion)
        Try
            SP_EliminarRel_CostoArticuloPagareConVigenciasIndividual = ""

            Dim CMD As New SqlCommand("SP_EliminarRel_CostoArticuloPagareConVigenciasIndividual", CON)
            CMD.CommandType = CommandType.StoredProcedure

            Dim PRM1 As New SqlParameter("@IdCostoAP", SqlDbType.BigInt)
            PRM1.Direction = ParameterDirection.Input
            PRM1.Value = locIdCostoAP
            CMD.Parameters.Add(PRM1)

            Dim PRM2 As New SqlParameter("@IdRelVigencia", SqlDbType.BigInt)
            PRM2.Direction = ParameterDirection.Input
            PRM2.Value = LocIdRelVigencia
            CMD.Parameters.Add(PRM2)


            Dim PRM3 As New SqlParameter("@Msj", SqlDbType.VarChar, 250)
            PRM3.Direction = ParameterDirection.Output
            PRM3.Value = ""
            CMD.Parameters.Add(PRM3)


            CON.Open()
            CMD.ExecuteNonQuery()
            SP_EliminarRel_CostoArticuloPagareConVigenciasIndividual = PRM3.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Function

    Private Sub SP_ConsultaRel_CostoArticuloPagareConVigenciasIndividual(locIdCostoAP As Long, LocIdRelVigencia As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("SP_ConsultaRel_CostoArticuloPagareConVigenciasIndividual", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@IdCostoAP", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = locIdCostoAP
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@IdRelVigencia", SqlDbType.BigInt)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = LocIdRelVigencia
        CMD.Parameters.Add(PRM2)

        Dim SqlReader As SqlDataReader


        Try
            CON.Open()
            SqlReader = CMD.ExecuteReader

            While (SqlReader.Read)


                DIA_INICIALNumericUpDown.Value = SqlReader(2).ToString
                DIA_FINALNumericUpDown.Value = SqlReader(3).ToString()
                'Fechas
                Periodo_InicialDateTimePicker.MinDate = CDate(SqlReader(4).ToString)
                Periodo_FinalDateTimePicker.MinDate = CDate(SqlReader(4).ToString)
                'Fecha
                Periodo_InicialDateTimePicker.Value = SqlReader(4).ToString
                Periodo_FinalDateTimePicker.Value = SqlReader(5).ToString
                Me.CostoText.Text = SqlReader(6).ToString
                Me.CostoText.Text = CType(Me.CostoText.Text, Double)
                Me.RentaPrincipalText.Text = SqlReader(7).ToString
                Me.RentaPrincipalText.Text = CType(Me.RentaPrincipalText.Text, Double)
                Me.RentaAdicionalText.Text = SqlReader(8).ToString
                Me.RentaAdicionalText.Text = CType(Me.RentaAdicionalText.Text, Double)
                Me.RentaAdicionalText2.Text = CType(SqlReader(9).ToString, Double)
                Me.RentaAdicionalText3.Text = CType(SqlReader(10).ToString, Double)

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub ConCostosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConCostosPagare", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@OP", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = 2
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@TIPSER", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = 0
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@ID", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = IdPagare
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@idcompania", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = 0
        CMD.Parameters.Add(PRM4)
        Dim SqlReader As SqlDataReader

        'Dim PRM5 As New SqlParameter("@Servicio", SqlDbType.Int)
        'PRM5.Direction = ParameterDirection.Input
        'PRM5.Value = Tipo_Servicio
        'CMD.Parameters.Add(PRM5)

        Try
            CON.Open()
            SqlReader = CMD.ExecuteReader

            While (SqlReader.Read)
                Me.TipServCombo.SelectedValue = SqlReader(0).ToString
                ' Me.AparatoCombo.SelectedValue = SqlReader(1).ToString
                'Me.CostoText.Text = SqlReader(2).ToString
                'Me.CostoText.Text = CType(Me.CostoText.Text, Double)
                'Me.RentaPrincipalText.Text = SqlReader(3).ToString
                'Me.RentaPrincipalText.Text = CType(Me.RentaPrincipalText.Text, Double)
                'Me.RentaAdicionalText.Text = SqlReader(4).ToString
                'Me.RentaAdicionalText.Text = CType(Me.RentaAdicionalText.Text, Double)

                Me.ComboBoxCompanias.SelectedValue = SqlReader(2).ToString
                'Me.RentaAdicionalText2.Text = CType(SqlReader(7).ToString, Double)
                'Me.RentaAdicionalText3.Text = CType(SqlReader(8).ToString, Double)
                Me.CBoxSERVICIO.Text = SqlReader(1).ToString
                'If SqlReader(5).ToString = "False" Then
                '    NormalRadioBut.Checked = True
                'Else
                '    HDRadioBut.Checked = True
                'End If
                'ComBoxServicio.Text = SqlReader(9).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub TipServCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipServCombo.SelectedIndexChanged
        If Me.TipServCombo.SelectedValue = 2 Then
            'Me.NormalRadioBut.Visible = False
            'Me.HDRadioBut.Visible = False
            'Me.CMBLabel3.Visible = False
            ''CMBLabel7.Visible = False
            RentaAdicionalText.Visible = False
            RentaAdicionalText2.Visible = False
            RentaAdicionalText3.Visible = False
            LabelRenta0.Visible = True
            LabelRenta1.Visible = False
            LabelRenta2.Visible = False
            LabelRenta3.Visible = False
            ''Label2.Visible = False
            ''Label3.Visible = False
            CMBLabel4.Text = "Aparato"
        Else
            'Me.NormalRadioBut.Visible = True
            'Me.HDRadioBut.Visible = True
            'Me.CMBLabel3.Visible = True
            ''CMBLabel7.Visible = True
            RentaAdicionalText.Visible = True
            RentaAdicionalText2.Visible = True
            RentaAdicionalText3.Visible = True
            LabelRenta1.Visible = True
            LabelRenta2.Visible = True
            LabelRenta3.Visible = True
            ''Label2.Visible = True
            ''Label3.Visible = True
            CMBLabel4.Text = "Setup Box"
        End If
        If TipServCombo.SelectedIndex >= 0 Then
            LlenaServicios(TipServCombo.SelectedValue)
            'MuestraMarcasAparatosPagare()
            'MuestraMarcasAparatosPagareList()
            ConsultaEquiposMarcadosList()
        End If

    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub



    Private Sub Guardar()
        If ValidaAparatosSeleccionados() = False Then
            MsgBox("Seleccione en Equipo por favor")
            Exit Sub
        End If
        If TipServCombo.SelectedIndex < 0 Then
            MsgBox("Seleccione el Tipo de Servicio por favor")
            Exit Sub
        End If
        If CBoxSERVICIO.SelectedIndex < 0 Then
            MsgBox("Seleccione el Servicio por favor")
            Exit Sub
        End If
        If ComboBoxCompanias.SelectedValue = 0 Then
            MsgBox("Seleccione una Compañía")
            Exit Sub
        End If
        Dim MsjTxt As String = ""
        MsjTxt = SP_GuardaCostosPagareNew(GloIdCostoAP, TipServCombo.SelectedValue, OpPagare, CBoxSERVICIO.SelectedValue)
        If MsjTxt.Length > 0 Then
            MsgBox(MsjTxt, MsgBoxStyle.Information)
        End If
        If OpPagare = "N" And MsjTxt.Length = 0 Then
            Panel4.Enabled = True
            OpPagare = "M"
        End If
        Limpia()
        ToolStripButton2.Enabled = True

    End Sub


    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        Guardar()
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Seleccione una Compañía")
        '    Exit Sub
        'End If
        'If Me.CostoText.TextLength = 0 Then
        '    MsgBox("Capture el costo del Aparato", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If
        'If Me.RentaPrincipalText.TextLength = 0 Then
        '    MsgBox("Capture el costo de la renta mensual del Aparato Principal", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If
        'If Me.RentaAdicionalText.TextLength = 0 And TipServCombo.SelectedValue = 3 Then
        '    MsgBox("Capture el costo de la renta mensual del Aparato Adicional", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If
        'If Me.AparatoCombo.SelectedValue > 0 Then
        '    GuardaCostosPagare() '
        '    BndActualizaPagare = True
        '    Me.Close()
        'Else
        '    MsgBox("No existen aparatos en la Lista", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If
    End Sub

    Private Sub NormalRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NormalRadioBut.CheckedChanged
        'If Me.Visible = True Then
        '    Me.AparatoCombo.SelectedValue = 0
        '    MuestraMarcasAparatosPagare()
        'End If
    End Sub

    Private Sub HDRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HDRadioBut.CheckedChanged
        'If Me.Visible = True Then
        '    Me.AparatoCombo.SelectedValue = 0
        '    MuestraMarcasAparatosPagare()
        'End If
    End Sub

    Private Sub CMBLabel7_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub MuestraServiciosRentas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, TipServCombo.SelectedValue)
            CBoxSERVICIO.DataSource = BaseII.ConsultaDT("MuestraServiciosRentas")
            CBoxSERVICIO.DisplayMember = "Servicio"
            CBoxSERVICIO.ValueMember = "CLV_SERVICIO"

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub LlenaGridVigencias(LocIdCostoAP As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdCostoAP", SqlDbType.BigInt, LocIdCostoAP)
            DataGridView1.DataSource = BaseII.ConsultaDT("SP_ConsultaRel_CostoArticuloPagareConVigencias")
            If TipServCombo.SelectedValue = 3 Then
                DataGridView1.Columns(0).HeaderText = "IdRel"
                DataGridView1.Columns(0).Width = 5
                DataGridView1.Columns(1).HeaderText = "IdCosto"
                DataGridView1.Columns(1).Width = 5
                DataGridView1.Columns(2).HeaderText = "Día Inicial"
                DataGridView1.Columns(2).Width = 100
                DataGridView1.Columns(3).HeaderText = "Día Final"
                DataGridView1.Columns(3).Width = 100
                DataGridView1.Columns(4).HeaderText = "Inicial Mes/Año"
                DataGridView1.Columns(4).Width = 100
                DataGridView1.Columns(5).HeaderText = "Final Mes/Año"
                DataGridView1.Columns(5).Width = 100
                DataGridView1.Columns(6).HeaderText = "Costo Equipo"
                DataGridView1.Columns(6).Width = 100
                DataGridView1.Columns(7).HeaderText = "Renta Principal"
                DataGridView1.Columns(7).Width = 100
                DataGridView1.Columns(8).HeaderText = "Renta Adic. 1ra."
                DataGridView1.Columns(8).Width = 100
                DataGridView1.Columns(9).HeaderText = "Renta Adic. 2ra."
                DataGridView1.Columns(9).Width = 100
                DataGridView1.Columns(10).HeaderText = "Renta Adic. 3ra."
                DataGridView1.Columns(10).Width = 100
            Else
                DataGridView1.Columns(0).HeaderText = "IdRel"
                DataGridView1.Columns(0).Width = 5
                DataGridView1.Columns(1).HeaderText = "IdCosto"
                DataGridView1.Columns(1).Width = 5
                DataGridView1.Columns(2).HeaderText = "Día Inicial"
                DataGridView1.Columns(2).Width = 100
                DataGridView1.Columns(3).HeaderText = "Día Final"
                DataGridView1.Columns(3).Width = 100
                DataGridView1.Columns(4).HeaderText = "Inicial Mes/Año"
                DataGridView1.Columns(4).Width = 100
                DataGridView1.Columns(5).HeaderText = "Final Mes/Año"
                DataGridView1.Columns(5).Width = 100
                DataGridView1.Columns(6).HeaderText = "Costo Equipo"
                DataGridView1.Columns(6).Width = 100
                DataGridView1.Columns(7).HeaderText = "Renta Principal"
                DataGridView1.Columns(7).Width = 100
                DataGridView1.Columns(8).HeaderText = ""
                DataGridView1.Columns(8).Width = 5
                DataGridView1.Columns(9).HeaderText = ""
                DataGridView1.Columns(9).Width = 5
                DataGridView1.Columns(10).HeaderText = ""
                DataGridView1.Columns(10).Width = 5
            End If
            If DataGridView1.Rows.Count > 0 Then
                GloIdRelVigencia = Me.DataGridView1.SelectedCells(0).Value
                ToolStripButton4.Enabled = True
                ToolStripButton2.Enabled = True
                Panel2.Enabled = True
            Else
                Panel2.Enabled = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub


    Private Sub ComBoxServicio_SelectedValueChanged(sender As Object, e As EventArgs) Handles CBoxSERVICIO.SelectedValueChanged


    End Sub

    Private Sub ComBoxServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBoxSERVICIO.SelectedIndexChanged

    End Sub

    Private Sub AparatoCombo_KeyDown(sender As Object, e As KeyEventArgs)
        'If e.KeyCode = Keys.Enter Then
        '    ListBoxSetupbox.Items.Add(AparatoCombo.SelectedText)
        'End If
    End Sub

    Private Sub AparatoCombo_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub CheckedListBox2_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub GuardaEquiposSeleccinadosAll()
        Dim indexChecked As Integer
        Dim itemChecked As Object
        Const quote As String = """"

        For Each indexChecked In CheckedListBox1.CheckedIndices
            ' The indexChecked variable contains the index of the item.
            'MsgBox("Index#: " + indexChecked.ToString() + ", is checked. Checked state is:" + _
            '                CheckedListBox1.GetItemCheckState(indexChecked).ToString() + ".")
            'MsgBox(MyList(indexChecked).ToString)
            InsertaRelEquipos(CLng(MyList(indexChecked).ToString()))

        Next

        'For Each indexChecked In CheckedListBox1.in
        '    ' The indexChecked variable contains the index of the item.
        '    MsgBox("Index#: " + MyList(indexChecked).ToString + " " + indexChecked.ToString() + ", is checked. Checked state is:" + _
        '                    CheckedListBox1.GetItemCheckState(indexChecked).ToString() + ".")

        'Next

        'For Each itemChecked In CheckedListBox1.CheckedItems

        '    ' Use the IndexOf method to get the index of an item.
        '    MsgBox("Item with title: " + quote + itemChecked.ToString() + quote + _
        '                    ", is checked. Checked state is: " + _
        '                    CheckedListBox1.GetItemCheckState(CheckedListBox1.Items.IndexOf(itemChecked)).ToString() + ".")


        'Next

    End Sub

    Public Function ValidaAparatosSeleccionados() As Boolean
        Dim indexChecked As Integer
        Dim itemChecked As Object
        Const quote As String = """"
        ValidaAparatosSeleccionados = False
        For Each indexChecked In CheckedListBox1.CheckedIndices
            ' The indexChecked variable contains the index of the item.
            'MsgBox("Index#: " + indexChecked.ToString() + ", is checked. Checked state is:" + _
            '                CheckedListBox1.GetItemCheckState(indexChecked).ToString() + ".")
            'MsgBox(MyList(indexChecked).ToString)

            'MyList(indexChecked).ToString()
            ValidaAparatosSeleccionados = True
        Next

        'For Each indexChecked In CheckedListBox1.in
        '    ' The indexChecked variable contains the index of the item.
        '    MsgBox("Index#: " + MyList(indexChecked).ToString + " " + indexChecked.ToString() + ", is checked. Checked state is:" + _
        '                    CheckedListBox1.GetItemCheckState(indexChecked).ToString() + ".")

        'Next

        'For Each itemChecked In CheckedListBox1.CheckedItems

        '    ' Use the IndexOf method to get the index of an item.
        '    MsgBox("Item with title: " + quote + itemChecked.ToString() + quote + _
        '                    ", is checked. Checked state is: " + _
        '                    CheckedListBox1.GetItemCheckState(CheckedListBox1.Items.IndexOf(itemChecked)).ToString() + ".")


        'Next

    End Function




    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck

    End Sub

    Private Sub CheckedListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox1.SelectedIndexChanged

    End Sub

    Private Sub ComboBoxIdArticulo_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub LabelRentaPrincipal_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub RentaAdicionalText_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub RentaAdicionalText2_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub RentaAdicionalText3_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub RentaPrincipalText_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Limpia()
        CostoText.Text = 0
        RentaPrincipalText.Text = 0
        RentaAdicionalText.Text = 0
        RentaAdicionalText2.Text = 0
        RentaAdicionalText3.Text = 0
        Me.DIA_INICIALNumericUpDown.Value = 1
        Me.DIA_FINALNumericUpDown.Minimum = 1
        Me.DIA_FINALNumericUpDown.Value = 1
        Me.Periodo_InicialDateTimePicker.MinDate = CDate(Now)
        Me.Periodo_FinalDateTimePicker.MinDate = CDate(Now)
        Me.Periodo_InicialDateTimePicker.Value = CDate(Now)
        Me.Periodo_FinalDateTimePicker.Value = CDate(Now)
    End Sub
    Private Sub Nuevo()
        If OpPagare = "N" Then
            Guardar()
        End If
        If GloIdCostoAP > 0 Then
            LocMoveOpcion = "N"
            Panel2.Enabled = False
            Panel3.Enabled = True
            Limpia()
            BindingNavigatorAddNewItem.Enabled = False
            ToolStripButton4.Enabled = False

            CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = True
            ToolStripButton2.Enabled = False
            ToolStripButton3.Enabled = True
        End If
    End Sub

    Private Sub Eliminar()
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            GloIdRelVigencia = Me.DataGridView1.SelectedCells(0).Value
            If GloIdRelVigencia > 0 Then
                ToolStripButton4.Enabled = True
                ToolStripButton2.Enabled = True
                LocMoveOpcion = "M"
                Panel2.Enabled = False
                Panel3.Enabled = True
                Limpia()
                BindingNavigatorAddNewItem.Enabled = True
                ToolStripButton4.Enabled = False
                CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
                ToolStripButton2.Enabled = False
                ToolStripButton3.Enabled = True
                Dim LocTxt As String = ""
                LocTxt = SP_EliminarRel_CostoArticuloPagareConVigenciasIndividual(GloIdCostoAP, GloIdRelVigencia)
                If LocTxt.ToString.Length > 0 Then
                    MsgBox(LocTxt.ToString, MsgBoxStyle.Information)
                End If
                GloIdRelVigencia = 0
                LlenaGridVigencias(GloIdCostoAP)
                Panel2.Enabled = True
            End If
        Else
            GloIdRelVigencia = 0
        End If

    End Sub

    Private Sub Modificar()
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            GloIdRelVigencia = Me.DataGridView1.SelectedCells(0).Value
            If GloIdRelVigencia > 0 Then


                ToolStripButton4.Enabled = True
                ToolStripButton2.Enabled = True
                LocMoveOpcion = "M"
                Panel2.Enabled = False
                Panel3.Enabled = True
                Limpia()
                BindingNavigatorAddNewItem.Enabled = False
                ToolStripButton4.Enabled = False
                CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = True
                'ToolStripButton2.Enabled = False
                ToolStripButton3.Enabled = True

                SP_ConsultaRel_CostoArticuloPagareConVigenciasIndividual(GloIdCostoAP, GloIdRelVigencia)
            End If
        Else
            GloIdRelVigencia = 0
        End If


    End Sub


    Private Sub Periodo_InicialDateTimePicker_ValueChanged(sender As Object, e As EventArgs)
        Me.Periodo_FinalDateTimePicker.MinDate = Me.Periodo_InicialDateTimePicker.Value
    End Sub


    Private Sub BindingNavigatorAddNewItem_Click_1(sender As Object, e As EventArgs) Handles BindingNavigatorAddNewItem.Click
        Nuevo()
    End Sub

    Private Sub CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Click
        Dim LocTxt As String = ""
        If LocMoveOpcion = "N" Then
            ''InsertarCostoPagare()

            LocTxt = ""
            LocTxt = Sp_ValidaVigencias(GloIdCostoAP, 0, DIA_INICIALNumericUpDown.Value, DIA_FINALNumericUpDown.Value, Periodo_InicialDateTimePicker.Value, Periodo_FinalDateTimePicker.Value)
            If LocTxt.ToString.Length > 0 Then
                MsgBox(LocTxt, MsgBoxStyle.Information)
            Else
                LocTxt = ""
                LocTxt = InsertarCostoPagare(GloIdCostoAP, DIA_INICIALNumericUpDown.Value, DIA_FINALNumericUpDown.Value, Periodo_InicialDateTimePicker.Value, Periodo_FinalDateTimePicker.Value,
    CostoText.Text, RentaPrincipalText.Text, RentaAdicionalText.Text, RentaAdicionalText2.Text, RentaAdicionalText3.Text)
                If LocTxt.ToString.Length > 0 Then
                    MsgBox(LocTxt, MsgBoxStyle.Information)
                Else
                    LlenaGridVigencias(GloIdCostoAP)
                    Panel3.Enabled = False
                    BindingNavigatorAddNewItem.Enabled = True
                    CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
                    ToolStripButton3.Enabled = False
                    'ToolStripButton2.Enabled = False

                    Panel2.Enabled = True
                    Panel4.Enabled = True
                End If
            End If

        ElseIf LocMoveOpcion = "M" And GloIdRelVigencia > 0 Then

            LocTxt = ""
            LocTxt = Sp_ValidaVigencias(GloIdCostoAP, GloIdRelVigencia, DIA_INICIALNumericUpDown.Value, DIA_FINALNumericUpDown.Value, Periodo_InicialDateTimePicker.Value, Periodo_FinalDateTimePicker.Value)

            If LocTxt.ToString.Length > 0 Then
                MsgBox(LocTxt, MsgBoxStyle.Information)
            Else
                LocTxt = ""
                LocTxt = ModificarCostoPagare(GloIdCostoAP, GloIdRelVigencia, DIA_INICIALNumericUpDown.Value, DIA_FINALNumericUpDown.Value, Periodo_InicialDateTimePicker.Value, Periodo_FinalDateTimePicker.Value,
    CostoText.Text, RentaPrincipalText.Text, RentaAdicionalText.Text, RentaAdicionalText2.Text, RentaAdicionalText3.Text)
                If LocTxt.ToString.Length > 0 Then
                    MsgBox(LocTxt, MsgBoxStyle.Information)
                Else
                    LlenaGridVigencias(GloIdCostoAP)
                    Panel3.Enabled = False
                    BindingNavigatorAddNewItem.Enabled = True
                    CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
                    ToolStripButton3.Enabled = False
                    ToolStripButton2.Enabled = False

                    Panel2.Enabled = True
                    Panel4.Enabled = True
                End If
            End If
        End If
    End Sub

    Private Sub LimpiaSeleccionEquipos()


        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()

        Dim comando As New SqlClient.SqlCommand("SP_LimpiaInsertaRel_CostoArticuloPagareConAparatos", conexion)

        Try

            'GENERAMOS EL CÓDIGO DE BARRAS DE ESTE ESTADO DE CUENTA


            'GUADAMOS EL CÓDIGO DE BARRAS GENERADO
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@IdCostoAP", SqlDbType.BigInt)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = GloIdCostoAP


            comando.ExecuteNonQuery()


            GC.Collect()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try


    End Sub


    Private Sub InsertaRelEquipos(LocIdEquipo As Long)


        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()

        Dim comando As New SqlClient.SqlCommand("SP_InsertaRel_CostoArticuloPagareConAparatos", conexion)

        Try

            'GENERAMOS EL CÓDIGO DE BARRAS DE ESTE ESTADO DE CUENTA


            'GUADAMOS EL CÓDIGO DE BARRAS GENERADO
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@IdCostoAP", SqlDbType.BigInt)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = GloIdCostoAP

            comando.Parameters.Add("@IdArticulo", SqlDbType.BigInt)
            comando.Parameters(1).Direction = ParameterDirection.Input
            comando.Parameters(1).Value = LocIdEquipo


            comando.ExecuteNonQuery()


            GC.Collect()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try


    End Sub


    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            GloIdRelVigencia = Me.DataGridView1.SelectedCells(0).Value
            ToolStripButton4.Enabled = True
            ToolStripButton2.Enabled = True
        Else
            GloIdRelVigencia = 0
        End If
    End Sub

    Private Sub ToolStripButton4_Click(sender As Object, e As EventArgs) Handles ToolStripButton4.Click
        Modificar()
    End Sub

    Private Sub cancelar()
        Limpia()
        BindingNavigatorAddNewItem.Enabled = True
        ToolStripButton4.Enabled = True
        ToolStripButton2.Enabled = True
        CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
        Panel2.Enabled = True
        Panel3.Enabled = False
    End Sub
    Private Sub ToolStripButton3_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click
        cancelar()
    End Sub

    Private Sub Periodo_InicialDateTimePicker_ValueChanged_1(sender As Object, e As EventArgs) Handles Periodo_InicialDateTimePicker.ValueChanged

    End Sub

    Private Sub Periodo_FinalDateTimePicker_ValueChanged(sender As Object, e As EventArgs) Handles Periodo_FinalDateTimePicker.ValueChanged

    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        Eliminar()
    End Sub

    Private Sub DIA_INICIALNumericUpDown_ValueChanged(sender As Object, e As EventArgs) Handles DIA_INICIALNumericUpDown.ValueChanged
        DIA_FINALNumericUpDown.Minimum = DIA_INICIALNumericUpDown.Value
    End Sub

    Private Sub Label2_Click(sender As System.Object, e As System.EventArgs) Handles Label2.Click

    End Sub


End Class