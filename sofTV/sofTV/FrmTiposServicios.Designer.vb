﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTiposServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_TipSerLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTiposServicios))
        Me.Button5 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ConceptoTextBox1 = New System.Windows.Forms.TextBox
        Me.CONTipServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.CONTipServTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONTipServTableAdapter
        Me.CONTipServBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONTipServBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.IEPSCheckBox = New System.Windows.Forms.CheckBox
        Clv_TipSerLabel = New System.Windows.Forms.Label
        ConceptoLabel = New System.Windows.Forms.Label
        Label1 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.CONTipServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONTipServBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTipServBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_TipSerLabel
        '
        Clv_TipSerLabel.AutoSize = True
        Clv_TipSerLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipSerLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TipSerLabel.Location = New System.Drawing.Point(64, 59)
        Clv_TipSerLabel.Name = "Clv_TipSerLabel"
        Clv_TipSerLabel.Size = New System.Drawing.Size(50, 15)
        Clv_TipSerLabel.TabIndex = 0
        Clv_TipSerLabel.Text = "Clave :"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ConceptoLabel.Location = New System.Drawing.Point(23, 97)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(91, 15)
        ConceptoLabel.TabIndex = 2
        ConceptoLabel.Text = "Descripción :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(48, 303)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(93, 15)
        Label1.TabIndex = 4
        Label1.Text = "Deshabilitar :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(508, 241)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 130
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.IEPSCheckBox)
        Me.Panel1.Controls.Add(Me.ConceptoTextBox1)
        Me.Panel1.Controls.Add(Clv_TipSerLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel1.Controls.Add(ConceptoLabel)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Panel1.Location = New System.Drawing.Point(12, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(632, 199)
        Me.Panel1.TabIndex = 17
        '
        'ConceptoTextBox1
        '
        Me.ConceptoTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ConceptoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipServBindingSource, "Concepto", True))
        Me.ConceptoTextBox1.Location = New System.Drawing.Point(120, 97)
        Me.ConceptoTextBox1.Name = "ConceptoTextBox1"
        Me.ConceptoTextBox1.Size = New System.Drawing.Size(445, 21)
        Me.ConceptoTextBox1.TabIndex = 100
        '
        'CONTipServBindingSource
        '
        Me.CONTipServBindingSource.DataMember = "CONTipServ"
        Me.CONTipServBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipServBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(120, 57)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_TipSerTextBox.TabIndex = 1
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CONTipServBindingSource, "habilitar", True))
        Me.CheckBox1.Location = New System.Drawing.Point(147, 304)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 110
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CONTipServTableAdapter
        '
        Me.CONTipServTableAdapter.ClearBeforeFill = True
        '
        'CONTipServBindingNavigator
        '
        Me.CONTipServBindingNavigator.AddNewItem = Nothing
        Me.CONTipServBindingNavigator.BindingSource = Me.CONTipServBindingSource
        Me.CONTipServBindingNavigator.CountItem = Nothing
        Me.CONTipServBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONTipServBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTipServBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONTipServBindingNavigatorSaveItem})
        Me.CONTipServBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONTipServBindingNavigator.MoveFirstItem = Nothing
        Me.CONTipServBindingNavigator.MoveLastItem = Nothing
        Me.CONTipServBindingNavigator.MoveNextItem = Nothing
        Me.CONTipServBindingNavigator.MovePreviousItem = Nothing
        Me.CONTipServBindingNavigator.Name = "CONTipServBindingNavigator"
        Me.CONTipServBindingNavigator.PositionItem = Nothing
        Me.CONTipServBindingNavigator.Size = New System.Drawing.Size(660, 25)
        Me.CONTipServBindingNavigator.TabIndex = 120
        Me.CONTipServBindingNavigator.TabStop = True
        Me.CONTipServBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(68, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Cancelar"
        '
        'CONTipServBindingNavigatorSaveItem
        '
        Me.CONTipServBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONTipServBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONTipServBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONTipServBindingNavigatorSaveItem.Name = "CONTipServBindingNavigatorSaveItem"
        Me.CONTipServBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.CONTipServBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'IEPSCheckBox
        '
        Me.IEPSCheckBox.AutoSize = True
        Me.IEPSCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.IEPSCheckBox.ForeColor = System.Drawing.Color.DarkRed
        Me.IEPSCheckBox.Location = New System.Drawing.Point(120, 144)
        Me.IEPSCheckBox.Name = "IEPSCheckBox"
        Me.IEPSCheckBox.Size = New System.Drawing.Size(97, 19)
        Me.IEPSCheckBox.TabIndex = 102
        Me.IEPSCheckBox.Text = "Aplica IEPS"
        Me.IEPSCheckBox.UseVisualStyleBackColor = True
        '
        'FrmTiposServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(660, 289)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.CONTipServBindingNavigator)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CheckBox1)
        Me.Name = "FrmTiposServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tipo de Servicio"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONTipServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONTipServBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTipServBindingNavigator.ResumeLayout(False)
        Me.CONTipServBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONTipServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONTipServTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONTipServTableAdapter
    Friend WithEvents CONTipServBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONTipServBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents IEPSCheckBox As System.Windows.Forms.CheckBox
End Class
