﻿Public Class FrmVelInternet
    Private Sub damePolitica()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@Clv_equivalente", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.CreateMyParameter("@VelSub", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.CreateMyParameter("@VelBaj", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.CreateMyParameter("@UnidadSub", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.CreateMyParameter("@UnidadBaj", ParameterDirection.Output, SqlDbType.VarChar, 20)

        BaseII.ProcedimientoOutPut("Sp_damePolitica")
        Clv_EqTextBox.Text = BaseII.dicoPar("@Clv_equivalente").ToString()
        VelBajTextBox.Text = BaseII.dicoPar("@VelBaj").ToString()
        VelSubTextBox.Text = BaseII.dicoPar("@VelSub").ToString()
        ComboBoxUniSub.SelectedValue = BaseII.dicoPar("@UnidadSub").ToString()
        ComboBoxUniBaj.SelectedValue = BaseII.dicoPar("@UnidadBaj").ToString()

    End Sub

    Private Sub llenaComboUnidades()
        'Dim dt As DataTable
        BaseII.limpiaParametros()
        ' dt = BaseII.ConsultaDT("DameUnidadesMedidasDeVelocidad")
        ComboBoxUniSub.DataSource = BaseII.ConsultaDT("DameUnidadesMedidasDeVelocidad")
        ComboBoxUniBaj.DataSource = BaseII.ConsultaDT("DameUnidadesMedidasDeVelocidad")
        

    End Sub



    Private Sub FrmVelInternet_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        llenaComboUnidades()
        If opcion = "C" Or opcion = "M" Then
            damePolitica()
            If opcion = "C" Then
                GroupBox1.Enabled = False
                ToolStrip1.Enabled = False
            End If
        Else
            Clv_EqTextBox.Enabled = True
        End If


    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, gloClave)
        BaseII.Inserta("Sp_eliminaPolitica")
    End Sub

    Private Sub CONMotivoCancelacionBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CONMotivoCancelacionBindingNavigatorSaveItem.Click
        If Len(Clv_EqTextBox.Text) > 0 And Len(VelSubTextBox.Text) > 0 And Len(VelBajTextBox.Text) > 0 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, gloClave)
            BaseII.CreateMyParameter("@Clv_equivalente", SqlDbType.VarChar, Clv_EqTextBox.Text, 20)
            BaseII.CreateMyParameter("@VelSub", SqlDbType.VarChar, VelSubTextBox.Text, 20)
            BaseII.CreateMyParameter("@VelBaj", SqlDbType.VarChar, VelBajTextBox.Text, 20)
            BaseII.CreateMyParameter("@UnidadSub", SqlDbType.VarChar, ComboBoxUniSub.SelectedValue, 20)
            BaseII.CreateMyParameter("@UnidadBaj", SqlDbType.VarChar, ComboBoxUniBaj.SelectedValue, 20)
            BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 200)
            BaseII.ProcedimientoOutPut("Sp_guardaPolitica")
            MsgBox(BaseII.dicoPar("@Msj").ToString())
            GloBnd = True
            Me.Close()
        Else
            MsgBox("Datos incompletos")
            Exit Sub
        End If


    End Sub


    Private Sub VelBajTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles VelBajTextBox.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros 
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub VelSubTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles VelSubTextBox.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros 
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
End Class