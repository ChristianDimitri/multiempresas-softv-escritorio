Imports System.Data.SqlClient
Public Class FrmVisitas
    Private Clave As String = Nothing
    Private Descripcion As String = Nothing
    Private Sub DameDatosBitacora()
        Try
            If LocOpVisita = "M" Then
                Clave = Me.Clv_visitaTextBox.Text
                Descripcion = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub GuardaDatosBitacora(ByVal Op As Integer)
        Try
            If Op = 0 Then
                If LocOpVisita = "M" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Visita", Clave, Me.Clv_visitaTextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Descripci�n", Descripcion, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                ElseIf LocOpVisita = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Nueva Visita", "", "Visita: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                End If
            ElseIf Op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Elimin� Visita", "Visita: " + Me.DescripcionTextBox.Text, "", LocClv_Ciudad)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub COnsulta_CatalogoVisitasBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COnsulta_CatalogoVisitasBindingNavigatorSaveItem.Click
        Dim COn As New SqlConnection(MiConexion)
        COn.Open()
        If Me.DescripcionTextBox.Text = "" Then
            MsgBox("Capture la Descripci�n de la Visita", MsgBoxStyle.Information)
        Else
            Me.Validate()
            Me.COnsulta_CatalogoVisitasBindingSource.EndEdit()
            Me.COnsulta_CatalogoVisitasTableAdapter.Connection = COn
            Me.COnsulta_CatalogoVisitasTableAdapter.Update(Me.ProcedimientosArnoldo2.COnsulta_CatalogoVisitas)
            MsgBox("Se Ha Guardado con Exito", MsgBoxStyle.Information)
            LocBndVisitas = True
            GuardaDatosBitacora(0)
        End If
        COn.Close()
        Me.Close()
    End Sub

    Private Sub consulta(ByVal clv_visita As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.COnsulta_CatalogoVisitasTableAdapter.Connection = CON
        Me.COnsulta_CatalogoVisitasTableAdapter.Fill(Me.ProcedimientosArnoldo2.COnsulta_CatalogoVisitas, clv_visita)
        CON.Close()
    End Sub
    Private Sub FrmVisitas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Select Case LocOpVisita
            Case "N"
                Me.COnsulta_CatalogoVisitasBindingSource.AddNew()
            Case "C"
                Me.Panel1.Enabled = False
                consulta(Locclv_visita)
            Case "M"
                consulta(Locclv_visita)
        End Select
        DameDatosBitacora()
        If LocOpVisita = "N" Or LocOpVisita = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim Con As New SqlConnection(MiConexion)
        Con.Open()
        GuardaDatosBitacora(1)
        Me.COnsulta_CatalogoVisitasTableAdapter.Connection = Con
        Me.COnsulta_CatalogoVisitasTableAdapter.Delete(Locclv_visita)
        MsgBox("Se Elimino con Exito", MsgBoxStyle.Information)
        Con.Close()
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class