
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class Frmareatecnica
    Delegate Sub Reporte(ByVal op As String, ByVal Titulop As String)
    Public customersByCityReport As ReportDocument
    Dim op As String = Nothing
    Public bnd As Boolean = False
    Public bnd2 As Boolean = False
    Dim Titulo As String = Nothing
    Private opreporte As Integer = 0
    Private oClvCompania As Integer = 0

    Private Sub Llena_companias()
        Try


            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelCiudadGeneral")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 1
            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_Ciudad()
        Try
            BaseII.limpiaParametros()
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_ciudad")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"

            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
                locciudad = ComboBoxCiudad.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0", Op7 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim nClv_ciudad As String = "0"
            Dim nClv_calle As String = "0"

            Dim Status As String = Nothing

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If


            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If
            If Me.NOMCIUDAD.Text.Length > 0 Then
                Op6 = "1 "
            End If
            If Me.NOMCALLE.Text.Length > 0 Then
                Op7 = "1"
            End If

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing

            If op = 0 Then
                reportPath = RutaReportes + "\ReporteOrdenesListado.rpt"
                mySelectFormula = "Listado de Ordenes de Servicio"
            ElseIf op = 1 Then
                reportPath = RutaReportes + "\ReporteOrdenes.rpt"
                mySelectFormula = "Orden De Servicio: "
            End If

            '@Clv_OrdenIni bigint
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CLng(Me.NUMINILbl.Text)
            'customersByCityReport.SetParameterValue(9, CLng(Num1))
            ',@Clv_OrdenFin bigint
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CLng(Me.NUMFINLBL.Text)
            'customersByCityReport.SetParameterValue(10, CLng(Num2))
            '',@Fec1Ini Datetime
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            'customersByCityReport.SetParameterValue(11, Fec1Ini)
            '',@Fec1Fin Datetime,
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            'customersByCityReport.SetParameterValue(12, Fec1Fin)
            ''@Fec2Ini Datetime
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            'customersByCityReport.SetParameterValue(13, Fec2Ini)
            '',@Fec2Fin Datetime
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            'customersByCityReport.SetParameterValue(14, Fec2Fin)
            '',@Clv_Trabajo int
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            'customersByCityReport.SetParameterValue(15, nclv_trabajo)
            '',@Clv_Colonia int
            'If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            'customersByCityReport.SetParameterValue(16, nClv_colonia)
            '',@OpOrden int
            'customersByCityReport.SetParameterValue(17, OpOrdenar)
            'If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            'If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)


            Dim DS As New DataSet


            BaseII.limpiaParametros()
            If op = 0 Then
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CObj(0))
                BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, CShort(Op1))
                BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, CShort(Op2))
                BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, CShort(Op3))
                BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, CShort(Op4))
                BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, CShort(Op5))
                BaseII.CreateMyParameter("@op6", SqlDbType.SmallInt, CShort(Op6))
                BaseII.CreateMyParameter("@op7", SqlDbType.SmallInt, CShort(Op7))
                BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, CByte(StatusPen))
                BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, CByte(StatusEje))
                BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, CByte(StatusVis))
                BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, CLng(Num1))
                BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, CLng(Num2))
                BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, CObj(Fec1Ini))
                BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, CObj(Fec1Fin))
                BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, CObj(Fec2Ini))
                BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, CObj(Fec2Fin))
                BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, CLng(nclv_trabajo))
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, CLng(nClv_colonia))
                BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, CLng(1))
                BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, CLng(nClv_calle))
                BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, CLng(OpOrdenar))
                BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CLng(0))
                BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, 1)
                BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            ElseIf op = 1 Then
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CObj(0))
                BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, CShort(Op1))
                BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, CShort(Op2))
                BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, CShort(Op3))
                BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, CShort(Op4))
                BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, CShort(Op5))
                BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, CByte(StatusPen))
                BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, CByte(StatusEje))
                BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, CByte(StatusVis))
                BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, CLng(Num1))
                BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, CLng(Num2))
                BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, CObj(Fec1Ini))
                BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, CObj(Fec1Fin))
                BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, CObj(Fec2Ini))
                BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, CObj(Fec2Fin))
                BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, CLng(nclv_trabajo))
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, CLng(nClv_colonia))
                BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, CLng(OpOrdenar))
                BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, 1)
                BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, locciudad)
                BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            End If

            Dim listatablas As New List(Of String)

            If op = 0 Then
                listatablas.Add("ReporteAreaTecnicaOrdSer_Prueba")
                'listatablas.Add("CALLES")
                'listatablas.Add("CLIENTES")
                'listatablas.Add("COLONIAS")
                'listatablas.Add("DetOrdSer")
                'listatablas.Add("Rel_Contrato_NoInt")
                DS = BaseII.ConsultaDS("ReporteOrdenesListado", listatablas)
            ElseIf op = 1 Then
                listatablas.Add("ReporteOrdSer")
                listatablas.Add("Comentarios_DetalleOrden")
                listatablas.Add("DameDatosGenerales_2")
                listatablas.Add("DetOrdSer")
                listatablas.Add("Trabajos")
                listatablas.Add("ClientesConElMismoPoste")
                DS = BaseII.ConsultaDS("ReporteOrdSer", listatablas)
            End If

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(DS)

            If op = 0 Then
                customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            ElseIf op = 1 Then
                customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            End If

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {op, Titulo})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If

            customersByCityReport = Nothing
            bnd = True
            
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Frmareatecnica_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBusca_NumOrden = True Then
            GloBusca_NumOrden = False
            Me.NUMFINLBL.Text = GLONUMCLV_ORDEN_FIN
            Me.NUMINILbl.Text = GLONUMCLV_ORDEN_INI
            GLONUMCLV_ORDEN_FIN = 0
            GLONUMCLV_ORDEN_INI = 0
        End If
        If op = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "2" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECEJEINI.Text = GloFecha_Ini
            Me.FECEJEFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloNumClv_Trabajo
            Me.NOMTRABAJO.Text = GLONOMTRABAJO
        End If
        If GlobndClv_Colonia = True Then
            GlobndClv_Colonia = False
            '  Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If
        If GlobndClv_Ciudad = True Then
            GlobndClv_Ciudad = False
            Me.NOMCIUDAD.Text = GLONOMCIUDAD
        End If
        If GlobndClv_Calle = True Then
            GlobndClv_Calle = False
            Me.NOMCALLE.Text = GLONOMCALLE
        End If
        If bndjano = 1 Then
            Me.BackgroundWorker1.RunWorkerAsync()
            PantallaProcesando.Show()
            bndjano = 0
        End If
        


    End Sub





    Private Sub Frmareatecnica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        varfrmselcompania = ""
        colorea(Me, Me.Name)

        Llena_Ciudad()
        Llena_companias()
        DameClv_Session()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub LIMPIA()
        Me.CheckBox1.CheckState = CheckState.Unchecked
        Me.CheckBox2.CheckState = CheckState.Unchecked
        Me.CheckBox3.CheckState = CheckState.Unchecked
        Me.CheckBox5.CheckState = CheckState.Unchecked
        Me.CheckBox4.CheckState = CheckState.Unchecked
        Me.NUMFINLBL.Text = 0
        Me.NUMINILbl.Text = 0
        Me.FECSOLFIN.Text = ""
        Me.FECSOLINI.Text = ""
        Me.FECEJEFIN.Text = ""
        Me.FECEJEINI.Text = ""
        Me.CLV_TRABAJO.Text = 0
        Me.NOMTRABAJO.Text = ""
        Me.CLV_COLONIA.Text = 0
        Me.NOMCOLONIA.Text = ""
        Me.NOMCIUDAD.Text = ""
        Me.NOMCALLE.Text = ""
    End Sub


    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            GloPendientes = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloPendientes = 0
        End If
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            GloEjecutadas = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloEjecutadas = 0
        End If
    End Sub

    Private Sub VisitaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisitaCheckBox.CheckedChanged
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            GloVisita = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
        Else
            GloVisita = 0
        End If
    End Sub



    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            op = "0"
            GloIdCompania = ComboBoxCompanias.SelectedValue
            FrmSelOrdSer.Show()
        Else
            Me.NUMINILbl.Text = 0
            Me.NUMFINLBL.Text = 0
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            op = "2"
            FrmSelFechas.Show()
        Else
            Me.FECEJEINI.Text = ""
            Me.FECEJEFIN.Text = ""
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            op = "3"
            FrmSelTrabajo.Show()
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            ' FrmSelColonia.Show()
            FrmtblSeleccionColonia.Show()
        Else
            ' Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
            ELIMINAtblSeleccionColonia(eClv_Session, 1, 0)
        End If
    End Sub
    Private Sub ELIMINAtblSeleccionColonia(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Colonia As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.Inserta("ELIMINAtblSeleccionColonia")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        

        'If ComboBoxComExit Sub
        'End Ifpanias.SelectedIndex <= 0 Then

        '    MsgBox("Seleccione la Compa��a", MsgBoxStyle.Information, "Importante")
        '    
        'locciudad = ComboBoxCiudad.SelectedValue
        opreporte = 1
        varfrmselcompania = "normal"
        FrmSelCiudadJ.Show()
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'If ComboBoxCompanias.SelectedIndex <= 0 Then

        '    MsgBox("Seleccione la Compa��a", MsgBoxStyle.Information, "Importante")
        '    Exit Sub
        'End If
        varfrmselcompania = "normal"
        opreporte = 2
        bndjano = 0
        FrmSelCiudadJ.Show()
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            Select Case opreporte
                Case 1
                    ConfigureCrystalReports(0, "")
                Case 2
                    ConfigureCrystalReports(1, "")
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            op = "6"
            FrmtblSeleccionCiudad.Show()
        Else
            Me.NOMCIUDAD.Text = ""
            ELIMINAtblSeleccionCiudad(eClv_Session, 1, 0)
        End If
    End Sub
    Private Sub ELIMINAtblSeleccionCiudad(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Ciudad As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_Ciudad)
        BaseII.Inserta("ELIMINAtblSeleccionCiudad")
    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If Me.CheckBox7.CheckState = CheckState.Checked Then
            op = "7"
            FrmtblSeleccionCalle.Show()
        Else
            Me.NOMCALLE.Text = ""
            ELIMINAtblSeleccionCalle(eClv_Session, 1, 0)
        End If
    End Sub
    Private Sub ELIMINAtblSeleccionCalle(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Calle As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, Clv_Calle)
        BaseII.Inserta("ELIMINAtblSeleccionCalle")
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged

    End Sub

    Private Sub ComboBoxCompanias_SelectedValueChanged(sender As Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedValueChanged
        Try

        
            oClvCompania = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click

    End Sub
    Dim locciudad As Integer = 0
    Private Sub ComboBoxCiudad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        Try
            'locciudad = ComboBoxCiudad.SelectedValue
            Llena_companias()
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Frmareatecnica_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        
    End Sub
End Class