Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Public Class ImpresionEstadosDeCuenta2
    Private customersByCityReport As ReportDocument
    Private Impresora As String = Nothing
    Private a As Integer = Nothing
    Delegate Sub Reporte(ByVal contrato As Integer, ByVal contrato1 As Long, ByVal op As Integer)
    Private eRes As Integer = 0
    Private eMsg As String = String.Empty
    Private Checa_Si_Imprime As Integer = Nothing


    Private Sub ImpresionEstadosDeCuenta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 And LocbndEdoCuentaLog = 1 Then
            Me.TxtContratoIni.Text = GLOCONTRATOSEL
            LocbndEdoCuentaLog = 0
            GLOCONTRATOSEL = 0
        ElseIf GLOCONTRATOSEL > 0 And LocbndEdoCuentaLog = 2 Then
            Me.TxtContratoFin.Text = GLOCONTRATOSEL
            LocbndEdoCuentaLog = 0
            GLOCONTRATOSEL = 0
        ElseIf GLOCONTRATOSEL = 0 And LocbndEdoCuentaLog = 2 Then
            Me.TxtContratoFin.Text = GLOCONTRATOSEL
            LocbndEdoCuentaLog = 0
        ElseIf GLOCONTRATOSEL = 0 And LocbndEdoCuentaLog = 1 Then
            Me.TxtContratoIni.Text = GLOCONTRATOSEL
            LocbndEdoCuentaLog = 0
        End If
    End Sub

    Private Sub ImpresionEstadosDeCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LocbndEdoCuentaLog = 0
        colorea(Me, Me.Name)
        Valida_Si_Imprime_Edo_Cuenta()
        If Checa_Si_Imprime = 0 Then
            Me.CMBBtnImprimir.Text = "&Comenzar Proceso"
        End If
    End Sub

    Private Sub CMBBtnBusca1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnBusca1.Click
        GLOCONTRATOSEL = 0
        LocbndEdoCuentaLog = 1
        FrmSelCliente.Show()
    End Sub

    Private Sub CMBBtnBusca2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnBusca2.Click
        GLOCONTRATOSEL = 0
        LocbndEdoCuentaLog = 2
        FrmSelCliente.Show()
    End Sub

    Private Sub CMBBtnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnSalir.Click
        Me.Close()
    End Sub
    Private Sub Reporte_edo_cuenta(ByVal contrato As Long, ByVal contrato1 As Long, ByVal op As Integer)
        Try

            'customersByCityReport = New EstadoDeCuentaFinal
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            ' Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            ''customersByCityReport.DataSourceConnections(GloServerName, GloDatabaseName).SetConnection(GloServerName, GloDatabaseName, False, GloPassword)

            ''customersByCityReport.Subreports(0).DataSourceConnections(GloServerName, GloDatabaseName).SetConnection(GloServerName, GloDatabaseName, False, GloPassword)


            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\EstadoDeCuentaFinal.rpt"
            customersByCityReport.Load(reportPath)

            Dim connection As IConnectionInfo
            Dim serverName1 As String = GloServerName

            ' Establecer conexi�n con base de datos al informe principal
            For Each connection In customersByCityReport.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetConnection(serverName1, GloDatabaseName, False)
                        connection.SetLogon(GloUserID, GloPassword)
                End Select
            Next

            ' Establecer conexi�n al subinforme
            Dim subreport As ReportDocument
            For Each subreport In customersByCityReport.Subreports
                For Each connection In subreport.DataSourceConnections
                    connection.SetConnection(serverName1, GloDatabaseName, False)
                    connection.SetLogon(GloUserID, GloPassword)
                Next
            Next


            '@contrato1
            customersByCityReport.SetParameterValue(0, contrato)
            'contrato2
            customersByCityReport.SetParameterValue(1, contrato1)

            'Dim Ruta As New CrystalDecisions.Shared.ExportDestinationType

            ''With Ruta
            ''    .DiskFile = My.Applicatio
            ''End With


            'Dim opcion As New CrystalDecisions.Shared.ExportOptions

            'With opcion
            '    .ExportFormatType = ExportFormatType.PortableDocFormat
            '    .ExportDestinationType = ExportDestinationType.DiskFile

            'End With


            'customersByCityReport.ExportToStream(ExportFormatType.PortableDocFormat)
            'customersByCityReport.Export(True)


            'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            '
            If Checa_Si_Imprime = 1 Then
                customersByCityReport.PrintOptions.PrinterName = Impresora
                'If op = 0 Then
                '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                'ElseIf op = 1 Then
                customersByCityReport.PrintToPrinter(1, True, 2, 0)
                'End If
            End If

            customersByCityReport.Dispose()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        If IsNumeric(Me.TxtContratoIni.Text) = False Then
            MsgBox("Seleccione El Contrato Inicial Por Favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.TxtContratoFin.Text) = False Then
            MsgBox("Seleccione El Contrato Final Por Favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim i As Long = Nothing
        Dim a As Integer = Nothing
        Try
            If CInt(Me.TxtContratoIni.Text) <= CInt(Me.TxtContratoFin.Text) Then
                If Checa_Si_Imprime = 1 Then
                    'a = 0
                    'While (a < 2)
                    'If a = 0 Then
                    '    MsgBox("Se Imprimiran Las Primeras Hojas De Estado de Cuenta", MsgBoxStyle.Information)
                    'ElseIf a = 1 Then
                    '    MsgBox("Se Imprimiran El Resto De Hojas Del Estado de Cuenta", MsgBoxStyle.Information)
                    'End If
                    For i = CInt(Me.TxtContratoIni.Text) To CInt(Me.TxtContratoFin.Text)
                        ValidaImprimirEstadoDeCuenta(i)
                        If eRes = 0 Then
                            Reporte_edo_cuenta(i, i, 1)
                        End If
                    Next
                    'a += 1
                    'End While
                ElseIf Checa_Si_Imprime = 0 Then
                    For i = CInt(Me.TxtContratoIni.Text) To CInt(Me.TxtContratoFin.Text)
                        ValidaImprimirEstadoDeCuenta(i)
                        If eRes = 0 Then
                            Reporte_edo_cuenta(i, i, a)
                        End If
                    Next
                End If
            ElseIf CInt(Me.TxtContratoIni.Text) < CInt(Me.TxtContratoFin.Text) Then
                MsgBox("El Contrato Inicial Debe Ser Mayor Al Contrato Final", MsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Valida_Si_Imprime_Edo_Cuenta()
        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Valida_Si_Imprime_Edo_Cuenta"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@op", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = 0
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()

                Checa_Si_Imprime = prm1.Value
            End With
            CON80.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CMBBtnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnImprimir.Click
        Dame_Impresora_Ordenes()
        If a = 1 And Checa_Si_Imprime = 1 Then
            MsgBox("No Se Tiene una Impresora Asignada Para Impresi�n Del Estado De Cuenta (Ordenes)", MsgBoxStyle.Information)
            Exit Sub
        Else
            Me.BackgroundWorker1.RunWorkerAsync()
            PantallaProcesando.Show()
        End If
        'If IsNumeric(Me.TxtContratoIni.Text) = False Then
        '    MsgBox("Seleccione El Contrato Inicial Por Favor ", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        'If IsNumeric(Me.TxtContratoFin.Text) = False Then
        '    MsgBox("Seleccione El Contrato Final Por Favor ", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        'LocGloContratoIni = Me.TxtContratoIni.Text
        'LocGloContratoFin = Me.TxtContratoFin.Text
        'LocbndImpresionEdoCuentaLog = True
        'FrmImprimirContrato.Show()
        'Me.Close()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
        If Checa_Si_Imprime = 1 Then
            MsgBox("Se Imprimio con �xito", MsgBoxStyle.Information)
        ElseIf Checa_Si_Imprime = 0 Then
            MsgBox("Proceso T�rminado", MsgBoxStyle.Information)
        End If
        Me.Close()
    End Sub

    Private Sub Dame_Impresora_Ordenes()
        Dim Con As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "Dame_Impresora_Ordenes"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@Impresora varchar(50) output,@error int output
                Dim prm1 As New SqlClient.SqlParameter("@Impresora", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = ""
                .Parameters.Add(prm1)

                Dim prm2 As New SqlClient.SqlParameter("@error", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()
                Impresora = prm1.Value
                a = prm2.Value
            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TxtContratoIni_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtContratoIni.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtContratoIni, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TxtContratoFin_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtContratoFin.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtContratoFin, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub ValidaImprimirEstadoDeCuenta(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaImprimirEstadoDeCuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = String.Empty
        comando.Parameters.Add(parametro3)

        Try
            eRes = 0
            eMsg = String.Empty
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eRes = CInt(parametro2.Value.ToString)
            eMsg = parametro3.Value.ToString
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
End Class